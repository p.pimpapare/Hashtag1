# PPClibrary

[![CI Status](http://img.shields.io/travis/Pimpaporn Chaichompoo/PPClibrary.svg?style=flat)](https://travis-ci.org/Pimpaporn Chaichompoo/PPClibrary)
[![Version](https://img.shields.io/cocoapods/v/PPClibrary.svg?style=flat)](http://cocoapods.org/pods/PPClibrary)
[![License](https://img.shields.io/cocoapods/l/PPClibrary.svg?style=flat)](http://cocoapods.org/pods/PPClibrary)
[![Platform](https://img.shields.io/cocoapods/p/PPClibrary.svg?style=flat)](http://cocoapods.org/pods/PPClibrary)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

PPClibrary is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "PPClibrary"
```

## Author

Pimpaporn Chaichompoo, p.pimpapare@gmail.com

## License

PPClibrary is available under the MIT license. See the LICENSE file for more info.
