//
//  PCHTMLText.m
//  PCLibrary
//
//  Created by pimpaporn chaichompoo on 7/21/16.
//  Copyright © 2016 Pimpaporn Chaichompoo. All rights reserved.
//

#import "PCHTMLText.h"
#import <UIKit/UIKit.h>

@implementation PCHTMLText

+(void)setHTMl_text:(UILabel *)label_text font_HTML:(NSString *)font_name text_HTML:(NSString *)html_text
{
    NSString *receive_text = [NSString stringWithFormat:@"<font face =\"%@\">%@</font>",font_name,html_text];
    
    NSAttributedString * receive_text_html = [[NSAttributedString alloc] initWithData:[receive_text dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
    label_text.attributedText = receive_text_html;
}

+(NSString *)replaceHTML_by_string:(NSString *)HTML old_string:(NSString *)old_string new_string:(NSString *)new_string
{
    
    NSString *newHTMLString = [NSString stringWithFormat:@"%@",HTML];
    
    newHTMLString = [newHTMLString stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@",old_string] withString:[NSString stringWithFormat:@"%@",new_string]];
    
    return newHTMLString;
}

@end
