//
//  UIColor+PCColor_Helper.h
//  PPPCLibrary
//
//  Created by pimpaporn chaichompoo on 8/16/16.
//  Copyright © 2016 Pimpaporn Chaichompoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (PCColor_Helper)

+(void)add_bg_gradient:(UIColor *)color_one color_two:(UIColor *)color_two view_gradient:(UIView *)view_gradient;

+(UIColor *)colorFromHexString:(NSString *)hexString;
+(UIImage *)colorImage:(UIImage *)source withColor:(UIColor *)color;

@end
