//
//  UIColor+PCColor_Helper.m
//  PPPCLibrary
//
//  Created by pimpaporn chaichompoo on 8/16/16.
//  Copyright © 2016 Pimpaporn Chaichompoo. All rights reserved.
//

#import "PCColor_Helper.h"

@implementation UIColor (PCColor_Helper)

+(void)add_bg_gradient:(UIColor *)color_one color_two:(UIColor *)color_two view_gradient:(UIView *)view_gradient
{
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    [gradient setFrame:CGRectMake(0,0,view_gradient.frame.size.width,view_gradient.frame.size.height)];
    gradient.colors = [NSArray arrayWithObjects:(id)[color_one CGColor],(id)[color_two CGColor],(id)[color_two CGColor],(id)[color_two CGColor],nil];
    
    [view_gradient.layer insertSublayer:gradient atIndex:0];
}

+(UIColor *)colorFromHexString:(NSString *)hexString {
    
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

+(UIImage *)colorImage:(UIImage *)source withColor:(UIColor *)color
{
    // begin a new image context, to draw our colored image onto with the right scale
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(50,50), NO, [UIScreen mainScreen].scale);
    
    // get a reference to that context we created
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // set the fill color
    [color setFill];
    
    // translate/flip the graphics context (for transforming from CG* coords to UI* coords
    CGContextTranslateCTM(context, 0, 50);
    CGContextScaleCTM(context, 1.0, -1.0);
    
    CGContextSetBlendMode(context, kCGBlendModeColorBurn);
    CGRect rect = CGRectMake(0, 0,50,50);
    CGContextDrawImage(context, rect, source.CGImage);
    
    CGContextSetBlendMode(context, kCGBlendModeSourceIn);
    CGContextAddRect(context, rect);
    CGContextDrawPath(context,kCGPathFill);
    
    // generate a new UIImage from the graphics context we drew onto
    UIImage *coloredImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //return the color-burned image
    return coloredImg;
}


@end
