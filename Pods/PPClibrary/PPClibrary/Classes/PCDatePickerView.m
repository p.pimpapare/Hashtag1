//
//  PCPickerView.m
//  testRefactorCode
//
//  Created by pimpaporn chaichompoo on 6/13/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "PCDatePickerView.h"
#define DEVICE_H [UIScreen mainScreen].bounds.size.height
#define DEVICE_W [UIScreen mainScreen].bounds.size.width

@implementation PCDatePickerView

-(void)awakeFromNib{
    
    [self setFrame:CGRectMake(0, 0, DEVICE_W, DEVICE_H)];
    
    [self setUserInteractionEnabled:YES];
    [self setAlpha:1];
}

-(void)set_pickerview_UI:(NSString *)title_text font_title:(UIFont *)font_title title_font_color:(UIColor *)title_font_color font_btn:(UIFont *)font_btn btn_cancel_text_color:(UIColor *)btn_cancel_text_color btn_done_text_color:(UIColor *)btn_done_text_color
{

    [self.title_pickerView setText:[NSString stringWithFormat:@"%@",title_text]];
    [self.title_pickerView setFont:font_title];
    [self.title_pickerView setTextColor:title_font_color];
    
    [self.btn_cancel.titleLabel setFont:font_btn];
    [self.btn_cancel setTitleColor:btn_cancel_text_color forState:UIControlStateNormal];

    [self.btn_done.titleLabel setFont:font_btn];
    [self.btn_done setTitleColor:btn_done_text_color forState:UIControlStateNormal];
    
    [self setBackgroundColor:[UIColor clearColor]];
    [self.viewbg_pickerView.layer setCornerRadius:10];
    
    [self.btn_cancel addTarget:self action:@selector(closeDatePicker:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_cancel setTag:0];
    [self.btn_done addTarget:self action:@selector(acceptDatePicker:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_done setTag:1];
    
    // date picker
    [self.date_pickerView setDatePickerMode:UIDatePickerModeDate];
    [self.date_pickerView setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"th_TH"]];
    self.date_pickerView.calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierBuddhist];
}

-(void)closeDatePicker:(id)sender
{
    [self close_pickerView];
}

-(void)acceptDatePicker:(id)sender
{
    // set date format
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd MMMM yyyy"];
    NSLocale *locale = [[NSLocale alloc]
                        initWithLocaleIdentifier:@"th-TH"];
    [df setLocale:locale];
    
    self.seleceted_date = [NSString stringWithFormat:@"%@",[df stringFromDate:self.date_pickerView.date]];
    [self close_pickerView];
}

-(void)close_pickerView{
    
    [UIView animateWithDuration:0.4 animations:^{
        [self setAlpha:0];
    } completion:^(BOOL finished) {
        [self setUserInteractionEnabled:NO];
    }];
}

-(void)showDatePicker_With_SpecificDate:(NSString *)specificDate
{
    
    if (![specificDate isEqualToString:@" "]) {
        
        NSTimeZone *timezone = [NSTimeZone systemTimeZone];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:timezone];
        [dateFormatter setDateFormat:@"dd MMMM yyyy"];
        
        NSLocale *locale = [[NSLocale alloc]
                            initWithLocaleIdentifier:@"th-TH"];
        [dateFormatter setLocale:locale];
        
        NSDate *date = [dateFormatter dateFromString:specificDate];
        self.date_pickerView.date = date;
        
    }else{
        self.date_pickerView.date = [NSDate date];
    }
}

@end
