//
//  PPPCLibrary.h
//  PPPCLibrary
//
//  Created by pimpaporn chaichompoo on 8/16/16.
//  Copyright © 2016 Pimpaporn Chaichompoo. All rights reserved.
//

#ifndef PPPCLibrary_h
#define PPPCLibrary_h

#import "PCView.h"
#import "PCImage.h"
#import "PCSwitch.h"
#import "PCShadow.h"
#import "PCButton.h"
#import "PCLoading.h"
#import "PCHTMLText.h"
#import "PCTextfield.h"
#import "PCFunctions.h"
#import "PCViewImage.h"
#import "PCStatusBar.h"
#import "PCPickerView.h"
#import "PCMenuSideBar.h"
#import "PCMenuSideMenu.h"
#import "PCColor_Helper.h"
#import "PCNavigationBar.h"
#import "PCDateFormatter.h"
#import "PCNumberFormate.h"
#import "PCDatePickerView.h"
#import "PCTapGestureRecognizer.h"


///############

#define DEVICE_H [UIScreen mainScreen].bounds.size.height
#define DEVICE_W [UIScreen mainScreen].bounds.size.width

//--------------------- DEVICE SIZE
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)?TRUE:FALSE
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)?TRUE:FALSE

#define IS_IPHONE_4_OR_LESS (DEVICE_H <= 480)?TRUE:FALSE
#define IS_IPHONE_5 (DEVICE_H == 568)?TRUE:FALSE
#define IS_IPHONE_6 (DEVICE_W == 375)?TRUE:FALSE
#define IS_IPHONE_6P (DEVICE_W == 414)?TRUE:FALSE

#define IS_IPAD (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)?TRUE:FALSE
#define IS_IPAD_PRO (DEVICE_H >= 1366.0)?TRUE:FALSE


//--------------------- NSUserdefualt
#define GET_USER_DEFAULT_INFO(x) id userInfo; NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults]; if (userDefaults){    userInfo = (id)[userDefaults objectForKey:x]; }

#define SET_USER_DEFAULT_INFO(x,y)  NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults]; if (userDefaults){ [userDefaults setObject:x forKey:y]; }

#define REMOVE_ALL_USER_DEFAULT_INFO     [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]]; [[NSUserDefaults standardUserDefaults] synchronize];

#define REMOVE_USER_DEFAULT_INFO(x)      [[NSUserDefaults standardUserDefaults] removeObjectForKey:x]; [[NSUserDefaults standardUserDefaults] synchronize];

#define INTERNET_CONNECTION -(BOOL)check_internet_connection { Reachability *internet = [Reachability reachabilityWithHostName: @"www.google.com"]; NetworkStatus netStatus = [internet currentReachabilityStatus]; switch (netStatus) { case NotReachable: { return NO; break; } case ReachableViaWWAN: { return YES; break; } case ReachableViaWiFi: { return YES; break; } } }


#endif /* PPPCLibrary_h */
