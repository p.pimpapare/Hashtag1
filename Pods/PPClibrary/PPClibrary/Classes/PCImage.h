//
//  PCImage.h
//  LannaLivingDemo
//
//  Created by pimpaporn chaichompoo on 7/29/16.
//  Copyright © 2016 Pimpaporn Chaichompoo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PCImage : NSObject

+(UIImage *)set_blur_image:(UIImageView *)image_blur image_name:(NSString *)image_name;
+(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;

@end
