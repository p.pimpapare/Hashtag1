//
//  PCPickerView.m
//  testRefactorCode
//
//  Created by pimpaporn chaichompoo on 6/13/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "PCPickerView.h"

@implementation PCPickerView

-(void)awakeFromNib{
    
    
    
}

-(void)set_pickerview_UI:(NSString *)title_text font_title:(UIFont *)font_title title_font_color:(UIColor *)title_font_color font_btn:(UIFont *)font_btn btn_cancel_text_color:(UIColor *)btn_cancel_text_color btn_done_text_color:(UIColor *)btn_done_text_color
{
    
    [self.title_pickerView setText:[NSString stringWithFormat:@"%@",title_text]];
    [self.title_pickerView setFont:font_title];
    [self.title_pickerView setTextColor:title_font_color];
    
    [self.btn_cancel.titleLabel setFont:font_btn];
    [self.btn_cancel setTitleColor:btn_cancel_text_color forState:UIControlStateNormal];
    
    [self.btn_done.titleLabel setFont:font_btn];
    [self.btn_done setTitleColor:btn_done_text_color forState:UIControlStateNormal];
    
    [self setBackgroundColor:[UIColor clearColor]];
    [self.viewbg_pickerView.layer setCornerRadius:10];
    
    [self.btn_cancel addTarget:self action:@selector(closeDatePicker:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_cancel setTag:0];
    [self.btn_done addTarget:self action:@selector(acceptDatePicker:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_done setTag:1];
}

-(void)closeDatePicker:(id)sender
{
    [self close_pickerView];
}

-(void)acceptDatePicker:(id)sender
{
    [self close_pickerView];
}

-(void)close_pickerView{
    
    [UIView animateWithDuration:0.4 animations:^{
        [self setAlpha:0];
    } completion:^(BOOL finished) {
        [self setUserInteractionEnabled:NO];
    }];
}

-(void)showPicker_array:(NSArray *)receive_picker_array
{
    picker_array = [receive_picker_array mutableCopy];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
        return picker_array.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
        self.seleceted_text = [picker_array objectAtIndex:row];
        return [picker_array objectAtIndex:row];
}

- (void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.seleceted_text = (NSString *)[picker_array objectAtIndex:row];
}

@end
