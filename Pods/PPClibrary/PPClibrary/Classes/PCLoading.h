//
//  PCLoading.h
//  testRefactorCode
//
//  Created by pimpaporn chaichompoo on 6/14/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCLoading : UIView

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UILabel *loading_text;

-(void)setloading:(BOOL)isLoading;
-(void)set_title_loading:(NSString *)loading_text;

@end
