//
//  PCStatusBar.h
//  testRefactorCode
//
//  Created by pimpaporn chaichompoo on 6/16/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCStatusBar : UIView

+(void)setStatusBar:(UIViewController *)viewController bgColor:(UIColor *)bgColor statusBarLightStyle:(BOOL)lightStyle;

@end
