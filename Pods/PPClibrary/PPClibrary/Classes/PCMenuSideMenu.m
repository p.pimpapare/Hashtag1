//
//  PCMenuSideMenu.m
//  PCLibrary
//
//  Created by pimpaporn chaichompoo on 7/26/16.
//  Copyright © 2016 Pimpaporn Chaichompoo. All rights reserved.
//

#import "PCMenuSideMenu.h"
#import "PPPCLibrary.h"

@implementation PCMenuSideMenu
{
    NSArray *btn_array;
}
-(void)awakeFromNib
{
    btn_array = [[NSArray alloc]initWithObjects:self.btn_menu_1,self.btn_menu_2,self.btn_menu_3,self.btn_menu_4,self.btn_menu_5,self.btn_menu_6, nil];
    
    [PCShadow set_shadow:1.5 shadowRadius:1.0 shadowOpacity:0.3 view:self];
    
    [self.image_viewbg.layer setMasksToBounds:YES];
    [self.image_profile.layer setCornerRadius:30];
    [self.image_profile setClipsToBounds:YES];
    
    for (UIButton *button in btn_array) {
        [button setAlpha:0];
        button.transform = CGAffineTransformMakeScale(0.9f, 0.9f);
    }
    
    self.image_viewbg.transform = CGAffineTransformMakeScale(0.9f, 0.9f);
    [self set_image_bg];
}

-(void)set_image_bg
{
    [self.image_viewbg setImage:self.image_profile.image];
    
    // create blur effect
    UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    
    // create vibrancy effect
    UIVibrancyEffect *vibrancy = [UIVibrancyEffect effectForBlurEffect:blur];

    UIVisualEffectView *effectView = [[UIVisualEffectView alloc]initWithEffect:blur];
    [effectView setFrame:CGRectMake(0, 0, DEVICE_W+100, DEVICE_H+100)];
    
    UIVisualEffectView *vibrantView = [[UIVisualEffectView alloc]initWithEffect:vibrancy];
    [vibrantView setFrame:CGRectMake(0, 0, DEVICE_W+100, DEVICE_H+100)];
    
    [self.image_viewbg addSubview:effectView];
    [self.image_viewbg addSubview:vibrantView];
}

- (IBAction)btn_close_side_menu:(id)sender
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];

    [self setNeedsUpdateConstraints];

    [UIView animateWithDuration:0.4 animations:^{
        
        [self.left_viewbg_main_menu setConstant:0];
        [self.right_viewbg_main_menu setConstant:0];
        [self.top_viewbg_main_menu setConstant:0];
        [self.bottom_viewbg_main_menu setConstant:0];
        [self layoutIfNeeded];
        
        for (UIButton *button in btn_array) {
            [button setAlpha:0];
            button.transform = CGAffineTransformMakeScale(0.9f,0.9f);
        }
        self.image_viewbg.transform = CGAffineTransformMakeScale(0.9f, 0.9f);
        
    } completion:^(BOOL finished) {
        
        [self.btn_close_side_menu setHidden:YES];
  
    }];
}

- (IBAction)btn_left_pressed:(id)sender
{
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];

    [self setNeedsUpdateConstraints];
    
    [UIView animateWithDuration:0.4 animations:^{
        
        for (UIButton *button in btn_array) {
            [button setAlpha:1];
            button.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
        }
        self.image_viewbg.transform = CGAffineTransformMakeScale(1.0f, 1.0f);

        [self.left_viewbg_main_menu setConstant:250];
        [self.right_viewbg_main_menu setConstant:-250];
        [self.top_viewbg_main_menu setConstant:50];
        [self.bottom_viewbg_main_menu setConstant:120];
        [self layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        [self.btn_close_side_menu setHidden:NO];
    }];
}


@end
