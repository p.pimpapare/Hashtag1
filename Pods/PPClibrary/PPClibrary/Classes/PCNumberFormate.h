//
//  NumberFormate.h
//  testRefactorCode
//
//  Created by pimpaporn chaichompoo on 6/13/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PCNumberFormate : NSObject

+(NSString *)format_phone:(NSString *)receivePhone;
+(NSString *)format_currency:(NSString *)currentcy_text;

+(NSString *)get_random_string:(int)length;

@end
