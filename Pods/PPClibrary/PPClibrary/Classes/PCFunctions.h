//
//  PCFunctions.h
//  PCLibrary
//
//  Created by pimpaporn chaichompoo on 7/21/16.
//  Copyright © 2016 Pimpaporn Chaichompoo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PCFunctions : NSObject

+(BOOL)check_internet_connection;
+(id)loadViewControllerWithName:(NSString *)viewControllerName;

+(NSString *)currentLanguageBundle;

@end
