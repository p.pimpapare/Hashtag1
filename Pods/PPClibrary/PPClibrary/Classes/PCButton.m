//
//  PCButton.m
//  testRefactorCode
//
//  Created by pimpaporn chaichompoo on 6/14/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "PCButton.h"

@implementation PCButton
{
    UIActivityIndicatorView *activityIndicator;
    
    UILabel *text_btn;
    UIColor *text_existing_color;
}

-(void)awakeFromNib
{
    [self.layer setShouldRasterize:YES];
    [self.layer setRasterizationScale:[UIScreen mainScreen].scale];
    
    [self setupActivityIndicator];
}

-(void)setUserInteractionEnabledButton:(BOOL)userInteractionEnabled{
    
    if (userInteractionEnabled == NO) {
        [text_btn setTextColor:[UIColor colorWithWhite:0.673 alpha:1.0]];
        [self setUserInteractionEnabled:NO];
    }else{
        [text_btn setTextColor:text_existing_color];
        [self setUserInteractionEnabled:YES];
    }
}

-(void)setUpTextLabel:(NSString *)text_button font_tile:(UIFont *)font_title text_color:(UIColor *)color{
    
    text_existing_color = color;
    
    text_btn = [[UILabel alloc]initWithFrame:CGRectMake(0,0,self.frame.size.width,self.frame.size.height)];
    [text_btn setTextAlignment:NSTextAlignmentCenter];
    [text_btn setTextColor:color];
    [text_btn setText:text_button];
    [self addSubview:text_btn];
}

-(void)setupActivityIndicator {
    
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:(UIActivityIndicatorViewStyleGray)];
    activityIndicator.center = CGPointMake(self.frame.size.width/2,self.frame.size.height/2);
    [activityIndicator setHidesWhenStopped:YES];
    [activityIndicator stopAnimating];
    [self addSubview:activityIndicator];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(activityIndicatorTaped:)];
    [activityIndicator
     addGestureRecognizer:tap];
}

-(void)activityIndicatorTaped:(id)sender {
    [self sendActionsForControlEvents:(UIControlEventTouchUpInside)];
}

-(void)setLoadingActivity:(BOOL)loading
{
    if (loading == YES) {
        [text_btn setTextColor:[UIColor clearColor]];
        [activityIndicator startAnimating];
        
    }else{
        [text_btn setTextColor:text_existing_color];
        [activityIndicator stopAnimating];
    }
}

// ###### SET UP CUSTOM UIBUTTON

+(void)setCornerRadius_Normal:(UIButton *)button
{
    [button.layer setCornerRadius:(button.frame.size.height<button.frame.size.width)?button.frame.size.height/2:button.frame.size.width/2];
}

+(void)setCornerRadius_WithSize:(UIButton *)button size:(int)size
{
    [button.layer setCornerRadius:size];
}

@end
