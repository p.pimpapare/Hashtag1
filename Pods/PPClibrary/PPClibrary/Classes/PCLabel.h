//
//  PCLabel.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 5/2/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

// This class is used for setting and organizing UILabel's functions.

#import <UIKit/UIKit.h>

@interface PCLabel : UILabel

+(void)set_underLine_text:(UILabel *)label text:(NSString *)text textColor:(UIColor *)textcolor;
+(void)set_shadow_label:(UILabel *)label;

@end
