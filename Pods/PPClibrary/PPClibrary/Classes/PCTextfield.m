//
//  PCTextfield.m
//  testRefactorCode
//
//  Created by pimpaporn chaichompoo on 6/13/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "PCTextfield.h"

@implementation PCTextfield

-(void)awakeFromNib
{
    [self setSpellCheckingType:UITextSpellCheckingTypeNo];
    [self setAutocorrectionType:UITextAutocorrectionTypeNo];
    [self setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [self setClearButtonMode:UITextFieldViewModeWhileEditing];
    [self setClearsOnBeginEditing:NO];
    
    [self.layer setShouldRasterize:YES];
    [self.layer setRasterizationScale:[UIScreen mainScreen].scale];
}

+(void)setPlaceholderColor:(UIColor *)color textfield:(UITextField *)textfield
{
    [textfield setValue:color forKeyPath:@"_placeholderLabel.textColor"];
}

+(void)setpadding_textfield:(UITextField *)textfield framLeft:(float)leftNum framRight:(float)rightNum
{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,leftNum,rightNum)];
    textfield.leftView = paddingView;
    textfield.leftViewMode = UITextFieldViewModeAlways;
}

+(void)set_image_uitextfield:(UITextField *)textfield image:(UIImage *)image_textfield
{
    UIView *view_textfield = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 40, textfield.frame.size.height)];
    
    UIImageView *img_view=[[UIImageView alloc] initWithFrame:CGRectMake(10,view_textfield.frame.size.height/2 - 11, 22, 22)];
    [img_view setImage:image_textfield];
    [img_view setContentMode:UIViewContentModeScaleAspectFit];
    [view_textfield addSubview:img_view];
    
    textfield.leftViewMode = UITextFieldViewModeAlways;
    textfield.leftView = view_textfield;
}

-(void)setBounderColor:(UIColor *)color
{
    [self.layer setBorderColor:[color CGColor]];
    [self.layer setBorderWidth:1];
    [self.layer setMasksToBounds:NO];
    [self.layer setCornerRadius:5];
}

-(void)clearBounderColor
{
    [self.layer setBorderColor:[[UIColor clearColor] CGColor]];
}

+(void)animateTextFieldWithContrant:(NSLayoutConstraint *)view_constant viewToAnimate:(UIView *)viewToAnimate up:(BOOL)up
{
    float movement = 0.0;
    
    if (up == YES) {
        movement = -120.0;
    }
    
    view_constant.constant = movement;
    [viewToAnimate setNeedsUpdateConstraints];
    [UIView animateWithDuration:0.5 animations:^{
        [viewToAnimate layoutIfNeeded];
    }];
}

+(void)animateTextField:(UIView *)viewToAnimate up:(BOOL)up
{
    float movement = 0.0;
    
    if (up == YES) {
        movement = -120.0;
    }else{
        movement = 120.0;
    }
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationBeginsFromCurrentState:YES];
    viewToAnimate.frame = CGRectMake(viewToAnimate.frame.origin.x, (viewToAnimate.frame.origin.y + movement), viewToAnimate.frame.size.width, viewToAnimate.frame.size.height);
    [UIView commitAnimations];
}

//##### Other Function

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
