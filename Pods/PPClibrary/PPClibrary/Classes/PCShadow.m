//
//  PCShadow.m
//  testRefactorCode
//
//  Created by pimpaporn chaichompoo on 6/16/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "PCShadow.h"
#import "PPPCLibrary.h"

@implementation PCShadow

+(void)set_shadow:(double)shadowOffset shadowRadius:(double)shadowRadius shadowOpacity:(double)shadowOpacity view:(UIView *)view{
    
    view.layer.masksToBounds = NO;
    view.layer.shadowOffset = CGSizeMake(0,shadowOffset);
    view.layer.shadowRadius = shadowRadius;
    view.layer.shadowOpacity = shadowOpacity;
}

+(void)set_border_line:(UIView *)view{
    
    [view.layer setBorderWidth:1.5];
    
    view.layer.borderColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.1].CGColor;
}

+(void)set_gradient_menu:(UIView *)view{
    
    [view setBackgroundColor:[UIColor colorWithRed:(241/255.0) green:(241/255.0) blue:(241/255.0) alpha:1.0]];
    
    UIColor *colorOne = [UIColor colorWithRed:(255/255.0) green:(255/255.0) blue:(255/255.0) alpha:0.2];
    
    UIColor *colorTwo = [UIColor colorWithRed:(189/255.0)  green:(190/255.0)  blue:(192/255.0)  alpha:0.13];
    
    // set backgroud shadow
    CAGradientLayer *gradient = [CAGradientLayer layer];
    [gradient setFrame:CGRectMake(0,0,DEVICE_W,view.frame.size.height)];
    gradient.colors = [NSArray arrayWithObjects:(id)[colorOne CGColor],(id)[colorTwo CGColor],(id)[colorOne CGColor],nil];
    
    [view.layer insertSublayer:gradient atIndex:0];
}

+(void)set_custom_gradient_menu:(int)color1 color2:(int)color2 color3:(int)color3 color21:(int)color21 color22:(int)color22 color23:(int)color23 view:(UIView *)view{
    
    UIColor *colorOne = [UIColor colorWithRed:(color1/255.0)  green:(color2/255.0)  blue:(color3/255.0)  alpha:1.0];
    UIColor *colorTwo = [UIColor colorWithRed:(color21/255.0) green:(color22/255.0) blue:(color23/255.0) alpha:1.0];
    
    // set backgroud shadow
    CAGradientLayer *gradient = [CAGradientLayer layer];
    [gradient setFrame:CGRectMake(0,0,DEVICE_W,view.frame.size.height)];
    gradient.colors = [NSArray arrayWithObjects:(id)[colorOne CGColor],(id)[colorTwo CGColor],nil];
    view.layer.sublayers = nil;
    [view.layer insertSublayer:gradient atIndex:0];
}

+(void)set_blur_image:(UIView *)view{

    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
        view.backgroundColor = [UIColor clearColor];
        
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurEffectView.frame = view.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        [view addSubview:blurEffectView];
    }
    else {
        view.backgroundColor = [UIColor blackColor];
    }
}

@end
