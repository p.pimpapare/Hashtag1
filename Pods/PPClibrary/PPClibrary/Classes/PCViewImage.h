//
//  PCViewImage.h
//  GuruVaccineDemo
//
//  Created by pimpaporn chaichompoo on 7/21/16.
//  Copyright © 2016 Pimpaporn Chaichompoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCViewImage : UIView<UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scroll_view;

@property (weak, nonatomic) IBOutlet UIButton *cancel_btn;
@property (weak, nonatomic) IBOutlet UIImageView *image_view;


// Zoom Image
-(void)setImageWithImage:(UIImage *)image;

@end
