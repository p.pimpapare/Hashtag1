//
//  PCNavigationBar.m
//  testRefactorCode
//
//  Created by pimpaporn chaichompoo on 6/15/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "PCNavigationBar.h"
#import "PCShadow.h"
#import "PPPCLibrary.h"

@implementation PCNavigationBar

-(void)awakeFromNib
{
    [self setFrame:CGRectMake(0,0, DEVICE_W, self.frame.size.height)];
    [self.viewbg_left setBackgroundColor:[UIColor clearColor]];
    [self.viewbg_right setBackgroundColor:[UIColor clearColor]];
    
    [PCShadow set_shadow:1.5 shadowRadius:1.0 shadowOpacity:0.5 view:self];
    [PCShadow set_gradient_menu:self];
}

-(void)setbg_color:(UIColor *)color left_text:(NSString *)left_text title_text:(NSString *)title_text
{
    [self setBackgroundColor:color];
    [self.viewbg_right setHidden:YES];
    [self.view_bg setBackgroundColor:color];
    
    [self.title_text setText:title_text];
    [self.text_left setText:left_text];
    [self setBackgroundColor:color];
}

-(void)set_corner_radius:(float)radius bgcolor:(UIColor *)color bottom:(BOOL)bottom{
    
    [self.viewbg_left setHidden:YES];
    [self.top_view_bg setConstant:15];
    [self.view_bg setBackgroundColor:color];

    if (bottom==YES) {
        
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.view_bg.bounds byRoundingCorners:( UIRectCornerBottomLeft | UIRectCornerBottomRight) cornerRadii:CGSizeMake(radius,radius)];
        
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.view_bg.bounds;
        maskLayer.path  = maskPath.CGPath;
        
        self.view_bg.layer.mask = maskLayer;
        
    }else{
        
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.view_bg.bounds byRoundingCorners:( UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(radius,radius)];
        
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.view_bg.bounds;
        maskLayer.path  = maskPath.CGPath;
        
        self.view_bg.layer.mask = maskLayer;
    }
}

@end
