//
//  PCDateFormatter.h
//  testRefactorCode
//
//  Created by pimpaporn chaichompoo on 6/14/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PCDateFormatter : NSObject

+(NSString *)sort_time_dd_mm_yyyy:(NSString *)receive_date split_text:(NSString *)split_text;
+(NSString *)sort_time_dd_mm_yyyy_to_full_month:(NSString *)receive_date split_text:(NSString *)split_text;
+(NSString *)sort_full_month_to_yyyy_mm_dd:(NSString *)receive_date;

@end
