//
//  PCTapGestureRecognizer.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 4/11/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "PCTapGestureRecognizer.h"

@implementation PCTapGestureRecognizer

+(void)tapDismissKeyBoard:(UIViewController *)classController view:(UIView *)view actionTarget:(SEL _Nonnull)target
{
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:classController
                                   action:target];
    [view addGestureRecognizer:tap];
}


@end
