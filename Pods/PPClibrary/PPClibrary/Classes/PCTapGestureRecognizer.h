//
//  PCTapGestureRecognizer.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 4/11/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCTapGestureRecognizer : UITapGestureRecognizer

+(void)tapDismissKeyBoard:(UIViewController * _Nullable)classController view:(UIView * _Nullable)view actionTarget:(SEL _Nonnull)target;

@end
