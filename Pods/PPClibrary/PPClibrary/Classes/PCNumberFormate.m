//
//  NumberFormate.m
//  testRefactorCode
//
//  Created by pimpaporn chaichompoo on 6/13/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "PCNumberFormate.h"

@implementation PCNumberFormate

+(NSString *)format_phone:(NSString *)receivePhone{
    
    NSString *zero, *firstSection, *second, *third, *firstplit;
    
    NSMutableString *phoneString = [[NSMutableString alloc]init];
    NSArray *convertStr = [receivePhone componentsSeparatedByString:@"-"];
    
    if ([convertStr count]==1) {
        
        zero = [receivePhone substringWithRange:NSMakeRange(0,1)];
        firstSection = [receivePhone substringWithRange:NSMakeRange(1,2)];
        second = [receivePhone substringWithRange:NSMakeRange(3,3)];
        third = [receivePhone substringWithRange:NSMakeRange(6,4)]; // ตัวหลังคือ range
        
        if ([firstSection isEqualToString:@"66"]) {
            
            [phoneString appendString:[NSString stringWithFormat:@"%@",receivePhone]];
            
        }else{
            
            [phoneString appendString:[NSString stringWithFormat:@"+66 %@ ",firstSection]];
            [phoneString appendString:[NSString stringWithFormat:@"%@ ",second]];
            [phoneString appendString:[NSString stringWithFormat:@"%@",third]];
        }
    }else if([convertStr count]>1){
        
        firstplit=[convertStr objectAtIndex:0];
        firstSection = [firstplit substringWithRange:NSMakeRange(1,2)];
        //    NSString *first = [firstplit objectAtIndex:2];
        second=[convertStr objectAtIndex:1];
        third=[convertStr objectAtIndex:2];
        
        [phoneString appendString:[NSString stringWithFormat:@"+66 %@ ",firstSection]];
        [phoneString appendString:[NSString stringWithFormat:@"%@ ",second]];
        [phoneString appendString:[NSString stringWithFormat:@"%@",third]];
        
    }else{
        [phoneString appendString:[NSString stringWithFormat:@"%@",receivePhone]];
    }
    
    return phoneString;
}

+(NSString *)format_currency:(NSString *)currentcy_text
{
    if (([currentcy_text isEqual:[NSNull null]])||(currentcy_text==nil)) {
        
        return @"-";
        
    }else{
        
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
        NSString *numberAsString = [numberFormatter stringFromNumber:[NSNumber numberWithDouble:[currentcy_text doubleValue]]];
        
        return numberAsString;
    }
}

+(NSString *)get_random_string:(int)length
{
    static NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    NSMutableString *randomString = [NSMutableString stringWithCapacity:length];
    for (int i=0; i<length; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random() % [letters length]]];
    }
    return randomString;
}


@end
