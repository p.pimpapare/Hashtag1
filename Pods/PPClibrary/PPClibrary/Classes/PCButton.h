//
//  PCButton.h
//  testRefactorCode
//
//  Created by pimpaporn chaichompoo on 6/14/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

//  pod 'PPClibrary', :git => 'https://pimpaporn@bitbucket.org/pimpaporn/pppclibrary.git'


#import <UIKit/UIKit.h>

@interface PCButton : UIButton{
    
}

+(void)setCornerRadius_Normal:(UIButton *)button;
+(void)setCornerRadius_WithSize:(UIButton *)button size:(int)size;

-(void)setLoadingActivity:(BOOL)loading;
-(void)setUpTextLabel:(NSString *)text_button font_tile:(UIFont *)font_title text_color:(UIColor *)color;

-(void)setUserInteractionEnabledButton:(BOOL)userInteractionEnabled;


/* ############ Example ############

[self.button_test setUpTextLabel:@"test" font_tile:[UIFont fontWithName:@"System" size:12] text_color:[UIColor redColor]];
[PCButton setCornerRadius_Normal:self.button_test];
[self.button_test setUserInteractionEnabledButton:NO];


- (IBAction)button_press:(id)sender {
    
    if (tap == NO) {
        
        [self.button_test setLoadingActivity:YES];
        tap = YES;
        
    }else{
        
        [self.button_test setLoadingActivity:NO];
        tap = NO;
    }
}
*/

@end
