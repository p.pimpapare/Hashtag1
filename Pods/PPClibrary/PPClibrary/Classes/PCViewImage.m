//
//  PCViewImage.m
//  GuruVaccineDemo
//
//  Created by pimpaporn chaichompoo on 7/21/16.
//  Copyright © 2016 Pimpaporn Chaichompoo. All rights reserved.
//

#import "PCViewImage.h"
#import "PPPCLibrary.h"

@implementation PCViewImage

-(void)awakeFromNib{
    
    [super awakeFromNib];
    
    [self setFrame:CGRectMake(0,0,DEVICE_W,DEVICE_H)];
    [self setup_object];
}

// ########################

-(void)setup_object
{
    [self.scroll_view setAutoresizingMask:-1];
    [self.scroll_view setMaximumZoomScale:5.0];
    [self.scroll_view setMinimumZoomScale:1.0];
    [self.scroll_view setDelegate:self];
    
    [self.image_view setAutoresizingMask:-1];
    [self.image_view setUserInteractionEnabled:NO];
    [self.image_view setContentMode:UIViewContentModeScaleAspectFit];
}

-(void)setImageWithImage:(UIImage *)image
{
    [self.image_view setImage:image];
}

// ########################

// ######################## UIScrollViewDelegate

- (nullable UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.image_view;
}

// ########################

@end
