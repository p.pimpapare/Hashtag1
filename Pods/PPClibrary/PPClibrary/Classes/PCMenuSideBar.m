//
//  PCMenuSideBar.m
//  GuruVaccineDemo
//
//  Created by pimpaporn chaichompoo on 7/3/16.
//  Copyright © 2016 Pimpaporn Chaichompoo. All rights reserved.
//

#import "PCMenuSideBar.h"
#import "PPPCLibrary.h"

@implementation PCMenuSideBar

-(void)awakeFromNib
{
    [self setFrame:CGRectMake(0,0,DEVICE_W,DEVICE_H)];
    [self setBackgroundColor:[UIColor clearColor]];
    [self.btn_close_menusidebar setAlpha:0];
    [self.right_view_bg setConstant:DEVICE_W];
    [self.left_view_bg setConstant:-DEVICE_W];
    
    [self.btn_menu_1 addTarget:self action:@selector(close_menusidebar:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view_menu_1 setBackgroundColor:[UIColor clearColor]];
    [self.view_menu_2 setBackgroundColor:[UIColor clearColor]];
    [self.view_menu_3 setBackgroundColor:[UIColor clearColor]];
    [self.view_menu_4 setBackgroundColor:[UIColor clearColor]];
    [self.view_menu_5 setBackgroundColor:[UIColor clearColor]];
    [self.view_menu_6 setBackgroundColor:[UIColor clearColor]];
    
    [UIColor add_bg_gradient:[UIColor lightGrayColor] color_two:[UIColor whiteColor] view_gradient:self.scrollview];
}

-(void)btn_menu_1_pressed:(id)sender
{
    [self close_menusidebar:nil];
}

- (IBAction)btn_left:(id)sender
{
    [self setNeedsUpdateConstraints];
    [self.btn_close_menusidebar setHidden:NO];
    
    [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:0.5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        
        [self.btn_close_menusidebar setAlpha:0.6];
        [self.left_view_bg setConstant:-10];
        [self.right_view_bg setConstant:10];
        [self layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        [self.btn_close_menusidebar setHidden:NO];
    }];
}

- (IBAction)btn_close_menu:(id)sender
{
    [self close_menusidebar:nil];
}

-(void)close_menusidebar:(id)sender
{
    [self setNeedsUpdateConstraints];
    
    [UIView animateWithDuration:0.5 animations:^{
        
        [self.btn_close_menusidebar setAlpha:0];
        [self.left_view_bg setConstant:-320];
        [self.right_view_bg setConstant:320];
        [self layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        [self.btn_close_menusidebar setHidden:YES];
    }];
}
@end
