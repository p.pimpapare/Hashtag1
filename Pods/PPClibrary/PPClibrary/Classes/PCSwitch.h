//
//  PCSwitch.h
//  testRefactorCode
//
//  Created by pimpaporn chaichompoo on 6/15/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCSwitch : UISwitch

-(void)set_turnOff_Switch:(NSString * _Nullable)title_alert subtitle_alert:(NSString * _Nullable)subtitle_alert cancel_title_btn:(NSString * _Nullable)cancel_title_btn title_confirm_btn:(NSString * _Nullable)title_confirm_btn view_class:(UIViewController * _Nullable)view_class actionTarget:(id _Nonnull)target actionInCancelButton:(SEL _Nullable)actionLeft actionInConfirmButton:(SEL _Nullable)actionRight;



/* ############ Example ############

 - (IBAction)switch_test:(id)sender {
 
 if (self.switch_test.on) {
 
 [self.switch_test setOn:YES animated:NO];
 
 }else{
 
 [self.switch_test set_turnOff_Switch:@"title" subtitle_alert:@"sub title" cancel_title_btn:@"Cancel" title_confirm_btn:@"Turn Off" view_class:self actionTarget:self actionInCancelButton:@selector(cancelSelected) actionInConfirmButton:@selector(confirmSelectd)];
 }
 }
 
 -(void)cancelSelected{
 [self.switch_test setOn:YES animated:NO];
 }
 
 -(void)confirmSelectd{
 [self.switch_test setOn:NO animated:NO];
 }
 
 */

@end
