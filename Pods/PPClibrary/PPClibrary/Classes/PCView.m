//
//  PCView.m
//  testObjectiveC
//
//  Created by pimpaporn chaichompoo on 7/20/16.
//  Copyright © 2016 Pimpaporn Chaichompoo. All rights reserved.
//

#import "PCView.h"
#import "PPPCLibrary.h"

@implementation PCView

+(void)setViewCornerRadius:(BOOL)top view_corner:(UIView *_Nullable)view_cornor corner_num:(int)corner_num
{
    if (top == YES) {
        
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0,view_cornor.frame.size.width,view_cornor.frame.size.height) byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(corner_num,corner_num)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = view_cornor.bounds;
    maskLayer.path  = maskPath.CGPath;
    view_cornor.layer.mask = maskLayer;

    }else{
       
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0,view_cornor.frame.size.width,view_cornor.frame.size.height) byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(corner_num,corner_num)];
        
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = view_cornor.bounds;
        maskLayer.path  = maskPath.CGPath;
        view_cornor.layer.mask = maskLayer;
    }
}

+(void)setStatusBar:(UIViewController *)viewController bgColor:(UIColor *)bgColor statusBarLightStyle:(BOOL)lightStyle
{
    
    if (lightStyle == YES) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }else
    {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }
    UIView *statusbar = [[UIView alloc]initWithFrame:CGRectMake(0,0,DEVICE_W,20)];
    [statusbar setBackgroundColor:bgColor];
    [viewController.view addSubview:statusbar];
}

+(void)showAlertWithTitle:(NSString * _Nullable)title Message:( NSString * _Nullable)msg leftButtonTitle:(NSString * _Nullable)leftTextTitle rightButtonTitle:(NSString * _Nullable)rightTextTitle actionTarget:(id _Nonnull)target actionInLeftButton:(SEL _Nullable)actionLeft actionInRightButton:(SEL _Nullable)actionRight TargetViewShow:(id _Nullable)target_view_show
{
    
    if ([UIAlertController class]) {
        // use UIAlertController
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:title
                                   message:msg
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        if (leftTextTitle) {
            UIAlertAction* ok = [UIAlertAction actionWithTitle:leftTextTitle style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action){
                                                           //Do Some action here
                                                           //UITextField *textField = alert.textFields[0];
                                                           //NSLog(@"text was %@", textField.text);
                                                           [target performSelectorOnMainThread:actionLeft withObject:nil waitUntilDone:YES];
                                                       }];
            [alert addAction:ok];
        }
        
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:(rightTextTitle)?rightTextTitle:@"OK" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           [target performSelectorOnMainThread:actionRight withObject:nil waitUntilDone:YES];
                                                       }];
        [alert addAction:cancel];
        
        //UIViewController *rootVC = [[UIApplication sharedApplication].delegate window].rootViewController;
        [target_view_show presentViewController:alert animated:YES completion:nil];
        
    }else{
        
        UIAlertView *alert = [[UIAlertView alloc] init];
        [alert setTitle:title];
        [alert setMessage:msg];
        if (leftTextTitle) {
            [alert addButtonWithTitle:leftTextTitle];
        }
        
        [alert addButtonWithTitle:(rightTextTitle)?rightTextTitle:@"OK"];
        
        [alert dismissWithClickedButtonIndex:(leftTextTitle)?1:0 animated:YES];
        
        [alert show];
    }
}

+(void)setViewAnimationWithDuration:(UIView * _Nullable)view_animation time_duration:(float)time showView:(BOOL)showView
{
    if (showView == YES) {
    
        [UIView animateWithDuration:time animations:^{
            [view_animation setAlpha:1];
        } completion:^(BOOL finished) {
            [view_animation setHidden:NO];
        }];

    }else{
    
    [UIView animateWithDuration:time animations:^{
        [view_animation setAlpha:0];
    } completion:^(BOOL finished) {
        [view_animation setHidden:YES];
    }];
        
    }
}

+(void)setConstantAnimationWithDuration:(UIView * _Nullable)classView constant_view:(NSLayoutConstraint * _Nullable)constant_view time_duration:(float)time constant:(float)constantNum
{
    [classView setNeedsUpdateConstraints];
    [constant_view setConstant:constantNum];

        [UIView animateWithDuration:time animations:^{
            
            [classView layoutIfNeeded];

        } completion:^(BOOL finished) {
        
        }];
}

-(void)setSelectedColor:(UITableViewCell *)cell_selected color_selected:(UIColor *_Nullable)color{
    
    UIView *selectedBackgroundView = [[UIView alloc] initWithFrame:cell_selected.bounds];
    selectedBackgroundView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    selectedBackgroundView.backgroundColor = color;
    cell_selected.selectedBackgroundView = selectedBackgroundView;
}


// Rotate View
-(void)setRotateView:(UIView *_Nullable)view_rotate duration_time:(float)time delay_time:(float)delay{

    [self rotateImage:view_rotate duration:time delay:delay curve:UIViewAnimationCurveEaseOut rotations:(M_PI/18)];
}

- (void) runSpinAnimationOnView:(UIView*)view duration:(CGFloat)duration rotations:(CGFloat)rotations repeat:(float)repeat;
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: rotations * 2.0 /* full rotation*/ * rotations * duration ];
    rotationAnimation.duration = duration;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = repeat;
    [view.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

- (void)rotateImage:(UIView *)image duration:(NSTimeInterval)duration delay:(NSTimeInterval)delay
              curve:(int)curve rotations:(CGFloat)rotations
{
    [UIView animateWithDuration:duration animations:^{
        [UIView setAnimationCurve:curve];
        image.transform = CGAffineTransformMakeRotation(rotations);
    } completion:^(BOOL finished) {
        [self rotateImage2:image duration:duration delay:delay curve:curve rotations:rotations];
    }];
    
    [UIView commitAnimations];
    
}
- (void)rotateImage2:(UIView *)image duration:(NSTimeInterval)duration delay:(NSTimeInterval)delay
               curve:(int)curve rotations:(CGFloat)rotations
{
    [UIView animateWithDuration:duration animations:^{
        [UIView setAnimationCurve:curve];
        image.transform = CGAffineTransformMakeRotation(-rotations);
    }
                     completion:^(BOOL finished){
                         [self rotateImage:image duration:duration delay:delay curve:curve rotations:rotations];
                         ;
                     }];
    [UIView commitAnimations];
    
}

@end
