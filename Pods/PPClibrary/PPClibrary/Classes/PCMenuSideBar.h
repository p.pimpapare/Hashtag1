//
//  PCMenuSideBar.h
//  GuruVaccineDemo
//
//  Created by pimpaporn chaichompoo on 7/3/16.
//  Copyright © 2016 Pimpaporn Chaichompoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCMenuSideBar : UIView

@property (weak, nonatomic) IBOutlet UILabel *title_text;
@property (weak, nonatomic) IBOutlet UIButton *btn_left;
@property (weak, nonatomic) IBOutlet UIButton *btn_right;

@property (weak, nonatomic) IBOutlet UIView *navbar_view;
@property (weak, nonatomic) IBOutlet UIView *sidebar_view;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;

// menu 1
@property (weak, nonatomic) IBOutlet UIView *view_menu_1;
@property (weak, nonatomic) IBOutlet UIButton *btn_menu_1;
@property (weak, nonatomic) IBOutlet UIImageView *img_menu_1;

// menu 2
@property (weak, nonatomic) IBOutlet UIView *view_menu_2;
@property (weak, nonatomic) IBOutlet UIButton *btn_menu_2;
@property (weak, nonatomic) IBOutlet UIImageView *img_menu_2;

// menu 3
@property (weak, nonatomic) IBOutlet UIView *view_menu_3;
@property (weak, nonatomic) IBOutlet UIButton *btn_menu_3;
@property (weak, nonatomic) IBOutlet UIImageView *img_menu_3;

// menu 4
@property (weak, nonatomic) IBOutlet UIView *view_menu_4;
@property (weak, nonatomic) IBOutlet UIButton *btn_menu_4;
@property (weak, nonatomic) IBOutlet UIImageView *img_menu_4;

// menu 5
@property (weak, nonatomic) IBOutlet UIView *view_menu_5;
@property (weak, nonatomic) IBOutlet UIButton *btn_menu_5;
@property (weak, nonatomic) IBOutlet UIImageView *img_menu_5;

// menu 6
@property (weak, nonatomic) IBOutlet UIView *view_menu_6;
@property (weak, nonatomic) IBOutlet UIButton *btn_menu_6;
@property (weak, nonatomic) IBOutlet UIImageView *img_menu_6;

@property (weak, nonatomic) IBOutlet UIView *viewbg_sidebar;
@property (weak, nonatomic) IBOutlet UIButton *btn_close_menusidebar;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *right_view_bg;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *left_view_bg;

/* ############ Example ############

PCMenuSideBar *menu_sidebar_class = [PCFunctions loadViewControllerWithName:@"PCMenuSideBar"];
[menu_sidebar_class.btn_menu_1 addTarget:self action:@selector(open_menu1:) forControlEvents:UIControlEventTouchUpInside];
[self.view addSubview:menu_sidebar_class];
 
 */


@end
