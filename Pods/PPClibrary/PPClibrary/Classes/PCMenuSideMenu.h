//
//  PCMenuSideMenu.h
//  PCLibrary
//
//  Created by pimpaporn chaichompoo on 7/26/16.
//  Copyright © 2016 Pimpaporn Chaichompoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCMenuSideMenu : UIView

@property (weak, nonatomic) IBOutlet UIView *viewbg_main_menu;
@property (weak, nonatomic) IBOutlet UIView *viewbg_navber;
@property (weak, nonatomic) IBOutlet UIView *view_navbar;
@property (weak, nonatomic) IBOutlet UIButton *btn_close_side_menu;

@property (weak, nonatomic) IBOutlet UIImageView *image_viewbg;

@property (weak, nonatomic) IBOutlet UIImageView *image_profile;

@property (weak, nonatomic) IBOutlet UILabel *title_text;

@property (weak, nonatomic) IBOutlet UIButton *btn_left;
@property (weak, nonatomic) IBOutlet UIButton *btn_right;

// menu 1
@property (weak, nonatomic) IBOutlet UIView *view_menu_1;
@property (weak, nonatomic) IBOutlet UIButton *btn_menu_1;
@property (weak, nonatomic) IBOutlet UIImageView *img_menu_1;

// menu 2
@property (weak, nonatomic) IBOutlet UIView *view_menu_2;
@property (weak, nonatomic) IBOutlet UIButton *btn_menu_2;
@property (weak, nonatomic) IBOutlet UIImageView *img_menu_2;

// menu 3
@property (weak, nonatomic) IBOutlet UIView *view_menu_3;
@property (weak, nonatomic) IBOutlet UIButton *btn_menu_3;
@property (weak, nonatomic) IBOutlet UIImageView *img_menu_3;

// menu 4
@property (weak, nonatomic) IBOutlet UIView *view_menu_4;
@property (weak, nonatomic) IBOutlet UIButton *btn_menu_4;
@property (weak, nonatomic) IBOutlet UIImageView *img_menu_4;

// menu 5
@property (weak, nonatomic) IBOutlet UIView *view_menu_5;
@property (weak, nonatomic) IBOutlet UIButton *btn_menu_5;
@property (weak, nonatomic) IBOutlet UIImageView *img_menu_5;

// menu 6
@property (weak, nonatomic) IBOutlet UIView *view_menu_6;
@property (weak, nonatomic) IBOutlet UIButton *btn_menu_6;
@property (weak, nonatomic) IBOutlet UIImageView *img_menu_6;

@property (weak, nonatomic) IBOutlet UIView *viewbg_sidebar;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *left_viewbg_main_menu;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top_viewbg_main_menu;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *right_viewbg_main_menu;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottom_viewbg_main_menu;


@end
