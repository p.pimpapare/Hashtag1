//
//  PCNavigationBar.h
//  testRefactorCode
//
//  Created by pimpaporn chaichompoo on 6/15/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCNavigationBar : UIView

@property (weak, nonatomic) IBOutlet UIView *viewbg_left;
@property (weak, nonatomic) IBOutlet UILabel *text_left;
@property (weak, nonatomic) IBOutlet UIButton *btn_left;

@property (weak, nonatomic) IBOutlet UIView *viewbg_right;
@property (weak, nonatomic) IBOutlet UIButton *btn_right;

@property (weak, nonatomic) IBOutlet UILabel *title_text;

@property (weak, nonatomic) IBOutlet UIView *view_bg;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top_view_bg;

-(void)setbg_color:(UIColor *)color left_text:(NSString *)left_text title_text:(NSString *)title_text;
-(void)set_corner_radius:(float)radius bgcolor:(UIColor *)color bottom:(BOOL)bottom;


/* ############ Example ############

round header bar

 [view_class set_corner_radius:20 bgcolor:[UIColor redColor] bottom:NO];

 */

@end
