//
//  PCImage.m
//  LannaLivingDemo
//
//  Created by pimpaporn chaichompoo on 7/29/16.
//  Copyright © 2016 Pimpaporn Chaichompoo. All rights reserved.
//

#import "PCImage.h"
#import "PPPCLibrary.h"
#import <UIKit/UIKit.h>

@implementation PCImage

+(UIImage *)set_blur_image:(UIImageView *)image_blur image_name:(NSString *)image_name
{
    image_blur.image = [UIImage imageNamed:image_name];
    [image_blur setFrame:CGRectMake(0, 0, DEVICE_W, DEVICE_H)];
    
    UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *effectView = [[UIVisualEffectView alloc]initWithEffect:blur];
    effectView.frame = image_blur.frame;
    
    [image_blur addSubview:effectView];
    
    return image_blur.image;
}

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {

    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}


@end
