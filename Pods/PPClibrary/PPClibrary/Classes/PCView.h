//
//  PCView.h
//  testObjectiveC
//
//  Created by pimpaporn chaichompoo on 7/20/16.
//  Copyright © 2016 Pimpaporn Chaichompoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCView : UIView


+(void)setViewCornerRadius:(BOOL)top view_corner:(UIView *_Nullable)view_cornor corner_num:(int)corner_num;

/* ###### Specific Corner radius
 
 [PCView setViewCornerRadius:YES view_corner:self.test_view corner_num:10];
 
 */


// Status Bar style
+(void)setStatusBar:(UIViewController *_Nullable)viewController bgColor:(UIColor *_Nullable)bgColor statusBarLightStyle:(BOOL)lightStyle;


+(void)showAlertWithTitle:(NSString * _Nullable)title Message:( NSString * _Nullable)msg leftButtonTitle:(NSString * _Nullable)leftTextTitle rightButtonTitle:(NSString * _Nullable)rightTextTitle actionTarget:(id _Nonnull)target actionInLeftButton:(SEL _Nullable)actionLeft actionInRightButton:(SEL _Nullable)actionRight TargetViewShow:(id _Nullable)target_view_show;

/* ###### Alert View

 [PCView showAlertWithTitle:@"test" Message:@"test message" leftButtonTitle:@"Cancel" rightButtonTitle:@"OK" actionTarget:self.view actionInLeftButton:@selector(test) actionInRightButton:@selector(test) TargetViewShow:self];
 */

+(void)setViewAnimationWithDuration:(UIView * _Nullable)view_animation time_duration:(float)time showView:(BOOL)showView;

/* ###### Animation with Duration

 [PCView setViewAnimationWithDuration:self.view time_duration:5 showView:YES];
 */


+(void)setConstantAnimationWithDuration:(UIView * _Nullable)classView constant_view:(NSLayoutConstraint * _Nullable)constant_view time_duration:(float)time constant:(float)constantNum;

/* ###### Constant with Duration

 [PCView setConstantAnimationWithDuration:self.view constant_view:self.height_view time_duration:2 constant:500];
 */


-(void)setSelectedColor:(UITableViewCell *_Nullable)cell_selected color_selected:(UIColor *_Nullable)color;

/* ###### Set Color Selected Cell (UITableViewCell)
 
[PCView setSelectedColor:self color_selected:[UIColor blueColor]];
 */


-(void)setRotateView:(UIView *_Nullable)view_rotate duration_time:(float)time delay_time:(float)delay;

/* ###### Rotate View

PCView *view_rotate_class = [PCFunctions loadViewControllerWithName:@"PCView"];
[view_rotate_class setRotateView:self.test_view duration_time:3 delay_time:0];
*/

@end
