//
//  PCLoading.m
//  testRefactorCode
//
//  Created by pimpaporn chaichompoo on 6/14/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "PCLoading.h"
#import "PPPCLibrary.h"

@implementation PCLoading

-(void)awakeFromNib
{
    [self.layer setCornerRadius:10];
    [self setFrame:CGRectMake(DEVICE_W/2-self.frame.size.width/2,DEVICE_H/2-self.frame.size.height/2, 100,100)];
    CGAffineTransform transform = CGAffineTransformMakeScale(1.5f, 1.5f);
    self.indicator.transform = transform;
}

-(void)setloading:(BOOL)isLoading
{
    if (isLoading == YES) {
        [self.indicator startAnimating];
    }else{
        [self.indicator stopAnimating];
    }
}

-(void)set_title_loading:(NSString *)loading_text
{
    [self.loading_text setText:[NSString stringWithFormat:@"%@",loading_text]];
}

@end
