//
//  PCTextfield.h
//  testRefactorCode
//
//  Created by pimpaporn chaichompoo on 6/13/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kSpace @" "
#define kCreditCardLength 16
#define kCreditCardLengthPlusSpaces (kCreditCardLength + 3)
#define kCreditCardObscureLength (kCreditCardLength - 4)

@interface PCTextfield : UITextField

@property (nonatomic, strong) NSString *creditCardBuf;
@property IBOutlet UITextField *txtCardNumber;

+(void)setPlaceholderColor:(UIColor *)color textfield:(UITextField *)textfield;
+(void)setpadding_textfield:(UITextField *)textfield framLeft:(float)leftNum framRight:(float)rightNum;
+(void)set_image_uitextfield:(UITextField *)textfield image:(UIImage *)image_textfield;

-(void)setBounderColor:(UIColor *)color;
-(void)clearBounderColor;

+(void)animateTextFieldWithContrant:(NSLayoutConstraint *)view_constant viewToAnimate:(UIView *)viewToAnimate up:(BOOL)up;
+(void)animateTextField:(UIView *)viewToAnimate up:(BOOL)up;

/* ############ Example ############

[PCTextfield set_image_uitextfield:self.textf_test image:[UIImage imageNamed:@"60.png"]];
[PCTextfield setBounderColor:self.textf_test color:[UIColor redColor]];

 */

@end
