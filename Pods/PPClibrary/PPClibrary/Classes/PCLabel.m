//
//  PCLabel.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 5/2/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "PCLabel.h"

@implementation PCLabel

+(void)set_underLine_text:(UILabel *)label text:(NSString *)text textColor:(UIColor *)textcolor{
    
    NSMutableAttributedString *commentString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",text]];
    
    [commentString setAttributes:@{NSForegroundColorAttributeName:textcolor,NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle]} range:NSMakeRange(0,[commentString length])];
    [label setAttributedText:commentString];
}

+(void)set_shadow_label:(UILabel *)label{
    
    label.shadowColor = [UIColor blackColor];
    label.shadowOffset = CGSizeMake(0,1);
}

@end
