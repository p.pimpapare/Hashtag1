//
//  PCPickerView.h
//  testRefactorCode
//
//  Created by pimpaporn chaichompoo on 6/13/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCPickerView : UIView<UIPickerViewDelegate,UIPickerViewDataSource>{
    
    NSMutableArray *picker_array;
}

@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;

@property (weak, nonatomic) IBOutlet UIButton *btn_cancel;
@property (weak, nonatomic) IBOutlet UIButton *btn_done;

@property (weak, nonatomic) IBOutlet UIView *viewbg_pickerView;
@property (weak, nonatomic) IBOutlet UILabel *title_pickerView;

@property (weak, nonatomic) NSString *seleceted_text;


-(void)set_pickerview_UI:(NSString *)title_text font_title:(UIFont *)font_title title_font_color:(UIColor *)title_font_color font_btn:(UIFont *)font_btn btn_cancel_text_color:(UIColor *)btn_cancel_text_color btn_done_text_color:(UIColor *)btn_done_text_color;

-(void)showPicker_array:(NSArray *)receive_picker_array;

@end
