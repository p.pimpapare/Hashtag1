//
//  PCFunctions.m
//  PCLibrary
//
//  Created by pimpaporn chaichompoo on 7/21/16.
//  Copyright © 2016 Pimpaporn Chaichompoo. All rights reserved.
//

#import "PCFunctions.h"
#import "Reachability.h"
#import <UIKit/UIKit.h>

@implementation PCFunctions

+(BOOL)check_internet_connection
{
    Reachability *internet = [Reachability reachabilityWithHostName: @"www.google.com"];
    NetworkStatus netStatus = [internet currentReachabilityStatus];
    
    switch (netStatus)
    {
        case NotReachable:
        {
            return NO;
            break;
        }
        case ReachableViaWWAN:
        {
            return YES;
            break;
        }
        case ReachableViaWiFi:
        {
            return YES;
            break;
        }
    }
}

+(id)loadViewControllerWithName:(NSString *)viewControllerName
{
    return [[NSBundle mainBundle] loadNibNamed:viewControllerName owner:self options:nil][0];
}

+(NSString *)currentLanguageBundle
{
    // Default language incase an unsupported language is found
    NSString *language = @"en";
    
    if ([NSLocale preferredLanguages].count) {
        // Check first object to be of type "en","es" etc
        // Codes seen by my eyes: "en-US","en","es-US","es" etc
        
        NSString *letterCode = [[NSLocale preferredLanguages] objectAtIndex:0];
        
        if ([letterCode rangeOfString:@"en"].location != NSNotFound) {
            // English
            language = @"en";
        } else if ([letterCode rangeOfString:@"th"].location != NSNotFound) {
            // Thailand
            language = @"th";
        } else if ([letterCode rangeOfString:@"fr"].location != NSNotFound) {
            // French
            language = @"fr";
        } // Add more if needed
    }
    
    return language;
}



@end
