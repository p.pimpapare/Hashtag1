//
//  PCStatusBar.m
//  testRefactorCode
//
//  Created by pimpaporn chaichompoo on 6/16/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "PCStatusBar.h"
#import "PPPCLibrary.h"

@implementation PCStatusBar

+(void)setStatusBar:(UIViewController *)viewController bgColor:(UIColor *)bgColor statusBarLightStyle:(BOOL)lightStyle
{
    
    if (lightStyle == YES) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }else
    {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }
    UIView *statusbar = [[UIView alloc]initWithFrame:CGRectMake(0,0,DEVICE_W,20)];
    [statusbar setBackgroundColor:bgColor];
    [viewController.view addSubview:statusbar];
}

@end
