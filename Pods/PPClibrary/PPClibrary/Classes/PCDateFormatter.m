//
//  PCDateFormatter.m
//  testRefactorCode
//
//  Created by pimpaporn chaichompoo on 6/14/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "PCDateFormatter.h"

@implementation PCDateFormatter

+(id)getMonth_fromJson{
    
    // get province from json file
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"months" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    NSDictionary * parsedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
    return parsedData[@"months"];
}

+(NSString *)sort_time_dd_mm_yyyy:(NSString *)receive_date split_text:(NSString *)split_text{
    // 2016/05/10 -> 10/05/2016
    
    NSArray *date_Split = [receive_date componentsSeparatedByString:split_text];
    return [NSString stringWithFormat:@"%@/%@/%@",[date_Split objectAtIndex:2],[date_Split objectAtIndex:1],[date_Split objectAtIndex:0]];
}

+(NSString *)sort_time_dd_mm_yyyy_to_full_month:(NSString *)receive_date split_text:(NSString *)split_text{
    // 2016/03/11 -> 11 มีนาคม 2559
    
    NSString *month_str;
    
    NSArray *date_array = [receive_date componentsSeparatedByString:split_text];
    
    NSString *year = [date_array objectAtIndex:0];
    NSString *month = [date_array objectAtIndex:1];
    NSString *date = [date_array objectAtIndex:2];
    
    int year_int = [year intValue];
    
    id month_dic =  [self getMonth_fromJson];
    
    for (int i = 0; i<[month_dic count]; i++) {
        
        if ([month_dic[i][@"id"] isEqualToString:month]) {
            month_str = [NSString stringWithFormat:@"%@",month_dic[i][@"name_full_th"]];
        }
    }
    
    return [NSString stringWithFormat:@"%@ %@ %d",date,month_str,year_int+543];
}

+(NSString *)sort_full_month_to_yyyy_mm_dd:(NSString *)receive_date{ // 11 มีนาคม 2559 -> 2016/03/11
    
    NSString *month_str;
    
    NSArray *date_array = [receive_date componentsSeparatedByString:@" "];
    
    NSString *year = [date_array objectAtIndex:2];
    NSString *month = [date_array objectAtIndex:1];
    NSString *date = [date_array objectAtIndex:0];
    
    int year_int = [year intValue];
    
    id month_dic =  [self getMonth_fromJson];
    
    for (int i = 0; i<[month_dic count]; i++) {
        
        if ([month_dic[i][@"name_full_th"] isEqualToString:month]) {
            month_str = [NSString stringWithFormat:@"%@",month_dic[i][@"id"]];
        }
    }
    return [NSString stringWithFormat:@"%d/%@/%@",year_int-543,month_str,date];
}

@end
