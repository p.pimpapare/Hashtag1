//
//  PCHTMLText.h
//  PCLibrary
//
//  Created by pimpaporn chaichompoo on 7/21/16.
//  Copyright © 2016 Pimpaporn Chaichompoo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PCHTMLText : NSObject

+(void)setHTMl_text:(UILabel *)label_text font_HTML:(NSString *)font_name text_HTML:(NSString *)html_text;
+(NSString *)replaceHTML_by_string:(NSString *)HTML old_string:(NSString *)old_string new_string:(NSString *)new_string;

/* ############ Example ############

[PCHTMLText setHTMl_text:self.test_text_one font_HTML:@"System" text_HTML:@"<b>หัวข้อ</b>เนื้อหา"];
*/

@end
