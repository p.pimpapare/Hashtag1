//
//  PCPickerView.h
//  testRefactorCode
//
//  Created by pimpaporn chaichompoo on 6/13/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCDatePickerView : UIView

@property (weak, nonatomic) IBOutlet UIDatePicker *date_pickerView;

@property (weak, nonatomic) IBOutlet UIButton *btn_cancel;
@property (weak, nonatomic) IBOutlet UIButton *btn_done;

@property (weak, nonatomic) IBOutlet UIView *viewbg_pickerView;
@property (weak, nonatomic) IBOutlet UILabel *title_pickerView;

@property (weak, nonatomic) NSString *seleceted_date;


-(void)set_pickerview_UI:(NSString *)title_text font_title:(UIFont *)font_title title_font_color:(UIColor *)title_font_color font_btn:(UIFont *)font_btn btn_cancel_text_color:(UIColor *)btn_cancel_text_color btn_done_text_color:(UIColor *)btn_done_text_color;

-(void)showDatePicker_With_SpecificDate:(NSString *)specificDate;


/* ############ Example ############
 
 #import "PCDatePickerView.h"

 PCDatePickerView *datePickerView;

 - (void)viewDidLoad {
 [super viewDidLoad];
 
 [self test_date_pickerView:@"2 มิถุนายน 2535"];
 }
 
 -(void)test_date_pickerView:(NSString *)date_text{
 
 datePickerView = [[NSBundle mainBundle] loadNibNamed:@"PCDatePickerView" owner:self options:nil][0];
 [datePickerView set_pickerview_UI:@"test" font_title:[UIFont fontWithName:@"System" size:14] title_font_color:[UIColor redColor] font_btn:[UIFont fontWithName:@"System" size:12]  btn_cancel_text_color:[UIColor blackColor] btn_done_text_color:[UIColor blueColor]];
 [datePickerView showDatePicker_With_SpecificDate:[NSString stringWithFormat:@"%@",date_text]];
 [datePickerView.btn_done addTarget:self action:@selector(acceptDatePicker:) forControlEvents:UIControlEventTouchUpInside];
 
 [self.view addSubview:datePickerView];
 }
 
 -(void)acceptDatePicker:(id)sender{
 
 NSLog(@"seleceted date %@",datePickerView.seleceted_date);
 }
 
*/

@end
