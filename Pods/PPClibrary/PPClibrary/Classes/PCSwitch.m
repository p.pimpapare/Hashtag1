
//
//  PCSwitch.m
//  testRefactorCode
//
//  Created by pimpaporn chaichompoo on 6/15/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "PCSwitch.h"

@implementation PCSwitch

-(void)awakeFromNib{
    
}

-(void)set_turnOff_Switch:(NSString *)title_alert subtitle_alert:(NSString *)subtitle_alert cancel_title_btn:(NSString *)cancel_title_btn title_confirm_btn:(NSString *)title_confirm_btn view_class:(UIViewController *)view_class actionTarget:(id _Nonnull)target actionInCancelButton:(SEL _Nullable)actionLeft actionInConfirmButton:(SEL _Nullable)actionRight
{

        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:title_alert message:subtitle_alert preferredStyle:UIAlertControllerStyleActionSheet];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:cancel_title_btn style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
            [target performSelectorOnMainThread:actionLeft withObject:nil waitUntilDone:YES];

            [self setOn:YES animated:NO];
            
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:title_confirm_btn style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
            
            [target performSelectorOnMainThread:actionRight withObject:nil waitUntilDone:YES];

            [self setOn:NO animated:NO];
            
        }]];
        
        [view_class presentViewController:actionSheet animated:YES completion:nil];
}

@end
