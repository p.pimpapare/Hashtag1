//
//  PCShadow.h
//  testRefactorCode
//
//  Created by pimpaporn chaichompoo on 6/16/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCShadow : UIView

+(void)set_shadow:(double)shadowOffset shadowRadius:(double)shadowRadius shadowOpacity:(double)shadowOpacity view:(UIView *)view;

+(void)set_border_line:(UIView *)view;

+(void)set_gradient_menu:(UIView *)view;
+(void)set_custom_gradient_menu:(int)color1 color2:(int)color2 color3:(int)color3 color21:(int)color21 color22:(int)color22 color23:(int)color23 view:(UIView *)view;

+(void)set_blur_image:(UIView *)view;

/* ############ Example ############

[PCShadow set_blur_image:self.view];

 */

@end
