//
//  ASAlertView.m
//  ASStandardLibrary
//
//  Created by Akarapas Wongkaew on 1/19/2559 BE.
//  Copyright © 2559 Akarapas Wongkaew. All rights reserved.
//

#import "ASAlert.h"

@implementation ASAlert

+(void)showAlertWithTitle:(NSString * _Nullable)title Message:( NSString * _Nullable)msg leftButtonTitle:(NSString * _Nullable)leftTextTitle rightButtonTitle:(NSString * _Nullable)rightTextTitle TargetViewShow:(id)target_view_show
{
    if ([UIAlertController class]) {
        // use UIAlertController
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:title
                                   message:msg
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        if (leftTextTitle) {
            UIAlertAction* ok = [UIAlertAction actionWithTitle:leftTextTitle style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action){
                                                           //Do Some action here
                                                           //UITextField *textField = alert.textFields[0];
                                                           //NSLog(@"text was %@", textField.text);
                                                       }];
            [alert addAction:ok];
        }
        
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:(rightTextTitle)?rightTextTitle:@"OK" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                       }];
        [alert addAction:cancel];
        
        /*
         [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
         textField.placeholder = @"folder name";
         textField.keyboardType = UIKeyboardTypeDefault;
         }];
         */
        
        //UIViewController *rootVC = [[UIApplication sharedApplication].delegate window].rootViewController;
        [target_view_show presentViewController:alert animated:YES completion:nil];
        
    }else{
        UIAlertView *alert = [[UIAlertView alloc] init];
        [alert setTitle:title];
        [alert setMessage:msg];
        if (leftTextTitle) {
            [alert addButtonWithTitle:leftTextTitle];
        }
        
        [alert addButtonWithTitle:(rightTextTitle)?rightTextTitle:@"OK"];
        
        [alert dismissWithClickedButtonIndex:(leftTextTitle)?1:0 animated:YES];
        
        [alert show];
    }
}

+(void)showAlertWithTitle:(NSString * _Nullable)title Message:( NSString * _Nullable)msg leftButtonTitle:(NSString * _Nullable)leftTextTitle rightButtonTitle:(NSString * _Nullable)rightTextTitle actionTarget:(id _Nonnull)target actionInLeftButton:(SEL _Nullable)actionLeft actionInRightButton:(SEL _Nullable)actionRight TargetViewShow:(id)target_view_show
{
    if ([UIAlertController class]) {
        // use UIAlertController
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:title
                                   message:msg
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        if (leftTextTitle) {
            UIAlertAction* ok = [UIAlertAction actionWithTitle:leftTextTitle style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action){
                                                           //Do Some action here
                                                           //UITextField *textField = alert.textFields[0];
                                                           //NSLog(@"text was %@", textField.text);
                                                           [target performSelectorOnMainThread:actionLeft withObject:nil waitUntilDone:YES];
                                                       }];
            [alert addAction:ok];
        }
        
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:(rightTextTitle)?rightTextTitle:@"OK" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           [target performSelectorOnMainThread:actionRight withObject:nil waitUntilDone:YES];
                                                       }];
        [alert addAction:cancel];
        
        //UIViewController *rootVC = [[UIApplication sharedApplication].delegate window].rootViewController;
        [target_view_show presentViewController:alert animated:YES completion:nil];
        
    }else{
        UIAlertView *alert = [[UIAlertView alloc] init];
        [alert setTitle:title];
        [alert setMessage:msg];
        if (leftTextTitle) {
            [alert addButtonWithTitle:leftTextTitle];
        }
        
        [alert addButtonWithTitle:(rightTextTitle)?rightTextTitle:@"OK"];
        
        [alert dismissWithClickedButtonIndex:(leftTextTitle)?1:0 animated:YES];
        
        [alert show];
    }
}

+(void)showAlertWithTitle:(NSString * _Nullable)title Message:( NSString * _Nullable)msg ButtonsTitle:(NSArray <NSString *> * _Nonnull)arrayButton Target:(id)target Block:(void (^ __nullable)(int buttonIndex))handler
{
    if ([UIAlertController class]) {
        // use UIAlertController
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:title
                                   message:msg
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        int index = 0;
        for (NSString *string in arrayButton) {
            UIAlertAction* alertAction = [UIAlertAction actionWithTitle:string style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction * action){
                                                                    handler(index);
                                                                    [alert dismissViewControllerAnimated:YES completion:nil];
                                                                }];
            [alert addAction:alertAction];
            index++;
        }
        
        //
        dispatch_async(dispatch_get_main_queue(), ^{
            [target presentViewController:alert animated:YES completion:nil];
        });
    }else{
        
    }
}

+(void)showAlertInputWithTitle:(NSString * _Nullable)title Message:( NSString * _Nullable)msg TextfieldsPlaceHolder:(NSArray <NSString *> * _Nonnull)arrayTextfileds ButtonsTitle:(NSArray <NSString *> * _Nonnull)arrayButton Target:(id)target Block:(void (^ __nullable)(int buttonIndex, NSArray *arrayTextFields))handler
{
    if ([UIAlertController class]) {
        // use UIAlertController
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:title
                                   message:msg
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        for (NSString *string in arrayTextfileds) {
            [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
                [textField setPlaceholder:string];
            }];
        }
        
        int index = 0;
        for (NSString *string in arrayButton) {
            UIAlertAction* alertAction = [UIAlertAction actionWithTitle:string style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction * action){
                                                                    handler(index,alert.textFields);
                                                                    [alert dismissViewControllerAnimated:YES completion:nil];
                                                                }];
            [alert addAction:alertAction];
            index++;
        }
        
        //
        dispatch_async(dispatch_get_main_queue(), ^{
            [target presentViewController:alert animated:YES completion:nil];
        });
    }else{
        
    }
}

@end