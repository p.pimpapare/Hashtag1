//
//  ASLoading.h
//  ASStandardLibrary
//
//  Created by Akarapas Wongkaew on 1/20/2559 BE.
//  Copyright © 2559 Akarapas Wongkaew. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>
#import "ASStandardLibrary.h"

@interface ASLoadingView : UIView

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *act_loading;

@end

@interface ASLoading : NSObject

+(void)showLoadingInView:(UIView *)view;
+(void)showLoadingInView:(UIView *)view WithText:(NSString *)text;
+(void)hideLoadingAllInView:(UIView *)view;
+(void)hideLoadingAll;

@end
