//
//  ASButton.m
//  ASStandardLibrary
//
//  Created by Akarapas Wongkaew on 5/9/2559 BE.
//  Copyright © 2559 Akarapas Wongkaew. All rights reserved.
//

#import "ASButton.h"

@implementation UIButton(ASButton)

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)setConnerRadiusWithRadius:(float)radius
{
    [self.layer setCornerRadius:radius];
    [self.layer setMasksToBounds:YES];
    
    [self.layer setShouldRasterize:YES];
    [self.layer setRasterizationScale:[UIScreen mainScreen].scale];
}

-(void)setImageToRightSideWithImage:(UIImage *)image
{
    //button.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    //button.titleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    //button.imageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    [self setImage:image forState:UIControlStateNormal];
    
    self.imageEdgeInsets = UIEdgeInsetsMake(0., self.frame.size.width - (image.size.width + 0.), 0., 0.);
    self.titleEdgeInsets = UIEdgeInsetsMake(0., 0., 0., image.size.width);
}

- (void)swapImageText
{
    self.transform = CGAffineTransformScale(self.transform, -1.0f, 1.0f);
    self.titleLabel.transform = CGAffineTransformScale(self.titleLabel.transform, -1.0f, 1.0f);
    self.imageView.transform = CGAffineTransformScale(self.imageView.transform, -1.0f, 1.0f);
}

@end
