//
//  ASDatePicker.m
//  ASStandardLibrary
//
//  Created by Akarapas Wongkaew on 2/2/2559 BE.
//  Copyright © 2559 Akarapas Wongkaew. All rights reserved.
//

#import "ASDatePicker.h"

@implementation ASDatePickerView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib
{
    [self.btn_cancel addTarget:self action:@selector(closeASDatePicker:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_cancel setTag:0];
    [self.btn_done addTarget:self action:@selector(acceptASDatePicker:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_done setTag:1];
    [self.datePicker setDatePickerMode:UIDatePickerModeDate];
    [self.datePicker setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"th_TH"]];
    self.datePicker.calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierBuddhist];
}

-(void)closeASDatePicker:(id)sender
{
    [ASDatePicker closeASDatePicker:self.btn_cancel Data:self.datePicker.date];
}

-(void)acceptASDatePicker:(id)sender
{
    [ASDatePicker closeASDatePicker:self.btn_done Data:self.datePicker.date];
}

@end

@implementation ASDatePicker

ASDatePickerCompletionDone completionReturnDone;
ASDatePickerCompletionCancel completionReturnCancel;

+(UIView *)getASDatePickerView
{
    UIViewController *rootVC = [[UIApplication sharedApplication].delegate window].rootViewController;
    UIView *rootView = rootVC.view;
    
    UIView *view_main = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rootView.frameWidth, rootView.frameHeight)];
    [view_main setBackgroundColor:[UIColor clearColor]];
    [view_main setTag:60000];
    
    UIView *view_bg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rootView.frameWidth, rootView.frameHeight)];
    [view_bg setBackgroundColor:[UIColor blackColor]];
    [view_bg setAlpha:0.8];
    [view_main addSubview:view_bg];
    
    ASDatePickerView *datePickerView = [[NSBundle mainBundle] loadNibNamed:@"ASDatePicker" owner:self options:nil][0];
    [datePickerView setFrame:CGRectMake(0, 0, DEVICE_W, datePickerView.frameHeight)];
    [datePickerView setCenter:CGPointMake(view_main.frameWidth/2, view_main.frameHeight/2)];
    [view_main addSubview:datePickerView];
    
    return view_main;
}

+(void)showDatePickerWithCompletionDone:(ASDatePickerCompletionDone)completionDone Cancel:(ASDatePickerCompletionCancel)completionCancel
{
    completionReturnDone = completionDone;
    completionReturnCancel = completionCancel;
    
    UIViewController *rootVC = [[UIApplication sharedApplication].delegate window].rootViewController;
    UIView *view_ASDatePickerView = [self getASDatePickerView];
    [view_ASDatePickerView setAlpha:0];
    [rootVC.view addSubview:view_ASDatePickerView];
    
    [rootVC.view setUserInteractionEnabled:NO];
    [UIView animateWithDuration:0.4 animations:^{
        [view_ASDatePickerView setAlpha:1];
    } completion:^(BOOL finished) {
        [rootVC.view setUserInteractionEnabled:YES];
    }];
}

+(void)closeASDatePicker:(id)sender Data:(id)data
{
    UIButton *btn = (UIButton *)sender;
    int tag = (int)[btn tag];
    NSLog(@"%s:%d",__PRETTY_FUNCTION__,tag);
    
    if (tag==1) {
        completionReturnDone(data);
    }else{
        completionReturnCancel(data);
    }
    
    UIViewController *rootVC = [[UIApplication sharedApplication].delegate window].rootViewController;
    
    [rootVC.view setUserInteractionEnabled:NO];
    for (UIView *view in rootVC.view.subviews) {
        if ([view isKindOfClass:[UIView class]]&&[view tag]==60000) {
            [UIView animateWithDuration:0.4 animations:^{
                [view setAlpha:0];
            } completion:^(BOOL finished) {
                [view removeFromSuperview];
                
                [rootVC.view setUserInteractionEnabled:YES];
            }];
        }
    }
}

@end
