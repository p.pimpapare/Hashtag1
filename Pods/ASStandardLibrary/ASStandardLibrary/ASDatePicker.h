//
//  ASDatePicker.h
//  ASStandardLibrary
//
//  Created by Akarapas Wongkaew on 2/2/2559 BE.
//  Copyright © 2559 Akarapas Wongkaew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASStandardLibrary.h"

@class ASDatePicker;
typedef void(^ASDatePickerCompletionDone)(id dateSelected);
typedef void(^ASDatePickerCompletionCancel)(id dateSelected);

@interface ASDatePickerView : UIView
{
    
}

@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UIButton *btn_cancel;
@property (weak, nonatomic) IBOutlet UIButton *btn_done;

@end

@interface ASDatePicker : NSObject
{
    
}

+(void)showDatePickerWithCompletionDone:(ASDatePickerCompletionDone)completionDone Cancel:(ASDatePickerCompletionCancel)completionCancel;
+(void)closeASDatePicker:(id)sender Data:(id)data;

@end
