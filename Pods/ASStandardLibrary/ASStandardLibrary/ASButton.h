//
//  ASButton.h
//  ASStandardLibrary
//
//  Created by Akarapas Wongkaew on 5/9/2559 BE.
//  Copyright © 2559 Akarapas Wongkaew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton(ASButton)

-(void)setConnerRadiusWithRadius:(float)radius;
-(void)setImageToRightSideWithImage:(UIImage *)image;
-(void)swapImageText;

@end
