//
//  ASCollectionView.m
//  ASStandardLibrary
//
//  Created by Akarapas Wongkaew on 4/11/2559 BE.
//  Copyright © 2559 Akarapas Wongkaew. All rights reserved.
//

#import "ASCollectionView.h"

@interface ASCollectionView()
{
    NSString *textEmptyState;
    UILabel *label;
}
@end

@implementation ASCollectionView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    textEmptyState = [NSString stringWithFormat:@"%@",@"No item."];
    
    [self checkEmptyObject:nil];
}

-(void)reloadData
{
    [super reloadData];
    
    [self checkEmptyObject:nil];
    
    if (self.refreshCtrl) {
        [self.refreshCtrl endRefreshing];
    }
}

-(void)checkEmptyObject:(id)sender
{
    if (![self numberOfItemsInSection:0]) {
        if (!label) {
            label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 40)];
            [label setTag:999];
            [label setCenter:CGPointMake(self.frame.size.width/2, self.frame.size.height/2)];
            [label setText:textEmptyState];
            [label setTextAlignment:NSTextAlignmentCenter];
            [label setAutoresizingMask:-1];
            //[label setFont:[UIFont pcfont_VarelaRound_WithSize:20]];
            //[label setTextColor:[UIColor pcColor_font_1]];
            [label setTextColor:[UIColor whiteColor]];
            [self addSubview:label];
            
            [self setUserInteractionEnabled:NO];
        }
    }else{
        
        [label removeFromSuperview];
        label = nil;
        
        [self setUserInteractionEnabled:YES];
    }
}

-(void)setTextEmptyState:(NSString *)text
{
    if (label) {
        [label setText:text];
    }
}

-(void)addRefreshCtrlWithTarget:(id)target Action:(SEL)action
{
    if (!self.refreshCtrl) {
        self.refreshCtrl = [[UIRefreshControl alloc] init];
        [self.refreshCtrl addTarget:target action:action forControlEvents:UIControlEventValueChanged];
        [self addSubview:self.refreshCtrl];
        
        [self setAlwaysBounceVertical:YES];
    }
}

@end
