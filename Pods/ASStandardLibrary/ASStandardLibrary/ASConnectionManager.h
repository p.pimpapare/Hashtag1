//
//  ASConnectionManager.h
//  ASStandardLibrary
//
//  Created by Akarapas Wongkaew on 1/19/2559 BE.
//  Copyright © 2559 Akarapas Wongkaew. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ASConnectionManager;
typedef void(^ASConnectionProgress)(float percent);
typedef void(^ASConnection)(ASConnectionManager *manager ,id result);
typedef void(^ASErrorConnection)(ASConnectionManager *manager ,id result);

typedef enum : NSUInteger {
    ASConnectionMethodGET,
    ASConnectionMethodPOST,
    ASConnectionMethodDELETE,
    ASConnectionMethodPUT,
} ASConnectionMethod;

@interface ASConnectionManager : NSObject

+ (id)sharedManager;

#pragma mark - MainPageRequestConnection
-(void)getConnectionWithURL:(NSString *)urlString ConnectionMethod:(ASConnectionMethod)method Parameters:(NSDictionary *)parameters Block:(ASConnection)connect Error:(ASErrorConnection)error;

-(void)getConnectionWithURL:(NSString *)urlString ConnectionMethod:(ASConnectionMethod)method Parameters:(NSDictionary *)parameters HeaderFields:(NSDictionary *)headerFields Block:(ASConnection)connect Error:(ASErrorConnection)error;

-(void)uploadImageWithURL:(NSString *)urlString ConnectionMethod:(ASConnectionMethod)method Parameters:(NSDictionary *)parameters ImageData:(NSData *)imageData ImageKey:(NSString *)imageKey ImageFileName:(NSString *)imageFileName Progress:(ASConnectionProgress)progress Block:(ASConnection)connect Error:(ASErrorConnection)error;

@end
