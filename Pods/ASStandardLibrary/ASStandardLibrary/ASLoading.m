//
//  ASLoading.m
//  ASStandardLibrary
//
//  Created by Akarapas Wongkaew on 1/20/2559 BE.
//  Copyright © 2559 Akarapas Wongkaew. All rights reserved.
//

#import "ASLoading.h"

#define ANIMATION_TIME 0.4

@implementation ASLoadingView

-(void)awakeFromNib
{
    [self.act_loading startAnimating];
    
    [self.layer setCornerRadius:5];
    [self.layer setMasksToBounds:YES];
}

@end

static NSMutableArray *arrayASLoadingView = nil;

@implementation ASLoading

+(ASLoadingView *)getViewLoading
{
    if (!arrayASLoadingView) {
        arrayASLoadingView = [NSMutableArray array];
    }
    
    ASLoadingView *viewLoading = [[NSBundle mainBundle] loadNibNamed:@"ASLoading" owner:self options:nil][0];
    [viewLoading setFrameWidth:(IS_IPAD)?(DEVICE_W/6):(DEVICE_W/3)];
    [viewLoading setFrameHeight:(IS_IPAD)?(DEVICE_W/6):(DEVICE_W/3)];
    [viewLoading.act_loading setCenter:viewLoading.center];
    
    [arrayASLoadingView addObject:viewLoading];
    
    return viewLoading;
}

+(void)showLoadingInView:(UIView *)view
{
    ASLoadingView *viewLoading = [self getViewLoading];
    [viewLoading setCenter:view.center];
    [viewLoading setAlpha:0];
    [view addSubview:viewLoading];
    
    [UIView animateWithDuration:ANIMATION_TIME animations:^{
        [viewLoading setAlpha:1];
    } completion:^(BOOL finished){
        
    }];
}

+(void)showLoadingInView:(UIView *)view WithText:(NSString *)text
{
    ASLoadingView *viewLoading = [self getViewLoading];
    
    int grap = 16;
    [viewLoading.act_loading setFrameTop:grap];
    
    UILabel *label = [[UILabel alloc] init];
    [label setFrame:CGRectMake(0, 0, viewLoading.frameWidth-grap, 0)];
    [label setNumberOfLines:0];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setTextColor:[UIColor whiteColor]];
    [label setText:text];
    [label setFrameHeight:[label.text boundingRectWithSize:CGSizeMake(label.frameWidth, 9999) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:label.font} context:nil].size.height];
    [label setFrameTop:viewLoading.act_loading.frameBottom+grap];
    [label setCenter:CGPointMake(viewLoading.frameWidth/2, label.center.y)];
    
    [viewLoading addSubview:label];
    [viewLoading setFrameHeight:label.frameBottom+grap];
    
    [viewLoading setCenter:view.center];
    [viewLoading setAlpha:0];
    [view addSubview:viewLoading];
    
    [UIView animateWithDuration:ANIMATION_TIME animations:^{
        [viewLoading setAlpha:1];
    } completion:^(BOOL finished){
        
    }];
}

+(void)hideLoadingAllInView:(UIView *)view
{
    for (ASLoadingView *asLoadingView in view.subviews) {
        if ([asLoadingView isKindOfClass:[ASLoadingView class]]) {
            [UIView animateWithDuration:ANIMATION_TIME animations:^{
                [asLoadingView setAlpha:0];
            } completion:^(BOOL finished){
                [asLoadingView removeFromSuperview];
            }];
            
        }
    }
}

+(void)hideLoadingAll
{
    for (ASLoadingView *asLoadingView in arrayASLoadingView) {
        if ([asLoadingView isKindOfClass:[ASLoadingView class]]) {
            [UIView animateWithDuration:ANIMATION_TIME animations:^{
                [asLoadingView setAlpha:0];
            } completion:^(BOOL finished){
                [asLoadingView removeFromSuperview];
            }];
        }
    }
    
    [arrayASLoadingView removeAllObjects];
}

@end
