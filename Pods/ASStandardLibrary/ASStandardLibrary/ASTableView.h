//
//  ASTableView.h
//  ASStandardLibrary
//
//  Created by Akarapas Wongkaew on 4/12/2559 BE.
//  Copyright © 2559 Akarapas Wongkaew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ASTableView : UITableView

@property (strong, nonatomic) UIRefreshControl *refreshCtrl;
-(void)setTextEmptyState:(NSString *)text;
-(void)addRefreshCtrlWithTarget:(id)target Action:(SEL)action;

@end
