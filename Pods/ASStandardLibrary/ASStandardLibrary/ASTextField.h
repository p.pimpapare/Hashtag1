//
//  ASTextField.h
//  ASStandardLibrary
//
//  Created by Akarapas Wongkaew on 5/9/2559 BE.
//  Copyright © 2559 Akarapas Wongkaew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField(ASTextField)

-(void)setLeftViewWithImage:(UIImage *)image;
-(void)setRightViewWithImage:(UIImage *)image;
-(void)setRightViewWithImage:(UIImage *)image Target:(id)target Action:(SEL)action;
-(void)setBounderColor:(UIColor *)color;
-(void)clearBounderColor:(id)sender;

@end
