//
//  ASTextField.m
//  ASStandardLibrary
//
//  Created by Akarapas Wongkaew on 5/9/2559 BE.
//  Copyright © 2559 Akarapas Wongkaew. All rights reserved.
//

#import "ASTextField.h"

#import "UIView+Adjust.h"

@implementation UITextField(ASTextField)

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

// ################### Private Function

-(UIView *)getView:(id)sender
{
    UIView *view = [[UIView alloc] init];
    [view setFrame:CGRectMake(0, 0, 56, 37)];
    return view;
}

-(UIImageView *)getImageViewWithImage:(UIImage *)image
{
    UIImageView *imageView = [[UIImageView alloc] init];
    [imageView setFrame:CGRectMake(0, 0, 22, 22)];
    [imageView setImage:image];
    [imageView setContentMode:UIViewContentModeScaleAspectFit];
    return imageView;
}

// ###################

-(void)setLeftViewWithImage:(UIImage *)image
{
    UIView *view = [self getView:nil];
    UIImageView *imageView = [self getImageViewWithImage:image];
    
    [imageView setCenter:CGPointMake(view.frameWidth/2, view.frameHeight/2)];
    [view addSubview:imageView];
    
    [self setLeftView:view];
    [self setLeftViewMode:UITextFieldViewModeAlways];
}

-(void)setRightViewWithImage:(UIImage *)image
{
    UIView *view = [self getView:nil];
    UIImageView *imageView = [self getImageViewWithImage:image];
    
    [imageView setCenter:CGPointMake(view.frameWidth/2, view.frameHeight/2)];
    [view addSubview:imageView];
    
    [self setRightView:view];
    [self setRightViewMode:UITextFieldViewModeAlways];
}

-(void)setRightViewWithImage:(UIImage *)image Target:(id)target Action:(SEL)action
{
    UIView *view = [self getView:nil];
    UIImageView *imageView = [self getImageViewWithImage:image];
    
    [imageView setCenter:CGPointMake(view.frameWidth/2, view.frameHeight/2)];
    [view addSubview:imageView];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:view.frame];
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:btn];
    
    [self setRightView:view];
    [self setRightViewMode:UITextFieldViewModeAlways];
}

-(void)setBounderColor:(UIColor *)color
{
    [self.layer setBorderColor:[color CGColor]];
    [self.layer setBorderWidth:1];
    [self.layer setMasksToBounds:NO];
    [self.layer setCornerRadius:5];
}

-(void)clearBounderColor:(id)sender
{
    [self.layer setBorderColor:[[UIColor clearColor] CGColor]];
}

@end
