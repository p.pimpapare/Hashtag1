//
//  NSString+VerifyData.m
//  Pods
//
//  Created by cHinn on 10/12/2558 BE.
//
//

#import "NSString+VerifyData.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSString (VerifyData)

- (NSString *)generateMD5
{
    const char *cStr = [self UTF8String];
    unsigned char digest[16];
    CC_MD5( cStr, (CC_LONG)strlen(cStr), digest ); // This is the md5 call
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return  output;
    
}

- (NSString *) stringByStrippingHTML
{
    NSRange r;
    NSString *s = [self copy];
    while ((r = [s rangeOfString:@"<BR>|<br>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@"\n"];
    while ((r = [s rangeOfString:@"<[^>]+>|&nbsp;" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}

- (NSString *) stringByStrippingNull
{
    NSRange r;
    NSString *s = [self copy];
    while ((r = [s rangeOfString:@"null|<null>|NULL|Null" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}

- (BOOL)isEmptyString
{
    if ([self isKindOfClass:[NSString class]]) {
        if ([self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0) {
            return YES;
        }else{
            return NO;
        }
    }
    return NO;
}

- (id)wrapHypen
{
    NSString *ret = @"-";
    id string = (id)self;
    if (!string || [string isKindOfClass:[NSNull class]] || [string isEmptyString]) {
        return ret;
    }
    
    return string;
}

@end
