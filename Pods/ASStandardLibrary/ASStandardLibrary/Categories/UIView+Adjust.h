
//
//  UIView+Adjust.h
//  AISService
//
//  Created by Kittipop Puangjit on 12/20/2557 BE.
//  Copyright (c) 2557 Yor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

CG_INLINE CGPoint CGPointOffset(const CGPoint point, CGFloat x, CGFloat y) {
    return CGPointMake(point.x + x, point.y + y);
}

CG_INLINE CGPoint CGRectTopRight(const CGRect rect) {
    return CGPointOffset(rect.origin, rect.size.width, 0);
}

CG_INLINE CGPoint CGRectBottomLeft(const CGRect rect) {
    return CGPointOffset(rect.origin, 0, rect.size.height);
}

CG_INLINE CGPoint CGRectBottomRight(const CGRect rect) {
    return CGPointOffset(rect.origin, rect.size.width, rect.size.height);
}

@interface UIView(Adjust)
-(void) addSubviewDocked:(UIView*)view;
-(UIButton*) addTransparentOverlayButton;
-(void) addTouchedUpInsideTarget:(id)target action:(SEL) action;
-(void) alignToSuperViewAtBottom;
-(void) alignToView:(UIView*)anchorView atBottomWithOffset:(CGFloat) offset;

@property (nonatomic, assign) CGFloat frameLeft;
@property (nonatomic, assign) CGFloat frameTop;
@property (nonatomic, assign) CGFloat frameBottom;
@property (nonatomic, assign) CGFloat frameRight;

@property (nonatomic, assign) CGPoint frameTopLeft;
@property (nonatomic, readonly) CGPoint frameTopRight;
@property (nonatomic, readonly) CGPoint frameBottomLeft;
@property (nonatomic, readonly) CGPoint frameBottomRight;

@property (nonatomic, assign) CGFloat frameHeight;
@property (nonatomic, assign) CGFloat frameWidth;

@end


