//
//  UIView+UpdateAutoLayout.h
//  AISService
//
//  Created by cHinn on 9/8/2558 BE.
//  Copyright (c) 2558 Yor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (CNAutoLayout)

- (void) updateConstraintWithConstant:(CGFloat)constant forAttribute:(NSLayoutAttribute)attribute;
- (CGFloat) getConstantforAttribute:(NSLayoutAttribute)attribute;

@end
