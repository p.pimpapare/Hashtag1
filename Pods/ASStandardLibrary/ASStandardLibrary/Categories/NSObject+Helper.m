//
//  NSObject+Helper.m
//  ASStandardLibrary
//
//  Created by Akarapas Wongkaew on 1/19/2559 BE.
//  Copyright © 2559 Akarapas Wongkaew. All rights reserved.
//

#import "NSObject+Helper.h"

@implementation NSObject(Helper)

+(BOOL)dictionaryHaveNullValue:(NSMutableDictionary *)dic
{
    NSMutableArray *array = [NSMutableArray arrayWithArray:[(NSMutableDictionary *)dic allKeys]];
    for (id key in array) {
        if ([dic[key] isEqual:[NSNull null]]) {
            NSLog(@"cant save because data have null value.");
            return false;
            break;
        }
    }
    return true;
}

+(void)keepObject:(id)Object forKey:(NSString *)string
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    if ([Object isKindOfClass:[NSMutableDictionary class]]||[Object isKindOfClass:[NSDictionary class]]) {
        Object = [self clearNilValueInDictionary:Object];
    }else if ([Object isKindOfClass:[NSMutableArray class]]||[Object isKindOfClass:[NSArray class]]) {
        Object = [self clearNilValueInArray:Object];
    }
    
    [userDefault setObject:Object forKey:string];
    [userDefault synchronize];
}

+(NSMutableDictionary *)clearNilValueInDictionary:(NSMutableDictionary *)dicData
{
    NSArray *arrayKey = [NSMutableArray arrayWithArray:[dicData allKeys]];
    NSMutableDictionary *dicDataTemp = [NSMutableDictionary dictionaryWithDictionary:dicData];
    for (NSString *key in arrayKey) {
        if ([dicDataTemp[key] isEqual:[NSNull null]]||!dicDataTemp[key]) {
            [dicDataTemp setObject:[NSString stringWithFormat:@"null_value!"] forKey:key];
        }
    }
    return dicDataTemp;
}

+(NSMutableArray *)clearNilValueInArray:(NSMutableArray *)arrayData
{
    for (id value in arrayData) {
        if ([value isEqual:[NSNull null]]) {
            [arrayData replaceObjectAtIndex:[arrayData indexOfObject:value] withObject:[NSString stringWithFormat:@"null_value!"]];
        }
    }
    return arrayData;
}

+(id)getObjectForKey:(NSString *)string
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    return [userDefault objectForKey:string];
}

+(void)removeObjectForKey:(NSString *)string
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault removeObjectForKey:string];
    [userDefault synchronize];
    
    if (![userDefault objectForKey:string]) {
        NSLog(@"NSObject (FunctionAll):Remove Data for KEY:%@",string);
    }else NSLog(@"NSObject (FunctionAll):Can't Remove Data for KEY:%@",string);
}

+(void)showAllKey
{
    NSArray *keys = [[[[NSUserDefaults standardUserDefaults] dictionaryRepresentation] allKeys] copy];
    for(NSString *key in keys) {
        NSLog(@"Key Name: %@", key);
    }
}

@end
