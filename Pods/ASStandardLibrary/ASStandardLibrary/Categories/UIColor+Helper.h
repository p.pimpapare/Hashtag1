//
//  UIColor+Helper.h
//  ASStandardLibrary
//
//  Created by Akarapas Wongkaew on 1/12/2559 BE.
//  Copyright © 2559 Akarapas Wongkaew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor(Helper)

+(UIColor *)colorFromHexString:(NSString *)hexString;
+(UIColor *)colorFromHexString:(NSString *)hexString alpha:(float)alpha;

@end
