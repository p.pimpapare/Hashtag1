//
//  NSObject+VerifyData.m
//  AISService
//
//  Created by Yor on 1/24/15.
//  Copyright (c) 2015 Yor. All rights reserved.
//

#import "NSObject+VerifyData.h"

@implementation NSObject (VerifyData)

- (BOOL)isNullClass
{
    return [self isKindOfClass:[NSNull class]];
}

- (BOOL)isNSArrayClass
{
    return [self isKindOfClass:[NSArray class]];
}

- (BOOL)isNSDictionaryClass
{
    return [self isKindOfClass:[NSDictionary class]];
}

- (id)generateData:(id)value
{
    if (!value) {
        return nil;
    }
    
    if ([value isKindOfClass:[NSNull class]]) {
        return nil;
    }
    
    if ([value rangeOfString:@"null"].location != NSNotFound) {
        return nil;
    }
    
    return value;
    
}

- (id)generateStringData
{
    id string = (id)self;
    if (!string) {
        return nil;
    }
    
    if ([string isKindOfClass:[NSNull class]]) {
        return nil;
    }
    
    if ([string isKindOfClass:[NSString class]]) {
        if ([string rangeOfString:@"null"].location != NSNotFound) {
            return nil;
        }
    }
    
    return string;
}

@end
