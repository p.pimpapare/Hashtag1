//
//  UIView+UpdateAutoLayout.m
//  AISService
//
//  Created by cHinn on 9/8/2558 BE.
//  Copyright (c) 2558 Yor. All rights reserved.
//

#import "UIView+CNAutoLayout.h"

@implementation UIView (CNAutoLayout)

#pragma mark - Public Methods

- (void) updateConstraintWithConstant:(CGFloat)constant forAttribute:(NSLayoutAttribute)attribute
{
    NSLayoutConstraint * constraint = [self constraintForAttribute:attribute];
    if(constraint){
        [constraint setConstant:constant];
        [self layoutIfNeeded];
    }
}

- (CGFloat) getConstantforAttribute:(NSLayoutAttribute)attribute
{
    NSLayoutConstraint * constraint = [self constraintForAttribute:attribute];
    if (constraint) {
        return constraint.constant;
    }
    else {
        return -1;
    }
}

#pragma mark - Private Methods

- (NSLayoutConstraint*) constraintForAttribute:(NSLayoutAttribute)attribute
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"firstAttribute = %d && firstItem = %@", attribute, self];
    NSArray *fillteredArray = [[self.superview constraints] filteredArrayUsingPredicate:predicate];
    if(fillteredArray.count == 0)
    {
        NSArray *constraints = [self constraints];
        for(NSLayoutConstraint*constraint in constraints){
            if ([constraint firstAttribute] == attribute) {
                return constraint;
            }
        }
    }else
    {
        return fillteredArray.firstObject;
    }
    return nil;
}


@end
