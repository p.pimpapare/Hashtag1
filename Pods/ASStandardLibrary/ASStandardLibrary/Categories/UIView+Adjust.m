
//
//  UIView+Adjust.m
//  AISService
//
//  Created by Kittipop Puangjit on 12/20/2557 BE.
//  Copyright (c) 2557 Yor. All rights reserved.
//
#import "UIView+Adjust.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIView(Adjust)

-(void) addSubviewDocked:(UIView*)view {
    view.frame = self.bounds;
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self addSubview:view];
}

-(UIButton*) addTransparentOverlayButton; {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.backgroundColor = [UIColor clearColor];
    //button.layer.shouldRasterize = YES;
    [self addSubviewDocked:button];
    [self bringSubviewToFront:button];
    return button;
}

-(void) addTouchedUpInsideTarget:(id)target action:(SEL) action {
    // auto-enable user interaction
    self.userInteractionEnabled = YES;
    UIButton* button = [self addTransparentOverlayButton];
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
}

-(void) alignToSuperViewAtBottom {
    self.frameTop = CGRectBottomLeft(self.superview.bounds).y - self.frame.size.height;
}

-(void) alignToView:(UIView*)anchorView atBottomWithOffset:(CGFloat) offset {
    self.frameTop = anchorView.frameBottom + offset;
}

-(void) setFrameTop:(CGFloat)top {
    CGRect frameRect = self.frame;
    frameRect.origin.y = top;
    self.frame = frameRect;
}

-(void) setFrameBottom:(CGFloat)frameBottom {
    CGRect frameRect = self.frame;
    frameRect.origin.y = frameBottom - frameRect.size.height;
    self.frame = frameRect;
}

-(CGFloat) frameLeft {
    return self.frame.origin.x;
}

-(void) setFrameLeft:(CGFloat)left {
    //self.frame = CGRectMake2(CGPointMake(left, self.frameTop), self.frame.size);
    [self setFrameTopLeft:CGPointMake(left, self.frameTop)];
}

-(void) setFrameRight:(CGFloat)frameRight {
    CGRect frame = self.frame;
    frame.origin.x = frameRight - frame.size.width;
    self.frame = frame;
}

-(CGFloat) frameTop {
    return self.frame.origin.y;
}

-(CGFloat) frameBottom {
    return self.frameTop + self.frame.size.height;
}

-(CGFloat) frameRight {
    return self.frame.origin.x + self.frame.size.width;
}

-(CGPoint) frameTopLeft {
    return self.frame.origin;
}

-(void) setFrameTopLeft:(CGPoint) topLeft {
    self.frame = CGRectMake(topLeft.x, topLeft.y, self.frame.size.width, self.frame.size.height);
}

-(CGPoint) frameTopRight {
    return CGRectTopRight(self.frame);
}

-(CGPoint) frameBottomLeft {
    return CGRectBottomLeft(self.frame);
}

-(CGPoint) frameBottomRight {
    return CGRectBottomRight(self.frame);
}

-(CGFloat) frameWidth {
    return self.frame.size.width;
}

-(CGFloat) frameHeight {
    return self.frame.size.height;
}

-(void) setFrameWidth:(CGFloat)newWidth {
    CGRect r = self.frame;
    r.size.width = newWidth;
    self.frame = r;
}

-(void) setFrameHeight:(CGFloat)newHeight {
    CGRect r = self.frame;
    r.size.height = newHeight;
    self.frame = r;
}

@end
