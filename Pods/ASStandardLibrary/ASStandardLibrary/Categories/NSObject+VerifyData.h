//
//  NSObject+VerifyData.h
//  AISService
//
//  Created by Yor on 1/24/15.
//  Copyright (c) 2015 Yor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (VerifyData)

- (BOOL)isNullClass;
- (BOOL)isNSArrayClass;
- (BOOL)isNSDictionaryClass;
- (id)generateData:(id)value;
- (id)generateStringData;

@end
