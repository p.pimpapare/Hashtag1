//
//  NSObject+Helper.h
//  ASStandardLibrary
//
//  Created by Akarapas Wongkaew on 1/19/2559 BE.
//  Copyright © 2559 Akarapas Wongkaew. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifdef DEBUG
#   define NSLog if(1) NSLog
#else
#   define NSLog if(0) NSLog
#endif

#define DEVICE_H [UIScreen mainScreen].bounds.size.height
#define DEVICE_W [UIScreen mainScreen].bounds.size.width

#define KEYBOARD_H 216
#define KEYBOARD_H_IPAD 264

#define IS_IPAD ([[UIDevice currentDevice] userInterfaceIdiom]==UIUserInterfaceIdiomPad)

#define LOAD_IMAGE_FACEBOOK_WITH_ID(v) [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",v]
#define GET_VERSION_APP [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"]

// app delegate
#define APPDELEGATE (((AppDelegate*)[UIApplication sharedApplication].delegate))

// ETC
#define FRAME_OF_OBJECT(object) CGRectMake(object.frame.origin.x, object.frame.origin.y, object.frame.size.width, object.frame.size.height)
#define FRAME_OBJECT_Y(object) object.frame.origin.y+object.frame.size.height
#define FRAME_OBJECT_X(object) object.frame.origin.x+object.frame.size.width
#define ORIGIN_OBJECT_Y(object) object.frame.origin.y
#define ORIGIN_OBJECT_X(object) object.frame.origin.x
#define SET_FRAME_OBJECT_W(object,ww) [object setFrame:CGRectMake(object.frame.origin.x, object.frame.origin.y, ww, object.frame.size.height)]
#define SET_FRAME_OBJECT_H(object,hh) [object setFrame:CGRectMake(object.frame.origin.x, object.frame.origin.y, object.frame.size.width, hh)]
#define SET_ORIGIN_OBJECT_X(object,xx) [object setFrame:CGRectMake(xx, object.frame.origin.y, object.frame.size.width, object.frame.size.height)]
#define SET_ORIGIN_OBJECT_Y(object,yy) [object setFrame:CGRectMake(object.frame.origin.x, yy, object.frame.size.width, object.frame.size.height)]
#define SET_FRAME_OBJECT(object,xx,yy,ww,hh) [object setFrame:CGRectMake(xx, yy, ww, hh)]

// check version
#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

// ##############################################################################
#define CAT(a,b) a##b

#define defaultManagerName(class)  CAT(default,class)

//add this to .h
#define defaultManagerPrototype(class) +(instancetype)defaultManagerName(class)

//add this to .m
#define defaultManagerMethod(class)           \
defaultManagerPrototype(class) {                \
static class *sharedManager = nil;   \
static dispatch_once_t onceToken;               \
dispatch_once(&onceToken, ^{                        \
sharedManager = [[self alloc] init];                \
});                                                                         \
return sharedManager;                                           \
}

#define manager(class) [class defaultManagerName(class)]
// ##############################################################################

@interface NSObject(Helper)

+(BOOL)dictionaryHaveNullValue:(NSMutableDictionary *)dic;
+(void)keepObject:(id)Object forKey:(NSString *)string;
+(NSMutableDictionary *)clearNilValueInDictionary:(NSMutableDictionary *)dicData;
+(NSMutableArray *)clearNilValueInArray:(NSMutableArray *)arrayData;
+(id)getObjectForKey:(NSString *)string;
+(void)removeObjectForKey:(NSString *)string;
+(void)showAllKey;

@end
