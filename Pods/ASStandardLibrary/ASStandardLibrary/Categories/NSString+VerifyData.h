//
//  NSString+VerifyData.h
//  Pods
//
//  Created by cHinn on 10/12/2558 BE.
//
//

#import <Foundation/Foundation.h>

@interface NSString (VerifyData)
- (NSString *) generateMD5;
- (NSString *) stringByStrippingHTML;
- (NSString *) stringByStrippingNull;
- (BOOL)isEmptyString;
- (id)wrapHypen;
@end
