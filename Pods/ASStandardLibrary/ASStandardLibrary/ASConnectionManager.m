//
//  ASConnectionManager.m
//  ASStandardLibrary
//
//  Created by Akarapas Wongkaew on 1/19/2559 BE.
//  Copyright © 2559 Akarapas Wongkaew. All rights reserved.
//

#import "ASConnectionManager.h"

#import <AFNetworking/AFNetworking.h>

@implementation ASConnectionManager

+ (id)sharedManager
{
    static ASConnectionManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}

-(void)getConnectionWithURL:(NSString *)urlString ConnectionMethod:(ASConnectionMethod)method Parameters:(NSDictionary *)parameters Block:(ASConnection)connect Error:(ASErrorConnection)error
{
    
    ASConnection CallBackSuccess = connect;
    ASErrorConnection CallBackError = error;
    
    AFHTTPSessionManager *managers = [AFHTTPSessionManager manager];
    [managers setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [managers setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    [managers.requestSerializer setValue:@"" forHTTPHeaderField:@""];
    
    if (method==ASConnectionMethodGET)
    {
        [managers GET:urlString parameters:parameters progress:nil success:^(NSURLSessionDataTask *task, id responseObject)
        {
            CallBackSuccess(self,responseObject);
        } failure:^(NSURLSessionDataTask *operation, NSError *error){
            CallBackError(self,error);
        }];
    }
    else if(method==ASConnectionMethodPOST)
    {
        [managers POST:urlString parameters:parameters progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
            CallBackSuccess(self,responseObject);
        } failure:^(NSURLSessionDataTask *operation, NSError *error){

            CallBackError(self,error);
        }];
    }
    else if(method==ASConnectionMethodDELETE)
    {
        [managers DELETE:urlString parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            CallBackSuccess(self,responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            CallBackError(self,error);
        }];
    }
    else if(method==ASConnectionMethodPUT)
    {
        [managers PUT:urlString parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            CallBackSuccess(self,responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            CallBackError(self,error);
        }];
    }
}

-(void)getConnectionWithURL:(NSString *)urlString ConnectionMethod:(ASConnectionMethod)method Parameters:(NSDictionary *)parameters HeaderFields:(NSDictionary *)headerFields Block:(ASConnection)connect Error:(ASErrorConnection)error
{
    
    ASConnection CallBackSuccess = connect;
    ASErrorConnection CallBackError = error;
    
    AFHTTPSessionManager *managers = [AFHTTPSessionManager manager];
    [managers setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [managers setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    NSArray *arrayKeys = [NSArray arrayWithArray:(NSArray *)headerFields.allKeys];
    NSArray *arrayValues = [NSArray arrayWithArray:(NSArray *)headerFields.allValues];
    for (int i=0; i<[arrayKeys count]; i++) {
        [managers.requestSerializer setValue:arrayValues[i] forHTTPHeaderField:arrayKeys[i]];
    }
    
    if (method==ASConnectionMethodGET)
    {
        [managers GET:urlString parameters:parameters progress:nil success:^(NSURLSessionDataTask *task, id responseObject)
         {
             CallBackSuccess(self,responseObject);
         } failure:^(NSURLSessionDataTask *operation, NSError *error){
             CallBackError(self,error);
         }];
    }
    else if(method==ASConnectionMethodPOST)
    {
        [managers POST:urlString parameters:parameters progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
            CallBackSuccess(self,responseObject);
        } failure:^(NSURLSessionDataTask *operation, NSError *error){
            
            CallBackError(self,error);
        }];
    }
    else if(method==ASConnectionMethodDELETE)
    {
        [managers DELETE:urlString parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            CallBackSuccess(self,responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            CallBackError(self,error);
        }];
    }
    else if(method==ASConnectionMethodPUT)
    {
        [managers PUT:urlString parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            CallBackSuccess(self,responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            CallBackError(self,error);
        }];
    }
}

-(void)uploadImageWithURL:(NSString *)urlString ConnectionMethod:(ASConnectionMethod)method Parameters:(NSDictionary *)parameters ImageData:(NSData *)imageData ImageKey:(NSString *)imageKey ImageFileName:(NSString *)imageFileName Progress:(ASConnectionProgress)progress Block:(ASConnection)connect Error:(ASErrorConnection)error
{
    ASConnectionProgress CallBackProgress = progress;
    ASConnection CallBackSuccess = connect;
    ASErrorConnection CallBackError = error;
    
    // check image file name
    if (imageFileName.length!=0) {
        if ([imageFileName rangeOfString:@"."].location==NSNotFound) {
            imageFileName = [NSString stringWithFormat:@"%@.jpeg",imageFileName];
        }
    }else{
        NSDateFormatter *dateFormatter = [NSDateFormatter new];
        [dateFormatter setDateFormat:@"yyyy_MM_dd_HH_mm_ss"];
        imageFileName = [NSString stringWithFormat:@"uploadImage_%@.jpeg",[dateFormatter stringFromDate:[NSDate date]]];
    }
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:(method==ASConnectionMethodGET)?@"GET":(method==ASConnectionMethodPOST)?@"POST":(method==ASConnectionMethodDELETE)?@"DELETE":@"PUT" URLString:urlString parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        //[formData appendPartWithFileURL:[NSURL fileURLWithPath:@"file://path/to/image.jpg"] name:@"file" fileName:@"filename.jpg" mimeType:@"image/jpeg" error:nil];
        [formData appendPartWithFileData:imageData name:imageKey fileName:imageFileName mimeType:@"image/jpeg"];
    } error:nil];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      // This is not called back on the main queue.
                      // You are responsible for dispatching to the main queue for UI updates
                      dispatch_async(dispatch_get_main_queue(), ^{
                          //Update the progress view
                          //[progressView setProgress:uploadProgress.fractionCompleted];
                          CallBackProgress(uploadProgress.fractionCompleted);
                      });
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      if (error) {
                          NSLog(@"Error: %@", error);
                          
                          CallBackError(self,error);
                      } else {
                          NSLog(@"%@ %@", response, responseObject);
                          
                          CallBackSuccess(self,responseObject);
                      }
                  }];
    
    [uploadTask resume];
}

@end
