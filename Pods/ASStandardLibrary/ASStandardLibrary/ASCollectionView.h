//
//  ASCollectionView.h
//  ASStandardLibrary
//
//  Created by Akarapas Wongkaew on 4/11/2559 BE.
//  Copyright © 2559 Akarapas Wongkaew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ASCollectionView : UICollectionView

@property (strong, nonatomic) UIRefreshControl *refreshCtrl;
-(void)setTextEmptyState:(NSString *)text;
-(void)addRefreshCtrlWithTarget:(id)target Action:(SEL)action;

@end
