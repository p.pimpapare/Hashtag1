//
//  ASAlertView.h
//  ASStandardLibrary
//
//  Created by Akarapas Wongkaew on 1/19/2559 BE.
//  Copyright © 2559 Akarapas Wongkaew. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ASAlert : NSObject

+(void)showAlertWithTitle:(NSString * _Nullable)title Message:( NSString * _Nullable)msg leftButtonTitle:(NSString * _Nullable)cancelTextTitle rightButtonTitle:(NSString * _Nullable)okTextTitle TargetViewShow:(id _Nullable)target_view_show;

+(void)showAlertWithTitle:(NSString * _Nullable)title Message:( NSString * _Nullable)msg leftButtonTitle:(NSString * _Nullable)leftTextTitle rightButtonTitle:(NSString * _Nullable)rightTextTitle actionTarget:(id _Nonnull)target actionInLeftButton:(SEL _Nullable)actionLeft actionInRightButton:(SEL _Nullable)actionRight TargetViewShow:(id _Nullable)target_view_show;

+(void)showAlertWithTitle:(NSString * _Nullable)title Message:( NSString * _Nullable)msg ButtonsTitle:(NSArray <NSString *> * _Nonnull)arrayButton Target:(id)target Block:(void (^ __nullable)(int buttonIndex))handler;

+(void)showAlertInputWithTitle:(NSString * _Nullable)title Message:( NSString * _Nullable)msg TextfieldsPlaceHolder:(NSArray <NSString *> * _Nonnull)arrayTextfileds ButtonsTitle:(NSArray <NSString *> * _Nonnull)arrayButton Target:(id)target Block:(void (^ __nullable)(int buttonIndex, NSArray *arrayTextFields))handler;

@end