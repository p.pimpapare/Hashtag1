//
//  UsedCuponSuccess.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/2/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "UsedCuponSuccess.h"

@implementation UsedCuponSuccess

-(void)awakeFromNib{
    
    [super awakeFromNib];
    [self setFrame:CGRectMake(DEVICE_WIDTH/7, DEVICE_HEIGHT/2-self.frame.size.height/2,DEVICE_WIDTH-DEVICE_WIDTH/3.5,self.frame.size.height)];

    [self.textDetails setNumberOfLines:3];
//    [self.textDetails setText:@"ดำเนินการเรียบร้อยแล้ว !! \nท่านสามารถเรียกดูคูปอง \nได้ที่เมนูคูปองของฉัน"];
}

-(void)setlogoCoupon:(NSString *)logoImage{
    
    [self.logo sd_setImageWithURL:[NSURL URLWithString:logoImage]
                 placeholderImage:[UIImage imageNamed:@"picture_1_2"]options:SDWebImageProgressiveDownload];
}

-(void)setTextAlertView:(NSString *)text{
    
    [self.textDetails setText:[NSString stringWithFormat:@"%@",text]];
}

@end
