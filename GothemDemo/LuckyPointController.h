//
//  LuckyPointController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/31/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

// This class is used for displaying lucky draw information

#import <UIKit/UIKit.h>

@interface LuckyPointController : BaseViewController

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (weak, nonatomic) IBOutlet UIView *menuHeader;
@property (weak, nonatomic) IBOutlet UIView *statusBar;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingView;

@end
