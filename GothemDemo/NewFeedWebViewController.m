//
//  NewFeedWebViewController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/8/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "NewFeedWebViewController.h"

@interface NewFeedWebViewController ()

@end

@implementation NewFeedWebViewController

-(void)viewWillAppear:(BOOL)animated{
    GOOGLE_ANALYTICS(@"NewFeed_WebController");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];

    [self.loadingView startAnimating];
    [self class_UI];
}

-(void)class_UI{
    [self.webView setScalesPageToFit:YES];
    LINE_WITH_STATUSBAR_HEADER_NORMAL;
}

- (IBAction)backBtn:(id)sender {
    [self didMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

///// WEB_VIEW

-(void)didReceiveMemoryWarning{
    
    [super didReceiveMemoryWarning];
}

- (void)setView:(UIView *)view{
    
    if (view == nil){
        [self releaseSubviews];
    }
    
    [super setView:view];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    if (!webView.isLoading) {
        
        [self.loadingView stopAnimating];
        [self.loadingView setHidden:YES];
    }
}

-(void)openUrl:(NSString *)urlText{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",urlText]];
    NSURLRequest *req = [NSURLRequest requestWithURL:url];
    
    [self.webView loadRequest:req];
}

-(void)releaseSubviews{
    
    [self.webView setDelegate:nil];
    [self.webView stopLoading];
}

- (void)dealloc{
    
    [self releaseSubviews];
}

- (void)viewWillDisappear:(BOOL)animated{
    
    [self releaseSubviews];
}
///// END OF WEB_VIEW

@end
