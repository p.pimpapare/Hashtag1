//
//  SequeSettingQAController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/7/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

//  This class is base class of XLPagerTabStrip.

#import <UIKit/UIKit.h>

@interface SequeSettingQAController : BaseViewController

@property (weak, nonatomic) IBOutlet UILabel *titleText;
@property (weak, nonatomic) IBOutlet UILabel *backBtn;

@property (weak, nonatomic) IBOutlet UIView *statusBar;
@property (weak, nonatomic) IBOutlet UIImageView *lineBottom;
@property (weak, nonatomic) IBOutlet UIImageView *lineHeader;
@property (weak, nonatomic) IBOutlet UIView *menuHeader;

-(void)setTiltleHeader:(NSString *)title;
@end
