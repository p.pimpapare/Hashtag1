//
//  UserHistoryTableView.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/2/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "UserHistoryTableView.h"

@interface UserHistoryTableView (){
        
    MenuDetailInputCode *menuInputCode;
    MenuCouponDetail_Alert *menuAlert;
    
    MenuCuponDetailCode *menuCouponUsedCode; // barcode
}

@end

@implementation UserHistoryTableView

-(void)viewWillAppear:(BOOL)animated{
    
    GOOGLE_ANALYTICS(@"UserHistoryTableView");
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self.loadingView setHidden:NO];
    [self.loadingView startAnimating];
    
    offset_int = 0;
    
    [self set_initail_objects];
}

-(void)getHistoryUsedPoint:(NSString *)token{
    
    int type = 1;
    
    [Service get_user_use_point_history:token offset:offset_int type:type selfID:self block:^(id result) {
        
        uDateTime = [[NSMutableArray alloc]init];
        uPoint = [[NSMutableArray alloc]init];
        uName = [[NSMutableArray alloc]init];

        NSString *result_text = [NSString stringWithFormat:@"%@",result];
        
        if (![result_text isEqualToString:@"error"]) {
            
            for (int i=0; i<[result[@"history_use"] count];i++) {
                [uDateTime addObject:result[@"history_use"][i][@"created_at"]];
                [uPoint addObject:result[@"history_use"][i][@"usepoint"]];
                [uName addObject:result[@"history_use"][i][@"title"]];
            }
            finisedLoadingUsedPoint = YES;
            
            [self.loadingView stopAnimating];
            [self.loadingView setHidden:YES];
            
            offset_int = (int)[result[@"history_use"] count];

            [self.tableView reloadData];
        }
    }];
}

-(void)getHistorymyCoupon:(NSString *)token{
    
    // get myCupon
    [Service get_my_coupon:token selfID:self block:^(id result) {
        
        myCouponID = [[NSMutableArray alloc]init];
        myCouponTitle = [[NSMutableArray alloc]init];
        myCouponSubTitle = [[NSMutableArray alloc]init];
        myCouponImage = [[NSMutableArray alloc]init];
        myCouponDetails = [[NSMutableArray alloc]init];
        myCouponCounterTime = [[NSMutableArray alloc]init];
        myCouponUsePoint = [[NSMutableArray alloc]init];
        myCouponShareUrl = [[NSMutableArray alloc]init];
        myCouponBrandLogo = [[NSMutableArray alloc]init];
        myCouponCpuse_id = [[NSMutableArray alloc]init];
        
        NSString *result_text = [NSString stringWithFormat:@"%@",result];
        
        if (![result_text isEqualToString:@"error"]) {
            
            for (int i=0; i<[result[@"coupon"] count];i++) {
                
                [myCouponID addObject:result[@"coupon"][i][@"coupon_id"]];
                [myCouponTitle addObject:result[@"coupon"][i][@"coupon_title"]];
                [myCouponSubTitle addObject:result[@"coupon"][i][@"coupon_subtitle"]];
                
                [myCouponImage addObject:result[@"coupon"][i][@"coupon_img"]];
                [myCouponDetails addObject:result[@"coupon"][i][@"coupon_desc"]];
                [myCouponCounterTime addObject:result[@"coupon"][i][@"expire_time"]];
                [myCouponUsePoint addObject:result[@"coupon"][i][@"use_point"]];
                
                [myCouponBrandLogo addObject:result[@"coupon"][i][@"brand_logo"]];
                [myCouponCpuse_id addObject:result[@"coupon"][i][@"cpuse_id"]];
            }
            finisedLoadingMyPoint = YES;
            
            [self.loadingView setHidden:YES];
            [self.loadingView stopAnimating];
            
            [self.tableView reloadData];
        }
    }];
}

// set tableView
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectedRow = (int)indexPath.row;
    
    if (self.menu==2) {
        
        [self addPCBlack:self];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                       initWithTarget:self
                                       action:@selector(closeDetailView)];
        
        [pcblack addGestureRecognizer:tap];
        [self addMenuAlert:myCouponBrandLogo[selectedRow]];
    }
}

-(void)addMenuAlert:(NSString *)logoimage{
    
    menuAlert = [self loadNibWithName:@"MenuCouponDetail_Alert"];
    
    [menuAlert set_logo_coupon:logoimage];
    
    [menuAlert.usedCouponBtn addTarget:self action:@selector(usedCoupon) forControlEvents:UIControlEventTouchUpInside];
    [menuAlert.storeCouponBtn addTarget:self action:@selector(storeCoupon) forControlEvents:UIControlEventTouchUpInside];
    [menuAlert.cancelBtn addTarget:self action:@selector(closeDetailView) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:menuAlert];
    
    [menuAlert setAlpha:0];
    
    [PCView setViewAnimationWithDuration:menuAlert time_duration:0.4 showView:YES];
}

-(void)closeDetailView{
    [self setHiddenPCBlack:YES];
    [pcblack setUserInteractionEnabled:YES];
    [self.viewError setHidden:YES];
    [menuAlert removeFromSuperview];
    [menuInputCode removeFromSuperview];
    [menuCouponUsedCode removeFromSuperview];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.menu==1) {
        
        if ([uName count]==0) {
            
            if(finisedLoadingUsedPoint==YES){
                [self.warningBlank setHidden:NO];
            }
            return 0;
            
        }else{
            
            return [uName count];}
    }else{
        
        if([myCouponTitle count]==0){
            
            if(finisedLoadingMyPoint==YES){
                
                [self.warningBlank setHidden:NO];
                [self.tableView setHidden:YES];
                
            }return 0;
            
        }else{
            return [myCouponTitle count];
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    History_tableCell *cell = (History_tableCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    MenuCoupon_TableViewCell *cell2 = (MenuCoupon_TableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if((self.menu==1)||(self.menu==3)){
        
        if(cell == nil) {
            cell = [self loadNibWithName:@"History_tableCell"];
        }
        
        if(self.menu==1){
            
            [self splitDateTime:uDateTime[indexPath.row]];
            
            [cell setCell:[NSString stringWithFormat:@"%@",[dateWithTime objectAtIndex:0]] time:[NSString stringWithFormat:@"%@",[dateWithTime objectAtIndex:1]] point:[NSString stringWithFormat:@"รับ %@ คะแนน",uPoint[indexPath.row]] display:uName[indexPath.row] usedCoupon:YES];
        }
        return cell;
    }else{
        
        if(cell2 == nil) {
            cell2 = [self loadNibWithName:@"MenuCoupon_TableViewCell"];
        }
        [cell2 setUserCoupon:myCouponTitle[indexPath.row] detail:myCouponDetails[indexPath.row] time:myCouponCounterTime[indexPath.row] image:myCouponImage[indexPath.row]];
        return cell2;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(self.menu==1){
        return [[self loadNibWithName:@"History_tableCell"] frame].size.height;
    }else{
        return [[self loadNibWithName:@"MenuCoupon_TableViewCell"] frame].size.height;
    }
}

-(void)set_initail_objects{
    
    finisedLoadingUsedPoint = NO;
    finisedLoadingMyPoint = NO;
    
    [self class_UI];
}

-(void)class_UI{
    
    LINE_WITH_STATUSBAR_HEADER_NORMAL;
    TABLEVIEW_COLLECTION_REFRESH(self.tableView);
    
    [BaseViewController setLineColor:self.lineB bottom:self.lineH];
    [self.tableView setSeparatorColor:[UIColor clearColor]];
}

- (void)refresh:(UIRefreshControl *)refreshControl {
    
    if(self.menu==1) {
        [self getHistoryUsedPoint:[self getUserToken]];
    }else if(self.menu==2){
        [self getHistorymyCoupon:[self getUserToken]];
    }
    [refreshControl endRefreshing];
}

-(void)storeCoupon{
    [self closeDetailView];
}

-(void)usedCoupon{
    
    [menuAlert removeFromSuperview];
    [self addUsedCouponView:myCouponBrandLogo[selectedRow]];
}

-(void)goToReadmore{
    
    [self goToReadMore:myCouponID[selectedRow] title:myCouponTitle[selectedRow] detail:myCouponDetails[selectedRow]point:myCouponUsePoint[selectedRow]logo:myCouponBrandLogo[selectedRow]];
}

-(void)addUsedCouponView:(NSString *)logoImage{
    
    menuInputCode = [[NSBundle mainBundle] loadNibNamed:@"MenuDetailInputCode" owner:self options:nil][0];
    
    [menuInputCode set_logo_coupon:logoImage];
    
    [self.view addSubview:menuInputCode];
    
    // Animation
    [menuInputCode setAlpha:0];
    
    [PCView setViewAnimationWithDuration:menuInputCode time_duration:0.4 showView:YES];
}

-(void)splitCodeText:(NSString *)text{
    
    NSArray *split = [text componentsSeparatedByString:@"Code "];
    NSString *codeText = [split objectAtIndex:1];
    //    NSLog(@"%@",codeText);
    
    id passwordEmail; // checking user password
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if (userDefaults){
        passwordEmail = (id)[userDefaults objectForKey:@"PasswordEmail"];
    }
    
    [menuAlert removeFromSuperview];
    [menuInputCode removeFromSuperview];
    
    if ([codeText isEqualToString:[NSString stringWithFormat:@"%@",passwordEmail]]) { // password correct
        [self postService_useMyCoupon];
    }else{
        [self.viewError setHidden:NO];
        [self.view insertSubview:self.viewError aboveSubview:pcblack];
        [self.imageLogo sd_setImageWithURL:[NSURL URLWithString:myCouponBrandLogo[selectedRow]]
                          placeholderImage:[UIImage imageNamed:@" "]options:SDWebImageProgressiveDownload];
    }
}

-(void)postService_useMyCoupon{
    
    [Service post_user_use_my_coupon:[self getUserToken] myCouponID:myCouponCpuse_id[selectedRow] selfID:self block:^(id result) {
        
        NSString *resultText = [NSString stringWithFormat:@"%@",result];
        if ([resultText isEqualToString:@"error"]) {
            
//            ALERT_MESSAGE_NETWORK( ,GOTO_PREVIOUS)
            
        }else{
            NSString *successStatus = [NSString stringWithFormat:@"%@",result[@"success"]];
            
            if ([successStatus isEqualToString:@"1"]) {
                
                NSString *duration = [NSString stringWithFormat:@"%@",result[@"duration"]];
                NSString *code = [NSString stringWithFormat:@"%@",result[@"code"]];
                
                [pcblack setBg_color:true alpha:1.0];
                
                menuCouponUsedCode = [[NSBundle mainBundle] loadNibNamed:@"MenuCuponDetailCode" owner:self options:nil][0];
                
                [menuCouponUsedCode setMenuCoupon:myCouponTitle[selectedRow] detail:myCouponDetails[selectedRow] time:duration code:code];
                
                [menuCouponUsedCode.usedCodeBtn addTarget:self action:@selector(closeDetailView) forControlEvents:UIControlEventTouchUpInside];
                [menuCouponUsedCode.readmoreBtn addTarget:self action:@selector(goToReadmore) forControlEvents:UIControlEventTouchUpInside];
                [menuCouponUsedCode.shareBtn addTarget:self action:@selector(goToShare) forControlEvents:UIControlEventTouchUpInside];
                
                [self.view addSubview:menuCouponUsedCode];
                
                [menuCouponUsedCode setAlpha:0];
                
                [PCView setViewAnimationWithDuration:menuCouponUsedCode time_duration:0.4 showView:YES];
            }
        }
    }];
}

-(void)goToShare{

    [self shareInfo:myCouponID[selectedRow]];
}

-(void)shareInfo:(NSString *)coupon_ID{
    
    [Service getShareUrl:[self getUserToken] couponID:coupon_ID selfID:self block:^(id result) {
        
        [self share_activity:[NSString stringWithFormat:@"%@",result[@"url"]]];
  
    }];
}

- (IBAction)backBtn:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)setHeaderText:(NSString *)title menuNum:(int)num{
    
    self.menu = num;
    
    [self.titlteHeader setText:[NSString stringWithFormat:@"%@",title]];
    
    if(num==1) {
        [self getHistoryUsedPoint:[self getUserToken]];
    }else if(num==2){
        [self getHistorymyCoupon:[self getUserToken]];
    }
}

-(NSArray *)sortMonth{
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    NSArray *infoDate = [df monthSymbols];
    
    return infoDate;
}

-(NSString *)sortYear{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    NSString *yearString = [formatter stringFromDate:[NSDate date]];
    
    return yearString;
}

-(NSArray *)splitDateTime:(NSString *)dateTime{
    
    NSArray *splitToArray = [dateTime componentsSeparatedByString:@" "];
    NSString *date = [splitToArray objectAtIndex:0];
    NSString *time = [splitToArray objectAtIndex:1];
    
    NSArray *array_time = [time componentsSeparatedByString:@":"];
    NSString *hour = [array_time objectAtIndex:0];
    NSString *minute = [array_time objectAtIndex:1];
    
    NSString *new_time = [NSString stringWithFormat:@"%@:%@",hour,minute];
    
    dateWithTime = [NSArray arrayWithObjects:date,new_time, nil];
    return dateWithTime;
}

-(void)goToReadMore:(NSString *)coupon_ID title:(NSString *)couponReadMoreTitle detail:(NSString *)couponReadModeDetails point:(NSString *)couponPoint logo:(NSString *)couponLogo{
    
    ReadMoreController *readmore = [[NSBundle mainBundle] loadNibNamed:@"ReadMoreController" owner:self options:nil][0];
    [readmore setDetailReadmore:coupon_ID title:couponReadMoreTitle point:couponPoint logo:couponLogo];
    
    [self presentViewController:readmore animated:YES completion:nil];
}

-(void)goToUsedPoint{
    [self closeDetailView];
    [self getHistorymyCoupon:[self getUserToken]];
}

@end
