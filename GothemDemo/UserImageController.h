//
//  UserImageController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 5/10/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageStickerView.h"

@interface UserImageController : UIViewController<UIActionSheetDelegate>

@property (weak, nonatomic) IBOutlet UIView *statusBar;
@property (weak, nonatomic) IBOutlet UIView *menuHeader;

@property (weak, nonatomic) IBOutlet UIImageView *userImage1;
@property (weak, nonatomic) IBOutlet UIImageView *userImage2;
@property (weak, nonatomic) IBOutlet UIImageView *userImage3;

@property (weak, nonatomic) IBOutlet UIView *view1;
@property (weak, nonatomic) IBOutlet UIView *view2;
@property (weak, nonatomic) IBOutlet UIView *view3;

@property (weak, nonatomic) IBOutlet UIButton *btn_uploadImage1;
@property (weak, nonatomic) IBOutlet UIButton *btn_uploadImage2;
@property (weak, nonatomic) IBOutlet UIButton *btn_uploadImage3;

@property (weak, nonatomic) IBOutlet UIView *view_for_capture_image;

@property (retain, nonatomic) ImageStickerView *image_capture;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthImage1;

@end
