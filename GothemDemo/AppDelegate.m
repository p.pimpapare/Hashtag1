//
//  AppDelegate.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/19/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginController.h"
#import <sys/sysctl.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <Google/Analytics.h>
#import "Branch.h"
#import "BaseViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>

#include <objc/runtime.h>

@interface AppDelegate ()
@end

@import Firebase;

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    /*
     *  This method will be called when the application launch (closed and open it agians)
     */
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Selected_Back"];

    [UIApplication sharedApplication].applicationIconBadgeNumber = 0; // number of push noti
    
    // ######### Push noti Firebase
    // iOS 8 or later
    
    UIUserNotificationType allNotificationTypes =
    (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
    UIUserNotificationSettings *settings =
    [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
    [FIRApp configure];
    
    // ######### End of Firebase
    
    // test get existing application on device
    /*
     Class LSApplicationWorkspace_class = objc_getClass("LSApplicationWorkspace");
     NSObject* workspace = [LSApplicationWorkspace_class performSelector:@selector(defaultWorkspace)];
     [BaseViewController log_comment:@"apps: " text:[workspace performSelector:@selector(allApplications)]];
     */
    
    // ######### Add branch_io
    Branch *branch = [Branch getInstance];
    // #########
    
    // #########
    NSString *deviceName = [NSString stringWithFormat:@"%@",[[UIDevice currentDevice] name]];
    [branch setIdentity:deviceName];
    [branch initSessionWithLaunchOptions:launchOptions andRegisterDeepLinkHandler:^(NSDictionary *params, NSError *error) {
        // params are the deep linked params associated with the link that the user clicked before showing up.
        
        [BaseViewController log_comment:@"deep link data:" text:[params description]]; // log comment
        
        NSString *ref_code = [NSString stringWithFormat:@"%@",params[@"ref"]];
        NSString *referrer_code = [NSString stringWithFormat:@"%@",params[@"referrer"]];
        
        if (params[@"ref"] == nil && params[@"referrer"] != nil)
        {
            SET_USER_DEFAULT_INFO(referrer_code,@"refcode");
            
        }else if (params[@"ref"] == nil && params[@"referrer"] == nil)
        {
            SET_USER_DEFAULT_INFO(@"0",@"refcode");
            
        }else
        {
            SET_USER_DEFAULT_INFO(ref_code,@"refcode");
        }
        
        if (params[@"utm_source"]&&params[@"utm_medium"]&&params[@"utm_campaign"]) {
            
            // Assumes at least one tracker has already been initialized.
            
            id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-75515558-1"];
            tracker.allowIDFACollection = YES;
            
            //[tracker setAllowIDFACollection:YES];
            [[GAI sharedInstance].logger setLogLevel:kGAILogLevelVerbose];
            
            NSDictionary *campaignData = [NSDictionary dictionaryWithObjectsAndKeys:
                                          params[@"utm_source"], kGAICampaignSource,
                                          params[@"utm_medium"], kGAICampaignMedium,
                                          params[@"utm_campaign"], kGAICampaignName, nil];
            
            [tracker send:[[[GAIDictionaryBuilder createScreenView] setAll:campaignData] build]];
        }
    }];
    
    // ######### Local notification
    [launchOptions valueForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    // #########
    
    // ######### Add fabric
    [Fabric with:@[[Crashlytics class]]];
    // #########
    
    // ######### Add Google Analytics
    // Configure tracker from GoogleService-Info.plist.
    NSError *configureError;
    [[GGLContext sharedInstance] configureWithError:&configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    // Optional: configure GAI options.
    GAI *gai = [GAI sharedInstance];
    gai.trackUncaughtExceptions = YES;  // report uncaught exceptions
    gai.logger.logLevel = kGAILogLevelVerbose;  // remove before app release
    // ######### End of adding Google Analytic
    
    /*
     /// ######### Push noti
     // Register for Remote Notifications (Push Notification)
     if([[UIApplication sharedApplication] respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
     {
     [[UIApplication sharedApplication] registerUserNotificationSettings:  [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeAlert | UIUserNotificationTypeSound) categories:nil]];
     [[UIApplication sharedApplication] registerForRemoteNotifications];
     }
     else
     {
     [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIUserNotificationTypeAlert | UIUserNotificationTypeBadge |    UIUserNotificationTypeSound)];
     }
     ////// End regis push noti
     */
    
    GET_USER_DEFAULT_INFO(@"Device_info")
    
    if (userInfo == nil) {
        
        // ######### Finding device info
        NSString *mac_address;
        NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
        
        NSString *udid = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        
        float ver = [[[UIDevice currentDevice] systemVersion] floatValue];  // Finding os version
        
        /*    if (ver >= 8.0) {
         // Only executes on version 8 or above.
         }
         */
        
        InitAddresses();
        GetIPAddresses();
        GetHWAddresses(); // Finding mac address, IP
        
        int i;
        for (i=0; i<MAXADDRS; ++i){
            
            static unsigned long localHost = 0x7F000001; // 127.0.0.1
            unsigned long theAddr;
            
            theAddr = ip_addrs[i];
            
            if (theAddr == 0) break;
            if (theAddr == localHost) continue;
            
            [BaseViewController log_comment:@"Name" text:[NSString stringWithFormat:@"%s",if_names[i]]];
            [BaseViewController log_comment:@"MAC" text:[NSString stringWithFormat:@"%s",hw_addrs[i]]];
            [BaseViewController log_comment:@"IP" text:[NSString stringWithFormat:@"%s",ip_names[i]]];
            
            if (strncmp(if_names[i], "en", 2) == 0){
                [BaseViewController log_comment:@"Adapter en has a IP of" text:[NSString stringWithFormat:@"%s",ip_names[i]]];
            }
            mac_address = [NSString stringWithFormat:@"%s",hw_addrs[1]]; // set เอง
        }
        
        // get device type
        size_t size;
        sysctlbyname("hw.machine", NULL, &size, NULL, 0); // if error-> #import <sys/sysctl.h>
        char *machine = malloc(size);
        sysctlbyname("hw.machine", machine, &size, NULL, 0);
        NSString *platform = [NSString stringWithCString:machine encoding:NSUTF8StringEncoding];
        [BaseViewController log_comment:@"iPhone Device" text:[NSString stringWithFormat:@"%@",[self platformType:platform]]];
        
        NSString *deviceType = [NSString stringWithFormat:@"%@",[self platformType:platform]];
        
        free(machine);
        
        // set device information to nsuserdefualts
        self.deviceInfo = [[NSMutableDictionary alloc]init];
        [self.deviceInfo setObject:[NSString stringWithFormat:@"%@",version] forKey:@"version"];
        [self.deviceInfo setObject:[NSString stringWithFormat:@"%@",mac_address] forKey:@"mac"];
        [self.deviceInfo setObject:@"ios" forKey:@"os-type"];
        [self.deviceInfo setObject:[NSString stringWithFormat:@"%.2f",ver] forKey:@"os-version"];
        [self.deviceInfo setObject:[NSString stringWithFormat:@"%@",udid] forKey:@"UDID"];
        [self.deviceInfo setObject:[NSString stringWithFormat:@"%@",deviceType] forKey:@"ios_type"];
        
        [BaseViewController log_comment:@"Device info" text:[NSString stringWithFormat:@"%@",self.deviceInfo]];
        
        NSUserDefaults *deviceDefaults = [NSUserDefaults standardUserDefaults];
        if (deviceDefaults){
            [deviceDefaults setObject:self.deviceInfo forKey:@"Device_info"];
        }
    }
    // ######### Firebase
    // Add observer for InstanceID token refresh callback.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:) name:kFIRInstanceIDTokenRefreshNotification object:nil];
    // #########
    
    // Facebook SDK
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    return YES;
}

// (Branch io)
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    [[Branch getInstance] handleDeepLink:url];
    
    NSString *recheck_url = [NSString stringWithFormat:@"%@",url];
    NSString *split_url = [recheck_url substringWithRange:NSMakeRange(0,2)];

    if ([split_url isEqualToString:@"fb"]) {
        
        // Facebook SDK
        return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                              openURL:url
                                                    sourceApplication:sourceApplication
                                                           annotation:annotation];
    }else{
        return YES;
    }
}

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray *))restorationHandler {
    [[Branch getInstance] continueUserActivity:userActivity];
    return YES;
}

// (Push Notification)

- (void)tokenRefreshNotification:(NSNotification *)notification {
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    [BaseViewController log_comment:@"refreshedToken token: " text:refreshedToken]; // log comment
    
    if (refreshedToken != nil) {
        
        NSUserDefaults *deviceInfo = [NSUserDefaults standardUserDefaults];
        
        if (deviceInfo){
            [deviceInfo setObject:refreshedToken forKey:@"deviceToken"];
        }
    }
    
    GET_USER_DEFAULT_INFO(@"deviceToken");
    [BaseViewController log_comment:@"test device token " text:userInfo]; // log comment
    
    // Connect to FCM since connection may have failed when attempted before having a token.
    //        [self connectToFcm];
    // TODO: If necessary send token to application server.
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    [BaseViewController log_comment:@"Registered for notification device token: " text:(id)deviceToken]; // log comment
    
    [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeUnknown];
    
    [self connectToFcm];
    
    //    const void *devTokenBytes = [deviceToken bytes];
    /*
     NSString* newDeviceToken = [[[[deviceToken description]
     
     stringByReplacingOccurrencesOfString: @"<" withString: @""]
     
     stringByReplacingOccurrencesOfString: @">" withString: @""]
     
     stringByReplacingOccurrencesOfString: @" " withString: @""];
     
     NSUserDefaults *deviceInfo = [NSUserDefaults standardUserDefaults];
     
     if (deviceInfo){
     [deviceInfo setObject:newDeviceToken forKey:@"deviceToken"];
     }
    [BaseViewController log_comment:@"Device token" text:newDeviceToken];     
     */
}

- (void)connectToFcm {
    
    NSLog(@"Connecting to FCM...");
    
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Unable to connect to FCM. %@", error);
        } else {
            NSLog(@"Connected to FCM.");
        }
    }];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    
    [BaseViewController log_comment:@"Did Fail to Register for Remote Notifications" text:[NSString stringWithFormat:@"%@",error]];
    [BaseViewController log_comment:@"Error description" text:error.localizedDescription];
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    
    /*    //// Firebase
     [BaseViewController log_comment:@"Message ID:" text:[NSString stringWithFormat:@"%@",userInfo[@"gcm.message_id"]]];
     
     // Pring full message.
    [BaseViewController log_comment:@"Full message " text:[NSString stringWithFormat:@"%@",userInfo]];
     */
    
    NSString *alertMsg;
    
    if (userInfo[@"type"] == nil) {
        alertMsg = [NSString stringWithFormat:@"%@",userInfo[@"aps"][@"type"]];
    }else{
        alertMsg = [NSString stringWithFormat:@"%@",userInfo[@"type"]];
    }
    
//    [BaseViewController log_comment:@"alertMsg " text:alertMsg]; // log comment
    [BaseViewController log_comment:@"Full message " text:[NSString stringWithFormat:@"%@",userInfo]];

    //// End of Firebase
    
    /* มี type, message, title มาให้
     ในตัวแปร type อาจจะไม่มีค่า ถ้าเป็นงี้ ให้ defualt คือ เอา message มาใช้
     1 ถ้าเป็น message ก็แค่เข้าแอพปกติ
     2 type - inquiry (คือเวลาที่มีการตอบคำถามจาก admin) ให้เด้งไปหน้า details
     3 type - update_package คือ เวลามีการอัพเดทใหม่ ให้เด้งไป LaunchScreenController
     4 type - logout (คือเวลามีการ login ด้วย id ที่ซ้ำ) อาจมีข้อความเตือน user ให้หน่อย หลังจากนั้นให้เด้ง แอพออก หรือ ไปหน้า login ใหม่
     */
    
    if ([alertMsg isEqualToString:@"inquiry"]) {
        tag = 1;
        NSString *inquiry_id = [NSString stringWithFormat:@"%@",userInfo[@"id"]];
        
        //        NSLog(@"inquiry_id %@",inquiry_id);
        
        [self displaAlert_pushNoti:@"อัพเดท" message:@"ได้รับการตอบกลับ" inquiry_id:inquiry_id];
        // ไปหน้า inquiry โดยส่ง id ไปด้วย เพื่อ get details
    }else if ([alertMsg isEqualToString:@"update_package"]){
        tag = 2;
        [self displaAlert_pushNoti:@"อัพเดท" message:@"มีการอัพเดทใหม่" inquiry_id:@" "];
        // ไปหน้า before access
    }else if ([alertMsg isEqualToString:@"logout"]){
        tag = 3;
        // display alert message and redirect to login scene
        [self displaAlert_pushNoti:@"อัพเดท" message:@"มีการล็อคอินซ้ำ" inquiry_id:@" "];
    }
}

-(void)displaAlert_pushNoti:(NSString *)title message:(NSString *)message inquiry_id:(NSString *)inquiry_id{
    
    
    NSString *settingInfo;
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if (userDefaults){
        settingInfo = [NSString stringWithFormat:@"%@",(id)[userDefaults objectForKey:@"alarm_setting"]];
    }
    
    if ([settingInfo isEqualToString:@"selectedBtn"]) {
        AudioServicesPlaySystemSound (1352); //works ALWAYS as of this post
    }
    
    UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"ยกเลิก"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action){
                                   
                               }];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"ตกลง"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action){
                                    
                                    if (tag==1) { // inquiry
                                        
                                        UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
                                        HelpHistoryController *view_class = [[NSBundle mainBundle] loadNibNamed:@"HelpHistoryController" owner:self options:nil][0];
                                        [view_class problemId:inquiry_id];
                                        [navigationController pushViewController:view_class animated:YES];
                                        
                                    }else if (tag==2){ // update_package
                                        
                                        LaunchScreenController *view_class = (LaunchScreenController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"LaunchScreenController"];
                                        [navigationController pushViewController:view_class animated:YES];
                                        
                                    }else if (tag==3){ // logout
                                        
                                        UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
                                        LoginController *view_class = [[NSBundle mainBundle] loadNibNamed:@"LoginController" owner:self options:nil][0];
                                        [navigationController pushViewController:view_class animated:YES];

                                    /*    LoginController *logincontroller = (LoginController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"LoginController"];
                                     
                                        [navigationController pushViewController:logincontroller animated:YES];
                                      */
                                        
                                    }else{
                                        
                                    }
                                }];
    
    [alert addAction:noButton];
    [alert addAction:yesButton];
    
    UIWindow *alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    alertWindow.rootViewController = [[UIViewController alloc] init];
    alertWindow.windowLevel = UIWindowLevelAlert + 1;
    [alertWindow makeKeyAndVisible];
    [alertWindow.rootViewController presentViewController:alert animated:YES completion:nil];
}
///// End Push Notificaton

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

// This method will be called when the application lauch including when the user just swap to another scene.
- (void)applicationWillEnterForeground:(UIApplication *)application {
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Selected_Back"];

    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    id use_coupon = (id)[userDefaults objectForKey:@"USING_COUPON"];
    
    if (!use_coupon) {
        
        // For finding the ability of lockScreen scene on LaunchScreenController
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        LaunchScreenController *beforeAccess = [storyboard instantiateViewControllerWithIdentifier:@"LaunchScreenController"];
        UINavigationController *navigationController=[[UINavigationController alloc] initWithRootViewController:beforeAccess];
        navigationController.navigationBar.hidden = YES;
        self.window.rootViewController =nil;
        self.window.rootViewController = navigationController;
        [self.window makeKeyAndVisible];
    }
}

-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    
    UIAlertView *notificationAlert = [[UIAlertView alloc] initWithTitle:@"Notification"    message:@"This local notification" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    
    [notificationAlert show];
    
    [BaseViewController log_comment:@"didReceiveLocalNotification" text:nil];
}

-(NSString *) platformType:(NSString *)platform
{
    if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone3,3"])    return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone 5";
    if ([platform isEqualToString:@"iPhone5,2"])    return @"iPhone 5";
    if ([platform isEqualToString:@"iPhone5,3"])    return @"iPhone 5";
    if ([platform isEqualToString:@"iPhone5,4"])    return @"iPhone 5";
    if ([platform isEqualToString:@"iPhone6,1"])    return @"iPhone 5";
    if ([platform isEqualToString:@"iPhone6,2"])    return @"iPhone 5";
    if ([platform isEqualToString:@"iPhone7,2"])    return @"iPhone 6";
    if ([platform isEqualToString:@"iPhone7,1"])    return @"iPhone 6 Plus";
    if ([platform isEqualToString:@"iPhone8,1"])    return @"iPhone 6s";
    if ([platform isEqualToString:@"iPhone8,2"])    return @"iPhone 6s Plus";
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch";
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch";
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch";
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch";
    if ([platform isEqualToString:@"iPod5,1"])      return @"iPod Touch";
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2";
    if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2";
    if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2";
    if ([platform isEqualToString:@"iPad2,4"])      return @"iPad 2";
    if ([platform isEqualToString:@"iPad2,5"])      return @"iPad Mini";
    if ([platform isEqualToString:@"iPad2,6"])      return @"iPad Mini";
    if ([platform isEqualToString:@"iPad2,7"])      return @"iPad Mini";
    if ([platform isEqualToString:@"iPad3,1"])      return @"iPad 3";
    if ([platform isEqualToString:@"iPad3,2"])      return @"iPad 3";
    if ([platform isEqualToString:@"iPad3,3"])      return @"iPad 3";
    if ([platform isEqualToString:@"iPad3,4"])      return @"iPad 4";
    if ([platform isEqualToString:@"iPad3,5"])      return @"iPad 4";
    if ([platform isEqualToString:@"iPad3,6"])      return @"iPad 4";
    if ([platform isEqualToString:@"iPad4,1"])      return @"iPad Air";
    if ([platform isEqualToString:@"iPad4,2"])      return @"iPad Air";
    if ([platform isEqualToString:@"iPad4,3"])      return @"iPad Air";
    if ([platform isEqualToString:@"iPad4,4"])      return @"iPad Mini 2G";
    if ([platform isEqualToString:@"iPad4,5"])      return @"iPad Mini 2G";
    if ([platform isEqualToString:@"iPad4,6"])      return @"iPad Mini 2G";
    if ([platform isEqualToString:@"iPad4,7"])      return @"iPad Mini 3";
    if ([platform isEqualToString:@"iPad4,8"])      return @"iPad Mini 3";
    if ([platform isEqualToString:@"iPad4,9"])      return @"iPad Mini 3";
    if ([platform isEqualToString:@"iPad5,3"])      return @"iPad Air 2";
    if ([platform isEqualToString:@"iPad5,4"])      return @"iPad Air 2";
    if ([platform isEqualToString:@"AppleTV2,1"])   return @"Apple TV 2G";
    if ([platform isEqualToString:@"AppleTV3,1"])   return @"Apple TV 3";
    if ([platform isEqualToString:@"AppleTV3,2"])   return @"Apple TV 3";
    if ([platform isEqualToString:@"i386"])         return @"Simulator";
    if ([platform isEqualToString:@"x86_64"])       return @"Simulator";
    return platform;
}

- (void)applicationDidBecomeActive:(UIApplication *)application {

    //Facebook SDK
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
