//
//  XLPageStripHomeController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/24/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

//  This class is sub class of XLPagerTabStrip library for setting child class that it can be accessed by user sliding.

#import <UIKit/UIKit.h>
#import <XLBarPagerTabStripViewController.h>
#import <XLPagerTabStripViewController.h>

@interface XLPageStripHomeController : XLBarPagerTabStripViewController<XLPagerTabStripChildItem,XLPagerTabStripViewControllerDataSource,XLPagerTabStripViewControllerDelegate,UICollectionViewDataSource,UICollectionViewDelegate>{
    
    int selectedTab;
    int indexCell;

    NSMutableArray *arrayMenuBarImage;
    NSMutableArray *arrayMenuBarImage_black;
    
    NSMutableArray *swiptTab;

    UICollectionView *_collectionView;
    
    BOOL _isReload;
}

@property (weak, nonatomic) IBOutlet UIView *menuBar;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *menubar_width;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *menubar_left;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *menubar_right;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *menubar_center;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end

