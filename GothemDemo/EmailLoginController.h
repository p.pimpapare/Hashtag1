//
//  EmailLoginController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/21/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

// This class is allow the user to login with there email

#import <UIKit/UIKit.h>

@interface EmailLoginController : BaseViewController{
    
    NSString *resultErrorMsg;
    
    BOOL validateEmail;
    BOOL validatePassword;
}

@property (weak, nonatomic) IBOutlet UIView *statusBar;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *viewBg;

@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIImageView *logo;

@property (weak, nonatomic) IBOutlet UITextField *emailTextfield;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextfield;

@property (weak, nonatomic) IBOutlet UIButton *registerOnWebBtn;
@property (weak, nonatomic) IBOutlet UIButton *registeronWeb2;
@property (weak, nonatomic) IBOutlet UIButton *forgetPassBtn;
@property (weak, nonatomic) IBOutlet UILabel *regisText;

@property (weak, nonatomic) IBOutlet UIView *errorView1;
@property (weak, nonatomic) IBOutlet UIImageView *errorIcon1;

@property (weak, nonatomic) IBOutlet UIView *errorView2;
@property (weak, nonatomic) IBOutlet UIImageView *errorIcon2;

@property (weak, nonatomic) IBOutlet UILabel *telLable;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;

@property (weak, nonatomic) IBOutlet UILabel *telLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailTxtLable;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightLogo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthLogo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightScrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topLogo;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top_error_view;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *emailTop_constant;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomregister;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomregister2;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomregister3;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomregister4;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topForgotPass;


// UI for IPAD
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *email_txt_left;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *icon_email_left;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *icon_email_error_right;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *view_email_error_right;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *email_txt_right;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *icon_pass_left;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pass_txt_left;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pass_txt_right;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *icon_pass_error_right;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *view_pass_error_right;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btn_right;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btn_left;

-(void)getService:(NSString *)email password:(NSString *)password;

@end
