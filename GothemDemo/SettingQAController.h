//
//  SettingQAController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/7/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

//  This class is the root class of XLPagerTabStrip.

#import <UIKit/UIKit.h>
#import <XLBarPagerTabStripViewController.h>
#import <XLPagerTabStripViewController.h>

@interface SettingQAController : XLBarPagerTabStripViewController<XLPagerTabStripChildItem,XLPagerTabStripViewControllerDataSource,XLPagerTabStripViewControllerDelegate,UICollectionViewDataSource,UICollectionViewDelegate>{

//XLBarPagerTabStripViewController<XLPagerTabStripChildItem,XLPagerTabStripViewControllerDataSource,XLPagerTabStripViewControllerDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>{
    
    BOOL _isReload;
    UICollectionView *_collectionView;
    
    NSArray *titleMenu;
    int selectedTabMenu;
    int selectedTab;
    int indexCell;
}
@property (weak, nonatomic) IBOutlet UIView *menuBar;

@end
