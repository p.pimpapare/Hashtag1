//
//  MenuCollectionController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/20/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "MenuCollectionController.h"

@interface MenuCollectionController (){
    
    BOOL isAnimating;
}

@property (nonatomic) NSMutableArray* numbers;
@property (nonatomic) NSMutableArray* numberWidths;
@property (nonatomic) NSMutableArray* numberHeights;

@end

int num = 0;

@implementation MenuCollectionController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self.loadingView setHidden:NO];
    [self.loadingView startAnimating];
    
    [self menuCollecitron_UI];
}

-(void)regis_collection_RFQuiltLayout_cell{
    
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    
    RFQuiltLayout* layout = (id)[self.collectionView collectionViewLayout];
    layout.direction = UICollectionViewScrollDirectionVertical;
    layout.blockPixels = CGSizeMake(DEVICE_WIDTH/3,DEVICE_WIDTH/3);
    
    [self.collectionView reloadData];
}

-(void)getService:(NSString *)CId{
    
    arrayName = [[NSMutableArray alloc]init];
    arrayImage = [[NSMutableArray alloc]init];

    cell_height = [[NSMutableArray alloc]init];
    cell_width = [[NSMutableArray alloc]init];

    bID = [[NSMutableArray alloc]init];
    
    [Service get_category_brand_grid:[self getUserToken] cID:CId selfID:self block:^(id result) {
        
        NSString *resultText = [NSString stringWithFormat:@"%@",result];
        
        if (![resultText isEqualToString:@"error"]&&[result[@"brandgrid"] count]!=0) {
            
                    for (int i=0 ;i<[result[@"brandgrid"] count];i++) {
                        
                        [arrayName addObject:result[@"brandgrid"][i][@"brand_name"]];
                        [arrayImage addObject:result[@"brandgrid"][i][@"brand_grid_img"]];
                        [bID addObject:result[@"brandgrid"][i][@"brand_id"]];
                        
                        NSString *height_text = [NSString stringWithFormat:@"%@",result[@"brandgrid"][i][@"height"]];
                        NSString *width_text = [NSString stringWithFormat:@"%@",result[@"brandgrid"][i][@"width"]];
                        
                        int height_int = [height_text intValue];
                        int width_int = [width_text intValue];
                        
                        [cell_height addObject:[NSNumber numberWithInt:height_int]];
                        [cell_width addObject:[NSNumber numberWithInt:width_int]];

                    }
        }
        
        finisedLoading = YES;
        
        [self.loadingView setHidden:YES];
        [self.loadingView stopAnimating];
        
        [_collectionView reloadData];
        [self datasInit];
    }];
}

- (void)datasInit {
    
    num = 0;
    self.numbers = [@[] mutableCopy];
    self.numberWidths = @[].mutableCopy;
    self.numberHeights = @[].mutableCopy;
    
    for(; num<[cell_height count]; num++) {
        
        [self.numbers addObject:@(num)];
        [self.numberWidths addObjectsFromArray:cell_width];
        [self.numberHeights addObjectsFromArray:cell_height];
    }
}

// Set collection view
-(void)setCollection{
    
//    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
//    
//    // Set collection size between each other
//    layout.minimumLineSpacing=10;
//    layout.minimumInteritemSpacing=10;
//    layout.sectionInset = UIEdgeInsetsMake(10,10,10,10);
//    
//    [self.collectionView setCollectionViewLayout:layout];
//    [self.collectionView setDataSource:self];
//    [self.collectionView setDelegate:self];
    [self.collectionView setBackgroundColor:[UIColor clearColor]];
    
    // For collection refresh
    self.collectionView.alwaysBounceVertical = YES;
    
    // Register collectionView
    [_collectionView registerClass:[PCCollectionCell class] forCellWithReuseIdentifier:@"Cell"];
    [_collectionView registerNib:[UINib nibWithNibName:@"PCCollectionCell" bundle:nil]  forCellWithReuseIdentifier:@"Cell"];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    if([arrayName count]==0){
        
        if (finisedLoading==YES) {
            
            if (have_special_coupon==NO) {
                
                [self setWarningView];
            }
        }
        
        return 0;
    }else{
        return [arrayName count];
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    PCCollectionCell *cell=(PCCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    if(cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"PCCollectionCell" owner:self options:nil][0];
    }
    
    [cell setImagCell:arrayImage[indexPath.row]];
    
    return cell;
}

#pragma mark – RFQuiltLayoutDelegate

-(CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout blockSizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row >= self.numbers.count) {
        NSLog(@"Asking for index paths of non-existant cells!! %ld from %lu cells", (long)indexPath.row, (unsigned long)self.numbers.count);
    }
    
    CGFloat width = [[self.numberWidths objectAtIndex:indexPath.row] floatValue];
    CGFloat height = [[self.numberHeights objectAtIndex:indexPath.row] floatValue];
    return CGSizeMake(width, height);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetsForItemAtIndexPath:(NSIndexPath *)indexPath {
    return UIEdgeInsetsMake(5,5,5,5);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self goToMenuTableViewList:@"โปรโมชั่น" brand_id:bID[indexPath.row]];
}

- (IBAction)backBtn:(id)sender {
    GOTO_PREVIOUS;
}

-(void)setWarningView{
    [self.viewWarning setHidden:NO];
}

-(void)menuCollecitron_UI{
    
    finisedLoading = NO;
    
    SWIPETLIFE_TOGOTOPREVIOUS;
    LINE_WITH_STATUSBAR_HEADER_NORMAL;
    [self setBG_Logo];
    
    [self.viewWarning setBackgroundColor:[UIColor clearColor]];
    [self.view setBackgroundColor:[UIColor clearColor]];
}

- (void)swipeRight:(id)sender {
    GOTO_PREVIOUS;
}

-(void)setHeaderText:(NSString *)header categoryID:(NSString *)cID{
    
    cId_num = cID;
    
    [self.titleHeader setText:[NSString stringWithFormat:@"%@",header]];
    [self getService:cID];
    [self regis_collection_RFQuiltLayout_cell];
    [self setCollection];
}

- (IBAction)go_to_tableview:(id)sender {
    
    [self goToMenuTableViewList:@"โปรโมชั่น" brand_id:couponID];
}

-(void)goToMenuTableViewList:(NSString *)headerText brand_id:(NSString *)b_ID{
    
    MenuCouponListController *menuTableView = [[NSBundle mainBundle] loadNibNamed:@"MenuCouponListController" owner:self options:nil][0];
    [menuTableView setHeaderText:[NSString stringWithFormat:@"%@",headerText]brand_id:b_ID];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController pushViewController:menuTableView animated:YES];
    });
}

@end
