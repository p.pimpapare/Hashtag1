//
//  BaseViewController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/19/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

//  This class is a base class. It's used for setting segue between scene or setting base fuction that it is used in multiple class.

#import <UIKit/UIKit.h>
#import "MenuDetailInputCode.h"
#import "MenuCouponDetail_Alert.h"
#import "UsedCuponSuccess.h"
#import "MenuCuponDetailCode.h"
#import "MenuCouponDetail.h"

@interface BaseViewController : UIViewController{

    PCLoadingView *pcload;
    PCBg *pcblack;
}

// User's information
-(NSString *_Nullable)getUserToken;
-(NSString *_Nullable)getUserAgreed;
-(NSString *_Nullable)getUsert1C;
-(NSString *_Nullable)getUserName;
-(NSString *_Nullable)getUserEmail;
-(NSString *_Nullable)getUserPassword;

-(void)setUserProfile:(UILabel *_Nullable)name numberT1C:(UILabel *_Nullable)t1c balance:(UILabel *_Nullable)balance earn:(UILabel *_Nullable)earn luckydraw:(UILabel *_Nullable)leckydraw;

+(void)setUserLogoutStatus:(BOOL)logoutStatus;

-(void)alert_login_again;

// Device information
+(id _Nullable)getDeviceInfo;
+(id _Nullable)getCurrentVersion;
+(NSString *_Nullable)getDeviceToken;

// Draw red line UI
+(UIView *_Nullable)drawLineHeader;
+(UIView *_Nullable)drawLineBottom;


-(id _Nullable)loadNibWithName:(NSString *_Nullable)viewControllerName;
-(void)pushViewCtrlWithViewCtrlName:(NSString *_Nullable)viewCtrlName;

-(void)share_activity:(NSString * _Nullable)sharedInfo;

// Loading UI
-(void)addPCLoading:(UIViewController *_Nullable)classController;
-(void)setHiddenPCLoading:(BOOL)hide;
-(void)setLoadingForContainer;
-(void)setLoadingGrayColor;
-(void)setLoadingLockScreen;
-(void)setLoadingShare;


// Black bg
-(void)addPCBlack:(UIViewController *_Nullable)classController;
-(void)setHiddenPCBlack:(BOOL)hide;

-(void)setBg;
-(void)setBG_Logo;

-(NSMutableDictionary *_Nullable)checkNullValue:(NSMutableDictionary *_Nullable)dicData;
-(NSString *_Nullable)checkStringNull:(NSString *_Nullable)text_string;

+(BOOL)check_service_status:(id _Nullable)result;
+(void)log_comment:(id _Nullable)topic_text text:(NSString *_Nullable)text;

// This method is used for creating red line header and red line bottom that is used in the mutiple scene.
+(void)setLineColor:(UIView *_Nullable)lineHeader bottom:(UIView *_Nullable)lineBottom;


/// Error's Alert view

//-(void)network_error;
//-(void)service_error;

@property NSString *tagMenu;
+(NSString *_Nullable)action:(NSString *_Nullable) selecttag;

@end

#define CALL_CENTER_TEL @"0-2258-9770"
#define CALL_CENTER_EMAIL @"hashtag1@playpointthailand.com"
#define CALL_CENTER_TEXT @"ยังไม่ได้เป็นสมาชิก The 1 Card แตะ\n ยังไม่ได้ลงทะเบียน The 1 Card Online แตะ"

#define LINE_WITH_STATUSBAR_LOGIN [self.view addSubview:[BaseViewController drawLineHeader]]; [self.view addSubview:[BaseViewController drawLineBottom]]; [self.statusBar setBackgroundColor:[UIColor blackColor]];

#define LINE_WITH_STATUSBAR_NORMAL [self.view addSubview:[BaseViewController drawLineHeader]]; [self.view addSubview:[BaseViewController drawLineBottom]]; [self.statusBar setBackgroundColor:[PCColor color_F0F0F1]];

#define LINE_WITH_STATUSBAR_HEADER_NORMAL [self.view addSubview:[BaseViewController drawLineHeader]]; [self.view addSubview:[BaseViewController drawLineBottom]]; [self.statusBar setBackgroundColor:[PCColor color_F0F0F1]]; [PCColor set_shadow_headerBar:self.menuHeader];

#define SWIPETLIFE_TOGOTOPREVIOUS UISwipeGestureRecognizer * swipeRight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeRight:)]; swipeRight.direction=UISwipeGestureRecognizerDirectionRight; [self.view addGestureRecognizer:swipeRight];

#define TABLEVIEW_COLLECTION_REFRESH(x) UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init]; [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged]; [x addSubview:refreshControl];

#define ALERT_MESSAGE_update_version(title,sub_title,yes_action) UIAlertController * alert =   [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"%@",title] message:[NSString stringWithFormat:@"%@",sub_title] preferredStyle:UIAlertControllerStyleAlert]; UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"อัพเดท" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){ yes_action }];[alert addAction:yesButton]; [self presentViewController:alert animated:YES completion:nil];

#define ALERT_MESSAGE(title,sub_title,yes_action,no_action) UIAlertController * alert =   [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"%@",title] message:[NSString stringWithFormat:@"%@",sub_title] preferredStyle:UIAlertControllerStyleAlert]; UIAlertAction* noButton = [UIAlertAction actionWithTitle:@"ยกเลิก" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){ no_action }]; UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"ลองใหม่" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){ yes_action }]; [alert addAction:noButton]; [alert addAction:yesButton]; [self presentViewController:alert animated:YES completion:nil];

#define ALERT_MESSAGE_NETWORK(yes_action,no_action) UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"เกิดข้อผิดพลาดในการรับส่งข้อมูล" message:@"โปรดตรวจสอบการเชื่อมต่ออินเตอร์เน็ต และลองใหม่อีกครั้ง" preferredStyle:UIAlertControllerStyleAlert]; UIAlertAction* noButton = [UIAlertAction actionWithTitle:@"ยกเลิก" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){ no_action }]; UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"ลองใหม่" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){ yes_action }]; [alert addAction:noButton]; [alert addAction:yesButton]; [self presentViewController:alert animated:YES completion:nil];

#define ALERT_MESSAGE_YES(title,sub_title,yes_action) UIAlertController * alert =   [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"%@",title] message:[NSString stringWithFormat:@"%@",sub_title] preferredStyle:UIAlertControllerStyleAlert]; UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"ตกลง" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){ yes_action }]; [alert addAction:yesButton]; [self presentViewController:alert animated:YES completion:nil];

#define ALERT_MESSAGE_YES_NETWORK(yes_action) UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"เกิดข้อผิดพลาดในการรับส่งข้อมูล" message:@"โปรดตรวจสอบการเชื่อมต่ออินเตอร์เน็ต และลองใหม่อีกครั้ง" preferredStyle:UIAlertControllerStyleAlert]; UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"ตกลง" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){ yes_action }]; [alert addAction:yesButton]; [self presentViewController:alert animated:YES completion:nil];

#define GET_USER_DEFAULT_INFO(x) id userInfo; NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults]; if (userDefaults){    userInfo = (id)[userDefaults objectForKey:x]; }

#define REMOVE_USER_DEFAULT_INFO(x)      [[NSUserDefaults standardUserDefaults] removeObjectForKey:x]; [[NSUserDefaults standardUserDefaults] synchronize];

#define SET_USER_DEFAULT_INFO(x,y)  NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults]; if (userDefaults){ [userDefaults setObject:x forKey:y]; }

//#define WEB_VIEW() - (void)didReceiveMemoryWarning{[super didReceiveMemoryWarning];}- (void)setView:(UIView *)view{if (view == nil){[self releaseSubviews];}[super setView:view];} - (void)webViewDidFinishLoad:(UIWebView *)webView { if (!webView.isLoading) { [self.loadingView stopAnimating];[self.loadingView setHidden:YES];}} -(void)openUrl:(NSString *)urlText{ NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",urlText]]; NSURLRequest *req = [NSURLRequest requestWithURL:url]; [self.webView loadRequest:req];} -(void)releaseSubviews{self.webView = nil;self.webView.delegate = nil;[self.webView stopLoading];}- (void)dealloc{ [self releaseSubviews];}

#define KDCycleDelegate(x,sec) -(void)setPagerAdvertising{ _cycleBannerViewTop.autoPlayTimeInterval = sec; _cycleBannerViewBottom = [KDCycleBannerView new]; _cycleBannerViewBottom.frame = CGRectMake(0,0,self.cycleBannerViewBottom.frame.size.width,self.cycleBannerViewBottom.frame.size.height); _cycleBannerViewBottom.datasource = self; _cycleBannerViewBottom.delegate = self; _cycleBannerViewBottom.continuous = YES; [self.view addSubview:_cycleBannerViewBottom]; } - (NSArray *)numberOfKDCycleBannerView:(KDCycleBannerView *)bannerView {    return advertising_array; } - (UIViewContentMode)contentModeForImageIndex:(NSUInteger)index {    return UIViewContentModeScaleToFill; } - (UIImage *)placeHolderImageOfZeroBannerView {    return [UIImage imageNamed:x]; } - (void)cycleBannerView:(KDCycleBannerView *)bannerView didScrollToIndex:(NSUInteger)index { } - (void)cycleBannerView:(KDCycleBannerView *)bannerView didSelectedAtIndex:(NSUInteger)index { }

