
//
//  UnlockWebViewController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/9/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "UnlockWebViewController.h"
#import "LockScreenController.h"

@import AVFoundation;

@interface UnlockWebViewController ()

@property (nonatomic, strong) AFDropdownNotification *notification;

@end

@implementation UnlockWebViewController

-(void)viewWillAppear:(BOOL)animated{
    
    GOOGLE_ANALYTICS(@"Unlock_WebController");
    
    [self.backBtn setUserInteractionEnabled:NO];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
}


-(void)youTubePlayed:(id)sender{
    
    /*
     * This () is used for detecting UIWebview finish to play video
     *
     */
    
    if (youtubeAds == YES && isDisappear==NO) {
        
        [self.loadingView startAnimating];
        [self.loadingView setHidden:NO];
        
        youtubeAds = NO;
        
        [self post_collection_point];
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    
    isDisappear = YES;
    
    [timeAlert invalidate];
    [timeLoading invalidate];
    
    if (youtubeAds == YES) {
        [self.playerView stopVideo];
    }
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self.view_youtube setHidden:YES];
    
    notiAlreadyAlert = NO;
    
    [self.loadingView startAnimating];
    [self.loadingView setHidden:NO];
    
    [self class_UI];
    
    timeLoading = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(action_whenLoadingFinish) userInfo:nil repeats:NO];
}

-(void)alert_noti:(NSString *)alert_title sub_title:(NSString *)sub_title {
    
    /*
     * This () is called when any Ads has point, wheter it be #1 point or Luckydraw point
     *
     */
    
    GET_USER_DEFAULT_INFO(@"ALERT_ENABLED")
    
    //    if (soundsEnabled == YES) {
    //
    //        SET_USER_DEFAULT_INFO(@"YES",@"SOUND_ENABLED")
    
    
    if (notiAlreadyAlert == NO && ([userInfo isEqualToString:@"YES"] || (userInfo == nil))) {
        
        _notification = [[AFDropdownNotification alloc] init];
        _notification.notificationDelegate = (id)self;
        
        _notification.titleText = [NSString stringWithFormat:@"%@",alert_title];
        _notification.subtitleText = [NSString stringWithFormat:@"%@",sub_title];
        _notification.image = [UIImage imageNamed:@"aaaa"];
        
        [_notification presentInView:self.view withGravityAnimation:YES];
        
        GET_USER_DEFAULT_INFO(@"SOUND_ENABLED")
        
        if ( [userInfo isEqualToString:@"YES"] || (userInfo == nil)) {
            
            // custom coin sound effect, coin
            NSString *soundPath = [[NSBundle mainBundle] pathForResource:@"sound" ofType:@"mp3"];
            SystemSoundID soundID;
            AudioServicesCreateSystemSoundID((__bridge CFURLRef)[NSURL fileURLWithPath: soundPath], &soundID);
            AudioServicesPlaySystemSound (soundID);
            
        }
        
        AudioServicesPlaySystemSound (1352); //สั่น
        
        timeAlert = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(stop_notification) userInfo:nil repeats:NO];
        
        notiAlreadyAlert = YES;
    }
}

-(void)stop_notification{
    
    if (notiAlert == YES) {
        [_notification dismissWithGravityAnimation:YES];
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    if (!webView.isLoading) {
        
        if (!isDisappear && youtubeAds == NO && notiAlert == NO) {
            
            [self post_collection_point];
            
        }else{
            
            [self.backBtn setUserInteractionEnabled:YES];
        }
    }
}

-(void)post_collection_point{
    
    GET_USER_DEFAULT_INFO(@"Response_token_key");
    
    [Service post_collect_point:[self getUserToken] responseToken:userInfo unlock_type:@"c" selfID:self block:^(id result){
        
        [BaseViewController log_comment:@"Result colelect point" text:result];
        
        [self action_whenLoadingFinish];

        NSString *result_text = [NSString stringWithFormat:@"%@",result];
        
        if (![result_text isEqualToString:@"error"]) {
                        
            if([result count]==0){
                
                [self alert_login_again];
                
            }else{
                
                NSString *success_text = [NSString stringWithFormat:@"%@",result[@"success"]];
                
                if ([success_text isEqualToString:@"1"]) {
                    
                    notiAlert = YES;
                    
                    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                    id userInfo = (id)[userDefaults objectForKey:@"UserInfo"];
                    
                    NSMutableDictionary *dic_copy = [userInfo mutableCopy];
                    
                    NSString *luckydraw_status = [NSString stringWithFormat:@"%@",result[@"luckydraw"]];
                    
                    if ([luckydraw_status isEqualToString:@"1"]) { // luckydraw
                        
                        [dic_copy removeObjectForKey:@"luckydraw"];
                        [dic_copy setObject:result[@"luckydraw_count"] forKey:@"luckydraw"];

                        [self alert_noti:@"คุณได้รับสิทธิ์คะแนนสะสม 1 สิทธิ์" sub_title:[NSString stringWithFormat:@"คุณมีสิทธิ์สะสมทั้งหมด %@ สิทธิ์",result[@"luckydraw_count"]]];
                        
                    }else if ([luckydraw_status isEqualToString:@"0"]){ // point
                        
                        [dic_copy removeObjectForKey:@"earn"];
                        [dic_copy setObject:result[@"user_point"] forKey:@"earn"];

                        [self alert_noti:@"Hashtag 1" sub_title:[NSString stringWithFormat:@"คุณได้รับคะแนน %@ คะแนน",result[@"point"]]];
                    }
                    
                    [userDefaults setObject:dic_copy forKey:@"UserInfo"];
                    [BaseViewController log_comment:@"Update user info %@" text:[NSString stringWithFormat:@"%@",dic_copy]];
                }
            }
        }
    }];
}

-(void)action_whenLoadingFinish{
    
    [self.loadingView stopAnimating];
    [self.loadingView setHidden:YES];
    [self.backBtn setUserInteractionEnabled:YES];
}

-(void)class_UI{
    
    [self.titleText setFont:[PCFont fontRSU:20.0]];
    [self.webView setScalesPageToFit:YES];
}

-(void)setWebScene:(NSString *)name url:(NSString *)linkUrl{
    
    // Test for youtube mahanakorn
    //    if ([linkUrl isEqualToString:@"http://www.mahanakhon.com/"]) {
    //        linkUrl = @"https://www.youtube.com/watch?v=sGNf5PLRMVg";
    //    }
    
    [self.titleText setText:[NSString stringWithFormat:@"%@",name]];
    [self openUrl:[NSString stringWithFormat:@"%@",linkUrl]];
}

-(void)openUrl:(NSString *)urlText{
    
    NSString *url;
    
    NSString *html_link = [NSString stringWithFormat:@"%@",[ReplaceHTMLFont replaceHTML_blank_space:urlText]];
    
    if (([html_link rangeOfString:@"youtu.be"].location != NSNotFound)||([html_link rangeOfString:@"youtube"].location != NSNotFound)||([html_link rangeOfString:@"?v="].location != NSNotFound)||([html_link rangeOfString:@"y2u.be"].location != NSNotFound)) {
        
        youtubeAds  = YES;
        
        NSArray *split_text;
        NSString *url_yt_text;
        
        NSString *check_yt_link = [NSString stringWithFormat:@"%@",[html_link lastPathComponent]];
        
        if ([check_yt_link rangeOfString:@"watch"].location != NSNotFound) { // กรณีมี watch ซ้ำ
            
            split_text = [check_yt_link componentsSeparatedByString:@"="];
            url_yt_text = [NSString stringWithFormat:@"%@",[split_text objectAtIndex:1]];
            
        }else
        {
            url_yt_text = check_yt_link;
        }
        
        [self.view_youtube setHidden:NO];
        
        url = [NSString stringWithFormat:@"https://www.youtube.com/watch?v=%@&autoplay=1",[html_link lastPathComponent]];
        
        //        NSLog(@"%@",[html_link lastPathComponent]);
        
        NSDictionary *playerVars = @{
                                     @"playsinline":@1,
                                     @"controls" : @0,
                                     @"autoplay" : @1,
                                     @"showinfo" : @0,
                                     @"modestbranding" : @1,
                                     @"rel" : @0
                                     };
        
        [self.playerView setDelegate:self];
        [self.playerView loadWithVideoId:[NSString stringWithFormat:@"%@",url_yt_text] playerVars:playerVars];
        
    }else{
        
        // Check link if it donesn't start with http:// or https://
        if (([urlText rangeOfString:@"http://"].location != NSNotFound)||([urlText rangeOfString:@"https://"].location != NSNotFound)) {
            url = [NSString stringWithFormat:@"%@",[ReplaceHTMLFont replaceHTML_blank_space:urlText]];
        }else{
            url = [NSString stringWithFormat:@"http://%@",[ReplaceHTMLFont replaceHTML_blank_space:urlText]];
        }
    }
    
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
    [self.webView.scrollView setBounces:NO];
    [self.webView setMediaPlaybackRequiresUserAction:NO];
}
/*
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"ERROR : %@",error); //Get informed of the error FIRST
}
*/
- (void)playerView:(YTPlayerView *)playerView didChangeToState:(YTPlayerState)state {
    
    switch (state) {
            
        case kYTPlayerStateEnded:
            
            NSLog(@"Youtube Ended");
            
            if (youtubeAds == YES && isDisappear==NO) {
                
                [self.loadingView startAnimating];
                [self.loadingView setHidden:NO];
                
                youtubeAds = NO;
                
                [self post_collection_point];
            }
            
            break;
        case kYTPlayerStatePaused:
            NSLog(@"Youtube Paused");
            break;
        default:
            break;
    }
}

-(void)playerViewDidBecomeReady:(YTPlayerView *)playerView {
    
    [self.playerView playVideo];
}

- (IBAction)backBtn:(id)sender {
    
    [self.view setUserInteractionEnabled:NO];
    
    [self.loadingView startAnimating];
    [self go_to_home];
}

- (void)didReceiveMemoryWarning{
    
    [super didReceiveMemoryWarning];
}

- (void)releaseSubviews{
    self.webView = nil;
}

- (void)setView:(UIView *)view{
    
    if (view == nil){
        //        NSLog(@"setView: releasing subviews");
        [self releaseSubviews];
    }
    [super setView:view];
}

-(void)go_to_home{
    
    if (notViewADS==NO) {
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Selected_Back"];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        XLPageStripMenuBarController *homeScene =
        (XLPageStripMenuBarController *)
        [storyboard instantiateViewControllerWithIdentifier:@"XLPageStripMenuBarController"];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.navigationController pushViewController:homeScene animated:YES];
        });
    }
    
    [self.view setUserInteractionEnabled:YES];
    [self setHiddenPCLoading:YES];
    [self.loadingView stopAnimating];
}

-(void)set_loop_index_ads:(int)column index:(int)index{
    
    column_ads = column;
    index_ads = index;
}

- (IBAction)goToAds:(id)sender {
    
    [self.view setUserInteractionEnabled:NO];
    [self.loadingView startAnimating];
    
    NSMutableString *loop_index_ads = [[NSMutableString alloc]init];
    
    [loop_index_ads appendString:[NSString stringWithFormat:@"%d",column_ads]];
    [loop_index_ads appendString:[NSString stringWithFormat:@"%d",index_ads]];
    
    SET_USER_DEFAULT_INFO(loop_index_ads,@"Selected_Back")
    
    if (notViewADS==NO) {
        
        GOTO_PREVIOUS
    }
    [self.view setUserInteractionEnabled:YES];
}

@end
