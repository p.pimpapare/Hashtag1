
//  Created by pimpaporn chaichompoo on 2/11/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCNewFeedBarCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleMenu;
@property (weak, nonatomic) IBOutlet UIView *line;

-(void)setNewFeedBar:(NSString *)title;
-(void)deleteLine;
-(void)setTitleColor:(NSString *)color;
@end
