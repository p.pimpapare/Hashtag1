
//  Created by pimpaporn chaichompoo on 1/31/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "PCColor.h"

@implementation PCColor

+(UIColor *)color_424242{
    
    NSString *color_hex = @"#424242";
    UIColor *color = [self colorFromHexString:color_hex alpha:1.0];
    
    return color;
}

+(UIColor *)color_F0F0F1{
    
    NSString *color_hex = @"#F0F0F1";
    UIColor *color = [self colorFromHexString:color_hex alpha:1.0];
    
    return color;
}

+(UIColor *)color_ffffff{
    
    NSString *color_hex = @"#ffffff";
    UIColor *color = [self colorFromHexString:color_hex alpha:1.0];
    
    return color;
}

+(UIColor *)color_E4E4E4{
    
    NSString *color_hex = @"#E4E4E4";
    UIColor *color = [self colorFromHexString:color_hex alpha:1.0];
    
    return color;
}

+(UIColor *)color_323232{
    
    NSString *color_hex = @"#323232";
    UIColor *color = [self colorFromHexString:color_hex alpha:1.0];
    
    return color;
}

+(UIColor *)color_A8101D{
    
    NSString *color_hex = @"#A8101D";
    UIColor *color = [self colorFromHexString:color_hex alpha:1.0];
    
    return color;
}

+(UIColor *)color_A6A6A6{
    
    NSString *color_hex = @"#A6A6A6";
    UIColor *color = [self colorFromHexString:color_hex alpha:1.0];
    
    return color;
}

+(UIColor *)color_4285F4{
    
    NSString *color_hex = @"#4285F4";
    UIColor *color = [self colorFromHexString:color_hex alpha:1.0];
    
    return color;
}

+(UIColor *)color_A80F1C{
    
    NSString *color_hex = @"#A80F1C";
    UIColor *color = [self colorFromHexString:color_hex alpha:1.0];
    
    return color;
}

+(UIColor *)color_EDEDED{ // bg gray
    
    NSString *color_hex = @"#EDEDED";
    UIColor *color = [self colorFromHexString:color_hex alpha:1.0];
    
    return color;
}

+(UIColor *)color_on_menu2:(NSString *)colorMenu2{
    
    UIColor *hexColor = [self colorFromHexString:[NSString stringWithFormat:@"%@",colorMenu2] alpha:1.0];
    return hexColor;
}

+(UIColor *)color_767676{
    
    NSString *color_hex = @"#767676";
    UIColor *color = [self colorFromHexString:color_hex alpha:1.0];
    
    return color;
}

+(UIColor *)color_A32E3D{
    
    NSString *color_hex = @"#A32E3D";
    UIColor *color = [self colorFromHexString:color_hex alpha:1.0];
    
    return color;
}

+(UIColor *)color_8E8E8E{
    
    NSString *color_hex = @"#8E8E8E";
    UIColor *color = [self colorFromHexString:color_hex alpha:1.0];
    
    return color;
}

+(UIColor *)color_F5F5F5{
    
    NSString *color_hex = @"#F5F5F5";
    UIColor *color = [self colorFromHexString:color_hex alpha:1.0];
    
    return color;
}

+(void)set_shadow_headerBar:(UIView *)view{
    
    // set gradient
    /*    UIColor *colorOne = [UIColor colorWithRed:(255/255.0)  green:(255/255.0)  blue:(255/255.0)  alpha:1.0];
     UIColor *colorTwo = [UIColor colorWithRed:(235/255.0) green:(235/255.0) blue:(235/255.0) alpha:1.0];
     
     // set backgroud shadow
     CAGradientLayer *gradient = [CAGradientLayer layer];
     [gradient setFrame:CGRectMake(0,0,DEVICE_WIDTH+10,view.frame.size.height)];
     gradient.colors = [NSArray arrayWithObjects:(id)[colorOne CGColor],(id)[colorTwo CGColor],nil];
     
     [view.layer insertSublayer:gradient atIndex:0];
     
     view.layer.masksToBounds = NO;
     view.layer.shadowOffset = CGSizeMake(0,0.2);
     view.layer.shadowRadius = 2;
     view.layer.shadowOpacity = 0.5;
     */
    
    [view setBackgroundColor:[PCColor color_F0F0F1]];
    
    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0,view.frame.size.height-1,DEVICE_WIDTH,1)];
    [line setBackgroundColor:[UIColor colorWithRed:(163/255.0) green:(163/255.0) blue:(163/255.0) alpha:0.25]];
    [view addSubview:line];
    
}

+(void)set_whiteColor_headerBar:(UIView *)view{
    
    [view setBackgroundColor:[UIColor whiteColor]];
    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0,view.frame.size.height-1,DEVICE_WIDTH,1)];
    [line setBackgroundColor:[UIColor colorWithRed:(163/255.0) green:(163/255.0) blue:(163/255.0) alpha:0.25]];
    [view addSubview:line];
}

+(void)set_shadow:(UIView *)view{
    view.layer.masksToBounds = NO;
    view.layer.shadowOffset = CGSizeMake(0,0.2);
    view.layer.shadowRadius = 2;
    view.layer.shadowOpacity = 0.5;
}

+(void)set_inner_shadow:(UIView *)view{ // set inner shadow
    
    UIImageView *innerShadowView = [[UIImageView alloc] initWithFrame:view.bounds];
    
    innerShadowView.contentMode = UIViewContentModeScaleAspectFill;
    innerShadowView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [view addSubview:innerShadowView];
    
    [innerShadowView.layer setMasksToBounds:YES];
    
    [innerShadowView.layer setBorderColor:[PCColor color_E4E4E4].CGColor];
    [innerShadowView.layer setShadowColor:[UIColor lightGrayColor].CGColor];
    [innerShadowView.layer setBorderWidth:1.0f];
    
    [innerShadowView.layer setShadowOffset:CGSizeMake(0,0)];
    [innerShadowView.layer setShadowOpacity:1.0];
    
    [innerShadowView.layer setShadowRadius:3]; // this is the inner shadow thickness
}

+(UIColor *)colorFromHexString:(NSString *)hexString alpha:(float)alpha{
  
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:alpha];
}

@end
