
//  Created by pimpaporn chaichompoo on 2/10/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

#import <UIKit/UIKit.h>

@interface History_tableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UILabel *point;
@property (weak, nonatomic) IBOutlet UILabel *displayNum;

-(void)setCell:(NSString *)date time:(NSString *)time point:(NSString *)point display:(NSString *)displayNum usedCoupon:(BOOL)usedCoupon;

@end
