//
//  InquiryController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/7/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "InquiryController.h"
#import "HelpHistoryController.h"

@interface InquiryController (){
    BOOL selectedFirst;
    BOOL tabDropdown;
}

@end

@implementation InquiryController

-(void)viewWillAppear:(BOOL)animated{
    
    GOOGLE_ANALYTICS(@"InquiryController");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getInquiryType];
}

-(void)getInquiryType{
    
    userNum = [NSString stringWithFormat:@"%@",[self getUsert1C]];
    
    [Service get_ingury_type:[self getUserToken] selfID:self block:^(id result) {
        
        NSString *resultText = [NSString stringWithFormat:@"%@",result];
        
        if (![resultText isEqualToString:@"error"]) {
            
            inquiryData = result;
            titleProblem = [[NSMutableArray alloc]init];
            
            for (int i=0;i<[result[@"types"] count];i++) {
                [titleProblem addObject:result[@"types"][i][@"name"]];
            }
        }
    }];
    
    [self set_initail_objects];
}

-(void)getSubtypeByIndex:(int)indexPickerView{
    
    selectedImage = NO;
    
    subtype_name = [[NSMutableArray alloc]init];
    subtype_feilds = [[NSMutableArray alloc]init];
    
    subtypes = inquiryData[@"types"][indexPickerView];
    //    subtype_id = [NSString stringWithFormat:@"%@",subtypes[@"subtypes"][0][@"type_id"]];
    
    for (int i=0;i<[subtypes[@"subtypes"] count];i++) {
        
        [subtype_name addObject:subtypes[@"subtypes"][i][@"name"]];
        [subtype_feilds addObject:subtypes[@"subtypes"][i][@"fields"]];
    }
    
    //    NSLog(@"subtype_name count %d",[subtype_name count]);
}

-(void)getFeildOfSubtype:(int)indexPickerView{
    
    //    NSLog(@"subtype_feilds[indexPickerView] %@",subtype_feilds[indexPickerView]);
    
    indexSubtype = indexPickerView;
    [self base_textField];
    
    subtype_id = [NSString stringWithFormat:@"%@",subtypes[@"subtypes"][indexSubtype][@"id"]];
    
    if ([subtype_feilds[indexPickerView] count]!=0) {
        
        textfeild_array = [[NSMutableArray alloc]init];
        
        //        NSLog(@"!=0");
        //        NSLog(@"count %d",(int)[subtype_feilds[indexPickerView] count]);
        //        NSLog(@"subtype_feilds %@",subtype_feilds[indexPickerView]);
        
        int subtypes_int = (int)[subtype_feilds[indexPickerView] count];
        int heightDetail_int = (47*subtypes_int)+94;
        
        
        for (int i=0;i<[subtype_feilds[indexPickerView] count];i++) {
            
            UITextField *subtype_textfeild = [[UITextField alloc]init];
            [subtype_textfeild setTag:i+1];
            [subtype_textfeild setFrame:CGRectMake(32,self.qTypeTxtfield.frame.origin.y+47+(47*i),DEVICE_WIDTH-64,37)];
            subtype_textfeild.font = [UIFont fontWithName:@"RSU" size:14];
            [subtype_textfeild setPlaceholder:[NSString stringWithFormat:@"%@",subtype_feilds[indexPickerView][i][@"name"]]];
            [subtype_textfeild setBackgroundColor:[PCColor color_F5F5F5]];
            [subtype_textfeild setBorderStyle:UITextBorderStyleRoundedRect];
            
            [textfeild_array addObject:subtype_textfeild];
            [self.scrollView addSubview:subtype_textfeild];
        }
        
        [self.heightDetail setConstant:heightDetail_int];
        [self.hightBaseView setConstant:920];
        
    }else{
        [self base_textField];
    }
}

-(void)base_textField{
    
    for (UITextField *txtFeilds in textfeild_array) {
        [txtFeilds removeFromSuperview];
    }
    
    [self.heightDetail setConstant:94];
    [self.hightBaseView setConstant:800];
}

- (IBAction)uploadBtn:(id)sender {
    [self OpenGallery];
}

- (IBAction)cancleUpload:(id)sender {
    [self.image_name setText:@" "];
    [self.cancleUpload setHidden:YES];
}

- (IBAction)confirmBtn:(id)sender {
    
    if(([self.detailTextview.text length]==0)||([self.detailTextview.text length]==1)||(select_title==NO)||(subtype_id==nil))
    {
        [self setWarningViewandIcon:NO];
    }else
    {
        [self setWarningViewandIcon:YES];
        
        NSString *combinedDateTime = [NSString stringWithFormat:@"%@ %@",[NumberFormat sort_time_dd_mm_yyyy:self.dateTxtfield.text],self.timeTxtfield.text ];
        
        [self post_inquiry:combinedDateTime];
    }
}

-(void)post_inquiry:(NSString *)dateTime{
    
    NSMutableArray *field_mutableArray = [[NSMutableArray alloc]init];
    NSMutableString *mString = [@"[" mutableCopy];
    
    //    NSLog(@"get pivot%@",subtype_feilds[indexSubtype]);
    //    NSLog(@"count %lu",(unsigned long)[subtype_feilds[indexSubtype] count]);
    
    for(int i=0;i<[subtype_feilds[indexSubtype] count];i++)
    {
        [field_id addObject:subtype_feilds[indexSubtype][i][@"pivot"][@"field_id"]];
    }
    
    if ([subtype_feilds[indexSubtype] count]!=0) {
        
        int i=0;
        
        for (UITextField *txtFeilds in textfeild_array) {
            
            //            NSLog(@"field_id %@",field_id[i]);
            //            NSLog(@"txtFeilds %@",txtFeilds.text);
            
            [field_mutableArray addObject:[NSString stringWithFormat:@"%@",field_id[i]]];
            [field_mutableArray addObject:[NSString stringWithFormat:@"%@",txtFeilds.text]];
            i++;
        }
        
        NSArray *field_array = [field_mutableArray copy];
        
        for (int i=0;i<[field_array count]/2;i++) {
            
            if(i==0){
                [mString appendFormat:@"[%@,\"%@\"]",field_array[i],field_array[i+1]];
            }else{
                [mString appendFormat:@"[%@,\"%@\"]",field_array[i+i],field_array[i+i+1]];
            }
            //            NSLog(@"i = %d",i);
            
            if (i!=([field_array count]/2)-1)
            {
                [mString appendString:@","];
            }
        }
        
        [mString appendString:@"]"];
    }else{
        [mString setString:@"[]"];
    }
    
    //    NSLog(@"mString %@",mString);
    
    [self uploadImage:dateTime feild:mString];
}

-(void)uploadImage:(NSString *)dateTime feild:(NSMutableString *)mString{
    
    [self addPCLoading:self];
    [self setLoadingForContainer];
    [self.view setUserInteractionEnabled:NO];
    [self setHiddenPCLoading:NO];
    
    //        NSLog(@"dateTime %@",dateTime);
    //        NSLog(@"problem_id %@",subtype_id);
    //        NSLog(@"self.detailTextview.text %@",self.detailTextview.text);
    //        NSLog(@"mString %@",mString);
    //        NSLog(@"self.device.text %@",self.device.text);
    //        NSLog(@"image name %@",self.image_name.text);
    //        NSLog(@"selected image %d",selectedImage);
    
    //    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@inquiry",BASEURL]parameters:@{@"problem_datetime":dateTime,@"problem_subtype":subtype_id,@"problem_detail":self.detailTextview.text,@"fields":mString,@"device_manufacturer":@"Apple",@"device_name":self.device.text} constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@inquiry",BASEURL] parameters:@{@"problem_datetime":dateTime,@"problem_subtype":subtype_id,@"problem_detail":self.detailTextview.text,@"fields":mString,@"device_manufacturer":self.device.text,@"device_name":@"iPhone"} constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        if (selectedImage==YES) {
            [formData appendPartWithFileData:UIImageJPEGRepresentation([self resizeImage:image withWidth:300 withHeight:400],1) name:@"attach" fileName:@"attach.jpg" mimeType:@"image/jpg"];
        }
        
    } error:nil];
    
    [request setAllHTTPHeaderFields:@{@"version":[BaseViewController getDeviceInfo][@"version"],@"mac":[BaseViewController getDeviceInfo][@"mac"],@"os-type":@"ios",@"os-version":[BaseViewController getDeviceInfo][@"os-version"],@"Authorization":[self getUserToken]}];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      // This is not called back on the main queue.
                      // You are responsible for dispatching to the main queue for UI updates
                      dispatch_async(dispatch_get_main_queue(), ^{
                          //Update the progress view
                          //[progressView setProgress:uploadProgress.fractionCompleted];
                      });
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      
                      NSString *title_message;
                      
                      if (error) {
                          //                          NSLog(@"Error: %@", error);
                          title_message = @"เกิดข้อผิดพลาดกรุณาส่งใหม่ภายหลัง";
                          [self.view setUserInteractionEnabled:YES];
                          
                      } else {
                          
                          //                          NSLog(@"%@ %@", response, responseObject);
                          // success
                          [self setHiddenPCLoading:YES];
                          [self.view setUserInteractionEnabled:YES];
                          
                          title_message = @"คำถามได้ถูกส่งเรียบร้อยแล้ว";
                      }
                      
                      ALERT_MESSAGE_YES(title_message,@" ",[self closeDetailView];)
                  }];
    
    [uploadTask resume];
}

- (IBAction)okBtn:(id)sender {
    [self closeDetailView];
}

-(void)closeDetailView{
    
    for (UITextField *txtFeilds in textfeild_array) {
        [txtFeilds removeFromSuperview];
    }
    
    [self setHiddenPCLoading:YES];
    [self.titleTxtfield setText:@""];
    [self.titleTxtfield setPlaceholder:@"เลือกประเภทคำถาม"];
    [self.qTypeTxtfield setHidden:YES];
    [self.dropdownQtype setHidden:YES];
    [self.heightDetail setConstant:47];
    //    [self hideAlltextfeild:YES];
    [self.hightBaseView setConstant:800];
    [self.detailTextview setText:@""];
    [self.image_name setText:@""];
    [self.cancleUpload setHidden:YES];
    [self setHiddenPCBlack:YES];
}

-(void)setWarningViewandIcon:(BOOL)hide{
    [self.warningIcon setHidden:hide];
    [self.warningView setHidden:hide];
}

-(void)userTappedOnTel:(id)sender{
    NSString *phoneNumber = [@"tel://" stringByAppendingString:CALL_CENTER_TEL];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}

-(void)userTappedOnEmail:(id)sender{
    
    //put email info here:
    NSString *toEmail = CALL_CENTER_EMAIL;
    NSString *subject = @" ";
    NSString *body = @" ";
    
    //opens mail app with new email started
    NSString *email = [NSString stringWithFormat:@"mailto:%@?subject=%@&body=%@", toEmail,subject,body];
    email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
    [titleProblem_pickker endEditing:YES];
    [type_pickker endEditing:YES];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(void)ShowSelectedDate{
    
    // set date format
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd/MM/yy"];
    NSLocale *locale = [[NSLocale alloc]
                        initWithLocaleIdentifier:@"th-TH"];
    [df setLocale:locale];
    
    self.dateTxtfield.text = [NSString stringWithFormat:@"%@",[df stringFromDate:datePicker.date]];
    [self.dateTxtfield resignFirstResponder];
}

-(void)ShowSelectedTime{
    
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"HH:mm "];
    
    self.timeTxtfield.text = [NSString stringWithFormat:@"%@",[timeFormatter stringFromDate:timePicker.date]];
    [self.timeTxtfield resignFirstResponder];
}

-(void)ShowSelectTitleProblem{
    [self.view endEditing:YES];
}

-(void)ShowSelectTopicProblem{
    [self.view endEditing:YES];
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    if (pickerView==titleProblem_pickker) {
        return titleProblem.count;
    }else{
        return subtype_name.count;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    if (pickerView==titleProblem_pickker) {
        return [titleProblem objectAtIndex:row];
    }else{
        return [subtype_name objectAtIndex:row];
    }
}

- (void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    select_title = YES;
    
    if (pickerView==titleProblem_pickker) {
        
        [self getSubtypeByIndex:(int)row];
        
        [self base_textField];
        [self.qTypeTxtfield setHidden:NO];
        [self.dropdownQtype setHidden:NO];
        
        self.qTypeTxtfield.text = @"เลือกหัวข้อ";
        self.titleTxtfield.text = (NSString *)[titleProblem objectAtIndex:(int)row];
    }else{
        self.qTypeTxtfield.text = (NSString *)[subtype_name objectAtIndex:row];
        [self getFeildOfSubtype:(int)row];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == self.titleTxtfield && [textField.text length]!=0)
    {
        [titleProblem_pickker selectRow:0 inComponent:0 animated:YES];
        [self pickerView:titleProblem_pickker didSelectRow:0 inComponent:0];
        
    }else if (textField == self.qTypeTxtfield && [textField.text length]!=0)
    {
        [type_pickker selectRow:0 inComponent:0 animated:YES];
        [self pickerView:type_pickker didSelectRow:0 inComponent:0];
    }
}

// set select image from gallery
-(void)OpenGallery{
    
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.delegate = (id)self;
    imagePickerController.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:imagePickerController animated:YES completion:nil];
}

-(void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    image = [info valueForKey:UIImagePickerControllerOriginalImage];
    selectedImage = YES;
    //    image = [UIImage imageNamed:@"picture_1_1"];
    
    ALAssetsLibraryWriteImageCompletionBlock completeBlock = ^(NSURL *assetURL, NSError *error){
        if (!error) {
#pragma mark get image url from camera capture.
            
            
            imageURL = [NSString stringWithFormat:@"%@",assetURL];
            
            // create image NSData
            [self.image_name setText:[NSString stringWithFormat:@"%@",[self splitImageName:imageURL]]];
            [self.cancleUpload setHidden:NO];
        }
    };
    
    if(image){
        
        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
        [library writeImageToSavedPhotosAlbum:[image CGImage]
                                  orientation:(ALAssetOrientation)[image imageOrientation]
                              completionBlock:completeBlock];
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(NSString *)splitImageName:(NSString *)imagePath{
    
    NSString *imageName;
    NSArray *imageArray = [imagePath componentsSeparatedByString:@"="];
    imageName = [imageArray objectAtIndex:1];
    
    //    NSLog(@"%@",imageName);
    
    return imageName;
}

- (IBAction)dropDownBTn:(id)sender {
    
    NSMutableArray *label_array = [[NSMutableArray alloc]initWithObjects:self.device,self.software,self.appVersion,self.topicInfo, nil];
    
    if (tabDropdown==NO)
    {
        tabDropdown = YES;
        
        for (UILabel *label in label_array) {
            [label setHidden:YES];
        }
    }else if (tabDropdown==YES)
    {
        tabDropdown = NO;
        
        for (UILabel *label in label_array) {
            [label setHidden:NO];
        }
    }
}

-(UIImage*)resizeImage:(UIImage*)image_resize withWidth:(int)width withHeight:(int)height
{
    CGSize newSize = CGSizeMake(width, height);
    float widthRatio = newSize.width/image_resize.size.width;
    float heightRatio = newSize.height/image_resize.size.height;
    
    if(widthRatio > heightRatio)
    {
        newSize=CGSizeMake(image.size.width*heightRatio,image.size.height*heightRatio);
    }
    else
    {
        newSize=CGSizeMake(image.size.width*widthRatio,image.size.height*widthRatio);
    }
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

-(void)set_initail_objects{
    
    tabDropdown = NO;
    selectedImage = NO;
    self.titleTxtfield.text = @"เลือกประเภทคำถาม";
    field_id = [[NSMutableArray alloc]init];
    
    [PCLabel set_underLine_text:self.telThe1Card text:CALL_CENTER_TEL textColor:[PCColor color_A80F1C]];
    
    [PCLabel set_underLine_text:self.emailThe1Card text:CALL_CENTER_EMAIL textColor:[PCColor color_A80F1C]];
    
    [self class_UI];
}

-(void)class_UI{
    
    // get type login for setting user info
    id emailInfo;
    NSUserDefaults *emailDefaults = [NSUserDefaults standardUserDefaults];
    
    if (emailDefaults){
        emailInfo = (id)[emailDefaults objectForKey:@"UserEmail"];
    }
    
    if (emailInfo!=nil)
    {
        [self.email setText:[NSString stringWithFormat:@"%@",emailInfo]];
    }
    else if(emailInfo==nil)
    {
        [self.emailTitle setHidden:YES];
        [self.email setHidden:YES];
        [self.heightT1c setConstant:25];
        [self.heightT1cTitle setConstant:25];
        [self.t1cTitle setFont:[UIFont fontWithName:@"RSU" size:18.0]];
        [self.userNumber setFont:[UIFont fontWithName:@"RSU" size:18.0]];
    }
    [self.userNumber setText:userNum];
    [PCShadow set_shadow:0.2 shadowRadius:2 shadowOpacity:0.5 view:self.uploadBtn];

    // set device info
    [self.device setText:[NSString stringWithFormat:@"%@",[BaseViewController getDeviceInfo][@"os-type"]]];
    [self.software setText:[NSString stringWithFormat:@"%@",[BaseViewController getDeviceInfo][@"os-version"]]];
    [self.appVersion setText:[NSString stringWithFormat:@"%@",[BaseViewController getDeviceInfo][@"version"]]];
    
    self.timeTxtfield.text = [NSString stringWithFormat:@"%@",[CurrentDate_Temperature get_current_time]];
    self.dateTxtfield.text = [NSString stringWithFormat:@"%@",[CurrentDate_Temperature get_date_format_dd_mm_yy]];
    
    [self.detailTextview.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.detailTextview.layer setBorderWidth:0.5];
    [self.detailTextview.layer setCornerRadius:6];
    
    [PCTextfield setpadding_textfield:self.dateTxtfield framLeft:25 framRight:0];
    [PCTextfield setpadding_textfield:self.timeTxtfield framLeft:25 framRight:0];
    
    titleProblem_pickker = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 180.0f)];
    titleProblem_pickker.delegate = (id)self;
    titleProblem_pickker.dataSource = (id)self;
    [titleProblem_pickker setShowsSelectionIndicator:YES];
    self.titleTxtfield.inputView = titleProblem_pickker;
    
    UIToolbar *toolBarP = [[UIToolbar alloc]initWithFrame:CGRectMake(0,0,DEVICE_WIDTH,44)];
    [toolBarP setTintColor:[UIColor grayColor]];
    UIBarButtonItem *doneBtnP = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(ShowSelectTitleProblem)];
    [doneBtnP setTintColor:[PCColor color_A8101D]];
    UIBarButtonItem *spaceP=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBarP setItems:[NSArray arrayWithObjects:spaceP,doneBtnP,nil]];
    [self.titleTxtfield setInputAccessoryView:toolBarP];
    
    type_pickker = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 180.0f)];
    type_pickker.delegate = (id)self;
    type_pickker.dataSource = (id)self;
    [type_pickker setShowsSelectionIndicator:YES];
    self.qTypeTxtfield.inputView = type_pickker;
    
    UIToolbar *toolBarT = [[UIToolbar alloc]initWithFrame:CGRectMake(0,0,DEVICE_WIDTH,44)];
    [toolBarT setTintColor:[UIColor grayColor]];
    UIBarButtonItem *doneBtnT = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(ShowSelectTopicProblem)];
    [doneBtnT setTintColor:[PCColor color_A8101D]];
    UIBarButtonItem *spaceT=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBarT setItems:[NSArray arrayWithObjects:spaceT,doneBtnT,nil]];
    [self.qTypeTxtfield setInputAccessoryView:toolBarP];
    
    // set datePicker
    datePicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 180.0f)];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.maximumDate = [NSDate date];
    
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"th_TH"];
    datePicker.locale = locale;
    datePicker.calendar = [locale objectForKey:NSLocaleCalendar];
    
    [self.dateTxtfield setInputView:datePicker];
    
    UIToolbar *toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0,0,DEVICE_WIDTH,44)];
    [toolBar setTintColor:[UIColor grayColor]];
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(ShowSelectedDate)];
    [doneBtn setTintColor:[PCColor color_A8101D]];
    UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:space,doneBtn,nil]];
    [self.dateTxtfield setInputAccessoryView:toolBar];
    
    // set timePicker
    timePicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 180.0f)];
    timePicker.datePickerMode = UIDatePickerModeTime;
    timePicker.maximumDate = [NSDate date];
    
    NSLocale *localeTime = [[NSLocale alloc] initWithLocaleIdentifier:@"th"];
    [timePicker setLocale:localeTime];
    
    [self.timeTxtfield setInputView:timePicker];
    
    UIToolbar *toolBar2 = [[UIToolbar alloc]initWithFrame:CGRectMake(0,0,DEVICE_WIDTH,44)];
    [toolBar2 setTintColor:[UIColor grayColor]];
    UIBarButtonItem *doneBtn2 = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(ShowSelectedTime)];
    [doneBtn2 setTintColor:[PCColor color_A8101D]];
    UIBarButtonItem *space2=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar2 setItems:[NSArray arrayWithObjects:space2,doneBtn2,nil]];
    [self.timeTxtfield setInputAccessoryView:toolBar2];
    
    // set tap @selector
    [PCTapGesture tapDismissKeyBoard:self view:self.view];
    
    // open telephone & email
    UITapGestureRecognizer* gotoLink1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTappedOnTel:)];
    UITapGestureRecognizer* gotoLink2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTappedOnEmail:)];
    
    [self.telThe1Card setUserInteractionEnabled:YES];
    [self.telThe1Card addGestureRecognizer:gotoLink1];
    
    [self.emailThe1Card setUserInteractionEnabled:YES];
    [self.emailThe1Card addGestureRecognizer:gotoLink2];
}

@end
