
//  Created by pimpaporn chaichompoo on 2/3/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

// This class is a pop up view for displaying coupon information.

#import <UIKit/UIKit.h>

@interface MenuCouponDetail : UIView{

    BOOL tabBarcodeBtn;
    BOOL tabQRcodeBtn;
    BOOL donationType;
    
    NSString *userToken;
}

@property (weak, nonatomic) IBOutlet UIImageView *pImage;
@property (weak, nonatomic) IBOutlet UIImageView *pLogo;
@property (weak, nonatomic) IBOutlet UILabel *pTitle;
@property (weak, nonatomic) IBOutlet UILabel *pDetail;
@property (weak, nonatomic) IBOutlet UIView *viewLogo;
@property (weak, nonatomic) IBOutlet UIButton *black_bg;

@property (weak, nonatomic) IBOutlet UIButton *shareBtn;
@property (weak, nonatomic) IBOutlet UIButton *ReadMoreBtn;
@property (weak, nonatomic) IBOutlet UIButton *usedPointBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UILabel *notAvailText;
@property (weak, nonatomic) IBOutlet UIImageView *pUsedLogo;

@property (weak, nonatomic) IBOutlet UIImageView *line;
@property (weak, nonatomic) IBOutlet UIImageView *lineHeader;
@property (weak, nonatomic) IBOutlet UIImageView *lineBottom;


-(void)setPromotionDetail:(NSString *)couponID title:(NSString *)title detail:(NSString *)detail point:(NSString *)point image:(NSString *)image logo:(NSString *)logo available:(NSString *)available dealOwner:(NSString *)dealOwner canUse:(NSString *)canUse;

-(void)setPromotionStatus:(NSString *)couponID title:(NSString *)title detail:(NSString *)detail point:(NSString *)point image:(NSString *)image logo:(NSString *)logo errorText:(NSString *)errorText;

@end
