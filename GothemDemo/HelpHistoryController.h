//
//  HelpHistryController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/8/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpHistoryController : BaseViewController{
    
    int problemId;
    NSArray *dateWithTime;
    NSArray *dateWithTimeAns;
}

// menu header

@property (weak, nonatomic) IBOutlet UIView *menuHeader;
@property (weak, nonatomic) IBOutlet UIView *statusBar;

@property (weak, nonatomic) IBOutlet UIImageView *qImage;
@property (weak, nonatomic) IBOutlet UIImageView *rImage;

@property (weak, nonatomic) IBOutlet UILabel *qStatus;
@property (weak, nonatomic) IBOutlet UILabel *qDate;
@property (weak, nonatomic) IBOutlet UILabel *qTime;
@property (weak, nonatomic) IBOutlet UILabel *qTitle;

@property (weak, nonatomic) IBOutlet UILabel *question;
@property (weak, nonatomic) IBOutlet UILabel *title_question;
@property (weak, nonatomic) IBOutlet UILabel *detail_question;

@property (weak, nonatomic) IBOutlet UILabel *rStatus;
@property (weak, nonatomic) IBOutlet UILabel *rDate;
@property (weak, nonatomic) IBOutlet UILabel *rTime;

@property (weak, nonatomic) IBOutlet UITextView *rDetail;

@property (weak, nonatomic) IBOutlet UIView *viewAnswer;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightLine;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *height_viewBg;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *height_details_answer;

@property (weak, nonatomic) IBOutlet UIImageView *line1;
@property (weak, nonatomic) IBOutlet UIImageView *line2;

@property (weak, nonatomic) IBOutlet UILabel *question_label;
@property (weak, nonatomic) IBOutlet UILabel *topic_label;
@property (weak, nonatomic) IBOutlet UILabel *question_detail;
@property (weak, nonatomic) IBOutlet UILabel *text_des;


-(void)problemId:(NSString *)problem_ID;

// ยังไม่เสร็จ
// รับ แค่ id แล้วเช็คว่า อันนี้มี feild ไหม ถ้าไม่มีแสดงว่า เป็นคำถาม ถ้ามี คือ เปนคำตอบ
/*
 
 "id": 53,
 "user_id": "16",
 "problem_datetime": "2016-03-25 11:08:00",
 "problem_subtype": "ล็อกอินไม่ได้",
 "problem_detail": "rtrstttty",
 "problem_attach": "",
 "response_at": null,
 "response_message": null,
 "device_manufacturer": "LGE",
 "device_name": "Nexus 5",
 "response_status": "",
 "problem_type": "ปัญหาการเชื่อมต่อ",
 "fields": []
 
 ----
 
 "id": 63,
 "user_id": "16",
 "problem_datetime": "2016-03-25 09:49:00",
 "problem_subtype": "แต้มหาย/แต้มไม่ถูกต้อง/ไม่ได้รับแต้ม",
 "problem_detail": "asasssskiyffh\njvhfinh\njgyhgy\njbhjh",
 "problem_attach": "63.jpg",
 "response_at": "2016-03-25 09:54:23",
 "response_message": "ตอบ กลับ\nทดสอบ\nหลายๆบรรทัด\nหลายๆบรรทัด\nหลายๆบรรทัด\nหลายๆบรรทัด",
 "device_manufacturer": "LGE",
 "device_name": "Nexus 5",
 "response_status": "",
 "problem_type": "ปัญหาการใช้งาน/ด้านบัญชี",
 "fields": [
 [
 "ชื่อโฆษณาที่เกิดปัญหา",
 "aa"
 ],
 [
 "จำนวนแต้ม",
 "bbbb"
 ]
 ]
 }
 
 */

@end
