//
//  PCLoadingView.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 4/5/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

// This class is used for creating activity indicator UI for using as a loading view on mutiply scene.

#import <UIKit/UIKit.h>

@interface PCLoadingView : UIView

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;

-(void)set_size_for_container_view;
-(void)set_gray_loading_color;
-(void)set_size_for_lockScreen_view;
-(void)set_size_for_share_view;

@end
