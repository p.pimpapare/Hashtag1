//
//  HistoryController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/7/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

#import <UIKit/UIKit.h>

@interface HistoryController : BaseViewController<UITableViewDataSource,UITableViewDelegate>{
    
    __weak IBOutlet UITableView *tableVieww;
    __weak IBOutlet UIView *warningBlank;
    
    id qaInfo;
        
    // set history variable
    NSMutableArray *problemID;
    NSMutableArray *problemText;
    NSMutableArray *problemTime;
    NSMutableArray *problemResponse;
    
    NSArray *dateWithTime;
}

@end
