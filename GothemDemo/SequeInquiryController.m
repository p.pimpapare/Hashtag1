//
//  SequeInquiryController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/30/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "SequeInquiryController.h"

@interface SequeInquiryController ()

@end

@implementation SequeInquiryController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self class_UI];
}

-(void)setMenuBar{
    selectedTab = 0;
    [self setCollection];
}

-(void)setCollection{
    
    [self.menuBar setFrame:CGRectMake(0,0,DEVICE_WIDTH,51)];
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    
    // set collection size between each other
    layout.minimumLineSpacing=-1;
    layout.minimumInteritemSpacing=0;
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    // set collection view from right to left
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    
    _collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0,0,self.menuBar.frame.size.width,self.menuBar.frame.size.height) collectionViewLayout:layout];
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [_collectionView setScrollEnabled:NO];
    [_collectionView setBackgroundColor:[PCColor color_F0F0F1]];
    
    [self.menuBar addSubview:_collectionView];
    
    // register collectionView & tableView
    [_collectionView registerClass:[History_CollectionCell class] forCellWithReuseIdentifier:@"Cell"];
    [_collectionView registerNib:[UINib nibWithNibName:@"History_CollectionCell" bundle:nil]  forCellWithReuseIdentifier:@"Cell"];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [titleMenu count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    History_CollectionCell *cell=(History_CollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    if(cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"History_CollectionCell" owner:self options:nil][0];
    }
    // การ check menu bar
    if([swiptTab[indexPath.row]isEqualToString:@"0"]){
        [cell setselectedColor:0];
    }else{
        [cell setselectedColor:1];
    }
    [cell setHistoryCell:[NSString stringWithFormat:@"%@",titleMenu[indexPath.row]]];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(DEVICE_WIDTH/2+1,51);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    selectedTab = (int)indexPath.row;
    
    switch (selectedTab) {
        case 0:[self moveToViewControllerAtIndex:0 animated:YES]; break;
        case 1:[self moveToViewControllerAtIndex:1 animated:YES];break;
        default: break;
    }
    [_collectionView reloadData];
}
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0,0,0,0);
}

// set button bar
#pragma mark - XLPagerTabStripViewControllerDataSource

-(NSString *)titleForPagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController{
    return @"View";
}

-(UIColor *)colorForPagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController{
    return [UIColor whiteColor];
}

-(NSArray *)childViewControllersForPagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController{
    
    //     create child view controllers that will be managed by XLPagerTabStripViewController
    InquiryController * child_1 = (InquiryController *)[[NSBundle mainBundle] loadNibNamed:@"InquiryController" owner:self options:nil][0];
    HistoryController * child_2 = (HistoryController *)[[NSBundle mainBundle] loadNibNamed:@"HistoryController" owner:self options:nil][0];
    
    
    if (!_isReload){
        return @[child_1, child_2];
    }
    
    NSMutableArray * childViewControllers = [NSMutableArray arrayWithObjects:child_1, child_2, nil];
    NSUInteger count = [childViewControllers count];
    
    for (NSUInteger i = 0; i < count; ++i) {
        // Select a random element between i and end of array to swap with.
        NSUInteger nElements = count - i;
        NSUInteger n = (arc4random() % nElements) + i;
        [childViewControllers exchangeObjectAtIndex:i withObjectAtIndex:n];
    }
    
    NSUInteger nItems = 1 + (rand() % 4);
    return [childViewControllers subarrayWithRange:NSMakeRange(0, nItems)];
}

-(void)reloadPagerTabStripView{
    _isReload = YES;
    self.isProgressiveIndicator = (rand() % 2 == 0);
    self.isElasticIndicatorLimit = (rand() % 2 == 0);;
    [super reloadPagerTabStripView];
}

-(void)pagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController updateIndicatorFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex{
    
    for (int i=0;i<[swiptTab count];i++) {
        
        if (toIndex==i)
        {
            [swiptTab replaceObjectAtIndex:i withObject:@"1"];
        }
        else
        {
            [swiptTab replaceObjectAtIndex:i withObject:@"0"];
        }
        
    }
    [_collectionView reloadData];
}
// end for setting bar button

-(void)class_UI{
    
    [self.barView.selectedBar setBackgroundColor:[UIColor blackColor]];
    
    swiptTab = [[NSMutableArray alloc]initWithObjects:@"1",@"0", nil];
    titleMenu = @[@"สอบถาม",@"ประวัติการสอบถาม"];
    [self setMenuBar];
}

@end
