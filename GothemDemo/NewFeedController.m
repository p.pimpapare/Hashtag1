
//  Created by pimpaporn chaichompoo on 2/11/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "NewFeedController.h"
#import "ContainerViewController.h"
#import "ASStandardLibrary.h"

@interface NewFeedController (){
    
    BOOL time;
    BOOL loading_service;
}
@end

@implementation NewFeedController
@synthesize navBar;


-(void)viewWillAppear:(BOOL)animated{
    
    GOOGLE_ANALYTICS(@"NewFeedController");
    
    //#### UI
    [self.temp_c setHidden:NO];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    
    NSString *checkTime = [NSString stringWithFormat:@"%@",(id)[[NSUserDefaults standardUserDefaults] objectForKey:@"TimesAccess_ForNewsfeed"]];
    NSString *currentTime = [NSString stringWithFormat:@"%@", [CurrentDate_Temperature get_date_format_dd_mm_yy]];
    
    if (![checkTime isEqualToString:currentTime]) {
        
        [self getCurrentLocation];
        [[NSUserDefaults standardUserDefaults] setValue:[CurrentDate_Temperature get_date_format_dd_mm_yy] forKey:@"TimesAccess_ForNewsfeed"];
        
    }else{
        
        GET_USER_DEFAULT_INFO(@"Current_Location")
        
        [self.currentPlace setText:[NSString stringWithFormat:@"%@",userInfo]];
        
        if (userDefaults) {
            
            [self.temperature setText:[NSString stringWithFormat:@"%@",(id)[userDefaults objectForKey:@"Current_Temp"]]];
            
            if ([self.temperature.text isEqualToString:@" "]) {
                [self.temp_c setHidden:YES];
            }
        }
    }
}

- (void)viewDidLoad {
    
    [super viewDidLoad];

    [self.loadingView startAnimating];
    [self.loadingView setHidden:NO];
    [self.btn_back setHidden:YES];

    [self setHiddenPCBlack:NO];
    
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    [self.tableView setSeparatorColor:[UIColor clearColor]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notification_alert:) name:@"selected_newsfeed" object:nil];
    
    [self set_initail_objects];
}

-(void)set_newsfeed_1{
    
    GET_USER_DEFAULT_INFO(@"Newsfeed_1")
    
    itemGuid = [[NSMutableArray alloc]init];
    title = [[NSMutableArray alloc]init];
    thumbnail = [[NSMutableArray alloc]init];
    
    for (int i=0; i<[userInfo[@"feed"] count];i++) {
        
        [itemGuid addObject:userInfo[@"feed"][i][@"link"]];
        [title addObject:userInfo[@"feed"][i][@"title"]];
        [thumbnail addObject:userInfo[@"feed"][i][@"thumbnail"]];
    }
    
    if (userInfo!=nil) {
        
        [self.tableView reloadData];
    }
    [self.loadingView setHidden:YES];
    [self.btn_back setHidden:NO];
    [self.loadingView stopAnimating];
}

-(void)set_newsfeed_2{
    
    GET_USER_DEFAULT_INFO(@"Newsfeed_2")
    
    item_id2 = [[NSMutableArray alloc]init];
    itemGuid2 = [[NSMutableArray alloc]init];
    title2 = [[NSMutableArray alloc]init];
    thumbnail2 = [[NSMutableArray alloc]init];
    
    imageUrl2 = [NSString stringWithFormat:@"%@",userInfo[@"imageurl"]];
    
    for (int i=0; i<[userInfo[@"feed"] count];i++) {
        
        [item_id2 addObject:userInfo[@"feed"][i][@"item_id"]];
        [itemGuid2 addObject:userInfo[@"feed"][i][@"link"]];
        [title2 addObject:userInfo[@"feed"][i][@"title"]];
        [thumbnail2 addObject:userInfo[@"feed"][i][@"thumbnail"]];
    }
    
    if (userInfo!=nil) {
        
        [self.tableView reloadData];
    }
    [self.loadingView setHidden:YES];
    [self.btn_back setHidden:NO];
    [self.loadingView stopAnimating];
}

-(void)set_newsfeed_3{
    
    GET_USER_DEFAULT_INFO(@"Newsfeed_3")
    
    item_id3 = [[NSMutableArray alloc]init];
    itemGuid3 = [[NSMutableArray alloc]init];
    title3 = [[NSMutableArray alloc]init];
    thumbnail3 = [[NSMutableArray alloc]init];
    
    imageUrl3 = [NSString stringWithFormat:@"%@",userInfo[@"imageurl"]];
    
    for (int i=0; i<[userInfo[@"feed"] count];i++) {
        
        [item_id3 addObject:userInfo[@"feed"][i][@"item_id"]];
        [itemGuid3 addObject:userInfo[@"feed"][i][@"link"]];
        [title3 addObject:userInfo[@"feed"][i][@"title"]];
        [thumbnail3 addObject:userInfo[@"feed"][i][@"thumbnail"]];
    }
    
    if (userInfo!=nil) {
        
        [self.tableView reloadData];
    }
    [self.loadingView setHidden:YES];
    [self.btn_back setHidden:NO];
    [self.loadingView stopAnimating];
}

-(void)getCurrentLocation{
    
    // get the current location latitude and longitude
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [locationManager requestWhenInUseAuthorization];
    
    [locationManager startUpdatingLocation];
    
    CLLocation *location = [locationManager location];
    locationManager.delegate=self;
    
    [self setTemp:location];
}

// Called when location update
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    
    //    NSLog(@"OldLocation %f %f", oldLocation.coordinate.latitude, oldLocation.coordinate.longitude);
    //    NSLog(@"NewLocation %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
    
    
    if (newLocation.coordinate.latitude!=0.0) {
        
        if (time==NO) {
            
            [self setTemp:newLocation];
            [locationManager stopUpdatingLocation]; // เพื่อให้มันไม่ต้องอัพเดท location ตลอด ซึ่งเป็นสาเหตุที่ทำให้เครื่องช้า
            time = YES;
        }
    }
}

-(void)setTemp:(CLLocation *)location{
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    
    NSMutableArray *userDefaultLanguages = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
    [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"th", nil] forKey:@"AppleLanguages"];
    
    [geocoder reverseGeocodeLocation:location
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                       //                       NSLog(@"reverseGeocodeLocation:completionHandler: Completion Handler called!");
                       
                       if (error){
                           //                           NSLog(@"Geocode failed with error: %@", error);
                           //                           return;
                           
                       }
                       
                       CLPlacemark *placemark = [placemarks objectAtIndex:0];
                       
                       //                       NSLog(@"placemark.ISOcountryCode %@",placemark.ISOcountryCode);
                       //                       NSLog(@"placemark.country %@",placemark.country);
                       //                       NSLog(@"placemark.postalCode %@",placemark.postalCode);
                       //                       NSLog(@"placemark.administrativeArea %@",placemark.administrativeArea);
                       
                       
                       if (placemark.administrativeArea==nil) {
                           
                           if (oldLocation==nil) {
                               
                               [self.currentPlace setText:@" "];
                               
                               SET_USER_DEFAULT_INFO(@" ",@"Current_Location")
                               
                               if (userDefaults) {
                                   [userDefaults setObject:@" "forKey:@"Current_Temp"];
                               }
                               
                               [self.temperature setText:@" "];
                               [self.temp_c setHidden:YES];
                               
                           }
                           else{
                               [self.currentPlace setText:[NSString stringWithFormat:@"%@",oldLocation]];
                           }
                           
                           
                       }else{
                           
                           [self.temp_c setHidden:NO];
                           
                           oldLocation = [NSString stringWithFormat:@"%@",placemark.administrativeArea]; // Used when network error causes it can't get location.
                           
                           [self.currentPlace setText:[NSString stringWithFormat:@"%@",placemark.administrativeArea]];
                           
                           SET_USER_DEFAULT_INFO(placemark.administrativeArea,@"Current_Location")
                           
                           [self.temperature setText:[NSString stringWithFormat:@"%@",[CurrentDate_Temperature get_current_temp:placemark.administrativeArea]]];
                           
                           if (userDefaults) {
                               [userDefaults setObject:[CurrentDate_Temperature get_current_temp:placemark.administrativeArea] forKey:@"Current_Temp"];
                           }
                       }
                       
                       //                       NSLog(@"placemark.locality %@",placemark.locality);
                       //                       NSLog(@"placemark.subLocality %@",placemark.subLocality);
                       //                       NSLog(@"placemark.subThoroughfare %@",placemark.subThoroughfare);
                   }];
    [[NSUserDefaults standardUserDefaults] setObject:userDefaultLanguages forKey:@"AppleLanguages"];
}

-(void)getService_fromNewFeedDefualts1:(NSString *)token{
    
    [Service get_news_feed:token typeNews:@"1" selfID:self block:^(id result) {
        
        NSString *resultText = [NSString stringWithFormat:@"%@",result];
        
        if (![resultText isEqualToString:@"error"]) {
            
            SET_USER_DEFAULT_INFO(result,@"Newsfeed_1")
            
            itemGuid = [[NSMutableArray alloc]init];
            title = [[NSMutableArray alloc]init];
            thumbnail = [[NSMutableArray alloc]init];
            
            for (int i=0; i<[result[@"feed"] count];i++) {
                
                [itemGuid addObject:result[@"feed"][i][@"link"]];
                [title addObject:result[@"feed"][i][@"title"]];
                [thumbnail addObject:result[@"feed"][i][@"thumbnail"]];
            }
            
            finisedLoadType1 = YES;
            
            if (result!=nil) {
                
                [self.tableView reloadData];
            }
            
            [self.loadingView setHidden:YES];
            [self.btn_back setHidden:NO];
            [self.loadingView stopAnimating];
        }
    }];
}

-(void)getService_fromNewFeedDefualts2:(NSString *)token{
    
    [Service get_news_feed:token typeNews:@"2" selfID:self block:^(id result) {
        
        NSString *resultText = [NSString stringWithFormat:@"%@",result];
        
        if (![resultText isEqualToString:@"error"]) {
            
            SET_USER_DEFAULT_INFO(result,@"Newsfeed_2")
            
            item_id2 = [[NSMutableArray alloc]init];
            itemGuid2 = [[NSMutableArray alloc]init];
            title2 = [[NSMutableArray alloc]init];
            thumbnail2 = [[NSMutableArray alloc]init];
            
            imageUrl2 = [NSString stringWithFormat:@"%@",result[@"imageurl"]];
            
            for (int i=0; i<[result[@"feed"] count];i++) {
                
                [item_id2 addObject:result[@"feed"][i][@"item_id"]];
                [itemGuid2 addObject:result[@"feed"][i][@"link"]];
                [title2 addObject:result[@"feed"][i][@"title"]];
                [thumbnail2 addObject:result[@"feed"][i][@"thumbnail"]];
            }
            
            finisedLoadType2 = YES;
            
            if (result!=nil) {
                [self.tableView reloadData];
            }
            
            [self.loadingView setHidden:YES];
            [self.btn_back setHidden:NO];
            [self.loadingView stopAnimating];
        }
    }];
}

-(void)getService_fromNewFeedDefualts3:(NSString *)token{
    
    [Service get_news_feed:token typeNews:@"3" selfID:self block:^(id result) {
        
        NSString *resultText = [NSString stringWithFormat:@"%@",result];
        
        if (![resultText isEqualToString:@"error"]) {
            
            SET_USER_DEFAULT_INFO(result,@"Newsfeed_3")
            
            item_id3 = [[NSMutableArray alloc]init];
            itemGuid3 = [[NSMutableArray alloc]init];
            title3 = [[NSMutableArray alloc]init];
            thumbnail3 = [[NSMutableArray alloc]init];
            
            imageUrl3 = [NSString stringWithFormat:@"%@",result[@"imageurl"]];
            
            for (int i=0; i<[result[@"feed"] count];i++) {
                
                [item_id3 addObject:result[@"feed"][i][@"item_id"]];
                [itemGuid3 addObject:result[@"feed"][i][@"link"]];
                [title3 addObject:result[@"feed"][i][@"title"]];
                [thumbnail3 addObject:result[@"feed"][i][@"thumbnail"]];
            }
            finisedLoadType3 = YES;
            
            if (result!=nil) {
                [self.tableView reloadData];
            }
            
            [self.loadingView setHidden:YES];
            [self.btn_back setHidden:NO];
            [self.loadingView stopAnimating];
        }
        
        loading_service = YES;
    }];
}

-(void)setCollectionView{
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    layout.minimumLineSpacing=0;
    layout.minimumInteritemSpacing=0;
    layout.sectionInset = UIEdgeInsetsMake(0,0,0,0);
    
    // set collection view from right to left
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    
    _collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0,0,DEVICE_WIDTH,45) collectionViewLayout:layout];
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    [_collectionView setScrollEnabled:NO];
    [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [_collectionView setBackgroundColor:[PCColor color_323232]];
    
    [scrollView addSubview:_collectionView];
    [scrollView setContentSize:_collectionView.frame.size];
    
    // register collectionView
    [_collectionView registerClass:[PCNewFeedBarCollectionCell class] forCellWithReuseIdentifier:@"Cell"];
    [_collectionView registerNib:[UINib nibWithNibName:@"PCNewFeedBarCollectionCell" bundle:nil]  forCellWithReuseIdentifier:@"Cell"];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [newfeedMenu count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    PCNewFeedBarCollectionCell *cell=(PCNewFeedBarCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    if(cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"PCCollectionCell" owner:self options:nil][0];
    }
    
    if(selectedTabMenu==indexPath.row){
        
        [cell setNewFeedBar:newfeedMenu[indexPath.row]];
        [cell setTitleColor:@"white"];
        
    }else{
        
        [cell setNewFeedBar:newfeedMenu[indexPath.row]];
        [cell setTitleColor:@"gray"];
    }
    
    if (indexPath.row==2) {
        
        [cell deleteLine];
        
    }
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(DEVICE_WIDTH/3+2,45);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    selectedTabMenu = (int)indexPath.row;
    
    if (selectedTabMenu==0) {
        
    }else if (selectedTabMenu==1){
        
    }else if (selectedTabMenu==2){
        
    }
    
    [self.tableView reloadData];
    [_collectionView reloadData];
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0,0,0,0);
}

// Set tableView delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    int selectedRow = (int)indexPath.row;
    //    NSLog(@"%d",selectedRow);
    
    if (selectedTabMenu==0) {
        
        NSArray *splitNews = [itemGuid[selectedRow] componentsSeparatedByString:@"|"];
        
        if ([splitNews count]==1) {
            
            [self goToNewfeedWeb:[NSString stringWithFormat:@"%@",[ReplaceHTMLFont replaceHTML_blank_space:itemGuid[selectedRow]]]];
            
        }else{
            
            NSString *news = [splitNews objectAtIndex:1];
            [self goToNewfeedWeb:[NSString stringWithFormat:@"%@",news]];
        }
        
    }else if (selectedTabMenu==1){
        
        [self goToNewfeedWeb:[NSString stringWithFormat:@"%@feed2/%@/detail",BASEURL,item_id2[selectedRow]]];
        
    }else if (selectedTabMenu==2){
        
        [self goToNewfeedWeb:[NSString stringWithFormat:@"%@feed2/%@/detail",BASEURL,item_id3[selectedRow]]];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (selectedTabMenu==0) {
        
        if ([title count]<=0) {
            
            if (finisedLoadType1==YES) {
                [self.viewError setHidden:NO];
            }
            return 0;
        }else{
            [self.viewError setHidden:YES];
            return [title count];
        }
        
    }else if (selectedTabMenu==1){
        
        if ([title2 count]<=0) {
            
            if (finisedLoadType2==YES) {
                [self.viewError setHidden:NO];
            }
            return 0;
        }else{
            [self.viewError setHidden:YES];
            return [title2 count];
        }
        
    }else{
        
        if ([title3 count]<=0) {
            
            if (finisedLoadType3==YES) {
                [self.viewError setHidden:NO];
            }
            return 0;
        }else{
            [self.viewError setHidden:YES];
            return [title3 count];
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NewFeedTableViewCell *cell = (NewFeedTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if(cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"NewFeedTableViewCell" owner:self options:nil][0];
    }
    
    if (selectedTabMenu==0) {
        [cell setNewFeedTableCell:title[indexPath.row]];
        [cell.image sd_setImageWithURL:[NSURL URLWithString:thumbnail[indexPath.row]]
                      placeholderImage:[UIImage imageNamed:@"picture_1_1"]options:SDWebImageProgressiveDownload];
    }else if (selectedTabMenu==1){
        [cell setNewFeedTableCell:title2[indexPath.row]];
        [cell.image sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",imageUrl2,thumbnail2[indexPath.row]]]
                      placeholderImage:[UIImage imageNamed:@"picture_1_1"]options:SDWebImageProgressiveDownload];
    }else{
        [cell setNewFeedTableCell:title3[indexPath.row]];
        [cell.image sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",imageUrl3,thumbnail3[indexPath.row]]]
                      placeholderImage:[UIImage imageNamed:@"picture_1_1"]options:SDWebImageProgressiveDownload];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return [[[NSBundle mainBundle] loadNibNamed:@"NewFeedTableViewCell" owner:self options:nil][0] frame].size.height;
}

-(void)set_initail_objects{
    
    selectedTabMenu = 0;
    newfeedMenu = @[@"ข่าวฮอต",@"เดอะวันคาร์ด",@"ประกาศ"];
    
    [self class_UI];
}

-(void)class_UI{
    
    TABLEVIEW_COLLECTION_REFRESH(self.tableView);
    [self.statusBar setBackgroundColor:[PCColor color_F0F0F1]];
    [self.tableView setBackgroundColor:[UIColor whiteColor]];
    
    
    if (IS_IPAD==YES) {
        
        [self.btn_newsfeed_center setConstant:-127];
        
    }else{
        
        if (DEVICE_WIDTH>=PHONEPLUS) {
            [self.heightButton setConstant:125];
            [self.widthButton setConstant:32];
        }
    }
    NSString *daytxt = [NSString stringWithFormat:@", %@",[CurrentDate_Temperature get_day_name]];
    
    if([daytxt isEqualToString:@"(null)"]){
        
        [self.day setText:@" "];
        
    }else{
        [self.day setText:daytxt];
    }
    
    [self setCollectionView];
}

- (void)refresh:(UIRefreshControl *)refreshControl {
    
    if (selectedTabMenu==0) {
        [self getService_fromNewFeedDefualts1:[self getUserToken]];
    }else if (selectedTabMenu==1){
        [self getService_fromNewFeedDefualts2:[self getUserToken]];
    }else if (selectedTabMenu==2){
        [self getService_fromNewFeedDefualts3:[self getUserToken]];
    }
    [refreshControl endRefreshing];
}

-(void)goToNewfeedWeb:(NSString *)urlStr{
    
    NewFeedWebViewController *news = [[NSBundle mainBundle] loadNibNamed:@"NewFeedWebViewController" owner:self options:nil][0];
    [news openUrl:[NSString stringWithFormat:@"%@",urlStr]];
    [news.view setFrame:CGRectMake(20,0,DEVICE_WIDTH+20,DEVICE_HEIGHT)];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.view addSubview:news.view];
        [self addChildViewController:news];
        [news didMoveToParentViewController:self];
    });
}

- (IBAction)backBtn:(id)sender {
    
    if (self.view) {
        
        [UIView animateWithDuration:0.4 animations:^{
            [self.view setFrameLeft:DEVICE_W-8];
        } completion:^(BOOL finished) {
        }];
        
    }else{
    }
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)notification_alert:(NSNotification *)noti{
    
    NSString *strNoti = [noti object];
    
    if ([strNoti isEqualToString:@"selected"]) {
        
        if(loading_service==NO){
            
            [self getService_fromNewFeedDefualts1:[self getUserToken]];
            [self getService_fromNewFeedDefualts2:[self getUserToken]];
            [self getService_fromNewFeedDefualts3:[self getUserToken]];
            
        }else{
            [self set_newsfeed_1];
            [self set_newsfeed_2];
            [self set_newsfeed_3];
        }
        
        [self.loadingView setHidden:YES];
        [self.btn_back setHidden:NO];
        [self.loadingView stopAnimating];
        
    }else if([strNoti isEqualToString:@"set_ui"]){
        
        GET_USER_DEFAULT_INFO(@"InLockscreen")
        
        NSString *result_text = [NSString stringWithFormat:@"%@",userInfo];
        
        if ([result_text isEqualToString:@"NO"]) {
            [self.top_line_header setConstant:20];
        }else{
            [self.top_line_header setConstant:0];
        }
        
    }
}

@end
