
//  Created by pimpaporn chaichompoo on 2/15/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

#import <UIKit/UIKit.h>

@interface NotReadyAlert : UIView

@property (weak, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UILabel *txtWarning;

-(void)setTextWarning:(NSString *)titleHeadertext;

@end
