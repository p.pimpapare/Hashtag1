//
//  Service.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/25/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

#import <Foundation/Foundation.h>
@class Service;

typedef void(^BlockUserInfo)(id result);
typedef void(^BlockProduct)(id result);

@interface Service : NSObject

@property (nonatomic, strong)BlockUserInfo ResultObject1;
@property (nonatomic, strong)BlockProduct ResultObject2;


/*
 *
 * Service Authentication Feature
 *
 */


// This service is allow the user to login with their ID (user's the1card number, user's tel) and Login with their email & Log out

+(void)post_user_login_with_ID:(NSString *)userId deviceToken:(NSString *)deviceToken ref_code:(NSString *)ref_code selfID:(UIViewController*)selfID block:(BlockUserInfo)result;
+(void)post_user_login_with_Email:(NSString *)email password:(NSString *)password ref_code:(NSString *)ref_code deviceToken:(NSString *)deviceToken selfID:(UIViewController*)selfID block:(BlockUserInfo)result;

+(void)post_user_logout:(NSString *)token selfID:(UIViewController*)selfID block:(BlockUserInfo)result;


// This service is used for getting user's information

+(void)get_user_info:(NSString *)token selfID:(UIViewController*)selfID block:(BlockUserInfo)result;

// This service is used for getting user accepted information

+(void)post_user_acception:(NSString *)token selfID:(UIViewController*)selfID block:(BlockUserInfo)result;


/*
 *
 * Service Lockscreen Feature
 *
 */


// Service ads_update for checking service ads_info that it should be updated(loaded) or not?
+(void)get_ads_update:(NSString *)token date_time:(NSString *)date_time block:(BlockProduct)result;

// Service ads's info (artwork data, image ads,and icon artwork) -> LockScreen || MediaScreen
+(void)get_ads_info:(NSString *)token block:(BlockProduct)result;

// Service ads's info for getting collect_type and response package that is used in service collect_point
+(void)post_package_response:(NSString *)token package:(NSString *)packageID direction:(NSString *)direction before_packageID:(NSString *)beforePackageID viewTime:(NSString *)view_time selfID:(UIViewController*)selfID block:(BlockUserInfo)result;

// Service collect point for collecting ads's point
+(void)post_collect_point:(NSString *)token responseToken:(NSString *)response_token unlock_type:(NSString *)unlock_type selfID:(UIViewController*)selfID block:(BlockProduct)result;

// Service unlock used for sending response package key to the CMS
+(void)post_unlock:(NSString *)token responseToken:(NSString *)response_token selfID:(UIViewController*)selfID block:(BlockProduct)result;


/*
 *
 * Lucky Point Feature
 *
 */


// This service is used for getting lucky point's information for showing on menu 1 (สิทธิ์สะสม)
+(void)get_lucky_point_html:(UIViewController *)selfID block:(BlockProduct)result;


/*
 *
 * Service Version Update and App invited
 *
 */


// This service is used for checking application version
+(void)get_update_version:(NSString *)token selfID:(UIViewController*)selfID block:(BlockProduct)result;

// This service is used for getting invite's url link from the CMS
+(void)get_app_url_invite:(NSString *)token selfID:(UIViewController*)selfID block:(BlockProduct)result;


/*
 *
 * Service Banner
 *
 */


// This service is used for getting banner's image that is used on menu 1, 2,and 3
+(void)get_banner:(NSString *)token brannerId:(int)branID selfID:(UIViewController*)selfID block:(BlockProduct)result;


/*
 *
 * Service Coupon
 *
 */


// menu 1
// This service is used for getting hotpromotion's coupon on menu 1 (โปรโมชั่นพิเศษ)
+(void)get_hot_promotion:(NSString *)token selfID:(UIViewController*)selfID block:(BlockProduct)result;


// menu 2
// This service is used for getting categoty's name and image for displaying on menu 2
+(void)get_category:(NSString *)token selfID:(UIViewController*)selfID block:(BlockProduct)result;

// This service is used for getting category's brand grid after user tapped on category in menu 2
+(void)get_category_brand_grid:(NSString *)token cID:(NSString *)category_ID selfID:(UIViewController*)selfID block:(BlockProduct)result;

// This service is used for getting list of coupon after user tapped on category's brand grid in menu 2
+(void)get_coupon_from_brand_grid:(NSString *)token bID:(NSString *)brand_ID selfID:(UIViewController*)selfID block:(BlockProduct)result;

// This service is used for getting coupon status such as คูปองหมดอายุแล้ว
+(void)get_coupon_status:(NSString *)token couponID:(NSString *)couponID selfID:(UIViewController*)selfID block:(BlockProduct)result;

// This service is used for getting share's url of coupon
+(void)getShareUrl:(NSString *)token couponID:(NSString *)couponID selfID:(UIViewController*)selfID block:(BlockProduct)result;

// This service is used for getting coupon's details (รายละเอียด)
+(void)get_coupon_detail:(NSString *)token couponID:(NSString *)couponID selfID:(UIViewController*)selfID block:(BlockProduct)result;

// This service is used when user want to use coupon on menu coupon (คูปอง), but this service is unused yet
+(void)post_user_use_my_coupon:(NSString *)token myCouponID:(NSString *)couponID selfID:(UIViewController*)selfID block:(BlockProduct)result;

// Unused
+(void)get_coupon_from_CID:(NSString *)token cID:(int)c_ID selfID:(UIViewController*)selfID block:(BlockProduct)result;


/*
 *
 * Service User's history
 *
 */


// This service is used for getting user's coupon on menu 1 and 3 (คูปอง)
+(void)get_my_coupon:(NSString *)token selfID:(UIViewController*)selfID block:(BlockProduct)result;

// This service is used for getting user's lucky point history on menu 1 and 3 (ประวัติการรับคะแนน/สิทธิ์สะสม)
+(void)get_lucky_point_history:(UIViewController *)selfID block:(BlockProduct)result;

// This service is used for getting user's lucky point history on menu 1 and 3 (ประวัติการรับคะแนน/คะแนน #1)
+(void)get_user_get_point_history:(NSString *)token selfID:(UIViewController*)selfID block:(BlockProduct)result;

+(void)get_user_use_point_history:(NSString *)token offset:(int)offset type:(int)type selfID:(UIViewController*)selfID block:(BlockProduct)result;

// This service is used for getting user's used coupon on menu 1 and 3 (ประวัติการแลกคะแนน)
+(void)get_user_use_coupon:(NSString *)token email:(NSString *)email password:(NSString *)password couponID:(NSString *)couponID use:(int)use selfID:(UIViewController*)selfID block:(BlockProduct)result;


/*
 *
 * Service Setting feature
 *
 */

// Inquiry feature
// This service is used for getting inquiry's type title such as (เลือกประเภืคำถาม/ปัญหาการเชื่อมต่อ/ล็อคอินหลุด)
+(void)get_ingury_type:(NSString *)token selfID:(UIViewController*)selfID block:(BlockProduct)result;

// This service is used for getting list of user's inquiry history
+(void)get_user_ingury_history:(NSString *)token offset:(int)offset selfID:(UIViewController*)selfID block:(BlockProduct)result;

// This service is used for getting user's inquiry history details
+(void)get_user_ingury_history_detail:(NSString *)token id:(int)problem_id selfID:(UIViewController*)selfID block:(BlockProduct)result;

// This service is used for posting user's inquiry informations
+(void)post_inquiry:(NSString *)token problemDateTime:(NSString *)date_time problemSubtype:(NSString *)subtype problemDetails:(NSString *)details feilds:(NSMutableString *)feilds deviceManufacturer:(NSString *)device_menufaceturer deviceName:(NSString *)device_name imageFile:(NSData *)image_file selfID:(UIViewController*)selfID block:(BlockProduct)result;

// Question and Answer feature
// This service is used for getting user's question and answer
+(void)get_QA:(NSString *)token selfID:(UIViewController*)selfID block:(BlockProduct)result;

// About us feature
// This service is used for getting HTML usage agreement's information
+(void)get_usage_agreement:(UIViewController*)selfID block:(BlockProduct)result;

// This service is used for getting HTML usage privacy policy's information
+(void)get_usage_privacy_policy:(UIViewController*)selfID block:(BlockProduct)result;


/*
 *
 * Service Newsfeed
 *
 */


// This service is used for getting list of newsfeed
+(void)get_news_feed:(NSString *)token typeNews:(NSString *)newFeed selfID:(UIViewController*)selfID block:(BlockProduct)result;

// Unused
+(void)getIntroductionInfo:(NSString *)token block:(BlockProduct)result;

@end
