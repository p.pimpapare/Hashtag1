//
//  PCMenubarCell.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 4/4/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "PCMenubarCell.h"

@implementation PCMenubarCell

-(void)setImageMenuBarCell:(NSString *)image receiveIndex:(int)index_selected{
    
    [self.backgroundView setHidden:NO];
    
    if(index_selected==0){
        
        [self.image_menu1 setHidden:NO];
        [self.image_menu setHidden:YES];
        [self.image_menu3 setHidden:YES];
        
    }else if(index_selected==3){
        
        [self.image_menu3 setHidden:NO];
        [self.image_menu1 setHidden:YES];
        [self.image_menu setHidden:YES];
        [self.image_menu3 setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",image]]];
        
    }else{

        [self.image_menu setHidden:NO];
        [self.image_menu1 setHidden:YES];
        [self.image_menu3 setHidden:YES];
        [self.image_menu setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",image]]];
    }
    
    [self.view_selected setBackgroundColor:[UIColor blackColor]];
}

-(void)setColorSelected:(int)color{

    [self.view_selected setHidden:NO];
    [self.backgroundView setHidden:YES];
    
    (color==0)?[self.view_selected setBackgroundColor:[UIColor blackColor]]:[self.view_selected setBackgroundColor:[UIColor clearColor]];    
}
@end
