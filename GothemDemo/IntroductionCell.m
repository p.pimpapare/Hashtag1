//
//  IntroductionCell.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/9/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "IntroductionCell.h"

@implementation IntroductionCell

-(void)setIntroImage:(NSString *)url imageC:(NSString *)imageC index:(int)indexImage{

    self.menuNum = indexImage;
    
    [self.imageCell sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",url,imageC]]
                      placeholderImage:[UIImage imageNamed:@" "]options:SDWebImageProgressiveDownload];
}

@end
