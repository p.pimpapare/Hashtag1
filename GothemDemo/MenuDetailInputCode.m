//
//  MenuDetailUsedCoupon.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/17/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "MenuDetailInputCode.h"

@implementation MenuDetailInputCode

-(void)awakeFromNib{

    [super awakeFromNib];

    GET_USER_DEFAULT_INFO(@"UserEmail")

    [self.emailLabel setText:[NSString stringWithFormat:@"%@",userInfo]];
    
    [self setFrame:CGRectMake(0,0,DEVICE_WIDTH,DEVICE_HEIGHT)];
    
    [BaseViewController setLineColor:self.lineHeader bottom:self.lineBottom];
    [self.lineGray setBackgroundColor:[PCColor color_A6A6A6]];
    
    [PCTextfield setpadding_textfield:self.codeTextfeild framLeft:15 framRight:0];
    [PCTapGesture tapDismissKeyBoard:(id)self view:self];
}

-(void)dismissKeyboard {
    
    [self endEditing:YES];
}

-(void)set_logo_coupon:(NSString *)logoImage{
    
    [self.logo sd_setImageWithURL:[NSURL URLWithString:logoImage]
                 placeholderImage:[UIImage imageNamed:@"picture_1_2"]options:SDWebImageProgressiveDownload];
}

@end
