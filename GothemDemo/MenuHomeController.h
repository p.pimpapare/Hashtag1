//
//  MenuHomeController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/19/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

// This class is a base class of menu 1

#import <UIKit/UIKit.h>

@interface MenuHomeController : BaseViewController{
    
    NSMutableArray *advertising_array;
    NSString *currentVersion_Str;
    NSString *update_type;
    NSString *detailVersion_Str;
    
    NSString *shareUrl;
    
    BOOL sameTime;
    BOOL readmore;
    BOOL canInvite;
}

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *viewMenu;

@property (weak, nonatomic) IBOutlet UIImageView *line1;
@property (weak, nonatomic) IBOutlet UIImageView *line2;
@property (weak, nonatomic) IBOutlet UIImageView *line3;
@property (weak, nonatomic) IBOutlet UIImageView *line4;
@property (weak, nonatomic) IBOutlet UIView *line5;

// View user's profile
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *noLabel;
@property (weak, nonatomic) IBOutlet UILabel *earnLabel;
@property (weak, nonatomic) IBOutlet UILabel *balanceLabel;

@property (weak, nonatomic) IBOutlet UILabel *headerText1;
@property (weak, nonatomic) IBOutlet UILabel *headerText2;
@property (weak, nonatomic) IBOutlet UILabel *headerText3;
@property (weak, nonatomic) IBOutlet UILabel *headerText4;
@property (weak, nonatomic) IBOutlet UILabel *headerText5;
@property (weak, nonatomic) IBOutlet UILabel *subText1;
@property (weak, nonatomic) IBOutlet UILabel *subText2;
@property (weak, nonatomic) IBOutlet UILabel *subText3;
@property (weak, nonatomic) IBOutlet UILabel *subText4;
@property (weak, nonatomic) IBOutlet UILabel *subText5;

@property (weak, nonatomic) IBOutlet UILabel *noTopic;
@property (weak, nonatomic) IBOutlet UILabel *balanceTopic;
@property (weak, nonatomic) IBOutlet UILabel *earnTopic;
@property (weak, nonatomic) IBOutlet UILabel *point1;
@property (weak, nonatomic) IBOutlet UILabel *point2;

@property (weak, nonatomic) IBOutlet UIButton *menu1;
@property (weak, nonatomic) IBOutlet UIButton *menu2;
@property (weak, nonatomic) IBOutlet UIButton *menu3;
@property (weak, nonatomic) IBOutlet UIButton *menu4;
@property (weak, nonatomic) IBOutlet UIButton *menu5;
@property (weak, nonatomic) IBOutlet UILabel *luckyDraw_point;

@property (weak, nonatomic) IBOutlet UIView *viewMenu1;
@property (weak, nonatomic) IBOutlet UIView *viewMenu2;
@property (weak, nonatomic) IBOutlet UIView *viewMenu3;
@property (weak, nonatomic) IBOutlet UIView *viewMenu4;
@property (weak, nonatomic) IBOutlet UIView *viewMenu5;

@property (weak, nonatomic) IBOutlet UIView *viewProfile;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightAdvertising;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightHotPromo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightMenu5;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightScrollView;

@property NSString *tagMenu;
-(NSString *)action:(NSString *) selecttag;

@end
