//
//  PCFont.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 4/1/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "PCFont.h"

@implementation PCFont

+(UIFont *)fontRSU:(float)size_text{
    
    return [UIFont fontWithName:@"RSU" size:size_text];
}

@end
