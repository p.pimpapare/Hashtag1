//
//  MenuCouponDetail_Alert.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/17/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

// This class is pop up view that allowing the user to select Use coupon or Store coupon

#import <UIKit/UIKit.h>

@interface MenuCouponDetail_Alert : UIView

@property (weak, nonatomic) IBOutlet UIImageView *logo;

@property (weak, nonatomic) IBOutlet UIButton *usedCouponBtn;
@property (weak, nonatomic) IBOutlet UIButton *storeCouponBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;

@property (weak, nonatomic) IBOutlet UILabel *textDetail;

@property (weak, nonatomic) IBOutlet UIView *lineHeader;
@property (weak, nonatomic) IBOutlet UIView *lineGray;
@property (weak, nonatomic) IBOutlet UIView *lineBottom;
@property (weak, nonatomic) IBOutlet UIButton *black_btn;

-(void)set_logo_coupon:(NSString *)logoImage;

@end
