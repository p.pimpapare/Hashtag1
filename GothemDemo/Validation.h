//
//  Validation.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/7/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

// This class is used for validating email's formate such as xx@xxx.xxx and password's format 

#import <Foundation/Foundation.h>

@interface Validation : NSObject

+(BOOL)validateEmail:(NSString *)emailStr;
+(BOOL)validatePassword:(NSString *)passwordStr;

@end
