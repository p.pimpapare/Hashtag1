//
//  Announe+WebviewControllerViewController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 4/1/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "Announe_WebviewController.h"

@interface Announe_WebviewController ()

@end

@implementation Announe_WebviewController


-(void)viewWillAppear:(BOOL)animated{

    GOOGLE_ANALYTICS(@"Announe_WebviewController");
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self.loadingView startAnimating];
    [self class_UI];
}

-(void)class_UI{
    
    LINE_WITH_STATUSBAR_HEADER_NORMAL;
    [self.webView setScalesPageToFit:YES];
}

- (IBAction)backBtn:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

///// WEB_VIEW

-(void)didReceiveMemoryWarning{
     
     [super didReceiveMemoryWarning];
 }

- (void)setView:(UIView *)view{
    
    if (view == nil){
        [self releaseSubviews];
    }
    [super setView:view];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    if (!webView.isLoading) {
        
        [self.loadingView stopAnimating];
        [self.loadingView setHidden:YES];
    }
}

-(void)openUrl:(NSString *)urlText{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",urlText]];
    NSURLRequest *req = [NSURLRequest requestWithURL:url];

    [self.webView loadRequest:req];
}

-(void)releaseSubviews{
    
    [self.webView setDelegate:nil];
    [self.webView stopLoading];
}

- (void)dealloc{
    
    [self releaseSubviews];
}

- (void)viewWillDisappear:(BOOL)animated{
    
    [self releaseSubviews];
}
///// END OF WEB_VIEW

@end
