//
//  PCLoadingView.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 4/5/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "PCLoadingView.h"

@implementation PCLoadingView

-(void)awakeFromNib{
    
    [super awakeFromNib];

    int oringin_y = 0;
    
    if(DEVICE_HEIGHT<PHONE5_HEIGHT)
    {
        oringin_y = DEVICE_HEIGHT - 100;
    }else
    {
        oringin_y = DEVICE_HEIGHT - 150;
    }
    
    [self setFrame:CGRectMake(DEVICE_WIDTH/2-self.frame.size.width/2,oringin_y,self.frame.size.width,self.frame.size.height)];
    
    [self.layer setCornerRadius:5];
    [self setAlpha:1];
    [self.activityIndicatorView startAnimating];
}

-(void)set_size_for_container_view{
    
    [self setFrame:CGRectMake(DEVICE_WIDTH/2-self.frame.size.width/2,DEVICE_HEIGHT-250,self.frame.size.width,self.frame.size.height)];
    [self set_gray_loading_color];
}

-(void)set_size_for_lockScreen_view{
    
    [self setFrame:CGRectMake(DEVICE_WIDTH/2-self.frame.size.width/2,DEVICE_HEIGHT-100,self.frame.size.width,self.frame.size.height)];
}

-(void)set_size_for_share_view{
    
    [self setFrame:CGRectMake(DEVICE_WIDTH/2-self.frame.size.width/2,DEVICE_HEIGHT-80,self.frame.size.width,self.frame.size.height)];
}

-(void)set_gray_loading_color{
    
    [self.activityIndicatorView setColor:[UIColor grayColor]];
}

@end
