//
//  PCNavigationBarView.h
//  PlayCash
//
//  Created by Akarapas Wongkaew on 2/5/2559 BE.
//  Copyright © 2559 Akarapas Wongkaew. All rights reserved.
//

typedef enum : NSUInteger {
    PCNavigationBarBtnStyleBack,
} PCNavigationBarBtnStyle;

@interface PCNavigationBarView : UIView
{
    
}

@property (weak, nonatomic) IBOutlet PCButton *btnLeft;
@property (weak, nonatomic) IBOutlet PCButton *btnRight;
@property (weak, nonatomic) IBOutlet UILabel *text_title;

-(void)setBtnLeftStyle:(PCNavigationBarBtnStyle)style;
-(void)setBtnRightStyle:(PCNavigationBarBtnStyle)style;
-(void)settextTitleWithText:(NSString *)text;

@end
