//
//  ReceiveLuckyPointController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/31/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "ReceiveLuckyPointController.h"

@interface ReceiveLuckyPointController (){
    
    NSArray *dateWithTime;
    int selectedRow;
}

@end

@implementation ReceiveLuckyPointController

-(void)viewWillAppear:(BOOL)animated{
    
    GOOGLE_ANALYTICS(@"ReceiveLuckyPointController");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addPCLoading:self];
    [self setHiddenPCLoading:NO];
    [self setLoadingForContainer];
    
    [self getHistoryLuckyPoint:[self getUserToken]];
    [self class_UI];
}

-(void)getHistoryLuckyPoint:(NSString *)token{
    
    [Service get_lucky_point_history:self block:^(id result) {
        
        hDateTime = [[NSMutableArray alloc]init];
        hName = [[NSMutableArray alloc]init];
        
        NSString *result_text = [NSString stringWithFormat:@"%@",result];
        
        if (![result_text isEqualToString:@"error"]) {
            
            for (int i=0; i<[result[@"history_luckydraw"] count];i++) {
                [hDateTime addObject:result[@"history_luckydraw"][i][@"created_at"]];
                [hName addObject:result[@"history_luckydraw"][i][@"package_name"]];
            }
            finisedLoadingGetPoint = YES;
            [self.tableView reloadData];
            [self setHiddenPCLoading:YES];
            
        }
    }];
}

// set tableView
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectedRow = (int)indexPath.row;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if ([hName count]==0) {
        if (finisedLoadingGetPoint==YES) {
            [self.warningBlank setHidden:NO];
        }return 0;
    }else{
        return [hName count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    History_tableCell *cell = (History_tableCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if(cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"History_tableCell" owner:self options:nil][0];
    }
    [self splitDateTime:hDateTime[indexPath.row]];
    
    [cell setCell:[NSString stringWithFormat:@"%@",[dateWithTime objectAtIndex:0]]  time:[NSString stringWithFormat:@"%@",[dateWithTime objectAtIndex:1]] point:@"รับ 1 สิทธิ์" display:hName[indexPath.row] usedCoupon:NO];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return [[[NSBundle mainBundle] loadNibNamed:@"History_tableCell" owner:self options:nil][0] frame].size.height;
}

-(void)class_UI{
    
    finisedLoadingGetPoint = NO;
    TABLEVIEW_COLLECTION_REFRESH(self.tableView);
    
    [self.lineBottom setBackgroundColor:[PCColor color_A8101D]];
    [self.tableView setSeparatorColor:[UIColor clearColor]];
}

- (void)refresh:(UIRefreshControl *)refreshControl {
    [refreshControl endRefreshing];
    [self getHistoryLuckyPoint:[self getUserToken]];
}

-(NSArray *)splitDateTime:(NSString *)dateTime{
    
    NSArray *splitToArray = [dateTime componentsSeparatedByString:@" "];
    NSString *date = [splitToArray objectAtIndex:0];
    NSString *time = [splitToArray objectAtIndex:1];
    
    NSArray *array_time = [time componentsSeparatedByString:@":"];
    NSString *hour = [array_time objectAtIndex:0];
    NSString *minute = [array_time objectAtIndex:1];
    
    NSString *new_time = [NSString stringWithFormat:@"%@:%@",hour,minute];
    
    dateWithTime = [NSArray arrayWithObjects:date,new_time, nil];
    return dateWithTime;
}

@end
