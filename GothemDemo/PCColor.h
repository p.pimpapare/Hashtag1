
//  Created by pimpaporn chaichompoo on 1/31/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

// This class is used for setting and organizing UIColor's functions.

#import <UIKit/UIKit.h>

@interface PCColor : UIView

+(UIColor *)colorFromHexString:(NSString *)hexString alpha:(float)alpha;

+(UIColor *)color_323232;
+(UIColor *)color_F0F0F1;
+(UIColor *)color_A32E3D;
+(UIColor *)color_8E8E8E;
+(UIColor *)color_424242;
+(UIColor *)color_on_menu2:(NSString *)colorMenu2;
+(UIColor *)color_4285F4;
+(UIColor *)color_A80F1C;
+(UIColor *)color_767676;
+(UIColor *)color_ffffff;
+(UIColor *)color_A8101D;
+(UIColor *)color_A6A6A6;
+(UIColor *)color_F5F5F5;
+(UIColor *)color_EDEDED;

/// custom color and shadow
+(void)set_shadow_headerBar:(UIView *)view;
+(void)set_whiteColor_headerBar:(UIView *)view;
+(void)set_inner_shadow:(UIView *)view;
+(void)set_shadow:(UIView *)view;

@end
