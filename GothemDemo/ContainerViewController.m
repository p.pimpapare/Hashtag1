//
//  ContainerViewController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/22/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "ContainerViewController.h"
#import "ASStandardLibrary.h"

@interface ContainerViewController (){
    
    XLPageStripMenuBarController *homeView;
    LockScreenController *lockScreenView;
    NewFeedController *newFeedView;
    
    UIPanGestureRecognizer *screenEdgeGesture;
}

@end

@implementation ContainerViewController

-(void)viewWillAppear:(BOOL)animated{
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notification_alert:) name:@"selected_newfeed" object:nil];
    
    newFeedView = [self loadNibWithName:@"NewFeedController"];
    
    [self addChildNewfeed_Webview];
    [self checkNewFeedStatus];
    [self checkUser];
}

//----------- NEW FEED -----------

-(void)checkNewFeedStatus{
    
    id newfeedInfo;
    NSUserDefaults *newsfeedDefaults = [NSUserDefaults standardUserDefaults];
    
    if (newsfeedDefaults){
        newfeedInfo = (id)[newsfeedDefaults objectForKey:@"newsfeed_setting"];
    }
    
    [self addNewfeed];
    
    if (([newfeedInfo isEqualToString:@"selectedBtn"])||(newfeedInfo==nil)) {
    
        [self showNewfeed];
    
    }else if([newfeedInfo isEqualToString:@"unselectedBtn"]){
      
        [self hideNewfeed];
    }
}

-(void)addNewfeed{
    
    [newFeedView.view setFrame:CGRectMake(DEVICE_WIDTH,0,DEVICE_WIDTH,DEVICE_HEIGHT)];
    
    UIWindow* mainWindow = [[UIApplication sharedApplication] keyWindow];
    [mainWindow addSubview: newFeedView.view];
    [mainWindow makeKeyAndVisible];
    
    // set screenEdgeGesture
    screenEdgeGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(screenEdge_action:)];
    [screenEdgeGesture setMinimumNumberOfTouches:1];
    [screenEdgeGesture setMaximumNumberOfTouches:1];
    [newFeedView.view addGestureRecognizer:screenEdgeGesture];
}

-(void)showNewfeed{
    
    [newFeedView.view setHidden:NO];
    
    [UIView animateWithDuration:2.0 delay:0 usingSpringWithDamping:0.2 initialSpringVelocity:5.0 options:UIViewAnimationOptionCurveLinear animations:^{
        [newFeedView.view setFrameLeft:DEVICE_W-9];
    } completion:^(BOOL finished) {
        
    }];
}

-(void)hideNewfeed{
    
    [newFeedView.view setFrameLeft:DEVICE_W+11];
    [newFeedView.view setHidden:YES];
}

-(void)screenEdge_action:(UIPanGestureRecognizer *)recognizer{
    
    CGPoint translation = [recognizer translationInView:newFeedView.view];
    recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x,
                                         recognizer.view.center.y);
    [recognizer setTranslation:CGPointMake(0, 0) inView:newFeedView.view];
    
    if (recognizer.state == UIGestureRecognizerStateChanged) {
        
        [self action:@"set_ui"];

        if (newFeedView.view.frameLeft>DEVICE_W) {
            
            [newFeedView.view setFrameLeft:DEVICE_W-11]; //11
            
        }else if(newFeedView.view.frameLeft<=0){
         
            [newFeedView.view setFrameLeft:-6];//-11
        }
        
    }else if (recognizer.state == UIGestureRecognizerStateEnded) {
        
        [UIView animateWithDuration:0.4 animations:^{
        
            if (newFeedView.view.frameLeft<DEVICE_W/2) {
                
                [newFeedView.view setFrameLeft:-20];
                [self action:@"selected"];
                
            }else{
            
                [newFeedView.view setFrameLeft:DEVICE_W-11];
            }
            
        } completion:^(BOOL finished) {
        }];
    }
}

//-----------END OF NEW FEED -----------

-(void)checkUser{
    
    // Check lock screen setting that it's allowed or not?
    id lockScreenInfo, updateInfo;
    NSUserDefaults *lockScreenDefaults = [NSUserDefaults standardUserDefaults];
    
    if (lockScreenDefaults){
        lockScreenInfo = (id)[lockScreenDefaults objectForKey:@"lockscreen_setting"];
        updateInfo = (id)[lockScreenDefaults objectForKey:@"LockScreenInfo"];
    }
    
    if([lockScreenInfo isEqualToString:@"unselectedBtn"]){
       
        homeView = [self.storyboard instantiateViewControllerWithIdentifier:@"XLPageStripMenuBarController"];
     
        [self addChild:homeView];
    
    }else if(([lockScreenInfo isEqualToString:@"selectedBtn"])||(lockScreenInfo==nil)){
        
        /// check lock screen info have existing Ads or not
        NSString *totlePackage = [NSString stringWithFormat:@"%@",updateInfo[@"loop_items"][0][@"package_total"]];

        if (([totlePackage isEqualToString:@"0"])||([totlePackage isEqualToString:@"(null)"])) {
            
            homeView = [self.storyboard instantiateViewControllerWithIdentifier:@"XLPageStripMenuBarController"];         
            [self addChild:homeView];
            
        }else{
            
            lockScreenView = [self loadNibWithName:@"LockScreenController"];
            [self addChild:lockScreenView];
        }
    }
}

-(void)addChildNewfeed_Webview{
    
    NewFeedWebViewController *view_class = [self loadNibWithName:@"NewFeedWebViewController"];
    [self addChild:view_class];
}

-(void)addChild:(UIViewController *)childView{
    
    childView.view.frame = self.view.bounds;
    [self addChildViewController:childView];
    [self.view addSubview:childView.view];
    [childView didMoveToParentViewController:self];
}

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)notification_alert:(NSNotification *)noti{

    NSString *strNoti = [noti object];
    
    if ([strNoti isEqualToString:@"selectedBtn"]) {
        [self showNewfeed];
    }else if ([strNoti isEqualToString:@"unselectedBtn"]){
        [self hideNewfeed];
    }else if ([strNoti isEqualToString:@"Open_webView"]){
        [self addChildNewfeed_Webview];
    }
}

-(NSString *)action:(NSString *) selecttag{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"selected_newsfeed" object:[NSString stringWithFormat:@"%@",selecttag]];
    [self setTagMenu:selecttag];
    return selecttag;
}

@end
