//
//  Menu3TableViewCell.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/1/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

#import <UIKit/UIKit.h>

@interface Menu3TableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleText;
@property (weak, nonatomic) IBOutlet UILabel *detailText;

-(void)setCellInfo:(NSString *)title detail:(NSString *)detail;

@end
