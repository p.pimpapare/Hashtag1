//
//  ReadMoreController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/20/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "ReadMoreController.h"

@interface ReadMoreController ()
@end

@implementation ReadMoreController

-(void)viewWillAppear:(BOOL)animated{
    
    GOOGLE_ANALYTICS(@"ReadMoreController");
}

- (void)viewDidLoad {
    
    [self.point setHidden:YES];
    [self.icon_coin setHidden:YES];
    [self.titleDetail setHidden:YES];
    
    [super viewDidLoad];
    [self.loadingView startAnimating];
    [self class_UI];
}

-(void)setDetailReadmore:(NSString *)coupon_ID title:(NSString *)title point:(NSString *)point logo:(NSString *)logo{
    
    [self get_service_readmore:coupon_ID coupon_title:title coupon_point:point coupon_logo:logo];
}

-(void)get_service_readmore:(NSString *)coupon_ID coupon_title:(NSString *)coupon_title coupon_point:(NSString *)coupon_point coupon_logo:(NSString *)coupon_logo{
    
    [Service get_coupon_detail:[self getUserToken] couponID:coupon_ID selfID:self block:^(id result) {
        
        NSString *resultText = [NSString stringWithFormat:@"%@",result];
        
        if ([resultText isEqualToString:@"error"]) {
            
            [self get_service_readmore:coupon_ID coupon_title:coupon_title coupon_point:coupon_point coupon_logo:coupon_logo];
            
        }else{
            
            [self set_readmore_detail:result coupon_title:coupon_title coupon_point:coupon_point coupon_logo:coupon_logo];
        }
    }];
}

-(void)set_readmore_detail:(NSString *)details coupon_title:(NSString *)coupon_title coupon_point:(NSString *)coupon_point coupon_logo:(NSString *)coupon_logo{
    
    html =[NSString stringWithFormat:@"%@",[ReplaceHTMLFont replaceHTML_by_string:[NSString stringWithFormat:@"%@",[ReplaceHTMLFont replaceHTML_by_range:(id)details start_string:@"<style>" end_string:@"</style>" replaceString:@" "]] old_string:@"<html lang=\"en\">" new_string:@"<html lang=\"en\"> <style>html,body,head,span,h,p { font-family:RSU; }</style>"]];
    
    NSString *html2 = [NSString stringWithFormat:@"%@",[ReplaceHTMLFont replaceHTML_by_string:(id)html old_string:@"Cordia New" new_string:@"RSU"]];
    NSString *html3 = [NSString stringWithFormat:@"%@",[ReplaceHTMLFont replaceHTML_by_string:(id)html2 old_string:@"Tahoma" new_string:@"RSU"]];
    
    [self.webView loadHTMLString:html3 baseURL:nil];
    
    [self.point setHidden:NO];
    [self.icon_coin setHidden:NO];
    [self.titleDetail setHidden:NO];
    
    [self.titleDetail setText:[NSString stringWithFormat:@"%@",coupon_title]];
    [self.point setText:[NSString stringWithFormat:@"แลก %@ คะแนน",coupon_point]];
}

- (IBAction)backBtn:(id)sender {
    
    [self goToPrevoius];
}

-(void)class_UI{
    
    [self.statusBar setBackgroundColor:[PCColor color_424242]];
    [PCColor set_whiteColor_headerBar:self.viewHeader];
    [PCShadow set_shadow:-5 shadowRadius:5 shadowOpacity:0.3 view:self.viewShadow];

    [self.scrollview setContentSize:self.viewReadMore.frame.size];
}

-(void)goToPrevoius{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

///// WEB_VIEW

-(void)didReceiveMemoryWarning{
    
    [super didReceiveMemoryWarning];
}

- (void)setView:(UIView *)view{
    
    if (view == nil){
        [self releaseSubviews];
    }
    
    [super setView:view];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    if (!webView.isLoading) {
        
        [self.loadingView stopAnimating];
        [self.loadingView setHidden:YES];
    }
}

-(void)openUrl:(NSString *)urlText{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",urlText]];
    NSURLRequest *req = [NSURLRequest requestWithURL:url];
    
    [self.webView loadRequest:req];
}

-(void)releaseSubviews{
    
    [self.webView setDelegate:nil];
    [self.webView stopLoading];
}

- (void)dealloc{
    
    [self releaseSubviews];
}

- (void)viewWillDisappear:(BOOL)animated{
    
    [self releaseSubviews];
}
///// END OF WEB_VIEW

@end
