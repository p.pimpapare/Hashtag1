
//
//  UserImageController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 5/10/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "UserImageController.h"
#import "ImageStickerView.h"

@interface UserImageController (){
    
    int tagImage; // 1 = btn1, 2 = btn2, 3 = btn3
    NSString *image_name_1, *image_name_2, *image_name_3;
    
    BOOL selectedImage1, selectedImage2, selectedImage3;
}

@end

@implementation UserImageController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // image theme
    [self.image_capture setImageStickerWithImage:[UIImage imageNamed:@"picture_1_1"]];
    
    [self class_UI];
    [self check_image];
}

-(void)check_image{
    
    // load image
    NSArray *sysPaths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES );
    NSString *docDirectory = [sysPaths objectAtIndex:0];
    NSFileManager *filemgr = [NSFileManager defaultManager];
    NSArray *filelist= [filemgr contentsOfDirectoryAtPath:docDirectory error:nil];
    
    int count = (int)[filelist count];
    
    NSString *filePath1, *filePath2, *filePath3;
    
    
    if (count==1) {
        
        [self.view2 setHidden:NO];
        
        image_name_1 = [NSString stringWithFormat:@"%@",[filelist objectAtIndex:0]];
        
        filePath1 = [NSString stringWithFormat:@"%@/%@", docDirectory,image_name_1];
        [self.userImage1 setImage:[UIImage imageWithContentsOfFile:filePath1]];
        
        selectedImage1 = YES;
        
    }else if(count==2){
        
        [self.view2 setHidden:NO];
        [self.view3 setHidden:NO];
        
        image_name_1 = [NSString stringWithFormat:@"%@",[filelist objectAtIndex:0]];
        filePath1 = [NSString stringWithFormat:@"%@/%@", docDirectory,image_name_1];
        [self.userImage1 setImage:[UIImage imageWithContentsOfFile:filePath1]];
        
        image_name_2 = [NSString stringWithFormat:@"%@",[filelist objectAtIndex:1]];
        filePath2 = [NSString stringWithFormat:@"%@/%@", docDirectory,image_name_2];
        [self.userImage2 setImage:[UIImage imageWithContentsOfFile:filePath2]];
        
        selectedImage1 = YES;
        selectedImage2 = YES;
        
    }else if (count==3){
        
        [self.view2 setHidden:NO];
        [self.view3 setHidden:NO];
        
        image_name_1 = [NSString stringWithFormat:@"%@",[filelist objectAtIndex:0]];
        filePath1 = [NSString stringWithFormat:@"%@/%@", docDirectory,image_name_1];
        [self.userImage1 setImage:[UIImage imageWithContentsOfFile:filePath1]];
        
        image_name_2 = [NSString stringWithFormat:@"%@",[filelist objectAtIndex:1]];
        filePath2 = [NSString stringWithFormat:@"%@/%@", docDirectory,image_name_2];
        [self.userImage2 setImage:[UIImage imageWithContentsOfFile:filePath2]];
        
        image_name_3 = [NSString stringWithFormat:@"%@",[filelist objectAtIndex:2]];
        filePath3 = [NSString stringWithFormat:@"%@/%@", docDirectory,image_name_3];
        [self.userImage3 setImage:[UIImage imageWithContentsOfFile:filePath3]];
        
        selectedImage1 = YES;
        selectedImage2 = YES;
        selectedImage3 = YES;
    }
}

- (IBAction)btn_upload1:(id)sender {
    
    tagImage = 1;
    [self display_actionsheet:selectedImage1];
}

- (IBAction)btn_upload2:(id)sender {
    
    tagImage = 2;
    [self display_actionsheet:selectedImage2];
}

- (IBAction)btn_upload3:(id)sender {
    
    tagImage = 3;
    [self display_actionsheet:selectedImage3];
}

-(void)pickerImage_Gallery{
    
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.delegate = (id)self;
    imagePickerController.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:imagePickerController animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker
        didFinishPickingImage:(UIImage *)image
                  editingInfo:(NSDictionary *)editingInfo
{
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    [self.view_for_capture_image setHidden:NO];
    
    [self.image_capture setImageUserWithImage:image];
    
}

- (IBAction)btn_save:(id)sender {
    
    if (tagImage==1) {
        
        [self.userImage1 setImage:[self.image_capture getImageSticker]];
        [self.view2 setHidden:NO];
        
    }else if (tagImage==2){
        
        [self.userImage2 setImage:[self.image_capture getImageSticker]];
        [self.view3 setHidden:NO];
        
    }else if (tagImage==3){
        
        [self.userImage3 setImage:[self.image_capture getImageSticker]];
        
    }
    
    [self uploadImage_to_application];
    [self.view_for_capture_image setHidden:YES];
}

- (IBAction)btn_cancel:(id)sender {
    
    [self.view_for_capture_image setHidden:YES];
}


-(void)uploadImage_to_application{
    
    UIImage *image;
    NSString *documentsDirectory1,*savedImagePath1;
    NSString *documentsDirectory2,*savedImagePath2;
    NSString *documentsDirectory3,*savedImagePath3;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    
    if (tagImage==1) {
        
        documentsDirectory1 = [paths objectAtIndex:0];
        savedImagePath1 = [documentsDirectory1 stringByAppendingPathComponent:@"savedImage1.jpg"];
        image = self.userImage1.image; // imageView is my image from camera
        
        NSData *imageData = UIImagePNGRepresentation(image);
        [imageData writeToFile:savedImagePath1 atomically:NO];
        
        //        NSLog(@"%@",documentsDirectory1);
    }else if (tagImage==2){
        
        documentsDirectory2 = [paths objectAtIndex:0];
        savedImagePath2 = [documentsDirectory2 stringByAppendingPathComponent:@"savedImage2.jpg"];
        image = self.userImage2.image; // imageView is my image from camera
        
        NSData *imageData = UIImagePNGRepresentation(image);
        [imageData writeToFile:savedImagePath2 atomically:NO];
        
    }else if (tagImage==3){
        
        documentsDirectory3 = [paths objectAtIndex:0];
        savedImagePath3 = [documentsDirectory3 stringByAppendingPathComponent:@"savedImage3.jpg"];
        image = self.userImage3.image; // imageView is my image from camera
        
        NSData *imageData = UIImagePNGRepresentation(image);
        [imageData writeToFile:savedImagePath3 atomically:NO];
    }
}

-(void)display_actionsheet:(BOOL)show_delete{
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"รูปของฉัน" message:@" " preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"ยกเลิก" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    if (show_delete==YES) {
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"ลบรูปภาพปัจจุบัน" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString *fullPath;
            
            if (tagImage==1) {
                
                fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",image_name_1]];
                [self.userImage1 setImage:[UIImage imageNamed:@"default-medium"]];
                
                selectedImage1 = NO;
                
            }else if (tagImage==2){
                
                fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",image_name_2]];
                [self.userImage2 setImage:[UIImage imageNamed:@"default-medium"]];
                
                selectedImage2 = NO;
                
            }else if (tagImage==3){
                
                fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",image_name_3]];
                [self.userImage3 setImage:[UIImage imageNamed:@"default-medium"]];
                
                selectedImage3 = NO;
                
            }
            
            [fileManager removeItemAtPath: fullPath error:NULL];
            //            NSLog(@"image removed");
            
        }]];
    }
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"ถ่ายภาพ" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            [imagePickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
        }
        
        // image picker needs a delegate,
        [imagePickerController setDelegate:(id)self];
        
        // Place image picker on the screen
        [self presentViewController:imagePickerController animated:YES completion:nil];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"เลือกจากไลบรารี" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
        imagePickerController.delegate = (id)self;
        imagePickerController.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:imagePickerController animated:YES completion:nil];
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}

-(void)class_UI{
    
    [self.widthImage1 setConstant:DEVICE_WIDTH/3-25];
    LINE_WITH_STATUSBAR_HEADER_NORMAL;
    SWIPETLIFE_TOGOTOPREVIOUS
}

-(void)swipeRight:(UISwipeGestureRecognizer*)gestureRecognizer{
    GOTO_PREVIOUS;
}

- (IBAction)back_btn:(id)sender {
    GOTO_PREVIOUS
}

@end
