//
//  StackImageView.h
//  testCode
//
//  Created by Akarapas Wongkaew on 3/30/2559 BE.
//  Copyright © 2559 Akarapas Wongkaew. All rights reserved.

// This class is used for setting Lock Screen animation 
#import <UIKit/UIKit.h>

@interface StackImageView : UIView

// ############## Required to set *****
-(void)setIsLoadFromImageName:(BOOL)boo; // set first
-(void)setDataImageWithArray:(NSArray *)arrayData; // after set above
// >> Object is String
// >> if setIsLoadFromImageName = YES, object is name of image
// >> if setIsLoadFromImageName = NO, object is name of URL image for load image from SDWebImage
// ############## Required to set *****

// ############## Optional
-(void)setAnimationSlideTime:(float)time;
-(int)getIndex:(id)sender;
-(void)setCanScrollDown:(BOOL)boo;
-(void)setCanScrollUp:(BOOL)boo;
// ##############

-(void)setBaseUrl:(NSString *)baseUrlImage;
-(void)setUserUrl:(NSString *)baseUrlImage;

-(void)set_index_image_next;

@property NSString *tagMenu;
-(NSString *)action:(NSString *) selecttag;

@end
