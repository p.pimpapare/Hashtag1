//
//  MenuUserCuponController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/20/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

// This class is used for store the user coupon

#import <UIKit/UIKit.h>

@interface MenuUserCuponController : BaseViewController<UITableViewDataSource,UITableViewDelegate>{
    
    NSMutableArray *couponID;
    NSMutableArray *couponTitle;
    NSMutableArray *couponSubTitle;
    NSMutableArray *couponImage;
    NSMutableArray *couponDetails;
    NSMutableArray *couponCounterTime;
    NSMutableArray *couponUsePoint;
    NSMutableArray *couponCpuse_id;
    NSMutableArray *couponBrandLogo;
    
    int selectedRow;
    BOOL finishedLoading;
    
    NSString *userToken;
}

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingView;

@property (weak, nonatomic) IBOutlet UIView *statusBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIView *warningView;
@property (weak, nonatomic) IBOutlet UILabel *titleHeader;
@property (weak, nonatomic) IBOutlet UIView *menuHeader;

@property (weak, nonatomic) IBOutlet UIView *viewError;
@property (weak, nonatomic) IBOutlet UIImageView *imageLogo;

@property (weak, nonatomic) IBOutlet UIButton *backBtn;

-(void)setTiltleHeader:(NSString *)title type:(NSString *)type;

@end
