
//  Created by pimpaporn chaichompoo on 2/15/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "NotReadyAlert.h"

@implementation NotReadyAlert

-(void)awakeFromNib{
    
    [super awakeFromNib];

    [self setBackgroundColor:[UIColor clearColor]];
    [self.view.layer setCornerRadius:22];
    [self setFrame:CGRectMake(DEVICE_WIDTH/2-self.frame.size.width/2,DEVICE_HEIGHT-self.frame.size.height*4,self.frame.size.width,self.frame.size.height)];
}

-(void)setTextWarning:(NSString *)titleHeadertext{
    self.txtWarning.text = [NSString stringWithFormat:@"%@",titleHeadertext];
}

@end
