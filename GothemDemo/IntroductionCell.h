//
//  IntroductionCell.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/9/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

#import <UIKit/UIKit.h>

@interface IntroductionCell : UICollectionViewCell

@property int menuNum;

@property (weak, nonatomic) IBOutlet UIImageView *imageCell;

-(void)setIntroImage:(NSString *)url imageC:(NSString *)imageC index:(int)indexImage;

@end
