//
//  StackImageView.m
//  testCode
//
//  Created by Akarapas Wongkaew on 3/30/2559 BE.
//  Copyright © 2559 Akarapas Wongkaew. All rights reserved.
//

#import "StackImageView.h"
#import "ASStandardLibrary.h"


@interface StackImageView ()

#define TOP_MAIN_FRAME -90 //-44
#define DEVICE_H [UIScreen mainScreen].bounds.size.height
#define DEVICE_W [UIScreen mainScreen].bounds.size.width
#define FRAME_BOTTOM(view) view.frame.origin.y+view.frame.size.height
#define SET_FRAME_BOTTOM(view,offset) [view setFrame:CGRectMake(view.frame.origin.x, offset-view.frame.size.height, view.frame.size.width, view.frame.size.height)]

{
    UIPanGestureRecognizer *panGesture;
    UILongPressGestureRecognizer *tapGesture;
    
    UIButton *btn_press;
    
    UIImageView *imageView1;
    UIImageView *imageView2;
    UIImageView *imageView3;
    
    UIImage *placeholderImage;
    
    int index;
    NSArray *arrayDataImage;
    
    float animationTime;
    
    BOOL isLoadFromImageName;
    
    BOOL canScrollDown;
    BOOL canScrollUp;
    NSString *baseUrl;
    NSString *userUrl;
    
    BOOL scrollUP_beforeDown;
}
@end

@implementation StackImageView

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

-(void)awakeFromNib
{
    
    [super awakeFromNib];
    
    [self setDefault:nil];
    
    [self setClipsToBounds:NO];
    
    [self setBackgroundColor:[UIColor clearColor]];
    
    // set ImageView
    imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, TOP_MAIN_FRAME-self.frame.size.height, self.frame.size.width, self.frame.size.height)];
    imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    imageView3 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    
    [imageView1 setContentMode:UIViewContentModeCenter]; // + คือทั้งคู่
    [imageView1 setClipsToBounds:YES];
    
    [imageView2 setContentMode:UIViewContentModeCenter];
    [imageView2 setClipsToBounds:YES];
    
    [imageView3 setContentMode:UIViewContentModeCenter];
    [imageView3 setClipsToBounds:YES];
    
    [imageView1 setAutoresizingMask:-1];
    [imageView2 setAutoresizingMask:-1];
    [imageView3 setAutoresizingMask:-1];
    
    //    [imageView1 setBackgroundColor:[UIColor whiteColor]];
    //    [imageView2 setBackgroundColor:[UIColor whiteColor]];
    //    [imageView3 setBackgroundColor:[UIColor whiteColor]];
    
    [self addSubview:imageView3];
    [self addSubview:imageView2];
    [self addSubview:imageView1];
    
    btn_press = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [btn_press setFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [btn_press setFrame:CGRectMake(0, 0,DEVICE_WIDTH,DEVICE_HEIGHT)];
    [btn_press addTarget:self action:@selector(btn_press_down:) forControlEvents:UIControlEventTouchDown];
    [btn_press addTarget:self action:@selector(btn_press_up:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btn_press];
    
    panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGesture_press:)];
    [self addGestureRecognizer:panGesture];
    
    placeholderImage = [self imageWithImage:[UIImage imageNamed:@"thumb_ad"] scaledToSize:CGSizeMake(DEVICE_WIDTH, DEVICE_HEIGHT-24)];
    
    
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    
    
    
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    //    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(newWidth, newHeight), NO, 0.0);
    
    [sourceImage drawInRect:CGRectMake(0, 0, DEVICE_W, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(void)setDefault:(id)sender
{
    animationTime = 0.2;
    index = 0;
    arrayDataImage = [NSMutableArray array];
    //    isLoadFromImageName = YES;
    canScrollDown = NO;
    canScrollUp = YES;
}

-(void)setBaseUrl:(NSString *)baseUrlImage{
    baseUrl = [NSString stringWithFormat:@"%@",baseUrlImage];
}

-(void)setUserUrl:(NSString *)baseUrlImage{
    userUrl = [NSString stringWithFormat:@"%@",baseUrlImage];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
}

-(void)setDataImageWithArray:(NSArray *)arrayData
{
    index = 0;
    arrayDataImage = [NSArray array];
    
    if ([arrayData count]<3) {
        NSMutableArray *arrayTemp = [NSMutableArray array];
        [arrayTemp addObjectsFromArray:arrayData];
        [arrayTemp addObjectsFromArray:arrayData];
        [arrayTemp addObjectsFromArray:arrayData];
        
        arrayDataImage = [NSArray arrayWithArray:(NSArray *)arrayTemp];
        
        if (isLoadFromImageName) {
            [imageView1 setImage:[UIImage imageNamed:arrayDataImage[2]]];
            [imageView2 setImage:[UIImage imageNamed:[arrayDataImage firstObject]]];
            [imageView3 setImage:[UIImage imageNamed:arrayDataImage[1]]];
        }else{
            
            [self setImage:imageView1 url:[NSString stringWithFormat:@"%@%@",baseUrl,arrayDataImage[2]] imagename:arrayDataImage[2]];
            [self setImage:imageView2 url:[NSString stringWithFormat:@"%@%@",baseUrl,[arrayDataImage firstObject]] imagename:[arrayDataImage firstObject]];
            [self setImage:imageView3 url:[NSString stringWithFormat:@"%@%@",baseUrl,arrayDataImage[1]] imagename:arrayDataImage[1]];
            
        }
    }else{
        arrayDataImage = [NSArray arrayWithArray:arrayData];
        
        if (isLoadFromImageName) {
            [imageView1 setImage:[UIImage imageNamed:[arrayDataImage lastObject]]];
            [imageView2 setImage:[UIImage imageNamed:[arrayDataImage firstObject]]];
            [imageView3 setImage:[UIImage imageNamed:arrayDataImage[1]]];
        }else{
            
            [self setImage:imageView1 url:[NSString stringWithFormat:@"%@%@",baseUrl,[arrayDataImage lastObject]] imagename:[arrayDataImage lastObject]];
            [self setImage:imageView2 url:[NSString stringWithFormat:@"%@%@",baseUrl,[arrayDataImage firstObject]] imagename:[arrayDataImage firstObject]];
            [self setImage:imageView3 url:[NSString stringWithFormat:@"%@%@",baseUrl,arrayDataImage[1]] imagename:arrayDataImage[1]];
            
        }
    }
}

// รับ uiimage ส่ง image ไป

-(void)setImage:(UIImageView *)imageView url:(NSString *)url imagename:(NSString *)image_name{
    
    NSArray *image_array;
    NSString *check_image;
    
    NSLog(@"image name %@",image_name);
    
    image_array = [image_name componentsSeparatedByString:@"."];
    
    if ([image_array count]!=1) {
        check_image = [image_array objectAtIndex:1];
    }
    
    if ([check_image isEqualToString:@"gif"]) {
        
        [imageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",url]] placeholderImage:placeholderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
            
            
        } usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        [imageView setContentMode:UIViewContentModeScaleToFill];
        
    }else{
        
        __weak UIImageView *imageWeak = imageView;
        
        [imageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",url]] placeholderImage:placeholderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
            
            image = [self imageWithImage:image scaledToWidth:DEVICE_WIDTH];
            [imageWeak setImage:image]; // ตรงนี้
            
            //            [imageWeak setImage:[self imageWithImage:image scaledToSize:CGSizeMake(DEVICE_W, DEVICE_H)]]; // ตรงนี้
            
        } usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        [imageView setContentMode:UIViewContentModeCenter];
        //        [imageWeak setContentMode:UIViewContentModeScaleAspectFit];
        
    }
}

-(void)setAnimationSlideTime:(float)time
{
    if (time) {
        animationTime = time;
    }
}


-(int)getIndex:(id)sender
{
    return index;
}

-(void)setIsLoadFromImageName:(BOOL)boo
{
    isLoadFromImageName = boo;
}



-(void)btn_press_down:(id)sender
{
    //    NSLog(@"%s",__PRETTY_FUNCTION__);
    
    [UIView animateWithDuration:0.2 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:0.8 options:UIViewAnimationOptionCurveEaseIn animations:^{
        
        [imageView2 setTransform:CGAffineTransformScale(CGAffineTransformIdentity, 0.95, 0.95)];
        
    } completion:^(BOOL finished) {
    }];
}

-(void)btn_press_up:(id)sender
{
    //    NSLog(@"%s",__PRETTY_FUNCTION__);
    
    [UIView animateWithDuration:0.2 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:0.8 options:UIViewAnimationOptionCurveEaseIn animations:^{
        
        [imageView2 setTransform:CGAffineTransformIdentity];
        
    } completion:^(BOOL finished) {
    }];
}

-(void)setCanScrollDown:(BOOL)boo
{
    canScrollDown = boo;
}

-(void)setCanScrollUp:(BOOL)boo
{
    canScrollUp = boo;
}

bool isTramform = NO;
-(void)tapGesture_press:(UILongPressGestureRecognizer *)recognizer
{
    //    NSLog(@"tapGesture_press");
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        //        NSLog(@"UIGestureRecognizerStateBegan");
        
        [UIView animateWithDuration:0.2 animations:^{
            
            [imageView2 setTransform:CGAffineTransformScale(CGAffineTransformIdentity, 0.95, 0.95)];
            
        } completion:^(BOOL finished) {
        }];
        
    }else if (recognizer.state == UIGestureRecognizerStateChanged) {
        //        NSLog(@"UIGestureRecognizerStateChanged");
    }else if (recognizer.state == UIGestureRecognizerStateEnded) {
        //        NSLog(@"UIGestureRecognizerStateEnded");
        
        [UIView animateWithDuration:0.2 animations:^{
            
            [imageView2 setTransform:CGAffineTransformIdentity];
            
        } completion:^(BOOL finished) {
        }];
    }
}

-(void)panGesture_press:(UIPanGestureRecognizer *)recognizer
{
    BOOL isVelocityUp = NO;
    
    CGPoint velocity = [recognizer velocityInView:recognizer.view];
    
    
    if(velocity.y > 0)
    {
        //        NSLog(@"down");
    }
    else
    {
        //        NSLog(@"up");
        if (velocity.y<-350) {
            
            scrollUP_beforeDown = YES;
        }
        
        isVelocityUp = YES;
    }
    
    CGPoint translation = [recognizer translationInView:recognizer.view];
    //recognizer.view.center = CGPointMake(recognizer.view.center.x, recognizer.view.center.y + translation.y);
    [recognizer setTranslation:CGPointMake(0, 0) inView:recognizer.view];
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        //        NSLog(@"panGesture_press_UIGestureRecognizerStateBegan");
    }else if (recognizer.state == UIGestureRecognizerStateChanged) {
        //        NSLog(@"panGesture_press_UIGestureRecognizerStateChanged");
        
        if (isVelocityUp) { // scroll up
            
            if ((FRAME_BOTTOM(imageView1)>TOP_MAIN_FRAME)&&canScrollUp==YES) {
                [imageView1 setCenter:CGPointMake(imageView1.center.x, imageView1.center.y + translation.y)];
            }else{
                [imageView2 setCenter:CGPointMake(imageView2.center.x, imageView2.center.y + translation.y)];
            }
            
            //            if (canScrollUp==NO) {
            //                [imageView2 setCenter:CGPointMake(self.frameWidth/2, self.frameHeight/2)];
            //            }
        }else{ // scroll down
            
            if (FRAME_BOTTOM(imageView2)>self.frame.size.height-(self.frame.size.height*0.05)&&canScrollDown==YES) {
                
                [imageView1 setCenter:CGPointMake(imageView1.center.x, imageView1.center.y + translation.y)];
                
            }else{ // can not scroll down
                
                if (scrollUP_beforeDown==YES) {
                    
                    [imageView2 setCenter:CGPointMake(imageView2.center.x, imageView2.center.y + translation.y)];
                    
                }else{
                    
                    [imageView2 setCenter:CGPointMake(self.frameWidth/2, self.frameHeight/2)];
                    
                }
                
                // ###               [imageView2 setCenter:CGPointMake(imageView2.center.x, imageView2.center.y + translation.y)];
            }
            
            /*          if (canScrollDown==NO) {
             
             //                if (imageView2.frame.origin.y>=DEVICE_HEIGHT/2) {
             //                    [imageView2 setCenter:CGPointMake(self.frameWidth/2, self.frameHeight/2)];
             //
             //                }else{
             //                [imageView2 setCenter:CGPointMake(imageView2.center.x, imageView2.center.y + translation.y)];
             //
             //                }
             
             NSLog(@"imageView2.frame.origin.x %f",recognizer.view.center.x);
             
             if (scrollUP_beforeDown==YES) {
             [imageView2 setCenter:CGPointMake(imageView2.center.x, imageView2.center.y + translation.y)];
             scrollUP_beforeDown = NO;
             }else{
             [imageView2 setCenter:CGPointMake(self.frameWidth/2, self.frameHeight/2)];
             // ####                   [imageView2 setCenter:CGPointMake(self.frameWidth/2, self.frameHeight/2)];
             scrollUP_beforeDown = NO;
             
             
             }
             }
             */
            
        }
        
    }else if (recognizer.state == UIGestureRecognizerStateEnded) {
        //        NSLog(@"panGesture_press_UIGestureRecognizerStateEnded");
        
        [self setUserInteractionEnabled:NO];
        
        [self btn_press_up:nil];
        
        if (isVelocityUp) {
            
            BOOL __block isChanged = NO;
            
            [UIView animateWithDuration:animationTime animations:^{
                if ((FRAME_BOTTOM(imageView2)<self.frame.size.height/2||velocity.y<-300)&&canScrollUp==YES) {
                    SET_FRAME_BOTTOM(imageView2,TOP_MAIN_FRAME);
                    isChanged = YES;
                    
                    //                    NSLog(@"image size%@",NSStringFromCGSize(imageView2.image.size));
                    
                    NSMutableString *actionScroll = [[NSMutableString alloc]init];
                    [actionScroll appendString:[NSString stringWithFormat:@"%d",isVelocityUp]];
                    //                    [actionScroll appendString:[NSString stringWithFormat:@"%d",[self getIndex:nil]]];
                    [self action:[NSString stringWithFormat:@"%@",actionScroll]];
                    
                }else{ // not allow to  scroll down
                    [imageView2 setCenter:CGPointMake(self.frame.size.width/2, self.frame.size.height/2)];
                }
                
                if (FRAME_BOTTOM(imageView1)>TOP_MAIN_FRAME) {
                    SET_FRAME_BOTTOM(imageView1,TOP_MAIN_FRAME);
                }
            } completion:^(BOOL finished) {
                
                if (isChanged) {
                    [self set_index_image_next];
                }
                [self setUserInteractionEnabled:YES];
            }];
        }else{
            BOOL __block isChanged = NO;
            [UIView animateWithDuration:animationTime animations:^{
                
                if ((FRAME_BOTTOM(imageView1)>self.frame.size.height/2||velocity.y>300)&&canScrollDown==YES) {
                    SET_FRAME_BOTTOM(imageView1,self.frame.size.height);
                    isChanged = YES;
                    
                    NSMutableString *actionScroll = [[NSMutableString alloc]init];
                    [actionScroll appendString:[NSString stringWithFormat:@"%d ",isVelocityUp]];
                    [actionScroll appendString:[NSString stringWithFormat:@"%d",[self getIndex:nil]]];
                    [self action:[NSString stringWithFormat:@"%@",actionScroll]];
                    
                }else{
                    SET_FRAME_BOTTOM(imageView1,TOP_MAIN_FRAME);
                    SET_FRAME_BOTTOM(imageView2, self.frame.size.height);
                }
            } completion:^(BOOL finished) {
                if (isChanged) {
                    
                    [self set_index_image_previous];
                }
                
                [self setUserInteractionEnabled:YES];
            }];
        }
        
        scrollUP_beforeDown = NO;
        
    }
}

-(void)set_index_image_next{
    { // next image
        
        index++;
        
        if (index>[arrayDataImage count]-1) {
            
            index = 0;
        }
        
        if (isLoadFromImageName) {
            
            [imageView2 setImage:[UIImage imageNamed:arrayDataImage[index]]];
            
            if (index-1<0) {
                [imageView1 setImage:[UIImage imageNamed:[arrayDataImage lastObject]]];
            }else{
                [imageView1 setImage:[UIImage imageNamed:arrayDataImage[index-1]]];
            }
            
            if (index+1>[arrayDataImage count]-1) {
                [imageView3 setImage:[UIImage imageNamed:[arrayDataImage firstObject]]];
            }else{
                [imageView3 setImage:[UIImage imageNamed:arrayDataImage[index+1]]];
            }
        }else{
            
            [self setImage:imageView2 url:[NSString stringWithFormat:@"%@%@",baseUrl,arrayDataImage[index]] imagename:arrayDataImage[index]];
            
            if (index-1<0) {
                
                [self setImage:imageView1 url:[NSString stringWithFormat:@"%@%@",baseUrl,[arrayDataImage lastObject]] imagename:[arrayDataImage lastObject]];
                
            }else{
                
                [self setImage:imageView1 url:[NSString stringWithFormat:@"%@%@",baseUrl,arrayDataImage[index-1]] imagename:arrayDataImage[index-1]];
                
            }
            
            if (index+1>[arrayDataImage count]-1) {
                
                [self setImage:imageView3 url:[NSString stringWithFormat:@"%@%@",baseUrl,[arrayDataImage firstObject]] imagename:[arrayDataImage firstObject]];
                
            }else{
                
                [self setImage:imageView3 url:[NSString stringWithFormat:@"%@%@",baseUrl,arrayDataImage[index+1]] imagename:arrayDataImage[index+1]];
                
            }
        }
        
        [imageView2 setFrame:CGRectMake(0, 0, imageView2.frame.size.width, imageView2.frame.size.height)];
    }
}

-(void)set_index_image_previous{
    
    SET_FRAME_BOTTOM(imageView1,TOP_MAIN_FRAME);
    
    index--;
    if (index<0) {
        index = (int)[arrayDataImage count]-1;
    }
    
    if (isLoadFromImageName) {
        [imageView2 setImage:[UIImage imageNamed:arrayDataImage[index]]];
        
        if (index-1<0) {
            [imageView1 setImage:[UIImage imageNamed:[arrayDataImage lastObject]]];
        }else{
            [imageView1 setImage:[UIImage imageNamed:arrayDataImage[index-1]]];
        }
        
        if (index+1>[arrayDataImage count]-1) {
            [imageView3 setImage:[UIImage imageNamed:[arrayDataImage firstObject]]];
        }else{
            [imageView3 setImage:[UIImage imageNamed:arrayDataImage[index+1]]];
        }
    }else{
        
        [self setImage:imageView2 url:[NSString stringWithFormat:@"%@%@",baseUrl,arrayDataImage[index]] imagename:arrayDataImage[index]];
        
        if (index-1<0) {
            
            [self setImage:imageView1 url:[NSString stringWithFormat:@"%@%@",baseUrl,[arrayDataImage lastObject]] imagename:[arrayDataImage lastObject]];
            
        }else{
            
            [self setImage:imageView1 url:[NSString stringWithFormat:@"%@%@",baseUrl,arrayDataImage[index-1]] imagename:arrayDataImage[index-1]];
            
        }
        
        if (index+1>[arrayDataImage count]-1) {
            
            [self setImage:imageView3 url:[NSString stringWithFormat:@"%@%@",baseUrl,[arrayDataImage firstObject]] imagename:[arrayDataImage firstObject]];
            
        }else{
            
            [self setImage:imageView3 url:[NSString stringWithFormat:@"%@%@",baseUrl,arrayDataImage[index+1]] imagename:arrayDataImage[index+1]];
            
        }
    }
    [imageView2 setFrame:CGRectMake(0, 0, imageView2.frame.size.width, imageView2.frame.size.height)];
}


-(NSString *)action:(NSString *) selecttag{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"StackImageView" object:[NSString stringWithFormat:@"%@",selecttag]];
    [self setTagMenu:selecttag];
    return selecttag;
}

@end
