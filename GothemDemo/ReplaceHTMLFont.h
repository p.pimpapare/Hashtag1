//
//  ReplaceHTMLFont.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 4/22/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

// This class is used for setting font of HTML's string from service

#import <Foundation/Foundation.h>

@interface ReplaceHTMLFont : NSObject

+(NSString *)replaceHTML_by_range:(id)old_HTML start_string:(NSString *)start_string end_string:(NSString *)end_string replaceString:(NSString *)new_String;

+(NSString *)replaceHTML_by_string:(id)HTML old_string:(NSString *)old_string new_string:(NSString *)new_string;

+(NSString *)replaceHTML_blank_space:(NSString *)HTML;


@end
