//
//  XLPageStripHomeController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/24/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "XLPageStripHomeController.h"
#import "ViewController.h"

@interface XLPageStripHomeController (){
    
    BaseViewController *baseView;
}
@end

@implementation XLPageStripHomeController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self moveToViewControllerAtIndex:1 animated:YES];
    [self set_initail_objects];
}

-(void)setMenuBar{
    
    selectedTab = 0;
    [self setCollection];
    
    //    [_collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:1 inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
}

-(void)setCollection{
    
    [self.menuBar setFrame:CGRectMake(0,0,DEVICE_WIDTH,51)];
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    layout.minimumLineSpacing=0;
    layout.minimumInteritemSpacing=0;
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    
    _collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0,0,self.menuBar.frame.size.width,self.menuBar.frame.size.height) collectionViewLayout:layout];
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [_collectionView setScrollEnabled:NO];
    [_collectionView setBackgroundColor:[PCColor color_F0F0F1]];
    
    [self.menuBar addSubview:_collectionView];
    
    // register collectionView
    [_collectionView registerClass:[PCMenubarCell class] forCellWithReuseIdentifier:@"Cell"];
    [_collectionView registerNib:[UINib nibWithNibName:@"PCMenubarCell" bundle:nil]  forCellWithReuseIdentifier:@"Cell"];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [arrayMenuBarImage count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    PCMenubarCell *cell=(PCMenubarCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    indexCell= (int)indexPath.row;
    
    if(cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"PCMenubarCell" owner:self options:nil][0];
    }
    
    // check menu bar action
    
    if([swiptTab[indexPath.row]isEqualToString:@"1"]){
        [cell setImageMenuBarCell:[NSString stringWithFormat:@"%@",arrayMenuBarImage_black[indexPath.row]]receiveIndex:indexCell];
        [cell setColorSelected:0];
    }else{
        [cell setImageMenuBarCell:[NSString stringWithFormat:@"%@",arrayMenuBarImage[indexPath.row]]receiveIndex:indexCell];
        [cell setColorSelected:1];
    }
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    int width;
    
    if (DEVICE_WIDTH==375) {
        width = ((self.menuBar.frame.size.width)/5+1);
    }else{
        width = ((self.menuBar.frame.size.width)/5);
    }
    return CGSizeMake(width,51);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    selectedTab = (int)indexPath.row;
    
    switch (selectedTab) {
            
        case 0://[self moveToViewControllerAtIndex:0 animated:YES];
            [self goto_lockscreen:nil];
            break;
        case 1:[self moveToViewControllerAtIndex:1 animated:YES];break;
        case 2:[self moveToViewControllerAtIndex:2 animated:YES];break;
        case 3:[self moveToViewControllerAtIndex:3 animated:YES];break;
        case 4:[self moveToViewControllerAtIndex:4 animated:YES];break;
            
        default: break;
    }
    [_collectionView reloadData];
}
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0,0,0,0);
}

// Set button bar
#pragma mark - XLPagerTabStripViewControllerDataSource

-(NSString *)titleForPagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController{
    return @"View";
}

-(UIColor *)colorForPagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController{
    return [UIColor whiteColor];
}

-(NSArray *)childViewControllersForPagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController{
    
    //     Create child view controllers that will be managed by XLPagerTabStripViewController
    ViewController *child_0 = [[NSBundle mainBundle] loadNibNamed:@"ViewController" owner:self options:nil][0];
    MenuHomeController * child_1 = (MenuHomeController *)[[NSBundle mainBundle] loadNibNamed:@"MenuHomeController" owner:self options:nil][0];
    MenuShopController * child_2 = (MenuShopController *)[[NSBundle mainBundle] loadNibNamed:@"MenuShopController" owner:self options:nil][0];
    MenuCuponController * child_3 = (MenuCuponController *)[[NSBundle mainBundle] loadNibNamed:@"MenuCuponController" owner:self options:nil][0];
    MenuSettingController * child_4 = (MenuSettingController *)[[NSBundle mainBundle] loadNibNamed:@"MenuSettingController" owner:self options:nil][0];
    
    if (!_isReload){
        return @[child_0,child_1, child_2, child_3, child_4];
    }
    
    NSMutableArray * childViewControllers = [NSMutableArray arrayWithObjects:child_0,child_1, child_2, child_3, child_4, nil];
    NSUInteger count = [childViewControllers count];
    
    for (NSUInteger i = 0; i < count; ++i) {
        // Select a random element between i and end of array to swap with.
        NSUInteger nElements = count - i;
        NSUInteger n = (arc4random() % nElements) + i;
        [childViewControllers exchangeObjectAtIndex:i withObjectAtIndex:n];
    }
    
    NSUInteger nItems = 1 + (rand() % 5);
    return [childViewControllers subarrayWithRange:NSMakeRange(0, nItems)];
}

-(void)reloadPagerTabStripView{
    
    _isReload = YES;
    self.isProgressiveIndicator = (rand() % 2 == 0);
    self.isElasticIndicatorLimit = (rand() % 2 == 0);;
    [super reloadPagerTabStripView];
}

-(void)pagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController updateIndicatorFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex{
    
    for (int i=0;i<[swiptTab count];i++) {
        
        if (toIndex==i) {
            [swiptTab replaceObjectAtIndex:i withObject:@"1"];
        }else{
            [swiptTab replaceObjectAtIndex:i withObject:@"0"];
        }
    }
    
    if (fromIndex==1&&toIndex==0) {
        
        LockScreenController *lockscreen = [[NSBundle mainBundle] loadNibNamed:@"LockScreenController" owner:self options:nil][0];
        
        [self.navigationController.view.layer addAnimation:[self leftToRight] forKey:nil];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.navigationController pushViewController:lockscreen animated:NO];
        });
    }
    
    [_collectionView reloadData];
}
// end for setting bar button

-(void)set_initail_objects{
    
    indexCell=0;
    
    if (IS_IPAD==YES) {
        
        self.menubar_right.active = NO;
        self.menubar_left.active = NO;
        [self.menubar_width setConstant:375];
        
    }else{
        self.menubar_width.active = NO;
        self.menubar_center.active = NO;
    }
    
    swiptTab = [[NSMutableArray alloc]initWithObjects:@"0",@"1",@"0",@"0",@"0", nil];
    
    arrayMenuBarImage = [NSMutableArray arrayWithObjects:@"icon_lockscreen",@"icon_home",@"ic_shop",@"icon_cupon",@"ic_setting",nil];
    arrayMenuBarImage_black = [NSMutableArray arrayWithObjects:@"icon_lockscreen",@"icon_home_black",@"ic_shop_black",@"icon_cupon_black",@"ic_setting_black",nil];
    
    [self class_UI];
}

-(void)class_UI{
    
    [self.barView.selectedBar setBackgroundColor:[UIColor blackColor]];
    [self setMenuBar];
}

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (IBAction)goto_lockscreen:(id)sender {
    
    SET_USER_DEFAULT_INFO(@"YES",@"PRESENT_VIEW")
    
    LockScreenController *lockscreen =[[NSBundle mainBundle] loadNibNamed:@"LockScreenController" owner:self options:nil][0];
    [self.navigationController.view.layer addAnimation:[self leftToRight] forKey:nil];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController pushViewController:lockscreen animated:NO];
    });
}

-(CATransition *)leftToRight{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction =
    [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromLeft;
    return transition;
}

@end
