//
//  Announe+WebviewControllerViewController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 4/1/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Announe_WebviewController : BaseViewController

@property (weak, nonatomic) IBOutlet UIView *statusBar;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIView *menuHeader;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingView;

-(void)openUrl:(NSString *)urlText;

@end
