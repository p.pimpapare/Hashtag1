//
//  LoginController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/21/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "LoginController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@import Firebase;

@interface LoginController ()
@end

@implementation LoginController

-(void)viewWillAppear:(BOOL)animated{
    
    GOOGLE_ANALYTICS(@"LoginController");
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    
    NSArray *string_array = @[@"UserInfo",@"UserInfo",@"Selected_Back"];
    
    for (NSString *string in string_array){
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:string];
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    /// subcribe Firebase
    [[FIRMessaging messaging] subscribeToTopic:@"/topics/hashtag1"];
    [BaseViewController log_comment:@"Subscribed to news topic" text:nil];

    [self set_initail_objects];
}

-(void)set_initail_objects{
        
    [self.registerText setText:CALL_CENTER_TEXT];
    
    [PCTapGesture tapAction:self view:self.telLabel actionTarget:@selector(userTappedOnTel:)];
    [PCTapGesture tapAction:self view:self.self.emailTxtLabel actionTarget:@selector(userTappedOnEmail:)];
    
    [PCTapGesture tapAction:self view:self.mobilBtn actionTarget:@selector(mobileLogin:)];
    [PCTapGesture tapAction:self view:self.emailBtn actionTarget:@selector(emailLoginBtn:)];
    [PCTapGesture tapAction:self view:self.registerOnwebBtn3 actionTarget:@selector(gotoTermCon:)];
    
    [self.telLabel setUserInteractionEnabled:YES];
    [self.emailTxtLabel setUserInteractionEnabled:YES];
    
    [self class_UI];
}

- (void)class_UI {
    
    [self setBg];
    
    LINE_WITH_STATUSBAR_LOGIN;
    
    [PCTapGesture set_target_button:self.registerOnWebBtn type:1];
    [PCTapGesture set_target_button:self.registerOnWebBtn2 type:2];
    
    [self.heightLogo setConstant:HEIGHT_LOGO];
    [self.widthLogo setConstant:WIDTH_LOGO];
    
    if (IS_IPAD==YES) {
        
        [self.topLogo setConstant:150];
        
        if (DEVICE_HEIGHT==IPAD_PRO_HEIGHT) {
            
            [self set_button_ui:510 constant_two:555 constant_three:554 constant_four:32];
            
        }else{
            
            [self set_button_ui:310 constant_two:355 constant_three:354 constant_four:32];
        }
        
    }else{
        
        if(DEVICE_HEIGHT==PHONE4){
            
            [self.heightEmail setConstant:35];
            [self.heightMobile setConstant:35];
            [self.heightLogo setConstant:HEIGHT_LOGO45];
            [self.widthLogo setConstant:WIDTH_LOGO45];
            [self.topLogo setConstant:55];
            
        }else{
            
            [self set_button_ui:10 constant_two:53 constant_three:54 constant_four:30];
        }
    }
    
    [PCColor set_shadow:self.emailBtn];
    [PCColor set_shadow:self.mobilBtn];
}

-(void)set_button_ui:(int)btn_register_one constant_two:(int)btn_register_two constant_three:(int)btn_register_three constant_four:(int)btn_register_four{
    
    [self.bottomregister setConstant:btn_register_one];
    [self.bottomregister2 setConstant:btn_register_two];
    [self.bottomregister3 setConstant:btn_register_three];
    [self.bottomregister4 setConstant:btn_register_four];
}

-(void)gotoTermCon:(id)sender{
    [self pushViewCtrlWithViewCtrlName:@"TermAndConditionBeforeLogin"];
}

-(void)mobileLogin:(id)sender{
    
    /* Test for facebook login
     
     FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
     [login setLoginBehavior:FBSDKLoginBehaviorNative];
     [login
     logInWithReadPermissions: @[@"public_profile",@"email"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
     
     if (error) {
     
     ALERT_MESSAGE_YES(@"เกิดข้อผิดพลาด",@"กรุณาลองใหม่ภายหลัง", )
     NSLog(@"Process error");
     
     } else if (result.isCancelled) {
     
     NSLog(@"Cancelled");
     
     } else {
     
     [BaseViewController log_comment:@"Logged in:" text:(id)result];
     
     [self addPCLoading:self];
     [self setHiddenPCLoading:NO];
     
     [self fetchUserInfo];
     }
     }];
     */
    
    [self pushViewCtrlWithViewCtrlName:@"MobileLoginController"];
}

-(void)fetchUserInfo
{
    if ([FBSDKAccessToken currentAccessToken])
    {
        [BaseViewController log_comment:@"Token is available : " text:[[FBSDKAccessToken currentAccessToken] tokenString]];
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, link, first_name, last_name, picture.type(large), email, birthday, bio ,location ,friends ,hometown ,gender ,friendlists"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             
             if (!error)
             {
                 [BaseViewController log_comment:@"result is:" text:result];
             }
             else
             {
                 NSLog(@"Error %@",error);
             }
             
             REMOVE_USER_DEFAULT_INFO(@"FACEBOOK_TOKEN")
             SET_USER_DEFAULT_INFO([[FBSDKAccessToken currentAccessToken] tokenString],@"FACEBOOK_TOKEN");
             
             NSString *user_email;
             
             if (result[@"email"]==nil) {
                 
                 user_email = @" ";
                 
             }else{
                 user_email = result[@"email"];
             }
             
             [BaseViewController log_comment:@"Facebook token" text:[[FBSDKAccessToken currentAccessToken] tokenString]];
         }];
    }
}

-(void)emailLoginBtn:(id)sender{
    [self pushViewCtrlWithViewCtrlName:@"EmailLoginController"];
}

-(void)userTappedOnTel:(id)sender{
    
    NSString *phoneNumber = [@"tel://" stringByAppendingString:CALL_CENTER_TEL];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}

-(void)userTappedOnEmail:(id)sender{
    
    NSString *toEmail= CALL_CENTER_EMAIL;
    NSString *subject=@" ";
    NSString *body = @" ";
    
    NSString *email = [NSString stringWithFormat:@"mailto:%@?subject=%@&body=%@", toEmail,subject,body];
    email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
}

@end
