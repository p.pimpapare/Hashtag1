//
//  AnnounceController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/31/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnnounceController : BaseViewController<UITableViewDelegate,UITableViewDataSource>{
    
    // type 2
    NSMutableArray *item_id2;
    NSMutableArray *itemGuid2;
    NSMutableArray *title2;
    NSMutableArray *thumbnail2;
    NSString *imageUrl2;
    BOOL finisedLoadType2;
}

@property (weak, nonatomic) IBOutlet UIView *statusBar;
@property (weak, nonatomic) IBOutlet UIView *menuHeader;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *viewError;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingView;

@end
