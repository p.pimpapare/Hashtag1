//
//  HistoryController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/7/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "HistoryController.h"

@interface HistoryController (){
    
    BOOL finishLoading, isLoadingService;
    int offset_int;
}

@end

@implementation HistoryController

-(void)viewWillAppear:(BOOL)animated{
    
    GOOGLE_ANALYTICS(@"HistoryController");
}

- (void)viewDidLoad {
    
    [self addPCLoading:self];
    [self setHiddenPCLoading:NO];
    [self setLoadingGrayColor];
    [self setLoadingForContainer];
    
    offset_int = 0;
    
    [self set_parameter];
    [self getservice:offset_int];
    [self class_UI];
}

-(void)set_parameter{
    
    problemID = [[NSMutableArray alloc]init];
    problemTime = [[NSMutableArray alloc]init];
    problemResponse = [[NSMutableArray alloc]init];
    problemText = [[NSMutableArray alloc]init];
}

-(void)getservice:(int)offset{
    
    isLoadingService = YES;
    
    [Service get_user_ingury_history:[self getUserToken] offset:offset selfID:self block:^(id result) {
        
        if (offset==0) {
            [self set_parameter];
        }
        
        NSString *resultText = [NSString stringWithFormat:@"%@",result];
        
        if (![resultText isEqualToString:@"error"]) {
            
            for (int i=0;i<[result[@"inquiries"] count];i++) {
                [problemID addObject:[NSString stringWithFormat:@"%@",result[@"inquiries"][i][@"id"]]];
                [problemText addObject:[NSString stringWithFormat:@"%@",result[@"inquiries"][i][@"message"]]];
                [problemTime addObject:[NSString stringWithFormat:@"%@",result[@"inquiries"][i][@"time"]]];
                [problemResponse addObject:[NSString stringWithFormat:@"%@",result[@"inquiries"][i][@"response"]]];
            }
            
            finishLoading = YES;
            
            offset_int = (int)[problemID count];
            
            isLoadingService = NO;
            
//            NSLog(@"offset count %d",offset_int);
            
            [tableVieww reloadData];
            [self setHiddenPCLoading:YES];
        }
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //    int selectedRow = (int)indexPath.row;
    
    HelpHistoryController *helpScene = [[NSBundle mainBundle] loadNibNamed:@"HelpHistoryController" owner:self options:nil][0];

    [helpScene problemId:problemID[indexPath.row]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController pushViewController:helpScene animated:YES];
    });
    
    [tableVieww deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if ([problemID count]==0) {
        
        if (finishLoading==YES) {
            [warningBlank setHidden:NO];
        }
        
        return 0;
    }else{
        [warningBlank setHidden:YES];
        return [problemID count];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    CGPoint offset = aScrollView.contentOffset;
    CGRect bounds = aScrollView.bounds;
    CGSize size = aScrollView.contentSize;
    UIEdgeInsets inset = aScrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    // NSLog(@"offset: %f", offset.y);
    // NSLog(@"content.height: %f", size.height);
    // NSLog(@"bounds.height: %f", bounds.size.height);
    // NSLog(@"inset.top: %f", inset.top);
    // NSLog(@"inset.bottom: %f", inset.bottom);
    // NSLog(@"pos: %f of %f", y, h);
    
    float reload_distance = 10;
    if(y > h + reload_distance) {
        
        if (isLoadingService==NO) {
            
            [self getservice:offset_int];
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableview cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.view setUserInteractionEnabled:NO];
    
    HistoryTableCell *cell = (HistoryTableCell *)[tableview dequeueReusableCellWithIdentifier:@"Cell"];
    
    if(cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"HistoryTableCell" owner:self options:nil][0];
    }
    
    [self splitDateTime:problemTime[indexPath.row]];
    
    [cell setHistoryCell:[NSString stringWithFormat:@"%@",[dateWithTime objectAtIndex:0]] time:[NSString stringWithFormat:@"%@",[NumberFormat format_hour_minite:[dateWithTime objectAtIndex:1]]] details:problemText[indexPath.row] response:problemResponse[indexPath.row]];
    
    [self.view setUserInteractionEnabled:YES];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return [[self loadNibWithName:@"HistoryTableCell"] frame].size.height;
}

-(NSArray *)splitDateTime:(NSString *)dateTime{
    
    NSArray *splitToArray = [dateTime componentsSeparatedByString:@" "];
    NSString *date = [splitToArray objectAtIndex:0];
    NSString *time = [splitToArray objectAtIndex:1];
    dateWithTime = [NSArray arrayWithObjects:date,time, nil];
    return dateWithTime;
}

-(void)class_UI{
    
    [tableVieww setBackgroundColor:[UIColor clearColor]];
    [tableVieww setSeparatorColor:[UIColor clearColor]];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [tableVieww addSubview:refreshControl];
}

- (void)refresh:(UIRefreshControl *)refreshControl {

    [self getservice:0];
    [refreshControl endRefreshing];
}


@end
