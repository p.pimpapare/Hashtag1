//
//  HowToUseCollectionCell.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/4/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "HowToUseCollectionCell.h"

@implementation HowToUseCollectionCell

-(void)setHowtoImage:(NSString *)imageC index:(int)indexImage{
    //    [self setFrame:CGRectMake(0, 0, DEVICE_WIDTH, DEVICE_HEIGHT)];
    
    self.menuNum = indexImage;
    [self.imageCell setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",imageC]]];
}

-(NSString *)action:(NSString *) selecttag{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"select_howto" object:[NSString stringWithFormat:@"%@",selecttag]];
    [self setTagMenu:selecttag];
    return selecttag;
}

@end
