//
//  ReceiveLuckyPointController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/31/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

// This class is used for displaying user's history get point 

#import <UIKit/UIKit.h>

@interface ReceiveLuckyPointController :  BaseViewController<UITableViewDelegate,UITableViewDataSource>{
    
    // get point
    NSMutableArray *hDateTime;
    NSMutableArray *hName;
    
    BOOL finisedLoadingGetPoint;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *warningBlank;
@property int menu;

@property (weak, nonatomic) IBOutlet UIView *lineBottom;

@end
