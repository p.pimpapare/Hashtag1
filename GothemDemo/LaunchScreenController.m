//
//  LaunchScreenController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/10/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "LaunchScreenController.h"
#import "ContainerViewController.h"

#define CHECK_NETWORK_CONNECTION_FOR_LAUNCHSCREEN(ADD_USERDEFULAT,GOTO_ANOTHER_METHOD)  NSString *resultText = [NSString stringWithFormat:@"%@",result]; if (![resultText isEqualToString:@"error"]) { ADD_USERDEFULAT } GOTO_ANOTHER_METHOD

@interface LaunchScreenController ()
@end

@implementation LaunchScreenController

-(void)viewWillAppear:(BOOL)animated{
    
    [self check_notification_setting];
    [self check_times_to_access];
    
    [self set_logo_animation];
}

-(void)set_logo_animation
{
    [UIImageView animateWithDuration:0.1 animations:^{
        [self.logo setFrame:CGRectMake(self.logo.frame.origin.x+10,self.logo.frame.origin.y,131,131)];
    } completion:^(BOOL finished) {
        [self.logo setFrame:CGRectMake(self.logo.frame.origin.x+10,self.logo.frame.origin.y,131,131)];
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)check_notification_setting{
    
    //    NSLog(@"notiInfo in menuHome %@ %@ %@ %@",notiInfo,voiceInfo,alarmInfo,lockInfo);
    
    BOOL remoteNotificationsEnabled = false, noneEnabled,alertsEnabled, badgesEnabled, soundsEnabled;
    
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        // iOS8+
        remoteNotificationsEnabled = [UIApplication sharedApplication].isRegisteredForRemoteNotifications;
        
        UIUserNotificationSettings *userNotificationSettings = [UIApplication sharedApplication].currentUserNotificationSettings;
        
        noneEnabled = userNotificationSettings.types == UIUserNotificationTypeNone;
        alertsEnabled = userNotificationSettings.types & UIUserNotificationTypeAlert;
        badgesEnabled = userNotificationSettings.types & UIUserNotificationTypeBadge;
        soundsEnabled = userNotificationSettings.types & UIUserNotificationTypeSound;
        
        //        NSLog(@"should alertsEnabled %d",alertsEnabled);
        
        if (alertsEnabled != 0) {
            
            SET_USER_DEFAULT_INFO(@"YES",@"ALERT_ENABLED")
            
            if (soundsEnabled != 0)
            {
                SET_USER_DEFAULT_INFO(@"YES",@"SOUND_ENABLED")
            }else
            {
                SET_USER_DEFAULT_INFO(@"NO",@"SOUND_ENABLED")
            }
            
        }else{
            
            if([[NSUserDefaults standardUserDefaults] objectForKey:@"TimesAccess_ForUpdate"] ==nil){
                
                // Defualt ALERT_ENABLED and SOUND_ENABLED = YES
                [[NSUserDefaults standardUserDefaults] setValue:[CurrentDate_Temperature get_date_format_dd_mm_yy] forKey:@"TimesAccess_ForUpdate"];
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                
                if (userDefaults){
                    [userDefaults setObject:@"YES" forKey:@"ALERT_ENABLED"];
                    [userDefaults setObject:@"YES" forKey:@"SOUND_ENABLED"];
                }
                
                //                // Register for Remote Notifications (Push Notification)
                //                if([[UIApplication sharedApplication] respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
                //                {
                //                    [[UIApplication sharedApplication] registerUserNotificationSettings:  [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeAlert | UIUserNotificationTypeSound) categories:nil]];
                //                    [[UIApplication sharedApplication] registerForRemoteNotifications];
                //                }
                //                else
                //                {
                //                    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIUserNotificationTypeAlert | UIUserNotificationTypeBadge |    UIUserNotificationTypeSound)];
                //                }
                
            }else{
                
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                
                if (userDefaults){
                    [userDefaults setObject:@"NO" forKey:@"ALERT_ENABLED"];
                    [userDefaults setObject:@"NO" forKey:@"SOUND_ENABLED"];
                }
                
                [[UIApplication sharedApplication] unregisterForRemoteNotifications];
            }
        }
    }
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]) {
    }
}

-(void)check_times_to_access{ // Detect first times application launch, application will show intro scene
    
    GET_USER_DEFAULT_INFO(@"HasLaunchedOnce")
    
    if (userInfo==nil){
        
        IntroController *view_class = [self loadNibWithName:@"IntroController"];
        [view_class fromFirstTimesApplicaitonLauch:YES];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.navigationController pushViewController:view_class animated:YES];
        });
        
        SET_USER_DEFAULT_INFO(@"NO",@"HasLaunchedOnce")
        
    }else{
        [self checkUserToken];
    }
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"TimesAccess_ForUpdate"]==nil){
        [[NSUserDefaults standardUserDefaults] setValue:[CurrentDate_Temperature get_date_format_dd_mm_yy] forKey:@"TimesAccess_ForUpdate"];
    }
}

-(void)checkUserToken{ // Check user token, if token is null or logout status return "YES", application will show login scene.
    
    LoginController *view_class = [self loadNibWithName:@"LoginController"];

    GET_USER_DEFAULT_INFO(@"logoutStatus") // Finding logout status
    
    id existing_user_info;
    
    if (userDefaults){
        existing_user_info = (id)[userDefaults objectForKey:@"UserInfo"];
    }
    
    //    NSLog(@"User token %@",existing_user_info);
    
    NSString *logout_status = [NSString stringWithFormat:@"%@",userInfo];
    NSString *user_informaiton = [NSString stringWithFormat:@"%@",existing_user_info];
    NSString *userAgreed = [NSString stringWithFormat:@"%@",existing_user_info[@"agreed"]];
    
    (([user_informaiton length]==3)||([logout_status isEqualToString:@"YES"])||([user_informaiton isEqualToString:@"(null)"]))?
    
    [self.navigationController pushViewController:view_class animated:YES]: // true -> login again
    
    ([userAgreed isEqualToString:@"0"])?[self goToLogin]:
    
    [self setUpdate:[self getUserToken]]; // false -> lock screen
}

-(void)goToLogin{
    
    LoginController *view_class = [self loadNibWithName:@"LoginController"];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController pushViewController:view_class animated:YES];
    });
}

-(void)setUpdate:(NSString *)token{
    
    [self addPCLoading:self];
    [self setHiddenPCLoading:NO];
    
    if ([PCFunctions check_internet_connection]==false) {
        [self goto_home];
    }else{
        
        GET_USER_DEFAULT_INFO(@"AdvertisingInfo3")
        
        if (userInfo == nil || [userInfo count] <= 0) {
            [self setAdvertising1:token];
        }else{
            [self check_update_lockscreen:token];
        }
    }
}

-(void)check_update_lockscreen:(NSString *)token{
    
    NSString *current_date_time = [NSString stringWithFormat:@"%@",[CurrentDate_Temperature get_date_time]];
    
    [Service get_ads_update:token date_time:current_date_time block:^(id result) {
        
        NSString *result_text = [NSString stringWithFormat:@"%@",result];
        
        if (![result_text isEqualToString:@"error"]) {
            
            NSString *shouldUpdate = [NSString stringWithFormat:@"%@",result[@"shouldUpdate"]];
            
            if ([shouldUpdate isEqualToString:@"1"]) {
                [self setLockScreenInfo:token];
            }else{
                [self check_update_user_info];
            }
            
        }else
        {
            [self setLockScreenInfo:token];
        }
    }];
}

-(void)setAdvertising1:(NSString *)token{
    
    [Service get_banner:token brannerId:1 selfID:self block:^(id result) {
        
        CHECK_NETWORK_CONNECTION(
                                 SET_USER_DEFAULT_INFO(result,@"AdvertisingInfo1")
                                 [self setAdvertising2:token];)
    }];
}

-(void)setAdvertising2:(NSString *)token{
    
    [Service get_banner:token brannerId:2 selfID:self block:^(id result) {
        
        CHECK_NETWORK_CONNECTION(
                                 SET_USER_DEFAULT_INFO(result,@"AdvertisingInfo2")
                                 [self setAdvertising3:token];)
    }];
}

-(void)setAdvertising3:(NSString *)token{
    
    [Service get_banner:token brannerId:3 selfID:self block:^(id result) {
        
        CHECK_NETWORK_CONNECTION(
                                 SET_USER_DEFAULT_INFO(result,@"AdvertisingInfo3")
                                 [self setLockScreenInfo:token];)
    }];
}

-(void)setLockScreenInfo:(NSString *)token{
    
    [Service get_ads_info:token block:^(id result) {
        
        CHECK_NETWORK_CONNECTION_FOR_LAUNCHSCREEN(
                                                  SET_USER_DEFAULT_INFO(result,@"LockScreenInfo")
                                                  ,
                                                  [self check_update_user_info]; )
    }];
}

-(void)network_error{
    
    [self.view setUserInteractionEnabled:YES];
    ALERT_MESSAGE_NETWORK( , )
}

-(void)check_update_user_info{
    
    NSString *checkTime = [NSString stringWithFormat:@"%@",(id)[[NSUserDefaults standardUserDefaults] objectForKey:@"TimesAccess_ForUpdate"]];
    NSString *currentTime = [NSString stringWithFormat:@"%@", [CurrentDate_Temperature get_date_format_dd_mm_yy]];
    
    if (![checkTime isEqualToString:currentTime]) {
        
        [Service get_user_info:[self getUserToken] selfID:self block:^(id result) {
            
            NSString *resultText = [NSString stringWithFormat:@"%@",result];
            
            if (![resultText isEqualToString:@"error"]) {
                
                if ([result count]!=0) {
                    
                    /// Update user info per day
                    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                    id userInfo = (id)[userDefaults objectForKey:@"UserInfo"];
                    
                    NSMutableDictionary *dic_copy = [userInfo mutableCopy];
                    
                    [dic_copy removeObjectForKey:@"luckydraw"];
                    [dic_copy setObject:result[@"luckydraw"] forKey:@"luckydraw"];
                    
                    [dic_copy removeObjectForKey:@"earn"];
                    [dic_copy setObject:result[@"earn"] forKey:@"earn"];
                    
                    [dic_copy removeObjectForKey:@"point"];
                    [dic_copy setObject:result[@"point"] forKey:@"point"];
                    
                    [userDefaults setObject:dic_copy forKey:@"UserInfo"];
                    [BaseViewController log_comment:@"Update user info per day %@" text:[NSString stringWithFormat:@"%@",dic_copy]];
                    
                    [self goto_home];
                }
            }
            [self.view setUserInteractionEnabled:YES];
        }];
        
    }else{
        [self goto_home];
    }
}
-(void)goto_home{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController * mainView = [storyboard instantiateViewControllerWithIdentifier:@"ContainerViewController"] ;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController pushViewController:mainView animated:YES];
    });
}

-(BOOL)prefersStatusBarHidden{ // Hiding status bar
    return YES;
}

@end
