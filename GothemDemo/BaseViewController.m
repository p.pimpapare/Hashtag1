//
//  BaseViewController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/19/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "BaseViewController.h"
@import Firebase;

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];    
}

-(NSString *)getUserToken{
    
    GET_USER_DEFAULT_INFO(@"UserInfo")
    return userInfo[@"token"];
}

-(NSString *)getUserAgreed{
    
    GET_USER_DEFAULT_INFO(@"UserInfo")
    return userInfo[@"agreed"];
}

-(NSString *)getUsert1C{
    
    GET_USER_DEFAULT_INFO(@"UserInfo")
    return [self checkStringNull:[NSString stringWithFormat:@"%@",userInfo[@"t1c"]]];
}

-(NSString *)getUserName{
    
    GET_USER_DEFAULT_INFO(@"UserInfo")
    return [self checkStringNull:[NSString stringWithFormat:@"%@",userInfo[@"name"]]];
}

-(NSString *)get_total_point{
    
    GET_USER_DEFAULT_INFO(@"UserInfo")
    [BaseViewController log_comment:@"Userinfo" text:userInfo];
    return [self checkStringNull:[NSString stringWithFormat:@"%@",userInfo[@"point"]]];
}

-(NSString *)get_point_per_day{
    
    GET_USER_DEFAULT_INFO(@"UserInfo")
    return [self checkStringNull:[NSString stringWithFormat:@"%@",userInfo[@"earn"]]];
}

-(NSString *)get_user_lucky_draw{
    
    GET_USER_DEFAULT_INFO(@"UserInfo")
    return [self checkStringNull:[NSString stringWithFormat:@"%@",userInfo[@"luckydraw"]]];
}

-(NSString *)getUserEmail{
    
    GET_USER_DEFAULT_INFO(@"UserEmail")
    return userInfo;
}

-(NSString *)getUserPassword{
    
    GET_USER_DEFAULT_INFO(@"PasswordEmail")
    return userInfo;
}

+(id)getDeviceInfo{
    
    GET_USER_DEFAULT_INFO(@"Device_info")
    
    return userInfo;
}

+(id)getCurrentVersion{
    
    GET_USER_DEFAULT_INFO(@"Device_info")
    return userInfo[@"version"];
}

+(NSString *)getDeviceToken{
    
    NSString *deviceTokenInfo;
    NSUserDefaults *deviceToken = [NSUserDefaults standardUserDefaults];
    
    if (deviceToken){
        deviceTokenInfo = [NSString stringWithFormat:@"%@",(id)[deviceToken objectForKey:@"deviceToken"]];
    }
    
    return deviceTokenInfo;
}

-(void)setUserProfile:(UILabel *)name numberT1C:(UILabel *)t1c balance:(UILabel *)balance earn:(UILabel *)earn luckydraw:(UILabel *)luckydraw{
    
    [name setText:[NSString stringWithFormat:@"%@",[self getUserName]]];
    [t1c setText:[NSString stringWithFormat:@"%@",[self getUsert1C]]];
    [balance setText:[NSString stringWithFormat:@"%@",[NumberFormat format_currency:[NSString stringWithFormat:@"%@",[self get_total_point]]]]];
    [earn setText:[NSString stringWithFormat:@"%@",[self get_point_per_day]]];
    [luckydraw setText:[NSString stringWithFormat:@"%@ สิทธิ์",[self get_user_lucky_draw]]];
}

-(void)setBg{
    
    PCBg *pcbg = [[NSBundle mainBundle] loadNibNamed:@"PCBg" owner:self options:nil][0];
    [pcbg setBg_image];
    [self.view addSubview:pcbg];
    [self.view sendSubviewToBack:pcbg];
}

-(void)setBG_Logo{
    
    PCBg *pcbg_logo = [[NSBundle mainBundle] loadNibNamed:@"PCBg" owner:self options:nil][0];
    [pcbg_logo setBg_color:false alpha:1.0];
    [self.view addSubview:pcbg_logo];
    [self.view sendSubviewToBack:pcbg_logo];
}

-(void)share_activity:(NSString *)sharedInfo{
    
    NSArray *itemsToShare = @[sharedInfo];
    
    UIActivityViewController* activityController = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
    
    [activityController setExcludedActivityTypes:
     @[UIActivityTypeAssignToContact,
       UIActivityTypeCopyToPasteboard,
       UIActivityTypePrint,
       UIActivityTypeSaveToCameraRoll,
       UIActivityTypeAddToReadingList,
       UIActivityTypeAirDrop,
       @"com.apple.reminders.RemindersEditorExtension",
       @"com.apple.mobilenotes.SharingExtension"
       
       ]];
    
    
    /*
     UIKIT_EXTERN NSString *const UIActivityTypePostToFacebook     NS_AVAILABLE_IOS(6_0) __TVOS_PROHIBITED;
     UIKIT_EXTERN NSString *const UIActivityTypePostToTwitter      NS_AVAILABLE_IOS(6_0) __TVOS_PROHIBITED;
     UIKIT_EXTERN NSString *const UIActivityTypePostToWeibo        NS_AVAILABLE_IOS(6_0) __TVOS_PROHIBITED;    // SinaWeibo
     UIKIT_EXTERN NSString *const UIActivityTypeMessage            NS_AVAILABLE_IOS(6_0) __TVOS_PROHIBITED;
     UIKIT_EXTERN NSString *const UIActivityTypeMail               NS_AVAILABLE_IOS(6_0) __TVOS_PROHIBITED;
     UIKIT_EXTERN NSString *const UIActivityTypePrint              NS_AVAILABLE_IOS(6_0) __TVOS_PROHIBITED;
     UIKIT_EXTERN NSString *const UIActivityTypeCopyToPasteboard   NS_AVAILABLE_IOS(6_0) __TVOS_PROHIBITED;
     UIKIT_EXTERN NSString *const UIActivityTypeAssignToContact    NS_AVAILABLE_IOS(6_0) __TVOS_PROHIBITED;
     UIKIT_EXTERN NSString *const UIActivityTypeSaveToCameraRoll   NS_AVAILABLE_IOS(6_0) __TVOS_PROHIBITED;
     UIKIT_EXTERN NSString *const UIActivityTypeAddToReadingList   NS_AVAILABLE_IOS(7_0) __TVOS_PROHIBITED;
     UIKIT_EXTERN NSString *const UIActivityTypePostToFlickr       NS_AVAILABLE_IOS(7_0) __TVOS_PROHIBITED;
     UIKIT_EXTERN NSString *const UIActivityTypePostToVimeo        NS_AVAILABLE_IOS(7_0) __TVOS_PROHIBITED;
     UIKIT_EXTERN NSString *const UIActivityTypePostToTencentWeibo NS_AVAILABLE_IOS(7_0) __TVOS_PROHIBITED;
     UIKIT_EXTERN NSString *const UIActivityTypeAirDrop            NS_AVAILABLE_IOS(7_0) __TVOS_PROHIBITED;
     UIKIT_EXTERN NSString *const UIActivityTypeOpenInIBooks       NS_AVAILABLE_IOS(9_0) __TVOS_PROHIBITED;
     
     
     */
    
    if ( [activityController respondsToSelector:@selector(popoverPresentationController)] ) {
        // iOS8
        activityController.popoverPresentationController.sourceView = self.view; // เฉพาะ ipad เวลา share
        
    }
    [self presentViewController:activityController animated:YES completion:nil];
}

/*-(void)gotoLogout{
 
 // พี่ดู๋  For detecting the scene, if it come from menusetting, it will dismiss again until go to login scene.
 //    [self dismissViewControllerAnimated:NO completion:^{
 //        [[MenuSettingController mainMenu] dismissViewControllerAnimated:YES completion:^{
 //
 //        }] ;
 //    }];
 
 }
 */

+(UIView *)drawLineHeader {
    
    UIView *lineHeader = [[UIView alloc]initWithFrame:CGRectMake(0,20,DEVICE_WIDTH,2)];
    [lineHeader setBackgroundColor:[PCColor color_A8101D]];
    return lineHeader;
}

+(UIView *)drawLineBottom {
    
    UIView *lineBottom = [[UIView alloc]initWithFrame:CGRectMake(0,DEVICE_HEIGHT-2,DEVICE_WIDTH,2)];
    [lineBottom setBackgroundColor:[PCColor color_A8101D]];
    return lineBottom;
}

-(id)loadNibWithName:(NSString *)viewControllerName{
    return [[NSBundle mainBundle] loadNibNamed:viewControllerName owner:self options:nil][0];
}

-(void)pushViewCtrlWithViewCtrlName:(NSString *)viewCtrlName{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController pushViewController:[[NSBundle mainBundle] loadNibNamed:viewCtrlName owner:self options:nil][0] animated:YES];
    });
}

-(void)addPCLoading:(UIViewController *)classController{
    pcload = [[NSBundle mainBundle] loadNibNamed:@"PCLoadingView" owner:self options:nil][0];
    [classController.view addSubview:pcload];
    [pcload setHidden:YES];
}

-(void)setHiddenPCLoading:(BOOL)hide{
    [pcload setHidden:hide];
}

-(void)setLoadingLockScreen{
    [pcload set_size_for_lockScreen_view];
}

-(void)setLoadingShare{
    [pcload set_size_for_share_view];
}

-(void)setLoadingForContainer{
    [pcload set_size_for_container_view];
}

-(void)setLoadingGrayColor{
    [pcload set_gray_loading_color];
}

-(void)addPCBlack:(UIViewController *)classController{
    
    pcblack = [[NSBundle mainBundle] loadNibNamed:@"PCBg" owner:self options:nil][0];
    
    [pcblack setAlpha:0];
    [self.view addSubview:pcblack];
    
    [UIView animateWithDuration:0.4 animations:^{
        [pcblack setBg_color:YES alpha:0.6];
    } completion:^(BOOL finished) {
        
    }];
}

-(void)setHiddenPCBlack:(BOOL)hide{
    [pcblack setHidden:hide];
}

-(NSMutableDictionary *)checkNullValue:(NSMutableDictionary *)dicData{
    
    NSMutableArray *array = [NSMutableArray arrayWithArray:[(NSMutableDictionary *)dicData allKeys]];
    NSMutableDictionary *dicDataTemp = [NSMutableDictionary dictionaryWithDictionary:dicData];
    
    for (NSString *key in array) {
        
        if (![key isEqualToString:@"success"]) {
            
            if ([dicDataTemp[key] count]!=0) {
                
                for (int i=0;i<[dicDataTemp[key] count];i++) {
                    
                    NSMutableArray *subArray = [NSMutableArray arrayWithArray:[dicDataTemp[key][i] allKeys]];
                    NSMutableDictionary *subDicDataTemp = [NSMutableDictionary dictionaryWithDictionary:dicDataTemp[key][i]];
                    
                    for (int j=0;j<[subArray count];j++) {
                        
                        if ([subDicDataTemp[subArray[i]] isEqual:[NSNull null]]||!subDicDataTemp[subArray[i]]) {
                            [subDicDataTemp setObject:[NSString stringWithFormat:@" "] forKey:subArray[j]];
                        }
                    }
                }
            }else{
                
                if ([dicDataTemp[key] isEqual:[NSNull null]]||!dicDataTemp[key]) {
                    [dicDataTemp setObject:[NSString stringWithFormat:@" "] forKey:key];
                }
            }
        }
    }
    return dicDataTemp;
}

-(NSString *)checkStringNull:(NSString *)text_string{
    
    if (([text_string isEqualToString:@"(null)"])||(text_string==nil)) {
        return @" " ;
    }else{
        return text_string;
    }
}

//// login action
+(NSString *)action:(NSString *) selecttag{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"selected_newfeed" object:[NSString stringWithFormat:@"%@",selecttag]];
    return selecttag;
}

+(void)setUserLogoutStatus:(BOOL)logoutStatus{
    
    id logout;
    
    if (logoutStatus==YES)
    {
        logout = @"YES";
        [self action:@"unselectedBtn"];
        
    }else{
        logout = @"NO";
    }
    
    SET_USER_DEFAULT_INFO(logout,@"logoutStatus")
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)alert_login_again{
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"เกิดข้อผิดพลาด" message:@"มีการล็อคอินซ้ำ" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* btn_ok = [UIAlertAction
                                 actionWithTitle:@"ตกลง"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action){
                                     
                                     [BaseViewController setUserLogoutStatus:YES];
                                     
                                     LoginController *view_class = [self loadNibWithName:@"LoginController"];
                                     [self.navigationController pushViewController:view_class animated:YES];

                             /*        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                     UIViewController * loginView = [storyboard   instantiateViewControllerWithIdentifier:@"LoginController"] ;
                                     
                                     [self.navigationController pushViewController:loginView animated:YES];
                              */
                                 }];
        [alert addAction:btn_ok];
        
        [self presentViewController:alert animated:YES completion:nil];
    }];
}

+(BOOL)check_service_status:(id)result
{
    NSString *resultText = [NSString stringWithFormat:@"%@",result];
    
    if (![resultText isEqualToString:@"error"] && [result objectForKey:@"success"] != nil)
    {
        NSString *successText = [NSString stringWithFormat:@"%@",result[@"success"]];
        
        if ([successText isEqualToString:@"0"])
        {
            return false;
            
        }else{
            return true;
        }
        
    }else{
        return false;
    }
}

+(void)log_comment:(id)topic_text text:(NSString *)text
{
//    NSLog(@"%@ %@",topic_text,text);
}

+(void)setLineColor:(UIView *)lineHeader bottom:(UIView *)lineBottom{
    
    [lineHeader setBackgroundColor:[PCColor color_A8101D]];
    [lineBottom setBackgroundColor:[PCColor color_A8101D]];
}

@end
