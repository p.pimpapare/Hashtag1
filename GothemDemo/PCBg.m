
//  Created by pimpaporn chaichompoo on 1/28/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "PCBg.h"

@implementation PCBg

-(void)awakeFromNib{
    
    [super awakeFromNib];
    [self setFrame:CGRectMake(0,0,DEVICE_WIDTH,DEVICE_HEIGHT)];
}

-(void)setBg_image{
    [self setBackgroundColor:[UIColor clearColor]];
    [self setImage:[UIImage imageNamed:@"background"]];
}

-(void)setBg_color:(BOOL)blackColor alpha:(double)alpha_num{
    
    [self setImage:nil];
    [self setAlpha:alpha_num];
    
    if(blackColor == true)
    {
        [self setBackgroundColor:[UIColor blackColor]];
    }else
    {
        [self setBackgroundColor:[PCColor color_EDEDED]]; // gray color
    }
}

@end
