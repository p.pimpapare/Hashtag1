//
//  ViewController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 5/17/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

// This class is used for setting to be a child class of XLPageStripController library
#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@end
