
//  Created by pimpaporn chaichompoo on 2/2/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

#import <UIKit/UIKit.h>

@interface MenuCoupon_TableViewCell : UITableViewCell{
    
    NSTimer *timer;
    NSString *countTime;
}

@property (weak, nonatomic) IBOutlet UIView *viewBg;

@property (weak, nonatomic) IBOutlet UILabel *cTitle;
@property (weak, nonatomic) IBOutlet UIImageView *cImage;
@property (weak, nonatomic) IBOutlet UILabel *cDetail;
@property (weak, nonatomic) IBOutlet UILabel *cCode;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UIImageView *imageTime;

-(void)setCoupon:(NSString *)title detail:(NSString *)detail code:(NSString *)code image:(NSString *)image;
-(void)setUserCoupon:(NSString *)title detail:(NSString *)detail time:(NSString *)time image:(NSString *)image;

@end
