//
//  MenuUserCuponController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/20/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

// Use setHeaderText() for calling get getService_fromMycupon_type1() or type2,3()

#import "MenuUserCuponController.h"

@interface MenuUserCuponController (){
    
    MenuCouponDetail_Alert *menuAlert;
    MenuCuponDetailCode *menuCouponUsedCode; // barcode
}

@end

@implementation MenuUserCuponController

-(void)viewWillAppear:(BOOL)animated{
    
    GOOGLE_ANALYTICS(@"MenuUserCuponController");
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self.loadingView setHidden:NO];
    [self.loadingView startAnimating];
    
    [self set_initail_objects];
}

-(void)getService_fromMycupon_type1:(NSString *)token{
    
    [Service get_my_coupon:token selfID:self block:^(id result) {
        
        NSString *resultText = [NSString stringWithFormat:@"%@",result];
        
        if ([resultText isEqualToString:@"error"]) {
            
            [self getService_fromMycupon_type1:token];
            
        }else{
            
            couponID = [[NSMutableArray alloc]init];
            couponTitle = [[NSMutableArray alloc]init];
            couponSubTitle = [[NSMutableArray alloc]init];
            couponImage = [[NSMutableArray alloc]init];
            couponDetails = [[NSMutableArray alloc]init];
            couponCounterTime = [[NSMutableArray alloc]init];
            couponUsePoint = [[NSMutableArray alloc]init];
            couponBrandLogo = [[NSMutableArray alloc]init];
            couponCpuse_id = [[NSMutableArray alloc]init];
            
            for (int i=0; i<[result[@"coupon"] count];i++) {
                
                [couponID addObject:result[@"coupon"][i][@"coupon_id"]];
                [couponTitle addObject:result[@"coupon"][i][@"coupon_title"]];
                [couponSubTitle addObject:result[@"coupon"][i][@"coupon_subtitle"]];
                
                [couponImage addObject:result[@"coupon"][i][@"coupon_img"]];
                [couponDetails addObject:result[@"coupon"][i][@"coupon_desc"]];
                [couponCounterTime addObject:result[@"coupon"][i][@"expire_date"]];
                [couponUsePoint addObject:result[@"coupon"][i][@"use_point"]];
                [couponBrandLogo addObject:result[@"coupon"][i][@"brand_logo"]];
                [couponCpuse_id addObject:result[@"coupon"][i][@"cpuse_id"]];
            }
            
            finishedLoading = YES;
            
            [self.loadingView setHidden:YES];
            [self.loadingView stopAnimating];
            
            [self.tableView reloadData];
        }
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    selectedRow = (int)indexPath.row;
    
    [self addPCBlack:self];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(closeDetailView)];
    
    [pcblack addGestureRecognizer:tap];
    
    [self addMenuAlert:couponBrandLogo[selectedRow]];
}

-(void)addMenuAlert:(NSString *)logoimage{
    
    menuAlert = [self loadNibWithName:@"MenuCouponDetail_Alert"];
    
    [menuAlert set_logo_coupon:logoimage];
    
    [menuAlert.usedCouponBtn addTarget:self action:@selector(usedCoupon) forControlEvents:UIControlEventTouchUpInside];
    [menuAlert.storeCouponBtn addTarget:self action:@selector(storeCoupon) forControlEvents:UIControlEventTouchUpInside];
    [menuAlert.cancelBtn addTarget:self action:@selector(closeDetailView) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:menuAlert];
    
    [menuAlert setAlpha:0];
    
    [PCView setViewAnimationWithDuration:menuAlert time_duration:0.4 showView:YES];
}

-(void)closeDetailView{
    
    [self setHiddenPCBlack:YES];
    [pcblack setUserInteractionEnabled:YES];
    [menuAlert removeFromSuperview];
    [menuCouponUsedCode removeFromSuperview];
    [self.viewError setHidden:YES];
    [self.tableView setUserInteractionEnabled:YES];
    [self.backBtn setUserInteractionEnabled:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if([couponTitle count]==0){
        
        if (finishedLoading==YES) {
            [self.warningView setHidden:NO];
            [self.tableView setHidden:YES];
        }
        return 0;
    }else
    {
        [self.warningView setHidden:YES];
        [self.tableView setHidden:NO];
        return [couponTitle count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MenuCoupon_TableViewCell *cell = (MenuCoupon_TableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if(cell == nil) {
        cell = [self loadNibWithName:@"MenuCoupon_TableViewCell"];
    }
    
    [cell setUserCoupon:couponTitle[indexPath.row] detail:couponDetails[indexPath.row] time:couponCounterTime[indexPath.row] image:couponImage[indexPath.row]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [[self loadNibWithName:@"MenuCoupon_TableViewCell"] frame].size.height;
}

-(void)notification_alert:(NSNotification *)noti{
    
    NSString *strNoti = [noti object];
    
    if ([strNoti isEqualToString:@"finishedUsedCoupon"]){
        [self finishedUsedCoupon];
    }
}

-(void)storeCoupon{
    [self closeDetailView];
}

-(void)usedCoupon{
    [self postService_useMyCoupon];
}

-(void)finishedUsedCoupon{
    
    [self closeDetailView];
    [self getService_fromMycupon_type1:userToken];
}

-(void)postService_useMyCoupon{
    
    [menuAlert removeFromSuperview];
    
    [Service post_user_use_my_coupon:userToken myCouponID:couponCpuse_id[selectedRow] selfID:self block:^(id result) {
        
        NSString *resultText = [NSString stringWithFormat:@"%@",result];
      
        if (![resultText isEqualToString:@"error"]) {
            
            NSString *successStatus = [NSString stringWithFormat:@"%@",result[@"success"]];
            
            if ([successStatus isEqualToString:@"1"]) {
                
                NSString *duration = [NSString stringWithFormat:@"%@",result[@"duration"]];
                NSString *code = [NSString stringWithFormat:@"%@",result[@"code"]];
                
                [pcblack setBg_color:YES alpha:1.0];
                
                [pcblack setUserInteractionEnabled:NO];
                
                [self.tableView setUserInteractionEnabled:NO];
                [self.backBtn setUserInteractionEnabled:NO];
                
                menuCouponUsedCode = [[NSBundle mainBundle] loadNibNamed:@"MenuCuponDetailCode" owner:self options:nil][0];
                
                [menuCouponUsedCode setMenuCoupon:couponTitle[selectedRow] detail:couponDetails[selectedRow] time:duration code:code];
                
                [menuCouponUsedCode.usedCodeBtn addTarget:self action:@selector(finishedUsedCoupon) forControlEvents:UIControlEventTouchUpInside];
                [menuCouponUsedCode.readmoreBtn addTarget:self action:@selector(goToReadMore) forControlEvents:UIControlEventTouchUpInside];
                [menuCouponUsedCode.shareBtn addTarget:self action:@selector(goToShare) forControlEvents:UIControlEventTouchUpInside];
                
                [self.view addSubview:menuCouponUsedCode];
                
                [menuCouponUsedCode setAlpha:0];
                
                [UIView animateWithDuration:0.4 animations:^{
                    
                    [menuCouponUsedCode setAlpha:1.0];
                    
                } completion:^(BOOL finished) {
                    
                }];
            }
        }
    }];
}

-(void)goToShare{
    
    [self shareInfoBtn:couponID[selectedRow]];
}

-(void)shareInfoBtn:(NSString *)coupon_ID{
    
    [Service getShareUrl:userToken couponID:coupon_ID selfID:self block:^(id result) {
        [self share_activity:[NSString stringWithFormat:@"%@",result[@"url"]]];
    }];
}

-(void)goToReadMore{
    
    [self goToReadMore:couponID[selectedRow] title:couponTitle[selectedRow] detail:couponDetails[selectedRow]point:couponUsePoint[selectedRow]logo:couponBrandLogo[selectedRow]];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

-(void)set_initail_objects{
    
    selectedRow=0;
    userToken = [NSString stringWithFormat:@"%@",[self getUserToken]];
    
    [self class_UI];
}

-(void)class_UI{
    
    [self setBG_Logo];
    
    SWIPETLIFE_TOGOTOPREVIOUS;
    LINE_WITH_STATUSBAR_HEADER_NORMAL;
    TABLEVIEW_COLLECTION_REFRESH(self.tableView);
    
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    [self.tableView setSeparatorColor:[UIColor clearColor]];
}

- (void)refresh:(UIRefreshControl *)refreshControl {
    [self getService_fromMycupon_type1:userToken];
    [refreshControl endRefreshing];
}

-(void)swipeRight:(UISwipeGestureRecognizer*)gestureRecognizer{
    GOTO_PREVIOUS;
}

- (IBAction)backBtn:(id)sender {
    GOTO_PREVIOUS;
}

-(void)setTiltleHeader:(NSString *)title type:(NSString *)type{
    
    [self.titleHeader setText:[NSString stringWithFormat:@"%@",title]];
    
    GET_USER_DEFAULT_INFO(@"UserInfo")
    [self getService_fromMycupon_type1:userInfo[@"token"]];
}

-(void)goToReadMore:(NSString *)receiveID title:(NSString *)receivereadMoreTitle detail:(NSString *)receiveReadModeDetails point:(NSString *)receivePoint logo:(NSString *)logo{
    
    ReadMoreController *readmore = [[NSBundle mainBundle] loadNibNamed:@"ReadMoreController" owner:self options:nil][0];
    
    [readmore setDetailReadmore:receiveID title:receivereadMoreTitle point:receivePoint logo:logo];
    [self presentViewController:readmore animated:YES completion:nil];
}

@end
