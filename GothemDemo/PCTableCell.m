
//  Created by pimpaporn chaichompoo on 2/1/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "PCTableCell.h"

@implementation PCTableCell

-(void)awakeFromNib{
    
    [super awakeFromNib];
    [self setFrame:CGRectMake(0,0,DEVICE_WIDTH,54)];
    
    [self.selectedImage setHidden:YES];
    [self setBackgroundColor:[UIColor clearColor]];
    
    tab=NO;
    
    [self.loading setHidden:YES];
    [self.btn_update_version setHidden:YES];
    [self.btn_update_version.layer setCornerRadius:4];
    [PCShadow set_shadow:0.2 shadowRadius:2 shadowOpacity:0.5 view:self.btn_update_version];
    [self.btn_update_version.titleLabel setFont:[UIFont fontWithName:@"RSU" size:14.0]];
}

-(void)setIconCell:(NSString *)icon index:(int)index{
    
    switch (index) {
            
        case 0:
            [self.height setConstant:24];
            [self.width setConstant:13];
            [self.right setConstant:30];
            break;
        case 1:
            [self.height setConstant:21];
            [self.width setConstant:24];
            break;
        case 2:
            [self.height setConstant:23];
            [self.width setConstant:24];
            break;
        case 3:
            [self.height setConstant:21];
            [self.width setConstant:23];
            break;
        case 4:
            [self.height setConstant:21];
            [self.width setConstant:19];
            break;
        case 5:
            [self.height setConstant:19];
            [self.width setConstant:21];
            break;
        case 6:
            [self.height setConstant:15];
            [self.width setConstant:22];
            break;
        case 7:
            [self.height setConstant:22];
            [self.width setConstant:18];
            [self.right setConstant:28];
            break;
        case 8:
            [self.height setConstant:21];
            [self.width setConstant:21];
            break;
        default:
            [self.selectedImage setHidden:YES];
            break;
    }
    [self.iconInCell setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",icon]]];
}

-(void)setNameCell:(NSString *)name{
    
    [self.nameInCell setText:[NSString stringWithFormat:@"%@",name]];
}

-(void)set_cell_update_version:(NSString *)name{
    
    [self.btn_update_version setHidden:NO];
    
    NSString *result_text;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if (userDefaults){
        
        result_text = [NSString stringWithFormat:@"%@",(id)[userDefaults objectForKey:@"Should_Update"]];
    }
    
    if ([result_text isEqualToString:@"YES"]) {
        update_version = YES;
        [self.btn_update_version setTitle:@"อัพเดท" forState:UIControlStateNormal];
    }else{
        update_version = NO;
        [self.btn_update_version setHidden:YES];
//        [self.btn_update_version setTitle:@"ตรวจสอบเวอร์ชั่น" forState:UIControlStateNormal];
    }
    
    [self.nameInCell setText:[NSString stringWithFormat:@"%@",name]];
}

-(void)setNewsWidgetCell:(NSString *)name image:(NSString *)icon{
    
    NSArray *actionBtn = @[@"selectedBtn",@"unselectedBtn"];
    [self.selectedImage setHidden:NO];
    
    if ([self.ActionBtn isEqual:actionBtn[0]]) {
        [self.selectedImage setImage:[UIImage imageNamed:@"checkMark"]];
        [self action:@"selectedBtn"];
    }else if([self.ActionBtn isEqual:actionBtn[1]]){
        [self.selectedImage setImage:[UIImage imageNamed:@"circle_gray"]];
        [self action:@"unselectedBtn"];
    }
    
    [self setBackgroundColor:[UIColor clearColor]];
    [self.nameInCell setText:[NSString stringWithFormat:@"%@",name]];
    [self.iconInCell setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",icon]]];
}

-(void)closeTime{
    
    [self.loading stopAnimating];
    [self.loading setHidden:YES];
    [timer invalidate];
}

- (void)selectedNewfeedCell {
    
    if(tab==NO){
        
        tab=YES;
        [self.selectedImage setImage:[UIImage imageNamed:@"checkMark"]];
        [self action:@"selectedBtn"];
        
    }else if(tab==YES){
        
        tab=NO;
        [self.selectedImage setImage:[UIImage imageNamed:@"circle_gray"]];
        [self action:@"unselectedBtn"];
    }
}

-(NSString *)action:(NSString *) selecttag{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"select_menu_bar" object:[NSString stringWithFormat:@"%@",selecttag]];
    [self setTagMenu:selecttag];
    return selecttag;
}

- (IBAction)btn_update:(id)sender {
    
    if (update_version==YES) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://**ax.itunes**.apple.com/app/hashtag-1/id1109947887?ls=1&mt=8"]];
    }
}

@end
