//
//  ReplaceHTMLFont.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 4/22/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "ReplaceHTMLFont.h"

@implementation ReplaceHTMLFont


+(NSString *)replaceHTML_by_range:(id)old_HTML start_string:(NSString *)start_string end_string:(NSString *)end_string replaceString:(NSString *)new_String{
    
    NSString *newHTMLString = [NSString stringWithFormat:@"%@",old_HTML];
    
    int start_string_range = (int)[newHTMLString rangeOfString:[NSString stringWithFormat:@"%@",start_string]].location;
    int end_string_range = (int)[newHTMLString rangeOfString:[NSString stringWithFormat:@"%@",end_string]].location;
    int range = end_string_range-start_string_range;
    
    //    NSLog(@"start string %@ \n end_string %@",start_string,end_string);
    //    NSLog(@"start string %d \n end string %d",start_string_range,end_string_range);
    
    newHTMLString = [newHTMLString stringByReplacingCharactersInRange:NSMakeRange(start_string_range,range) withString:[NSString stringWithFormat:@"%@",new_String]];
    
    return newHTMLString;
}

+(NSString *)replaceHTML_by_string:(NSString *)HTML old_string:(NSString *)old_string new_string:(NSString *)new_string{
    
    NSString *newHTMLString = [NSString stringWithFormat:@"%@",HTML];
    
    newHTMLString = [newHTMLString stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@",old_string] withString:[NSString stringWithFormat:@"%@",new_string]];
    
    return newHTMLString;
}

+(NSString *)replaceHTML_blank_space:(NSString *)HTML{
    
    NSString *trimmed = [HTML stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    return trimmed;
}

@end
