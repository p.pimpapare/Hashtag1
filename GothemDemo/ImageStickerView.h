//
//  ImageStickerView.h
//  testCode
//
//  Created by Akarapas Wongkaew on 5/10/2559 BE.
//  Copyright © 2559 Akarapas Wongkaew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageStickerView : UIView
{
    
}

@property(nonatomic, strong) UIImageView *imageUser; // uiimage ที่user เลือก
@property(nonatomic, strong) UIImageView *imageSticker; // uiimage

-(void)setImageStickerWithImage:(UIImage *)image;
-(void)setImageUserWithImage:(UIImage *)image;
-(UIImage *)getImageSticker; // เลือก image

@end
