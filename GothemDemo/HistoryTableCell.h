//
//  HistoryTableCell.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/8/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryTableCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *hImage;
@property (weak, nonatomic) IBOutlet UILabel *hStatus;
@property (weak, nonatomic) IBOutlet UILabel *hDate;
@property (weak, nonatomic) IBOutlet UILabel *hTime;
@property (weak, nonatomic) IBOutlet UILabel *hDetails;

-(void)setHistoryCell:(NSString *)date time:(NSString *)time details:(NSString *)details response:(NSString *)response;

@end
