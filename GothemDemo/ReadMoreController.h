//
//  ReadMoreController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/20/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

// This class is used for displaying coupon's information details

#import <UIKit/UIKit.h>

@interface ReadMoreController : BaseViewController{
    
    NSString *html;
}

@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;
@property (weak, nonatomic) IBOutlet UIView *viewReadMore;

@property (weak, nonatomic) IBOutlet UILabel *titleDetail;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UILabel *point;
@property (weak, nonatomic) IBOutlet UIImageView *icon_coin;

@property (weak, nonatomic) IBOutlet UIView *statusBar;
@property (weak, nonatomic) IBOutlet UIView *viewHeader;
@property (weak, nonatomic) IBOutlet UIView *viewShadow;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingView;

-(void)setDetailReadmore:(NSString *)coupon_ID title:(NSString *)title point:(NSString *)point logo:(NSString *)logo;

@end
