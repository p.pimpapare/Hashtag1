//
//  MenuCouponDetail_Alert.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/17/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "MenuCouponDetail_Alert.h"

@implementation MenuCouponDetail_Alert

-(void)awakeFromNib{
    
    [super awakeFromNib];
    
    [self.storeCouponBtn setUserInteractionEnabled:YES];
    
    [self setFrame:CGRectMake(0,0,DEVICE_WIDTH,DEVICE_HEIGHT)];
    
    [BaseViewController setLineColor:self.lineHeader bottom:self.lineBottom];
    [self.lineGray setBackgroundColor:[PCColor color_A6A6A6]];
    
    [self.textDetail setText:@"กด \"ใช้คูปอง\" ที่หน้าร้านค้าเพื่อ\nรับสิทธ์พิเศษภายในเวลาที่กำหนด\nหรือ\n กด \"เก็บคูปอง\" เพื่อรับสิทธิ์พิเศษ\nไว้ใช้ภายหลังและสามารถ\nเรียกดูคูปองได้ในเมนูคูปองของฉัน"];
    
    // set two colors in one label range () ตัวหลัง นับสระด้วย
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:self.textDetail.text];
    [string addAttribute:NSForegroundColorAttributeName value:[PCColor color_A8101D] range:NSMakeRange(4,8)];
    [string addAttribute:NSForegroundColorAttributeName value:[PCColor color_A8101D] range:NSMakeRange(75,9)];
    self.textDetail.attributedText = string;
}

-(void)set_logo_coupon:(NSString *)logoImage{
    
    [self.logo sd_setImageWithURL:[NSURL URLWithString:logoImage]
                 placeholderImage:[UIImage imageNamed:@"picture_1_2"]options:SDWebImageProgressiveDownload];
}

@end
