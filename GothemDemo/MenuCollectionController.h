//
//  MenuCollectionController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/20/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

// This class is used for displaying coupon brandgrid inside coupon category menu (menu 2)

#import <UIKit/UIKit.h>
#import "RFQuiltLayout.h"

@interface MenuCollectionController : BaseViewController<RFQuiltLayoutDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate>{
    
    NSMutableArray *arrayName;
    NSMutableArray *arrayImage;
    NSMutableArray *bID;
    
    NSMutableArray *cell_height;
    NSMutableArray *cell_width;

    NSString *couponName;
    NSString *couponImage;
    NSString *couponID;
    
    NSString *couponName1_w2;
    NSString *couponImage1_w2;
    NSString *couponIDw1_2;

    NSString *couponName2_w2;
    NSString *couponImage2_w2;
    NSString *couponIDw2_2;
    
    id storeResult;
    NSString *cId_num;
    
    int selectRow;
    BOOL finisedLoading;
    BOOL have_special_coupon, have_special_coupon_2_2;
}

@property (weak, nonatomic) IBOutlet UILabel *titleHeader;

@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIView *statusBar;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingView;

// Warning view
@property (weak, nonatomic) IBOutlet UIView *viewWarning;
@property (weak, nonatomic) IBOutlet UILabel *warningText;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top_collection_view;

@property (weak, nonatomic) IBOutlet UIView *menuHeader;

-(void)setHeaderText:(NSString *)header categoryID:(NSString *)cID;

@end
