//
//  MenuCuponController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/19/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "MenuCuponController.h"

@interface MenuCuponController()<KDCycleBannerViewDataSource, KDCycleBannerViewDelegate>

@property (weak, nonatomic) IBOutlet KDCycleBannerView *cycleBannerViewTop;
@property (strong, nonatomic) KDCycleBannerView *cycleBannerViewBottom;

@end

@implementation MenuCuponController

KDCycleDelegate(@"picture_1_2",3)

-(void)viewWillAppear:(BOOL)animated{
    
    GOOGLE_ANALYTICS(@"MenuCuponController");
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self getAdvertising];
}

-(void)getAdvertising{
    
    advertising_array = [[NSMutableArray alloc]init];
    
    GET_USER_DEFAULT_INFO(@"AdvertisingInfo3")
    
    for (int i=0;i<[userInfo[@"banner"] count];i++) {
        
        [advertising_array addObject:userInfo[@"banner"][i][@"banner_image"]];
    }
    
    [self set_initail_objects];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //    int selectedRow = (int)indexPath.row;
    
    if (indexPath.row==0) {
        
        [self goToMenu3TableView:@"ประวัติการแลกคะแนน" menu:1];
   
    }else if(indexPath.row==1){
    
        [self goToMenu3TableView:@"คูปองของฉัน" menu:2];
    
    }else{
    
        [self goToReceivePoint];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [titleArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Menu3TableViewCell *cell = (Menu3TableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if(cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"Menu3TableViewCell" owner:self options:nil][0];
    }
    
    [cell setCellInfo:titleArray[indexPath.row] detail:detailarray[indexPath.row]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [[[NSBundle mainBundle] loadNibNamed:@"Menu3TableViewCell" owner:self options:nil][0] frame].size.height;
}

-(void)set_initail_objects{
    
    titleArray = [[NSMutableArray alloc]initWithObjects:@"ประวัติการแลกคะแนน",@"คูปองของฉัน",@"ประวัติการรับคะแนน", nil];
    detailarray = [[NSMutableArray alloc]initWithObjects:@"บันทึกการแลกคะแนนเพื่อรับสิทธิ์พิเศษ\nและของรางวัล",@"ตรวจสอบรายการคูปอง \nจากการแลกคะแนน ที่พร้อมใช้งาน",@"บันทึกการรับคะแนน\nจากการใช้งาน", nil];
    
    [self class_UI];
}

-(void)class_UI{
    
    // set bg
    [self setBG_Logo];
    
    [self.tableview setBackgroundColor:[UIColor clearColor]];
    [self.tableview setSeparatorColor:[UIColor clearColor]];
    
    if (IS_IPAD==YES) {
        
        if (DEVICE_HEIGHT==IPAD_PRO_HEIGHT) {
            
            [heightAdvertising setConstant:HEIGHT_ADVERTISING_IPAD_PRO];
        }else{
            
            [heightAdvertising setConstant:HEIGHT_ADVERTISING_IPAD];
        }
        
    }else{
        
        if(DEVICE_WIDTH==PHONE6_WIDTH){
            
            [heightAdvertising setConstant:HEIGHT_ADVERTISING+25];
            
        }else if (DEVICE_WIDTH>=PHONEPLUS){
            
            [heightAdvertising setConstant:HEIGHT_ADVERTISING+42];
            
        }else{
            
            [self.cycleBannerViewBottom setFrame:CGRectMake(0,0,DEVICE_WIDTH,HEIGHT_ADVERTISING)];
        }
    }
    [self setPagerAdvertising];
}

-(NSString *)titleForPagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController{
    return @"View";
}

-(void)goToMenu3TableView:(NSString *)title menu:(int)menuNum{
    
    UserHistoryTableView *menu3TableView = [[NSBundle mainBundle] loadNibNamed:@"UserHistoryTableView" owner:self options:nil][0];
    [menu3TableView setHeaderText:[NSString stringWithFormat:@"%@",title]menuNum:menuNum];
    
    [self presentViewController:menu3TableView animated:YES completion:nil];
}

-(void)goToReceivePoint{
    
    UIStoryboard *storybord = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController * receiveView = [storybord   instantiateViewControllerWithIdentifier:@"ContainerRecievePointController"];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:receiveView animated:YES completion:nil];
    });
}

@end
