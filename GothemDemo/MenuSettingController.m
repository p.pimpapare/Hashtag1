//
//  MenuSettingController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/19/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

#import "MenuSettingController.h"
#import "ContainerViewController.h"
#import "ContainerViewController.h"

@interface MenuSettingController (){
    
    NotReadyAlert *notReadyAlert;
    ContainerViewController *mainView;
    BOOL tab, tab_update;
}
@end

// พี่ดู๋
//MenuSettingController *__menu = nil;

@implementation MenuSettingController

-(void)viewWillAppear:(BOOL)animated{
    
    GOOGLE_ANALYTICS(@"MenuSettingController");
    [self setUserProfile];
    //    [self get_user_info];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self set_initail_objects];
}

-(void)setUserProfile{
    
    [self setUserProfile:self.userName numberT1C:self.userNum balance:self.userBalancePoint earn:self.userEarnedPoint luckydraw:nil];
}

-(void)set_initail_objects{
    
    arrayImageSetting = [NSMutableArray arrayWithObjects:@"ic_version",@"icon_newfeed_setting",@"profile1",@"noti",@"ic_aboutme",@"ic_notic",@"ichowto",@"help1",@"help2",nil];
    
    [self set_title_text:[NSString stringWithFormat:@"เวอร์ชั่นปัจจุบัน %@",[BaseViewController getCurrentVersion]]];
    [self class_UI];
}

-(void)set_title_text:(NSString *)update_version{
    
    arrayNameSetting = [NSMutableArray arrayWithObjects:update_version,@"เปิดใช้งานแถบข่าว",@"บัญชีของฉัน",@"การแจ้งเตือน",@"เกี่ยวกับเรา",@"ประกาศ",@"วิธีการใช้งาน",@"ความช่วยเหลือ",@"คำถามที่พบบ่อย",nil];
}

-(void)class_UI{
    
    // พี่ดู๋
    //    __menu =self;
    
    // set bg gradient
    self.vwGradient.flag=YES;
    self.vwGradient.code=2;
    [self.vwGradient setNeedsDisplay];
    
    int height_ads_num = 0;
    
    if (IS_IPAD==YES) {
        
        self.profile_view_left.active = NO;
        self.profile_view_right.active = NO;
        [self.profile_view_width setConstant:500];
        
        if (DEVICE_HEIGHT==IPAD_PRO_HEIGHT) {
            height_ads_num = HEIGHT_ADVERTISING_IPAD_PRO-10;
        }else{
            height_ads_num = HEIGHT_ADVERTISING_IPAD-10;
        }
        
    }else{
        
        self.profile_view_width.active = NO;
        self.profile_view_center_h.active = NO;
        self.profile_view_center_v.active = NO;
        
        if(DEVICE_WIDTH==PHONE6_WIDTH){
            height_ads_num = HEIGHT_ADVERTISING+10;
        }else if (DEVICE_WIDTH>=PHONEPLUS){
            height_ads_num = HEIGHT_ADVERTISING+32;
        }else{
            height_ads_num = HEIGHT_ADVERTISING-10;
        }
    }
    
    self.heightAdvertising.constant = height_ads_num;
    
    [self setBG_Logo];
    
    [self.tableview setSeparatorColor:[UIColor clearColor]];
    [self.tableview setBackgroundColor:[UIColor clearColor]];
    
    NSMutableArray *label_array = [[NSMutableArray alloc]initWithObjects:self.noTopic,self.balanceTopic,self.point1,self.point2,self.userBalancePoint,self.earnTopic,self.userName,self.userNum,self.earnTopic, nil];
    
    for (UILabel *label in label_array) {
        [label setFont:[PCFont fontRSU:15.0]];
    }
}

// set tableView delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    int selectedRow = (int)indexPath.row;
    PCTableCell *cell = (PCTableCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    if (indexPath.row==0) { // check update
        
        GET_USER_DEFAULT_INFO(@"Should_Update")
        
        NSString *update_text = [NSString stringWithFormat:@"%@",userInfo];
        
        if ([update_text isEqualToString:@"YES"]) {
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://**ax.itunes**.apple.com/app/hashtag-1/id1109947887?ls=1&mt=8"]];
            
        }else{
            
            [self set_title_text:[NSString stringWithFormat:@"เวอร์ชั่นปัจจุบัน %@",[BaseViewController getCurrentVersion]]];
            [self.tableview reloadData];
        }
        
    }else{
        
        switch (indexPath.row) {
                
            case 1:
                [cell selectedNewfeedCell];
                [self setAction_newfeedBtn];
                break;
            case 5:
                [self goToAnnounScene];
                break;
            case 6:
                [self goToHowTo];
                break;
            case 7:
                [self goToSettingQA:arrayNameSetting[indexPath.row]];
                break;
            default:
                [self gotoMenuSettingTableViewController:arrayNameSetting[indexPath.row] menuNum:selectedRow];
                break;
        }
    }
}

-(void)setAction_newfeedBtn{
    
    // remove for updating information
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"newsfeed_setting"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSUserDefaults *newfeedDefaults = [NSUserDefaults standardUserDefaults];
    
    if (newfeedDefaults){
        
        if(tab==NO){
            
            tab=YES;
            [newfeedDefaults setObject:@"selectedBtn" forKey:@"newsfeed_setting"];
            [self action:@"selectedBtn"];
            
        }else if(tab==YES){
            
            tab=NO;
            [newfeedDefaults setObject:@"unselectedBtn" forKey:@"newsfeed_setting"];
            [self action:@"unselectedBtn"];
        }
    }
}

-(void)closeAlert{
    [notReadyAlert removeFromSuperview];
    [self.view setUserInteractionEnabled:YES];
}

// พี่ดู๋
//+(instancetype)mainMenu{
//    return __menu;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [arrayNameSetting count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSArray *actoinBtn = @[@"unselectedBtn",@"selectedBtn"];
    
    PCTableCell *cell = (PCTableCell *)[tableView dequeueReusableCellWithIdentifier:@"PCTableCell"];
    
    GET_USER_DEFAULT_INFO(@"newsfeed_setting")
    
    if(cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"PCTableCell" owner:self options:nil][0];
    }
    
    if (indexPath.row==0) {
        
        [cell set_cell_update_version:arrayNameSetting[indexPath.row]];
        [cell setIconCell:arrayImageSetting[indexPath.row]index:(int)indexPath.row];
        
    }else if(indexPath.row==1){
        
        if([widgetSetting isEqualToString:@"unselectedBtn"]||[userInfo isEqualToString:@"unselectedBtn"]){
            [cell setActionBtn:actoinBtn[0]];
        }
        else if([widgetSetting isEqualToString:@"selectedBtn"]||[userInfo isEqualToString:@"selectedBtn"]||(userInfo==nil)){
            [cell setActionBtn:actoinBtn[1]];
        }
        
        [cell setNewsWidgetCell:arrayNameSetting[indexPath.row]image:arrayImageSetting[indexPath.row]];
        [cell setIconCell:arrayImageSetting[indexPath.row]index:(int)indexPath.row];
    }else{
        [cell setNameCell:arrayNameSetting[indexPath.row]];
        [cell setIconCell:arrayImageSetting[indexPath.row]index:(int)indexPath.row];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [[[NSBundle mainBundle] loadNibNamed:@"PCTableCell" owner:self options:nil][0] frame].size.height;
}

-(void)goToHowTo{
    
    HowToUseController *view_class = [self loadNibWithName:@"HowToUseController"];
    [self.navigationController pushViewController:view_class animated:YES];
}

-(void)goToAnnounScene{
    
    AnnounceController *view_class = [[NSBundle mainBundle] loadNibNamed:@"AnnounceController" owner:self options:nil][0];
    [self.navigationController pushViewController:view_class animated:YES];
}

-(void)goToSettingQA:(NSString *)title{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SequeSettingQAController *view_class = [storyboard instantiateViewControllerWithIdentifier:@"SequeSettingQAController"];
    [view_class setTiltleHeader:title];
    [self.navigationController pushViewController:view_class animated:YES];
}

-(void)gotoMenuSettingTableViewController:(NSString *)headerText menuNum:(int)menuNum{
    
    MenuSettingTableViewController *view_class = [[NSBundle mainBundle] loadNibNamed:@"MenuSettingTableViewController" owner:self options:nil][0];
    [view_class setHeaderText:[NSString stringWithFormat:@"%@",headerText]];
    [view_class receiveMenu:menuNum];
    [self.navigationController pushViewController:view_class animated:YES];
}

-(NSString *)action:(NSString *) selecttag{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"selected_newfeed" object:[NSString stringWithFormat:@"%@",selecttag]];
    [self setTagMenu:selecttag];
    return selecttag;
}

@end
