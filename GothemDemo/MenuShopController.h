//
//  MenuShopController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/19/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

// This class is a base class of menu 2

#import <UIKit/UIKit.h>

@interface MenuShopController : BaseViewController<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>{
    
    NSMutableArray *cID;
    NSMutableArray *cName;
    NSMutableArray *cImage;
    NSMutableArray *colorTextName;
    NSMutableArray *advertising_array;

    __weak IBOutlet NSLayoutConstraint *heightAdvertising;
}

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end
