//
//  TermAndConditionBeforeLogin.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/19/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.


#import "TermAndConditionBeforeLogin.h"

@interface TermAndConditionBeforeLogin ()

@end

@implementation TermAndConditionBeforeLogin

-(void)viewWillAppear:(BOOL)animated{
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self.loadingView startAnimating];

    [self getService];
}

-(void)getService{
    
    [Service get_usage_agreement:self block:^(id result) {
        
        NSString *result_text = [NSString stringWithFormat:@"%@",result];
        
        if (![result_text isEqualToString:@"error"]) {
            
            documentInfo =[NSString stringWithFormat:@"%@",[ReplaceHTMLFont replaceHTML_by_string:[NSString stringWithFormat:@"%@",[ReplaceHTMLFont replaceHTML_by_range:result start_string:@"<style>" end_string:@"</style>" replaceString:@" "]] old_string:@"<html lang=\"en\">" new_string:@"<html lang=\"en\"> <style>html,body,head,span,h,p { font-family:RSU; }</style>"]];
            
            [self createWebViewWithHTML:result];
        }
    }];
    
    [self class_UI];
}

- (void)createWebViewWithHTML:(NSString *)text{
    
    [self.webView setBackgroundColor:[UIColor clearColor]];
    [self.webView loadHTMLString:documentInfo baseURL:nil];
}

- (IBAction)backBtn:(id)sender {
    
    GOTO_PREVIOUS;
}

-(void)class_UI{
    
    SWIPETLIFE_TOGOTOPREVIOUS
    LINE_WITH_STATUSBAR_HEADER_NORMAL;
}

-(void)swipeRight:(UISwipeGestureRecognizer*)gestureRecognizer{
    
    GOTO_PREVIOUS;
}

///// WEB_VIEW

-(void)didReceiveMemoryWarning{
    
    [super didReceiveMemoryWarning];
}

- (void)setView:(UIView *)view{
    
    if (view == nil){
        [self releaseSubviews];
    }
    
    [super setView:view];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    if (!webView.isLoading) {
        
        [self.loadingView stopAnimating];
        [self.loadingView setHidden:YES];
    }
}

-(void)openUrl:(NSString *)urlText{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",urlText]];
    NSURLRequest *req = [NSURLRequest requestWithURL:url];
    
    [self.webView loadRequest:req];
}

-(void)releaseSubviews{
    
    [self.webView setDelegate:nil];
    [self.webView stopLoading];
}

- (void)dealloc{
    
    [self releaseSubviews];
}

- (void)viewWillDisappear:(BOOL)animated{
    
    [self releaseSubviews];
}
///// END OF WEB_VIEW

@end
