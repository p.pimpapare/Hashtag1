//
//  HelpHistryController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/8/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "HelpHistoryController.h"
#import "SequeInquiryController.h"

@interface HelpHistoryController (){
    SequeInquiryController *view_seque;
}

@end

@implementation HelpHistoryController

-(void)viewWillAppear:(BOOL)animated{
    
    GOOGLE_ANALYTICS(@"HelpHistoryController");
    
    [self.loadingView setHidden:NO];
    [self.loadingView startAnimating];
    
    [self class_UI];
    
    [view_seque.view setUserInteractionEnabled:NO];
    
    [Service get_user_ingury_history_detail:[self getUserToken] id:problemId selfID:self block:^(id result) {
        
        [BaseViewController log_comment:@"Result inquiry " text:[NSString stringWithFormat:@"%@",result]];
        //        NSLog(@"result %@",result);
        
        NSString *resultText = [NSString stringWithFormat:@"%@",result];
        
        if (![resultText isEqualToString:@"error"]) {
            
            [self splitDateTime:[NSString stringWithFormat:@"%@",result[@"problem_datetime"]]];
            [self.question setText:[NSString stringWithFormat:@"%@",result[@"problem_type"]]];
            [self.title_question setText:[NSString stringWithFormat:@"%@",result[@"problem_subtype"]]];
            [self.qTitle setText:[NSString stringWithFormat:@"%@",result[@"problem_subtype"]]];
            [self.detail_question setText:[NSString stringWithFormat:@"%@",result[@"problem_detail"]]];
            [self.rDetail setText:[NSString stringWithFormat:@"%@",result[@"response_message"]]];
            [self.qDate setText:[NSString stringWithFormat:@"%@",[dateWithTime objectAtIndex:0]]];
            [self.qTime setText:[NSString stringWithFormat:@"%@",[NumberFormat format_hour_minite:[dateWithTime objectAtIndex:1]]]];
            
            NSString *response_at_text = [NSString stringWithFormat:@"%@",result[@"response_at"]];
            
            if ([response_at_text isEqualToString:@"<null>"]) {
                
                [self.heightLine setConstant:10];
                [self.viewAnswer setHidden:YES];
                
            }else{
                
                [self splitDateTimeAnswer:[NSString stringWithFormat:@"%@",result[@"response_at"]]];
                
                [self.rDate setText:[NSString stringWithFormat:@"%@",[dateWithTimeAns objectAtIndex:0]]];
                [self.rTime setText:[NSString stringWithFormat:@"%@",[NumberFormat format_hour_minite:[dateWithTimeAns objectAtIndex:1]]]];
                
                [self.heightLine setConstant:196];
                [self.viewAnswer setHidden:NO];

//                NSLog(@"number of line %lu",[self.detail_question.text length]);
//                NSLog(@"number of rDetail %lu",(unsigned long)[self.rDetail.text length]);
                
            }
            
            if ([self.detail_question.text length] >= 200) {
                [self.height_viewBg setConstant:700];
            }
            
            if ([self.rDetail.text length] != 0) {
                
                if ([self.rDetail.text length] >= 200 && [self.rDetail.text length] < 400) {
                    
                    [self.height_details_answer setConstant:300];
                    [self.height_viewBg setConstant:680];
                    [self.heightLine setConstant:310];
                }
                
                else if ([self.rDetail.text length] >= 400){
                    
                    [self.height_details_answer setConstant:500];
                    [self.heightLine setConstant:510];
                    [self.height_viewBg setConstant:980];
                }
            }
            
        }else if ([resultText isEqualToString:@"error"]){
            
            ALERT_MESSAGE_NETWORK( ,GOTO_PREVIOUS GOTO_PREVIOUS)
        }
        
        [self set_hidden_ui];
        
        [self.loadingView setHidden:YES];
        [self.loadingView stopAnimating];
        
        [self.view setUserInteractionEnabled:YES];
    }];
}

-(void)set_hidden_ui{
    
    NSMutableArray *ui_array = [[NSMutableArray alloc]initWithObjects:self.line1,self.line2,self.qImage,self.qStatus,self.text_des,self.question_label,self.detail_question,self.question_detail,self.topic_label, nil];
    
    for (id _ in ui_array) {
        [_ setHidden:NO];
    }
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
}

-(void)problemId:(NSString *)problem_ID{
    
    problemId = [problem_ID intValue];
}

-(void)class_UI{
    
    SWIPETLIFE_TOGOTOPREVIOUS;
    LINE_WITH_STATUSBAR_HEADER_NORMAL;
    
    [self.rDetail setFont:[UIFont fontWithName:@"RSU" size:16]];
    
    [self.line1 setBackgroundColor:[PCColor color_A6A6A6]];
    [self.line2 setBackgroundColor:[PCColor color_A6A6A6]];
}

-(NSArray *)splitDateTime:(NSString *)dateTime{
    
    //    NSLog(@"dateTime %@",dateTime);
    if (([dateTime isEqualToString:@"<null>"])||([dateTime isEqualToString:@"(null)"])) {
        dateTime = @"- -";
    }
    
    NSArray *splitToArray = [dateTime componentsSeparatedByString:@" "];
    NSString *date = [splitToArray objectAtIndex:0];
    NSString *sortDate = [CurrentDate_Temperature get_date_month:date];
    
    NSString *time = [splitToArray objectAtIndex:1];
    dateWithTime = [NSArray arrayWithObjects:sortDate,time, nil];
    return dateWithTime;
}

-(NSArray *)splitDateTimeAnswer:(NSString *)dateTime{
    
    //    NSLog(@"dateTime %@",dateTime);
    if (([dateTime isEqualToString:@"<null>"])||([dateTime isEqualToString:@"(null)"])) {
        
        dateTime = @"- -";
        
        NSString *error = @"error";
        dateWithTimeAns = [NSArray arrayWithObjects:error, nil];
        
        return dateWithTimeAns;
        
    }else{
        
        NSArray *splitToArray = [dateTime componentsSeparatedByString:@" "];
        NSString *date = [splitToArray objectAtIndex:0];
        NSString *sortDate = [CurrentDate_Temperature get_date_month:date];
        
        NSString *time = [splitToArray objectAtIndex:1];
        dateWithTimeAns = [NSArray arrayWithObjects:sortDate,time, nil];
        return dateWithTimeAns;
    }
}

-(void)swipeRight:(UISwipeGestureRecognizer*)gestureRecognizer{
    GOTO_PREVIOUS;
}

- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
