//
//  UserHistoryTableView.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/2/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

// This class is used for storing list of user's coupon

#import <UIKit/UIKit.h>

@interface UserHistoryTableView : BaseViewController<UITableViewDelegate,UITableViewDataSource>{
    
    // used point
    NSMutableArray *uDateTime;
    NSMutableArray *uPoint;
    NSMutableArray *uName;
    
    // mycoupon
    NSMutableArray *myCouponID;
    NSMutableArray *myCouponTitle;
    NSMutableArray *myCouponSubTitle;
    NSMutableArray *myCouponImage;
    NSMutableArray *myCouponDetails;
    NSMutableArray *myCouponCounterTime;
    NSMutableArray *myCouponUsePoint;
    NSMutableArray *myCouponShareUrl;
    NSMutableArray *myCouponCpuse_id;
    NSMutableArray *myCouponBrandLogo;

    BOOL finisedLoadingUsedPoint;
    BOOL finisedLoadingMyPoint;
    BOOL finisedLoadingGetPoint;
    
    NSArray *dateWithTime;
    
    int selectedRow;
    int offset_int;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIView *statusBar;
@property (weak, nonatomic) IBOutlet UIView *warningBlank;
@property int menu;

@property (weak, nonatomic) IBOutlet UIView *menuHeader;
@property (weak, nonatomic) IBOutlet UILabel *titlteHeader;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingView;

// error view
@property (weak, nonatomic) IBOutlet UIView *viewError;
@property (weak, nonatomic) IBOutlet UIImageView *imageLogo;
@property (weak, nonatomic) IBOutlet UIView *lineH;
@property (weak, nonatomic) IBOutlet UIView *lineB;

-(void)setHeaderText:(NSString *)title menuNum:(int)num;

@end
