//
//  PCMenubarCell.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 4/4/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

// This class is used for setting menubar's collection view cell

#import <UIKit/UIKit.h>

@interface PCMenubarCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *image_menu3;
@property (weak, nonatomic) IBOutlet UIImageView *image_menu;
@property (weak, nonatomic) IBOutlet UIView *view_selected;
@property (weak, nonatomic) IBOutlet UIImageView *image_menu1;

-(void)setImageMenuBarCell:(NSString *)image receiveIndex:(int)index_selected;
-(void)setColorSelected:(int)color;

@end
