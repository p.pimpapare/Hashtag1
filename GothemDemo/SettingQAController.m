//
//  SettingQAController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/7/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "SettingQAController.h"

@interface SettingQAController (){
    
    NSMutableArray *swiptTab;
}

@end

@implementation SettingQAController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self class_UI];
}

-(NSString *)titleForPagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController{
 return @"view";
 }
/*
 -(void)setCollection{
 // set collection view
 UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
 
 // set collection size between each other
 layout.minimumLineSpacing=0;
 layout.minimumInteritemSpacing=0;
 layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
 
 // set collection view from right to left
 [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
 
 _collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0,0,DEVICE_WIDTH,45) collectionViewLayout:layout];
 [_collectionView setDataSource:self];
 [_collectionView setDelegate:self];
 
 [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
 
 [self.menuBar addSubview:_collectionView];
 
 // register collectionView เสมอ
 [_collectionView registerClass:[History_CollectionCell class] forCellWithReuseIdentifier:@"Cell"];
 [_collectionView registerNib:[UINib nibWithNibName:@"History_CollectionCell" bundle:nil]  forCellWithReuseIdentifier:@"Cell"];
 
 //	()
 }
 
 - (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
 return [titleMenu count];
 }
 
 - (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
 
 History_CollectionCell *cell=(History_CollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
 
 if(cell == nil) {
 cell = [[NSBundle mainBundle] loadNibNamed:@"History_CollectionCell" owner:self options:nil][0];
 }
 // การ check menu bar
 if([swiptTab[indexPath.row]isEqualToString:@"0"]){
 [cell setselectedColor:0];
 }else{
 [cell setselectedColor:1];
 }
 [cell setHistoryCell:[NSString stringWithFormat:@"%@",titleMenu[indexPath.row]]];
 
 return cell;
 }
 
 - (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
 return CGSizeMake(DEVICE_WIDTH/2,44);
 }
 
 - (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
 // set menu color when user tab on that menu
 selectedTabMenu = (int)indexPath.row;
 
 switch (selectedTabMenu) {
 case 0:
 [self moveToViewControllerAtIndex:0 animated:YES]; break;
 case 1:
 [self moveToViewControllerAtIndex:1 animated:YES];break;
 default: break;
 }
 
 [_collectionView reloadData];
 }
 
 - (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
 // TODO: Deselect item
 }
 
 - (UIEdgeInsets)collectionView:
 (UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
 return UIEdgeInsetsMake(0,0,0,0);
 }
 
 // set button bar
 #pragma mark - XLPagerTabStripViewControllerDataSource
 
 -(NSArray *)childViewControllersForPagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController{
 
 //     create child view controllers that will be managed by XLPagerTabStripViewController
 InquiryController * child_1 = (InquiryController *)[self.storyboard instantiateViewControllerWithIdentifier:@"InquiryController"];
 HistoryController * child_2 = (HistoryController *)[self.storyboard instantiateViewControllerWithIdentifier:@"HistoryController"];
 
 if (!_isReload){
 return @[child_1, child_2];
 }
 
 NSMutableArray * childViewControllers = [NSMutableArray arrayWithObjects:child_1, child_2, nil];
 NSUInteger count = [childViewControllers count];
 for (NSUInteger i = 0; i < count; ++i) {
 // Select a random element between i and end of array to swap with.
 NSUInteger nElements = count - i;
 NSUInteger n = (arc4random() % nElements) + i;
 [childViewControllers exchangeObjectAtIndex:i withObjectAtIndex:n];
 }
 NSUInteger nItems = 1 + (rand() % 4);
 return [childViewControllers subarrayWithRange:NSMakeRange(0, nItems)];
 }
 
 -(void)reloadPagerTabStripView{
 
 _isReload = YES;
 self.isProgressiveIndicator = (rand() % 2 == 0);
 self.isElasticIndicatorLimit = (rand() % 2 == 0);;
 [super reloadPagerTabStripView];
 }
 
 -(void)pagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController updateIndicatorFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex{
 
 for (int i=0;i<[swiptTab count];i++) {
 if (toIndex==i) {
 [swiptTab replaceObjectAtIndex:i withObject:@"1"];
 }else{
 [swiptTab replaceObjectAtIndex:i withObject:@"0"];
 }
 }
 [_collectionView reloadData];
 
 }
 // end for setting bar button
 */
-(void)class_UI{
    
//    [self.barView setHidden:YES];
//    swiptTab = [[NSMutableArray alloc]initWithObjects:@"1",@"0",@"0",@"0", nil];
//    
//    titleMenu = @[@"สอบถาม",@"ประวัติการสอบถาม"];
    [self setMenuBar];
}

-(void)setMenuBar{
    selectedTab = 0;
    [self setCollection];
}

-(void)setCollection{
    // set collection view
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    
    // set collection size between each other
    layout.minimumLineSpacing=0;
    layout.minimumInteritemSpacing=0;
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    // set collection view from right to left
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    
    _collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0,0,DEVICE_WIDTH,45) collectionViewLayout:layout];
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    
    [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [_collectionView setBackgroundColor:[UIColor clearColor]];
    [_collectionView setShowsHorizontalScrollIndicator:NO];
    [_collectionView setShowsVerticalScrollIndicator:NO];
    
    [self.menuBar addSubview:_collectionView];
    
    // set paging
    [_collectionView setPagingEnabled:YES];
    
    // register collectionView เสมอ
    [_collectionView registerClass:[PCCollectionCell class] forCellWithReuseIdentifier:@"Cell"];
    [_collectionView registerNib:[UINib nibWithNibName:@"History_CollectionCell" bundle:nil]  forCellWithReuseIdentifier:@"Cell"];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 2;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    History_CollectionCell *cell=(History_CollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    if(cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"History_CollectionCell" owner:self options:nil][0];
    }
    // การ check menu bar
    if([swiptTab[indexPath.row]isEqualToString:@"0"]){
        [cell setselectedColor:0];
    }else{
        [cell setselectedColor:1];
    }
    [cell setHistoryCell:[NSString stringWithFormat:@"%@",titleMenu[indexPath.row]]];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(119,45);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    selectedTabMenu = (int)indexPath.row;
    
}
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    // TODO: Deselect item
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0,0,0,0);
}

// end for setting bar button

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

@end
