
//  Created by pimpaporn chaichompoo on 1/28/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

// This class is used for setting view background for using on Authentication class.
#import <UIKit/UIKit.h>

@interface PCBg : UIImageView

-(void)setBg_image;
-(void)setBg_color:(BOOL)blackColor alpha:(double)alpha_num;

@end
