//
//  CallServiceAfterLogin.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 9/13/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CallServiceAfterLogin : BaseViewController

@property NSString *tagMenu;
-(NSString *)action:(NSString *) selecttag;

-(void)call_service_from:(NSString *)token type:(int)type;

@end
