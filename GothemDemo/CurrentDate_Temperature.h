//
//  CurrentDate_Temperature.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/26/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

// This service is used for getting current date, time and getting temperature from current's location

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface CurrentDate_Temperature : NSObject<CLLocationManagerDelegate>

+(NSString *)get_day_name;
+(NSString *)get_date_format_dd_mm_yy;

+(NSString *)get_current_time;
+(NSString *)get_time_am_pm;

+(NSString *)get_current_temp:(NSString *)location;

+(NSString *)get_date_month_name:(NSString *)dateStr;
+(NSString *)get_date_month:(NSString *)dateStr;

+(NSString *)get_date_time;

@end
