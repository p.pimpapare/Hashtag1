
//  Created by pimpaporn chaichompoo on 2/1/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "PCCollectionCell.h"

@implementation PCCollectionCell

-(void)awakeFromNib{

    [super awakeFromNib];

    self.tap = NO;
    [self.backgroundView setHidden:YES];
    [self setBackgroundColor:[UIColor whiteColor]];
    [PCShadow set_shadow:1.5 shadowRadius:1 shadowOpacity:0.15 view:self];
}

-(void)setImagCell:(NSString *)image{
    
    [self.imageCell sd_setImageWithURL:[NSURL URLWithString:image]
                      placeholderImage:[UIImage imageNamed:@"picture_1_2"]options:SDWebImageProgressiveDownload];
}

-(void)setNameInCell:(NSString *)name colorText:(NSString *)color{
    
    [self.nameCell setText:name];
    [self.nameCell setTextColor:[PCColor color_on_menu2:color]];
}

-(void)setIconInCell:(NSString *)icon{
    
    [self.iconCell sd_setImageWithURL:[NSURL URLWithString:icon]
                     placeholderImage:[UIImage imageNamed:@" "]options:SDWebImageProgressiveDownload];
    
    if (IS_IPAD==YES) {        
        [self.icon_width setConstant:72];
        [self.icon_height setConstant:72];
    }
}

@end
