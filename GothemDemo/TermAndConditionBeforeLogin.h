//
//  TermAndConditionBeforeLogin.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/19/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

#import <UIKit/UIKit.h>

@interface TermAndConditionBeforeLogin : UIViewController{
    
    NSString *documentInfo;
}

@property (weak, nonatomic) IBOutlet UIView *statusBar;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIView *menuHeader;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingView;

@end
