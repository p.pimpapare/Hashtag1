
//  Created by pimpaporn chaichompoo on 2/2/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "MenuCouponHeaderView.h"

@implementation MenuCouponHeaderView

-(void)setMenuCouponHeaderView:(NSString *)titleText{
    
    [self.bgView setFrame:CGRectMake(20,5, DEVICE_WIDTH,51)];
    [self setBackgroundColor:[UIColor colorWithRed:(81/255.0) green:(81/255.0) blue:(81/255.0) alpha:1.0]];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bgView.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(20.0, 20.0)];
    
    // set background cornor
    CAShapeLayer *borderLayer = [[CAShapeLayer alloc] init];
    borderLayer.frame = self.bgView.bounds;
    borderLayer.path  = maskPath.CGPath;
    borderLayer.backgroundColor = [UIColor colorWithRed:(81/255.0) green:(81/255.0) blue:(81/255.0) alpha:1.0].CGColor;

        borderLayer.fillColor = [UIColor colorWithRed:(251/255.0) green:(251/255.0) blue:(251/255.0) alpha:1.00].CGColor;
    [self.bgView.layer addSublayer:borderLayer];
    
    CATextLayer *label = [[CATextLayer alloc] init];
    [label setFont:@"RSU-Bold"];
    [label setFontSize:20];
    [label setString:[NSString stringWithFormat:@"%@",titleText]];
    [label setFrame:CGRectMake(0,borderLayer.frame.size.height/2-14,DEVICE_WIDTH,51)];
    [label setAlignmentMode:kCAAlignmentCenter];
    [borderLayer addSublayer:label];
    [label setForegroundColor:[[PCColor color_A80F1C] CGColor]];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelBtn addTarget:self
                  action:@selector(goToPrevious)
        forControlEvents:UIControlEventTouchUpInside];
    [cancelBtn setTitle:@"X" forState:UIControlStateNormal];
    [cancelBtn setTitleColor:[UIColor colorWithRed:(175/255.0) green:(175/255.0) blue:(175/255.0) alpha:1.0] forState:UIControlStateNormal];
    cancelBtn.frame = CGRectMake(DEVICE_WIDTH-65,borderLayer.frame.size.height/2-28,80,51);
    [self.bgView addSubview:cancelBtn];
    
    //    [borderLayer addSubview:self.cacelBtn];
}

-(void)goToPrevious{
    [self action:@"cancelBtn"];
}

-(NSString *)action:(NSString *) selecttag{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"select_cancel" object:[NSString stringWithFormat:@"%@",selecttag]];
    [self setTagStatus:selecttag];
    return selecttag;
}

@end
