//
//  MenuCuponController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/19/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

// This class is a base class of menu 3

#import <UIKit/UIKit.h>
#import "XLPagerTabStripViewController.h"

@interface MenuCuponController : BaseViewController<XLPagerTabStripChildItem,UITableViewDelegate,UITableViewDataSource>{
    
    int numMenu3;
    UIView *viewForresizeImage;
    UIView *overlay;
    UIImageView *shadowView;
    UIPageControl *pageControl;
    
    NSMutableArray *titleArray;
    NSMutableArray *detailarray;
    NSMutableArray *advertising_array;

    __weak IBOutlet NSLayoutConstraint *heightAdvertising;
}

@property (weak, nonatomic) IBOutlet UITableView *tableview;

@end
