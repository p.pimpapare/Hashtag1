//
//  SubMenuSettingController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/21/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "SubMenuSettingController.h"

@interface SubMenuSettingController ()

@end

@implementation SubMenuSettingController

-(void)viewWillAppear:(BOOL)animated{
    
    GOOGLE_ANALYTICS(@"SubMenuSettingController");
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self.loadingView startAnimating];
    [self.view setUserInteractionEnabled:NO];
    
    [self getService_fromserviceDefaults];
}

-(void)getService_fromserviceDefaults{
    
    id intro;
    NSUserDefaults *categoryDefaults = [NSUserDefaults standardUserDefaults];
    
    if (categoryDefaults){
        
        intro = (id)[categoryDefaults objectForKey:@"introductionInfo"];
        
        introBg = [NSString stringWithFormat:@"%@",intro[@"background"]];
        introImage = [NSString stringWithFormat:@"%@",intro[@"introduction"]];
        
        [self class_UI];
    }
}

- (IBAction)backBtn:(id)sender {
    GOTO_PREVIOUS;
}

-(void)class_UI{
    
    SWIPETLIFE_TOGOTOPREVIOUS;
    LINE_WITH_STATUSBAR_HEADER_NORMAL;
}

- (void)swipeRight:(id)sender {
    GOTO_PREVIOUS;
}

-(void)setHeaderText:(NSString *)title detail:(NSString *)detail type:(BOOL)qa{
    [self.titleHeader setText:[NSString stringWithFormat:@"%@",title]];
}

-(void)indexAboutUsDetail:(NSString *)index{
    
    if ([index isEqualToString:@"2"]) {
        
        [Service get_usage_privacy_policy:self block:^(id result) {
            NSString *resultText = [NSString stringWithFormat:@"%@",result];
            
            if (![resultText isEqualToString:@"error"]){
                
                documentInfo =[NSString stringWithFormat:@"%@",[ReplaceHTMLFont replaceHTML_by_string:[NSString stringWithFormat:@"%@",[ReplaceHTMLFont replaceHTML_by_range:result start_string:@"<style>" end_string:@"</style>" replaceString:@" "]] old_string:@"<html lang=\"en\">" new_string:@"<html lang=\"en\"> <style>html,body,head,span,h,p { font-family:RSU; }</style>"]];
                [self createWebViewWithHTML:documentInfo];
            }
        }];
        
    }else{
        
        [Service get_usage_agreement:self block:^(id result) {
            
            NSString *resultText = [NSString stringWithFormat:@"%@",result];
            
            if (![resultText isEqualToString:@"error"]) {
                
                documentInfo =[NSString stringWithFormat:@"%@",[ReplaceHTMLFont replaceHTML_by_string:[NSString stringWithFormat:@"%@",[ReplaceHTMLFont replaceHTML_by_range:result start_string:@"<style>" end_string:@"</style>" replaceString:@" "]] old_string:@"<html lang=\"en\">" new_string:@"<html lang=\"en\"> <style>html,body,head,span,h,p { font-family:RSU; }</style>"]];
                [self createWebViewWithHTML:documentInfo];
            }
        }];
    }
}

- (void) createWebViewWithHTML:(NSString *)text{
    
    [self.view setUserInteractionEnabled:YES];
    
    [self.webView setBackgroundColor:[UIColor clearColor]];
    [self.webView loadHTMLString:text baseURL:nil];
}

///// WEB_VIEW

-(void)didReceiveMemoryWarning{
    
    [super didReceiveMemoryWarning];
}

- (void)setView:(UIView *)view{
    
    if (view == nil){
        [self releaseSubviews];
    }
    
    [super setView:view];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    if (!webView.isLoading) {
        
        [self.loadingView stopAnimating];
        [self.loadingView setHidden:YES];
    }
}

-(void)openUrl:(NSString *)urlText{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",urlText]];
    NSURLRequest *req = [NSURLRequest requestWithURL:url];
    
    [self.webView loadRequest:req];
}

-(void)releaseSubviews{
    
    [self.webView setDelegate:nil];
    [self.webView stopLoading];
}

- (void)dealloc{
    
    [self releaseSubviews];
}

- (void)viewWillDisappear:(BOOL)animated{
    
    [self releaseSubviews];
}
///// END OF WEB_VIEW

@end
