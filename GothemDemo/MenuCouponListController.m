//
//  MenuTableViewListController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/20/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

#import "MenuCouponListController.h"

@interface MenuCouponListController (){
    
    MenuCouponDetail *menuCouponDetail;
    MenuDetailInputCode *menuInputCode;
    MenuCouponDetail_Alert *menuAlert;
    
    UsedCuponSuccess *usedCoupon;
    MenuCuponDetailCode *menuCouponUsedCode; // barcode
}

@end

@implementation MenuCouponListController

-(void)viewWillAppear:(BOOL)animated{
    
    GOOGLE_ANALYTICS(@"MenuCouponListController");
}

- (void)viewDidLoad {
    
    //    [super viewDidLoad];
    [self.loadingView setHidden:NO];
    [self.loadingView startAnimating];
    
    [self set_initail_objects];
}

// For seperating type of coupon ( Menu2 or HotPromo )
-(void)setHeaderText:(NSString *)title brand_id:(NSString *)brandID{
    
    [self.titleHeader setText:[NSString stringWithFormat:@"%@",title]];
    
    brandID_str = brandID;
    
    if (brandID==nil) {
        
        menu2 = NO;
        [self getService_fromHotPromotionDefaults:[self getUserToken]];
        
    }else{
        
        menu2 = YES;
        [self getService_fromBrandId:[self getUserToken] brand:brandID];
    }
}

-(void)getService_fromHotPromotionDefaults:(NSString *)token{ // get service hotpromo
    
    [Service get_hot_promotion:token selfID:self block:^(id result) {
        
        NSString *resultText = [NSString stringWithFormat:@"%@",result];
        
        if ([resultText isEqualToString:@"error"]) {
            
            [self getService_fromHotPromotionDefaults:token];
            
        }else{
            
            if ([result count] == 0) {
                [self alert_login_again];
            }else{
                
                hotpromoID = [[NSMutableArray alloc]init];
                hotpromoTitle = [[NSMutableArray alloc]init];
                hotpromoSubTitle = [[NSMutableArray alloc]init];
                hotpromoImage = [[NSMutableArray alloc]init];
                hotpromoDetails = [[NSMutableArray alloc]init];
                hotpromoCounterTime = [[NSMutableArray alloc]init];
                hotpromoUsePoint = [[NSMutableArray alloc]init];
                hotpromoLogo = [[NSMutableArray alloc]init];
                hotpromoBrandName = [[NSMutableArray alloc]init];
                hotpromoAvailable = [[NSMutableArray alloc]init];
                hotpromoDealOwner  =[[NSMutableArray alloc]init];
                hotpromoCanUse = [[NSMutableArray alloc]init];
                
                for (int i=0; i<[result[@"hotpromo"] count];i++) {
                    
                    [hotpromoID addObject:result[@"hotpromo"][i][@"coupon_id"]];
                    [hotpromoTitle addObject:result[@"hotpromo"][i][@"coupon_title"]];
                    [hotpromoSubTitle addObject:result[@"hotpromo"][i][@"coupon_subtitle"]];
                    [hotpromoImage addObject:result[@"hotpromo"][i][@"coupon_img"]];
                    [hotpromoDetails addObject:result[@"hotpromo"][i][@"coupon_desc"]];
                    [hotpromoCounterTime addObject:result[@"hotpromo"][i][@"counter_time"]];
                    [hotpromoUsePoint addObject:result[@"hotpromo"][i][@"use_point"]];
                    [hotpromoLogo addObject:result[@"hotpromo"][i][@"brand_logo"]];
                    [hotpromoBrandName addObject:result[@"hotpromo"][i][@"brand_name"]];
                    [hotpromoAvailable addObject:result[@"hotpromo"][i][@"available"]];
                    [hotpromoDealOwner addObject:result[@"hotpromo"][i][@"deal_owner"]];
                    [hotpromoCanUse addObject:result[@"hotpromo"][i][@"canuse"]];
                }
                
                finisedLoadingHotPromo = YES;
                
                [self.loadingView setHidden:YES];
                [self.loadingView stopAnimating];
                
                [self.tableView reloadData];
            }
        }
    }];
}

-(void)getService_fromBrandId:(NSString *)token brand:(NSString *)brandId{ // get service menucoupon
    
    [Service get_coupon_from_brand_grid:token bID:brandId selfID:self block:^(id result) {
        
        NSString *resultText = [NSString stringWithFormat:@"%@",result];
        
        if (![resultText isEqualToString:@"error"]) {
            
            couponID = [[NSMutableArray alloc]init];
            couponTitle = [[NSMutableArray alloc]init];
            couponSubTitle = [[NSMutableArray alloc]init];
            couponImage = [[NSMutableArray alloc]init];
            couponDetails = [[NSMutableArray alloc]init];
            couponCounterTime = [[NSMutableArray alloc]init];
            couponUsePoint = [[NSMutableArray alloc]init];
            
            couponBrandName = [[NSMutableArray alloc]init];
            couponBrandLogo = [[NSMutableArray alloc]init];
            couponAvailable = [[NSMutableArray alloc]init];
            couponDealOwner = [[NSMutableArray alloc]init];
            couponCanUse = [[NSMutableArray alloc]init];
            
            for (int i=0;i<[result[@"coupon"] count];i++) {
                
                [couponID addObject:result[@"coupon"][i][@"coupon_id"]];
                [couponTitle addObject:result[@"coupon"][i][@"coupon_title"]];
                [couponSubTitle addObject:result[@"coupon"][i][@"coupon_subtitle"]];
                [couponImage addObject:result[@"coupon"][i][@"coupon_img"]];
                [couponDetails addObject:result[@"coupon"][i][@"coupon_desc"]];
                [couponCounterTime addObject:result[@"coupon"][i][@"counter_time"]];
                [couponUsePoint addObject:result[@"coupon"][i][@"use_point"]];
                [couponBrandName addObject:result[@"coupon"][i][@"brand_name"]];
                [couponBrandLogo addObject:result[@"coupon"][i][@"brand_logo"]];
                [couponAvailable addObject:result[@"coupon"][i][@"available"]];
                [couponDealOwner addObject:result[@"coupon"][i][@"deal_owner"]];
                [couponCanUse addObject:result[@"coupon"][i][@"canuse"]];
            }
            
            finisedLoadingCoupon = YES;
            [self.tableView reloadData];
            
            [self.loadingView setHidden:YES];
            [self.loadingView stopAnimating];
        }
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    selectedRow = (int)indexPath.row;
    
    /*
     Step 1: App pop up coupon details after users select coupon from coupon's list
     */
    
    [self addPCBlack:self];
    
    menuCouponDetail = [[NSBundle mainBundle] loadNibNamed:@"MenuCouponDetail" owner:self options:nil][0];
    
    [menuCouponDetail.shareBtn addTarget:self action:@selector(btn_share_pressed) forControlEvents:UIControlEventTouchUpInside];
    [menuCouponDetail.ReadMoreBtn addTarget:self action:@selector(btn_readmore_pressed) forControlEvents:UIControlEventTouchUpInside];
    [menuCouponDetail.usedPointBtn addTarget:self action:@selector(btn_used_point_pressed) forControlEvents:UIControlEventTouchUpInside];
    [menuCouponDetail.cancelBtn addTarget:self action:@selector(btn_close_pressed) forControlEvents:UIControlEventTouchUpInside];
    [menuCouponDetail.black_bg addTarget:self action:@selector(btn_close_pressed) forControlEvents:UIControlEventTouchUpInside];
    
    if (menu2==NO) {
        
        [menuCouponDetail setPromotionDetail:hotpromoID[selectedRow] title:hotpromoSubTitle[selectedRow] detail:hotpromoDetails[selectedRow] point:hotpromoUsePoint[selectedRow] image:hotpromoImage[selectedRow]  logo:hotpromoLogo[selectedRow] available:hotpromoAvailable[selectedRow] dealOwner:hotpromoDealOwner[selectedRow] canUse:hotpromoCanUse[selectedRow]];
        
    }else{
        
        [menuCouponDetail setPromotionDetail:couponID[selectedRow] title:couponSubTitle[selectedRow] detail:couponDetails[selectedRow] point:couponUsePoint[selectedRow] image:couponImage[selectedRow]  logo:couponBrandLogo[selectedRow] available:couponAvailable[selectedRow] dealOwner:couponDealOwner[selectedRow] canUse:couponCanUse[selectedRow]];
    }
    
    [self.view addSubview:menuCouponDetail];
    
    // Add animation transaction
    [PCView setViewAnimationWithDuration:menuCouponDetail time_duration:0.4 showView:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (menu2==NO) {
        
        if([hotpromoTitle count]<=0){
            
            if (finisedLoadingHotPromo==YES) {
                [self.warning_text setText:@"ไม่พบข้อมูล"];
                [self.warningBlank setHidden:NO];
            }
            return 0;
            
        }else{
            return [hotpromoTitle count];
        }
        
    }else{
        
        if(([couponTitle count]<=0)){
            
            if (finisedLoadingCoupon==YES) {
                
                [self.warning_text setText:@"เร็วๆนี้"];
                [self.warningBlank setHidden:NO];
            }
            return 0;
            
        }else{
            return [couponTitle count];
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MenuCoupon_TableViewCell *cell = (MenuCoupon_TableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if(cell == nil) {
        
        cell = [[NSBundle mainBundle] loadNibNamed:@"MenuCoupon_TableViewCell" owner:self options:nil][0];
    }
    
    if (menu2==NO) {
        
        [cell setCoupon:hotpromoTitle[indexPath.row] detail:hotpromoDetails[indexPath.row] code:[NSString stringWithFormat:@"%@",hotpromoUsePoint[indexPath.row]]image:hotpromoImage[indexPath.row]];
    }else{
        
        [cell setCoupon:couponTitle[indexPath.row] detail:couponDetails[indexPath.row] code:[NSString stringWithFormat:@"%@",couponUsePoint[indexPath.row]]image:couponImage[indexPath.row]];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [[[NSBundle mainBundle] loadNibNamed:@"MenuCoupon_TableViewCell" owner:self options:nil][0] frame].size.height;
}

-(void)notification_alert:(NSNotification *)noti{
    
    NSString *strNoti = [noti object];
    
    if([strNoti isEqualToString:@"finishedUsedCoupon"]){
        [self finishedUsedCoupon];
    }
}

-(void)finishedUsedCoupon{
    
    [self btn_close_pressed];
    
    (menu2==NO)?[self getService_fromHotPromotionDefaults:[self getUserToken]]:[self getService_fromBrandId:[self getUserToken] brand:brandID_str];
}

-(void)btn_readmore_pressed{ // For getting service Readmore
    
    [self.loadingView setHidden:NO];
    [self.loadingView startAnimating];
    
    if (menu2==NO) {
        
        [self goToReadMore:hotpromoID[selectedRow] title:hotpromoTitle[selectedRow] point:hotpromoUsePoint[selectedRow] logo:hotpromoLogo[selectedRow]];
        
    }else{
        
        [self goToReadMore:couponID[selectedRow] title:couponTitle[selectedRow] point:couponUsePoint[selectedRow] logo:couponBrandLogo[selectedRow]];
    }
}

-(void)goToReadMore:(NSString *)coupon_ID title:(NSString *)coupon_title point:(NSString *)coupon_point logo:(NSString *)coupon_logo{
    
    [self.loadingView setHidden:YES];
    [self.loadingView stopAnimating];
    
    ReadMoreController *readmore = [[NSBundle mainBundle] loadNibNamed:@"ReadMoreController" owner:self options:nil][0];
    
    [readmore setDetailReadmore:coupon_ID title:coupon_title point:coupon_point logo:coupon_logo];
    [self presentViewController:readmore animated:YES completion:nil];
}

-(void)btn_share_pressed{
    
    [self.loadingView setHidden:NO];
    [self.loadingView startAnimating];
    
    if (menu2==NO) {
        
        [self getServiceShareUrl:hotpromoID[selectedRow]];
        
    }else{
        
        [self getServiceShareUrl:couponID[selectedRow]];
    }
}

-(void)getServiceShareUrl:(NSString *)coupon_ID{
    
    //    NSLog(@"%@",coupon_ID);
    
    [Service getShareUrl:[self getUserToken] couponID:coupon_ID selfID:self block:^(id result) {
        
        NSString *resultText = [NSString stringWithFormat:@"%@",result];
        
        if (![resultText isEqualToString:@"error"]) {
            
            NSString *success = [NSString stringWithFormat:@"%@",result[@"success"]];
            
            if([success isEqualToString:@"0"]){
                
                NSString *message = [NSString stringWithFormat:@"%@",result[@"message"]];
                
                UIAlertController * alert =   [UIAlertController alertControllerWithTitle:@"เกิดข้อผิดพลาด" message:[NSString stringWithFormat:@"%@",message] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"ตกลง" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
                    [self btn_close_pressed]; }];
                
                [alert addAction:yesButton];
                [self presentViewController:alert animated:YES completion:nil];
                
            }else{
                
                [self shareInfo:[NSString stringWithFormat:@"%@",result[@"url"]]];
            }
        }
    }];
}

-(void)shareInfo:(NSString *)shareURL{
    
    [self.loadingView setHidden:YES];
    [self.loadingView stopAnimating];
    
    NSArray *itemsToShare = @[shareURL];
    
    UIActivityViewController* activityController = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
    
    [activityController setExcludedActivityTypes:
     @[UIActivityTypeAssignToContact,
       UIActivityTypeCopyToPasteboard,
       UIActivityTypePrint,
       UIActivityTypeSaveToCameraRoll,
       UIActivityTypeAddToReadingList,
       UIActivityTypeAirDrop,
       @"com.apple.reminders.RemindersEditorExtension",
       @"com.apple.mobilenotes.SharingExtension"
       ]];
    
    
    if ( [activityController respondsToSelector:@selector(popoverPresentationController)] ) {
        // iOS8
        activityController.popoverPresentationController.sourceView = self.view_share; // เฉพาะ ipad เวลา share
        
    }
    [self presentViewController:activityController animated:YES completion:nil];
}

-(void)btn_used_point_pressed{
    
    [menuCouponDetail removeFromSuperview];
    (menu2==NO)?[self add_coupon_popup:hotpromoLogo[selectedRow]]:[self add_coupon_popup:couponBrandLogo[selectedRow]];
}

-(void)add_coupon_popup:(NSString *)logoimage{
    
    /*
     Step 2: Add pop up for allowing users to select "ใช้คูปอง" or "เก็บคูปอง".
     */
    
    menuAlert = [self loadNibWithName:@"MenuCouponDetail_Alert"];
    [menuAlert set_logo_coupon:logoimage];
    
    [menuAlert.usedCouponBtn addTarget:self action:@selector(btn_used_coupon_pressed) forControlEvents:UIControlEventTouchUpInside];
    [menuAlert.storeCouponBtn addTarget:self action:@selector(btn_store_coupon_pressed) forControlEvents:UIControlEventTouchUpInside];
    [menuAlert.cancelBtn addTarget:self action:@selector(btn_close_pressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:menuAlert];
    
    // Add animation transaction
    [menuAlert setAlpha:0];
    [PCView setViewAnimationWithDuration:menuAlert time_duration:0.4 showView:YES];
}

// Receive action from user selected
-(void)btn_used_coupon_pressed{
    
    useStatus = 1; // Use coupon
    [self add_popup_confirm_password];
}

-(void)btn_store_coupon_pressed{
    
    useStatus = 0; // Stroe coupon
    [self add_popup_confirm_password];
}

-(void)add_popup_confirm_password{ // For receiving user password after they selected Collect poin or Use point.
    
    (menu2==NO)?[self display_popup_confirm_password:hotpromoLogo[selectedRow]]:[self display_popup_confirm_password:couponBrandLogo[selectedRow]];
}


-(void)display_popup_confirm_password:(NSString *)logoImage{
    
    /*
     Step 3: Display pop up for comfirm user password after users selected use coupon or store coupon
     */
    
    menuInputCode = [[NSBundle mainBundle] loadNibNamed:@"MenuDetailInputCode" owner:self options:nil][0];
    
    [menuInputCode set_logo_coupon:logoImage];
    
    [menuInputCode.confirmBtn addTarget:self action:@selector(btn_confirm_password_pressed) forControlEvents:UIControlEventTouchUpInside];
    
    [menuInputCode.cancelBtn addTarget:self action:@selector(btn_close_pressed) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:menuInputCode];
    
    // Add animation transaction
    [menuInputCode setAlpha:0];
    [PCView setViewAnimationWithDuration:menuInputCode time_duration:0.4 showView:YES];
}

-(void)btn_confirm_password_pressed{
    
    // For receiving user password and checking coupon status
    
    NSString *codeText =[NSString stringWithFormat:@"%@",menuInputCode.codeTextfeild.text];
    
    /*
     Suppose user use coupon success
     
     SET_USER_DEFAULT_INFO(@"YES",@"USING_COUPON")
     
     menuCouponUsedCode = [[NSBundle mainBundle] loadNibNamed:@"MenuCuponDetailCode" owner:self options:nil][0];
     
     [menuCouponUsedCode setMenuCoupon:@"test title" detail:@"test details" time:@"3600" code:@"test code"];
     
     [menuCouponUsedCode.usedCodeBtn addTarget:self action:@selector(btn_close_pressed) forControlEvents:UIControlEventTouchUpInside];
     [menuCouponUsedCode.readmoreBtn addTarget:self action:@selector(btn_readmore_pressed) forControlEvents:UIControlEventTouchUpInside];
     [menuCouponUsedCode.shareBtn addTarget:self action:@selector(btn_share_pressed) forControlEvents:UIControlEventTouchUpInside];
     
     [self.view addSubview:menuCouponUsedCode];
     
     [menuCouponUsedCode setAlpha:0];
     
     [PCView setViewAnimationWithDuration:menuCouponUsedCode time_duration:0.4 showView:YES]
     
     */
    
    ////////
    
    if (menu2==NO) {
        
        [self check_coupon_stauts:hotpromoID[selectedRow] title:hotpromoTitle[selectedRow] detail:hotpromoDetails[selectedRow] usedPoint:hotpromoUsePoint[selectedRow] image:hotpromoImage[selectedRow] logo:hotpromoLogo[selectedRow] userPassword:codeText];
    }else{
        
        [self check_coupon_stauts:couponID[selectedRow] title:couponTitle[selectedRow] detail:couponDetails[selectedRow] usedPoint:couponUsePoint[selectedRow] image:couponImage[selectedRow] logo:couponBrandLogo[selectedRow] userPassword:codeText];
    }
}

-(void)check_coupon_stauts:(NSString *)coupon_ID title:(NSString *)coupon_title detail:(NSString *)coupon_detail usedPoint:(NSString *)coupon_usedPoint image:(NSString *)coupon_image logo:(NSString *)coupon_Logo userPassword:(NSString *)password{
    
    [Service get_coupon_status:[self getUserToken] couponID:coupon_ID selfID:self block:^(id result) {
        
        NSString *resultText = [NSString stringWithFormat:@"%@",result];
        
        if ([resultText isEqualToString:@"error"]) {
            
            ALERT_MESSAGE_YES_NETWORK( )
            
        }else{
            
            [menuAlert removeFromSuperview];
            [menuInputCode removeFromSuperview];
            
            if ([password isEqualToString:[self getUserPassword]]) { // password correct
                
                NSString *successStatus = [NSString stringWithFormat:@"%@",result[@"success"]];
                
                if ([successStatus isEqualToString:@"0"]) { // success = false
                    
                    [self addUseCouponSuccess:coupon_Logo alertText:result[@"message"]];
                    
                }else{ // success = true
                    
                    [self postCodeCouponService:coupon_ID title:coupon_title detail:coupon_detail coupon_logo:coupon_Logo];
                }
                
            }else{
                
                [self addUseCouponSuccess:coupon_Logo alertText:@"รหัสผ่านไม่ถูกต้อง"];
            }
        }
    }];
}

-(void)addUseCouponSuccess:(NSString *)logoImage alertText:(NSString *)alertText{
    
    usedCoupon = [[NSBundle mainBundle] loadNibNamed:@"UsedCuponSuccess" owner:self options:nil][0];
    
    [usedCoupon setlogoCoupon:logoImage];
    [usedCoupon setTextAlertView:alertText];
    
    [usedCoupon.confrimBtn addTarget:self action:@selector(btn_close_pressed) forControlEvents:UIControlEventTouchUpInside];
    
    [usedCoupon setAlpha:0];
    [self.view addSubview:usedCoupon];
    
    [PCView setViewAnimationWithDuration:usedCoupon time_duration:0.4 showView:YES];
}

-(void)postCodeCouponService:(NSString *)coupon_ID title:(NSString *)coupon_title detail:(NSString *)coupon_detail coupon_logo:(NSString *)coupon_logo {
    
    NSString *successText = @"ดำเนินการเรียบร้อยแล้ว !! \nท่านสามารถเรียกดูคูปอง \nได้ที่เมนูคูปองของฉัน";
    
    // useStatus 0 = Collect , 1 = Used
    
    if (useStatus == 0) { // collect coupon
        
        [Service get_user_use_coupon:[self getUserToken] email:[self getUserEmail] password:[self getUserPassword] couponID:coupon_ID use:useStatus selfID:self block:^(id result){
            
            NSString *resultText = [NSString stringWithFormat:@"%@",result];
            
            if ([resultText isEqualToString:@"error"]) {
                
                ALERT_MESSAGE_YES_NETWORK(GOTO_PREVIOUS)
                
            }else{
                
                NSString *successStatus = [NSString stringWithFormat:@"%@",result[@"success"]];
                [menuCouponDetail removeFromSuperview];
                [menuAlert removeFromSuperview];
                
                if([successStatus isEqualToString:@"1"])
                {
                    [self display_success_popup:successText];
                    
                }else{
                    [self display_success_popup:result[@"message"]];
                }
            }
        }];
        
    }else{ // used coupon
        
        [Service get_user_use_coupon:[self getUserToken] email:[self getUserEmail] password:[self getUserPassword] couponID:coupon_ID use:useStatus selfID:self block:^(id result){
            
            NSString *resultText = [NSString stringWithFormat:@"%@",result];
            
            if ([resultText isEqualToString:@"error"]) {
                
                ALERT_MESSAGE_YES_NETWORK(GOTO_PREVIOUS)
                
            }else{
                
                SET_USER_DEFAULT_INFO(@"YES",@"USING_COUPON")
                
                NSString *successText = [NSString stringWithFormat:@"%@",result[@"success"]];
                
                if([successText isEqualToString:@"0"]){
                    
                    [self addUseCouponSuccess:coupon_logo alertText:result[@"message"]];
                    
                }else{
                    
                    NSString *duration = [NSString stringWithFormat:@"%@",result[@"duration"]];
                    NSString *code = [NSString stringWithFormat:@"%@",result[@"code"]];
                    
                    [self.tableView setUserInteractionEnabled:NO];
                    [self.backBtn setUserInteractionEnabled:NO];
                    
                    menuCouponUsedCode = [[NSBundle mainBundle] loadNibNamed:@"MenuCuponDetailCode" owner:self options:nil][0];
                    
                    [menuCouponUsedCode setMenuCoupon:coupon_title detail:coupon_detail time:duration code:code];
                    
                    [menuCouponUsedCode.usedCodeBtn addTarget:self action:@selector(btn_close_pressed) forControlEvents:UIControlEventTouchUpInside];
                    [menuCouponUsedCode.readmoreBtn addTarget:self action:@selector(btn_readmore_pressed) forControlEvents:UIControlEventTouchUpInside];
                    [menuCouponUsedCode.shareBtn addTarget:self action:@selector(btn_share_pressed) forControlEvents:UIControlEventTouchUpInside];
                    
                    [self.view addSubview:menuCouponUsedCode];
                    
                    [menuCouponUsedCode setAlpha:0];
                    
                    [PCView setViewAnimationWithDuration:menuCouponUsedCode time_duration:0.4 showView:YES];
                }
            }
        }];
    }
}

-(void)btn_close_pressed{
    
    REMOVE_USER_DEFAULT_INFO(@"USING_COUPON")
    
    [self.loadingView setHidden:YES];
    [self.loadingView stopAnimating];
    [self setHiddenPCBlack:YES];
    
    [menuCouponDetail removeFromSuperview];
    [menuAlert removeFromSuperview];
    [menuInputCode removeFromSuperview];
    [usedCoupon removeFromSuperview];
    [menuCouponUsedCode removeFromSuperview];
    [self.viewPasswordFalse setHidden:YES];
    [self.tableView setUserInteractionEnabled:YES];
    [self.backBtn setUserInteractionEnabled:YES];
}

-(void)display_success_popup:(NSString *)text{
    
    (menu2==NO)?[self addUseCouponSuccess:hotpromoLogo[selectedRow] alertText:text]:[self addUseCouponSuccess:couponBrandLogo[selectedRow] alertText:text];
}

-(void)set_initail_objects{
    
    readyReload = NO;
    selectedRow = 0;
    
    [self class_UI];
}

-(void)class_UI{
    
    TABLEVIEW_COLLECTION_REFRESH(self.tableView);
    SWIPETLIFE_TOGOTOPREVIOUS;
    LINE_WITH_STATUSBAR_HEADER_NORMAL;
    
    [self setBG_Logo];
    
    [self.tableView setSeparatorColor:[UIColor clearColor]];
    [self.tableView setBackgroundColor:[UIColor clearColor]];
}

- (void)refresh:(UIRefreshControl *)refreshControl {
    
    (menu2==NO)?[self getService_fromHotPromotionDefaults:[self getUserToken]]:[self getService_fromBrandId:[self getUserToken] brand:brandID_str];
    [refreshControl endRefreshing];
}

-(void)swipeRight:(UISwipeGestureRecognizer*)gestureRecognizer{
    
    GOTO_PREVIOUS;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

-(void)goToClose:(UIPanGestureRecognizer *)pan{
    
    if(pan.state==UIGestureRecognizerStateEnded ){
        
        [UIView animateWithDuration:0.5 animations:^{
            [menuCouponDetail setAlpha:!menuCouponDetail.alpha];
            [menuCouponDetail setFrame:CGRectMake(DEVICE_WIDTH/2-menuCouponDetail.frame.size.width/2,DEVICE_HEIGHT,menuCouponDetail.frame.size.width,menuCouponDetail.frame.size.height)];
        } completion:^(BOOL finish){
            [menuCouponDetail removeFromSuperview];
        }];
    }
}

- (IBAction)backBtn:(id)sender {
    GOTO_PREVIOUS;
}

@end
