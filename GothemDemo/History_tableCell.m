
//  Created by pimpaporn chaichompoo on 2/10/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "History_tableCell.h"

@implementation History_tableCell

- (void)awakeFromNib {
    
    [super awakeFromNib];
    [self setBackgroundColor:[UIColor clearColor]];
    [self setFrame:CGRectMake(0,0,DEVICE_WIDTH,68)];
    
    [self.point setFont:[UIFont fontWithName:@"RSU-Bold" size:16]];
    [self.displayNum setFont:[UIFont fontWithName:@"RSU-Bold" size:16]];
    [self.point setTextColor:[UIColor colorWithRed:(163/255.0)  green:(46/255.0)  blue:(61/255.0)  alpha:1.0]];
    [self.displayNum setTextColor:[UIColor colorWithRed:(163/255.0)  green:(46/255.0)  blue:(61/255.0)  alpha:1.0]];
}

-(void)setCell:(NSString *)date time:(NSString *)time point:(NSString *)point display:(NSString *)displayNum usedCoupon:(BOOL)usedCoupon{
    
    if (usedCoupon==YES)
    {
        [self.date setText:[NSString stringWithFormat:@"%@ น.",time]];
        [self.time setText:[NSString stringWithFormat:@"%@",[CurrentDate_Temperature get_date_month:date]]];
    }
    else
    {
        [self.date setText:[NSString stringWithFormat:@"%@",[CurrentDate_Temperature get_date_month_name:date]]];
        [self.time setText:[NSString stringWithFormat:@"%@ น.",time]];
    }
    [self.point setText:[NSString stringWithFormat:@"%@",point]];
    [self.displayNum setText:[NSString stringWithFormat:@"%@",displayNum]];
}

@end
