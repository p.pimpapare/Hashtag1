//
//  HistoryTableCell.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/8/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "HistoryTableCell.h"

@implementation HistoryTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setBackgroundColor:[UIColor clearColor]];
}

-(void)setHistoryCell:(NSString *)date time:(NSString *)time details:(NSString *)details response:(NSString *)response{
    
    NSString *image_text, *status_text;
    UIColor *status_color;
    
    if ([response isEqualToString:@"false"])
    {
        image_text = @"icon_q";
        status_text = @"[กำลังดำเนินการ]";
        status_color = [PCColor color_A8101D];
    }
    else if ([response isEqualToString:@"true"])
    {
        image_text = @"ic_check";
        status_text = @"[ดำเนินการเสร็จสิ้น]";
        status_color = [PCColor color_4285F4];
    }
    
    [self.hImage setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",image_text]]];
    [self.hStatus setTextColor:status_color];
    [self.hStatus setText:[NSString stringWithFormat:@"%@",status_text]];
    
    [self.hDate setText:[NSString stringWithFormat:@"%@",[CurrentDate_Temperature get_date_month:date]]];
    [self.hTime setText:[NSString stringWithFormat:@"%@",time]];
    
    if ([details isEqualToString:@"<null>"] || details == nil || [details isEqualToString:@"(null)"])
    {
        [self.hDetails setText:@" "];
    }
    else
    {
        [self.hDetails setText:[NSString stringWithFormat:@"%@",details]];
    }
}

@end
