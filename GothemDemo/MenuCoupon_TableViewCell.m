
//  Created by pimpaporn chaichompoo on 2/2/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "MenuCoupon_TableViewCell.h"

@implementation MenuCoupon_TableViewCell

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateCountdown) userInfo:nil repeats: YES];
    
    self.viewBg.backgroundColor = [UIColor clearColor];
    [self setBackgroundColor:[UIColor clearColor]];
    
    [self.cDetail setFont:[UIFont fontWithName:@"RSU" size:14.0]];
    [self.cDetail setLineBreakMode:2];
    
    self.cImage.layer.masksToBounds = YES;
    [self.cImage.layer setCornerRadius:2];
    
    [PCColor set_inner_shadow:self.cImage];
}

-(void)setCoupon:(NSString *)title detail:(NSString *)detail code:(NSString *)code image:(NSString *)image{
    
    [self.cCode setHidden:NO];
    [self.cImage setBackgroundColor:[UIColor whiteColor]];
    
    [self.cTitle setText:[NSString stringWithFormat:@"%@",title]];
    [self.cDetail setText:[NSString stringWithFormat:@"%@",detail]];
    [self.cCode setText:[NSString stringWithFormat:@"%@ คะแนน",[NumberFormat format_currency:code]]];
    [self.cImage sd_setImageWithURL:[NSURL URLWithString:image]
                   placeholderImage:[UIImage imageNamed:@"picture_1_1"]options:SDWebImageProgressiveDownload];
}

-(void)setUserCoupon:(NSString *)title detail:(NSString *)detail time:(NSString *)time image:(NSString *)image{
    
    [self.imageTime setHidden:NO];
    [self.time setHidden:NO];
    
    countTime = time;
    
    NSString *time_text = [NSString stringWithFormat:@"%@",[NumberFormat find_range_of_time:time]];
    
    [self.time setText:[NSString stringWithFormat:@"%@",time_text]];
    
    [self.cTitle setText:[NSString stringWithFormat:@"%@",title]];
    [self.cDetail setText:[NSString stringWithFormat:@"%@",detail]];
    [self.cImage sd_setImageWithURL:[NSURL URLWithString:image]
                   placeholderImage:[UIImage imageNamed:@"picture_1_1"]options:SDWebImageProgressiveDownload];
}

-(void)updateCountdown {
    
    NSString *time_text = [NSString stringWithFormat:@"%@",[NumberFormat find_range_of_time:countTime]];
    
    [self.time setText:[NSString stringWithFormat:@"%@",time_text]];
    
    NSArray *split_day = [self.time.text componentsSeparatedByString:@" วัน"];
    
    if (([split_day count]>=1)||([time_text isEqualToString:@"หมดเวลาการใช้งาน"])||[time_text isEqualToString:@"0"]) {
        
        [timer invalidate];
    }
}

@end
