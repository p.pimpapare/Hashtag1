//
//  ForgotPassController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/21/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "MobileLoginController.h"
#import "ContainerViewController.h"

@interface MobileLoginController ()

@end

@implementation MobileLoginController

-(void)viewWillAppear:(BOOL)animated{
    
    GOOGLE_ANALYTICS(@"MobileLoginController");
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self set_initail_objects];
}

-(void)getService:(NSString *)userId{
    
    [self setHiddenPCLoading:NO];
    [self.view setUserInteractionEnabled:NO];
    
    GET_USER_DEFAULT_INFO(@"refcode")
    
    NSString *refcode = [NSString stringWithFormat:@"%@",userInfo];
    
    [Service post_user_login_with_ID:userId deviceToken:[BaseViewController getDeviceToken] ref_code:refcode selfID:self block:^(id result) {
        
        [BaseViewController log_comment:@"User info: " text:result];
        
        [self.loginBtn setUserInteractionEnabled:YES];
        
        NSString *resultText = [NSString stringWithFormat:@"%@",result];
        
        if ([resultText isEqualToString:@"error"]) {
            
            [self setHiddenPCLoading:YES];
            [self network_error];
            
        }else if ([result count]==0) {
            
            [self service_error];
            
        }else{
            
            NSString *successText = [NSString stringWithFormat:@"%@",(NSString *)result[@"success"]];
            
            if ([successText isEqualToString:@"0"]) {
                
                resultErrorMsg = [NSString stringWithFormat:@"%@",result[@"message"]];
                
                [self login_error];
                
            }else{
                
                [self removeNSUserdefualts];
                
                // Check user agreement : if the result = 0, go to term & condition scene
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"UserInfo"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                
                if (userDefaults){
                    
                    [userDefaults setObject:result forKey:@"UserInfo"];
                }
                
                NSString *userAgreed = [NSString stringWithFormat:@"%@",result[@"agreed"]];
                
                ([userAgreed isEqualToString:@"1"])?[self callService:result[@"token"]]:[self goToTerm];
            }
        }
    }];
}

-(void)removeNSUserdefualts{
    
    // remove for updating userInfo, settingInfo
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"UserEmail"];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"UserInfo"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"newsfeed_setting"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"noti_setting"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"voice_setting"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"alarm_setting"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"led_setting"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"lockscreen_setting"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)validateInfo:(NSString *)emailStr{
    
    if ([self.numTextfield.text length]==0) {
        return false;
    }else{
        return true;
    }
}

-(void)gotoTermCon:(id)sender{
    
    [self pushViewCtrlWithViewCtrlName:@"TermAndConditionBeforeLogin"];
}

-(void)set_initail_objects{
    
    validateInfo = NO;
    
    [PCTapGesture tapAction:self view:self.telLabel actionTarget:@selector(userTappedOnTel:)];
    [PCTapGesture tapAction:self view:self.self.emailLabel actionTarget:@selector(userTappedOnEmail:)];
    
    [self.telLabel setUserInteractionEnabled:YES];
    [self.emailLabel setUserInteractionEnabled:YES];
    
    [self.regisText setText:CALL_CENTER_TEXT];
    
    [self.regisText setFont:[PCFont fontRSU:16.0]];
    [self.accessText setFont:[PCFont fontRSU:13.0]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notification_alert:) name:@"Service_status" object:nil];
    
    [self class_UI];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)notification_alert:(NSNotification *)noti{
    
    [self.view setUserInteractionEnabled:YES];
    
    NSString *strNoti = [noti object];

    if ([strNoti isEqualToString:@"gotoHome"]) {
        
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UIViewController * mainView = [storyboard   instantiateViewControllerWithIdentifier:@"ContainerViewController"] ;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.navigationController pushViewController:mainView animated:YES];
            });
        
    }else if([strNoti isEqualToString:@"network_error"]){
        [self network_error];
    }else{
        [self service_error];
    }
}

-(void)class_UI{
    
    [self setBg];
    [self addPCLoading:self];
    
    SWIPETLIFE_TOGOTOPREVIOUS;
    LINE_WITH_STATUSBAR_LOGIN;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"gotoHome" object:nil]; // Solution when NSNoti accessing in twice times

    [self.numTextfield setValue:[PCColor color_767676] forKeyPath:@"_placeholderLabel.textColor"];
    
    [PCTapGesture set_target_button:self.registerOnWebBtn type:1];
    [PCTapGesture set_target_button:self.registerOnWebBtn2 type:2];
    [PCTapGesture tapDismissKeyBoard:self view:self.viewBg];
    
    [PCTextfield setpadding_textfield:self.numTextfield framLeft:15 framRight:0];
    
    [self.heightLogo setConstant:HEIGHT_LOGO];
    [self.widthLogo setConstant:WIDTH_LOGO];
    
    if (IS_IPAD==YES) {
        
        [self.topLogo setConstant:150];
        [self.heightLabel setConstant:55];
        
        int bottomreister_num = 0,bottomregister2_num = 0,
        bottomregister3_num = 0, bottomregister4_num = 0;
        
        int range_ipad = DEVICE_WIDTH/3;
        
        [self.txtfeild_left setConstant:range_ipad];
        [self.txtfeild_right setConstant:range_ipad];
        [self.btn_left setConstant:range_ipad];
        [self.btn_right setConstant:range_ipad];
        
        [self.topic_left setConstant:range_ipad];
        [self.icon_error_right setConstant:range_ipad+7];
        [self.view_error_right setConstant:range_ipad+5];
        
        if (DEVICE_HEIGHT==IPAD_PRO_HEIGHT) {
            
            bottomreister_num = 510;
            bottomregister2_num = 555;
            bottomregister3_num = 554;
            bottomregister4_num = 32;
            
        }else{
            
            bottomreister_num = 310;
            bottomregister2_num = 355;
            bottomregister3_num = 354;
            bottomregister4_num = 32;
        }
        
        [self.bottomregister setConstant:bottomreister_num];
        [self.bottomregister2 setConstant:bottomregister2_num];
        [self.bottomregister3 setConstant:bottomregister3_num];
        [self.bottomregister4 setConstant:bottomregister4_num];
        
    }else{
        
        if(DEVICE_HEIGHT==PHONE4){
            
            [self.heightLogo setConstant:HEIGHT_LOGO45];
            [self.widthLogo setConstant:WIDTH_LOGO45];
            [self.topLogo setConstant:55];
            [self.heightLabel setConstant:50];
            [self.heightTextfield setConstant:72];
            [self.topErrorView setConstant:50];
            
        }else{
            [self.bottomregister setConstant:10];
            [self.bottomregister2 setConstant:53];
            [self.bottomregister3 setConstant:54];
            [self.bottomregister4 setConstant:30];
        }
    }
}

-(void)swipeRight:(UISwipeGestureRecognizer*)gestureRecognizer{
    GOTO_PREVIOUS;
}

// Set textfield delegate
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.numTextfield resignFirstResponder];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    [self.errorView setHidden:YES];
    [self.errorIcon setHidden:YES];
    [self.loginBtn setUserInteractionEnabled:YES];
    
    if (DEVICE_WIDTH==PHONE45_WIDTH) {
        [PCTextfield animateTextField:self.viewBg up:YES];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    [self.loginBtn setUserInteractionEnabled:YES];
    
    if (DEVICE_WIDTH==PHONE45_WIDTH) {
        [PCTextfield animateTextField:self.viewBg up:NO];
    }
}

// This method is not allow the user to input the number more than 10 degits
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    // Prevent crashing undo bug – see note below.
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return newLength <= 10;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

- (IBAction)loginBtn:(id)sender {
    
    [self.view endEditing:YES];
    
    if([self.numTextfield.text length]==0){
        
        validateInfo = NO;
        [self.errorView setHidden:NO];
        [self.errorIcon setHidden:NO];
        [self.loginBtn setUserInteractionEnabled:NO];
        
    }else{ // login pass

        [self.view setUserInteractionEnabled:NO];
        [self getService:self.numTextfield.text];
    }
}

-(void)closeErrorScene{
    
    [self.errorView setHidden:YES];
    [self.errorIcon setHidden:YES];
    [self.view setUserInteractionEnabled:YES];
}

-(void)goToTerm{
    [self pushViewCtrlWithViewCtrlName:@"TermAndConditionController"];
    [self.view setUserInteractionEnabled:YES];
}

-(void)callService:(NSString *)token{
    
//    [self.view setUserInteractionEnabled:NO];
    
    CallServiceAfterLogin *viewclass = [[CallServiceAfterLogin alloc]init];
    [viewclass call_service_from:token type:1];
}

-(void)network_error{
    
    [self.view setUserInteractionEnabled:YES];
    
    ALERT_MESSAGE_NETWORK([self.view setUserInteractionEnabled:NO]; [self getService:self.numTextfield.text];,[self setHiddenPCLoading:YES];)
}

-(void)service_error{
    
    [self.view setUserInteractionEnabled:YES];
    
    ALERT_MESSAGE(@"ระบบขัดข้อง",@"โปรดลองใหม่อีกครั้ง",[self getService:self.numTextfield.text];,[self setHiddenPCLoading:YES];)
}

-(void)login_error{
    
    [self.view setUserInteractionEnabled:YES];
    
    ALERT_MESSAGE(@"ข้อผิดพลาด",resultErrorMsg,[self getService:self.numTextfield.text];,[self setHiddenPCLoading:YES];)
}

- (IBAction)backBtn:(id)sender {
    
    GOTO_PREVIOUS
}

-(void)userTappedOnTel:(id)sender{
    NSString *phoneNumber = [@"tel://" stringByAppendingString:CALL_CENTER_TEL];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}

-(void)userTappedOnEmail:(id)sender{
    
    //put email info here:
    NSString *toEmail = CALL_CENTER_EMAIL ;
    NSString *subject = @" ";
    NSString *body = @" ";
    
    //opens mail app with new email started
    NSString *email = [NSString stringWithFormat:@"mailto:%@?subject=%@&body=%@", toEmail,subject,body];
    email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
}

@end
