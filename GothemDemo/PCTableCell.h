
//  Created by pimpaporn chaichompoo on 2/1/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

#import <UIKit/UIKit.h>

@interface PCTableCell : UITableViewCell{
    BOOL tab, tab_update, update_version;
    NSTimer *timer;
}

@property (weak, nonatomic) IBOutlet UIImageView *iconInCell;
@property (weak, nonatomic) IBOutlet UILabel *nameInCell;
//@property (weak, nonatomic) IBOutlet UIButton *selectedBtn;
@property (weak, nonatomic) IBOutlet UIImageView *selectedImage;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *width;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *right;

@property (weak, nonatomic) IBOutlet UIButton *btn_update_version;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loading;

@property (weak, nonatomic) NSArray *ActionBtn;
@property NSString *tagMenu;
-(NSString *)action:(NSString *) selecttag;

-(void)set_cell_update_version:(NSString *)name;

-(void)setIconCell:(NSString *)icon index:(int)index;
-(void)setNameCell:(NSString *)name;
-(void)setNewsWidgetCell:(NSString *)name image:(NSString *)icon;
- (void)selectedNewfeedCell;

@end
