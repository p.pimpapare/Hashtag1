//
//  QAController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/15/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "QuestionAnswerController.h"

@interface QuestionAnswerController ()

@end

@implementation QuestionAnswerController

-(void)viewWillAppear:(BOOL)animated{
    
    GOOGLE_ANALYTICS(@"QuestionAnswerController");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.loadingView startAnimating];
    [self class_UI];
}

-(void)setQuestionAnswer:(NSString *)question answer:(NSString *)answer{
    
    [self.questionTxt setText:[NSString stringWithFormat:@"%@",question]];

    NSString *answerText = [NSString stringWithFormat:@"%@",[ReplaceHTMLFont replaceHTML_by_string:(id)answer old_string:@"BrowalliaUPC" new_string:@"RSU"]];
    NSString *answerText2 = [NSString stringWithFormat:@"%@",[ReplaceHTMLFont replaceHTML_by_string:(id)answerText old_string:@"Helvetica" new_string:@"RSU"]];
    NSString *answerText3 = [NSString stringWithFormat:@"%@",[ReplaceHTMLFont replaceHTML_by_string:(id)answerText2 old_string:@"Cordia New" new_string:@"RSU"]];
    NSString *answerText4 = [NSString stringWithFormat:@"%@",[ReplaceHTMLFont replaceHTML_by_string:(id)answerText3 old_string:@"Angsana New" new_string:@"RSU"]];
    NSString *answerText5 = [NSString stringWithFormat:@"%@",[ReplaceHTMLFont replaceHTML_by_string:(id)answerText4 old_string:@"Calibri" new_string:@"RSU"]];
    
    [self.webView loadHTMLString:answerText5 baseURL:nil];
}

-(void)class_UI{

    LINE_WITH_STATUSBAR_HEADER_NORMAL;
    SWIPETLIFE_TOGOTOPREVIOUS;
}

-(void)swipeRight:(UISwipeGestureRecognizer*)gestureRecognizer{
    GOTO_PREVIOUS;
}

- (IBAction)backBtn:(id)sender {
    GOTO_PREVIOUS;
}

///// WEB_VIEW

-(void)didReceiveMemoryWarning{
    
    [super didReceiveMemoryWarning];
}

- (void)setView:(UIView *)view{
    
    if (view == nil){
        [self releaseSubviews];
    }
    
    [super setView:view];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    if (!webView.isLoading) {
        
        [self.loadingView stopAnimating];
        [self.loadingView setHidden:YES];
    }
}

-(void)openUrl:(NSString *)urlText{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",urlText]];
    NSURLRequest *req = [NSURLRequest requestWithURL:url];
    
    [self.webView loadRequest:req];
}

-(void)releaseSubviews{
    
    [self.webView setDelegate:nil];
    [self.webView stopLoading];
}

- (void)dealloc{
    
    [self releaseSubviews];
}

- (void)viewWillDisappear:(BOOL)animated{
    
    [self releaseSubviews];
}
///// END OF WEB_VIEW

@end
