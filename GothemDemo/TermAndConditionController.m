//
//  TermConditionController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/11/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "TermAndConditionController.h"
#import "ContainerViewController.h"

@interface TermAndConditionController (){
    NSString *documentInfo;
}
@end

@implementation TermAndConditionController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.loadingView setHidden:NO];
    [self.loadingView startAnimating];
        
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notification_alert:) name:@"Service_status" object:nil];

    [self class_UI];
    
    [self get_service];
    [self.webView setHidden:YES];
}

-(void)get_service{
    
    [Service get_usage_agreement:self block:^(id result) {
        
        NSString *resultText = [NSString stringWithFormat:@"%@",result];
        
        if (![resultText isEqualToString:@"error"]) {
            
            documentInfo =[NSString stringWithFormat:@"%@",[ReplaceHTMLFont replaceHTML_by_string:[NSString stringWithFormat:@"%@",[ReplaceHTMLFont replaceHTML_by_range:result start_string:@"<style>" end_string:@"</style>" replaceString:@" "]] old_string:@"<html lang=\"en\">" new_string:@"<html lang=\"en\"> <style>html,body,head,span,h,p { font-family:RSU; }</style>"]];
            [self createWebViewWithHTML:documentInfo];
            
            [self.loadingView stopAnimating];
            [self.loadingView setHidden:YES];
        }
    }];
}

- (void) createWebViewWithHTML:(NSString *)text{
    
    [self.view setUserInteractionEnabled:YES];
    
    [self.webView setHidden:NO];
    [self.webView setOpaque:NO];
    [self.webView setBackgroundColor:[UIColor clearColor]];
    
    [self.webView loadHTMLString:text baseURL:nil];
}

-(void)class_UI{
    
    selected = NO;
    
    [self.heightMenuBar setConstant:HEIGHT_MENUBAR];
    [PCColor set_shadow_headerBar:self.viewHeader];
    
    [self.confirmBtn setUserInteractionEnabled:NO];
    [self.line setBackgroundColor:[PCColor color_A6A6A6]];
    [PCColor set_shadow:self.viewHeader];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"gotoHome" object:nil]; // Solution when NSNoti accessing in twice times
}

- (IBAction)backBtn:(id)sender {
    
    LoginController *view_class = [self loadNibWithName:@"LoginController"];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController pushViewController:view_class animated:YES];
    });

   /* UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main"
                                                         bundle:nil];
    LoginController *loginScene = (LoginController *)
    [storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
    [self.navigationController pushViewController:loginScene animated:NO];
    */
}

- (IBAction)selectedBtn:(id)sender {
    
    if (selected==NO) {
        
        selected=YES;
        [self.selectBtn setBackgroundImage:[UIImage imageNamed:@"checkMark"] forState:UIControlStateNormal];
        [self.confirmBtn setBackgroundColor:[PCColor color_A8101D]];
        [self.confirmBtn setUserInteractionEnabled:YES];
        
    }else if (selected==YES){
        
        selected=NO;
        [self.selectBtn setBackgroundImage:[UIImage imageNamed:@"circle_red-1"] forState:UIControlStateNormal];
        [self.confirmBtn setBackgroundColor:[UIColor colorWithRed:(187/255.0) green:(187/255.0) blue:(187/255.0) alpha:1.0]];
        [self.confirmBtn setUserInteractionEnabled:NO];
    }
}

- (IBAction)confrimBtn:(id)sender {
    
    SET_USER_DEFAULT_INFO(@"Accepted",@"TermInfo")
    [self.view setUserInteractionEnabled:NO];
    
    [Service post_user_acception:[self getUserToken] selfID:self block:^(id result) {
        [self callService];
    }];
}

-(void)callService{
    
    [self.loadingView setHidden:NO];
    [self.loadingView stopAnimating];
    
    GET_USER_DEFAULT_INFO(@"UserInfo")
    
    CallServiceAfterLogin *viewclass = [[CallServiceAfterLogin alloc]init];
    [viewclass call_service_from:[NSString stringWithFormat:@"%@",userInfo[@"token"]] type:1];
}

-(void)network_error{
    
    [self.view setUserInteractionEnabled:YES];
    
    ALERT_MESSAGE_NETWORK([self.view setUserInteractionEnabled:NO]; [self callService];,
                          [self.loadingView setHidden:YES];
                          [self.loadingView stopAnimating];
                          
                          )
}

///// WEB_VIEW

-(void)didReceiveMemoryWarning{
    
    [super didReceiveMemoryWarning];
}

- (void)setView:(UIView *)view{
    
    if (view == nil){
        [self releaseSubviews];
    }
    
    [super setView:view];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    if (!webView.isLoading) {
        
        [self.loadingView stopAnimating];
        [self.loadingView setHidden:YES];
    }
}

-(void)openUrl:(NSString *)urlText{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",urlText]];
    NSURLRequest *req = [NSURLRequest requestWithURL:url];
    
    [self.webView loadRequest:req];
}

-(void)releaseSubviews{
    
    [self.webView setDelegate:nil];
    [self.webView stopLoading];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self releaseSubviews];
}

-(void)notification_alert:(NSNotification *)noti{
    
    [self.view setUserInteractionEnabled:YES];

    NSString *strNoti = [noti object];
    
    if ([strNoti isEqualToString:@"gotoHome"]) {
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController * mainView = [storyboard   instantiateViewControllerWithIdentifier:@"ContainerViewController"] ;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.navigationController pushViewController:mainView animated:YES];
        });
        
    }else{
        [self network_error];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [self releaseSubviews];
}
///// END OF WEB_VIEW

@end
