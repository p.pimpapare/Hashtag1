//
//  ViewController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/19/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "XLPageStripMenuBarController.h"

@interface XLPageStripMenuBarController (){
    PCBg *pcblack;
}
@end

@implementation XLPageStripMenuBarController

-(void)viewWillAppear:(BOOL)animated{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self class_UI];
}

-(void)class_UI{
    
    LINE_WITH_STATUSBAR_NORMAL;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notification_alert:) name:@"select_menu_bar" object:nil];
}

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)notification_alert:(NSNotification *)noti{
    
    NSString *strNoti = [noti object];
    
    if ([strNoti isEqualToString:@"selectedBtn"]){
        
        [[NSUserDefaults standardUserDefaults] setObject:strNoti forKey:@"widgetSetting"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    else if([strNoti isEqualToString:@"unselectedBtn"]){
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"widgetSetting"];
    }
}

@end
