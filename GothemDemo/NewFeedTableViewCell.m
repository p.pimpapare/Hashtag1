
//  Created by pimpaporn chaichompoo on 2/11/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "NewFeedTableViewCell.h"

@implementation NewFeedTableViewCell

- (void)awakeFromNib {
    
    [super awakeFromNib];
    [self setBackgroundColor:[UIColor clearColor]];
    [self.title setFont:[UIFont fontWithName:@"RSU" size:15]];
    [self.detail setFont:[UIFont fontWithName:@"RSU" size:13]];
}

-(void)setNewFeedTableCell:(NSString *)title{
    [self.title setText:[NSString stringWithFormat:@"%@",title]];
}

@end
