//
//  XLPageStripMenuBarController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/19/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

//  This class is base class of XLPagerTabStrip library

#import <UIKit/UIKit.h>

@interface XLPageStripMenuBarController : UIViewController{
    BOOL _isReload;
}

@property (weak, nonatomic) IBOutlet UIView *statusBar;

@end

