//
//  PCTapGestureRecognizer.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 4/11/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "PCTapGesture.h"

@implementation PCTapGesture

+(void)set_target_button:(UIButton * _Nonnull)button type:(int)type
{
    /*
     1 = new user
     2 = existing user
     3 = forgot password
     */
    
    switch (type) {
        case 1:
            [button addTarget:self
                       action:@selector(new_user_tap_on_link:)
             forControlEvents:UIControlEventTouchUpInside];
            break;
        case 2:
            [button addTarget:self
                       action:@selector(existing_user_tap_on_link:)
             forControlEvents:UIControlEventTouchUpInside];
            break;
        case 3:
            [button addTarget:self
                       action:@selector(user_tap_on_link_forgot_password:)
             forControlEvents:UIControlEventTouchUpInside];
            break;
        default:
            [button addTarget:self
                       action:@selector(new_user_tap_on_link:)
             forControlEvents:UIControlEventTouchUpInside];
            break;
    }
}

+(void)new_user_tap_on_link:(id)sender{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:URL_NEW_REGISTER]];
}

+(void)existing_user_tap_on_link:(id)sender{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:URL_EXISTING_REGISTER]];
}

+(void)user_tap_on_link_forgot_password:(id)sender{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:URL_FORGOTPASSWORD]];
}

+(void)tapDismissKeyBoard:(UIViewController *)classController view:(UIView *)view{
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:classController
                                   action:@selector(dismissKeyboard)];
    [view addGestureRecognizer:tap];
}

+(void)tapAction:(UIViewController *)classController view:(id)view actionTarget:(SEL _Nonnull)target{
 
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:classController
                                   action:target];
    [view addGestureRecognizer:tap];
}

@end
