//
//  SequereceivePointController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/31/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SequereceivePointController : XLBarPagerTabStripViewController<XLPagerTabStripChildItem,XLPagerTabStripViewControllerDataSource,XLPagerTabStripViewControllerDelegate,UICollectionViewDataSource,UICollectionViewDelegate>{
    
    int selectedTab;
    BOOL _isReload;
    UICollectionView *_collectionView;
    
    NSArray *titleMenu;
    int selectedTabMenu;
    NSMutableArray *swiptTab;
}

@property (weak, nonatomic) IBOutlet UIView *menuBar;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end
