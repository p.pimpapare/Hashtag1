
//  Created by pimpaporn chaichompoo on 2/3/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "MenuCouponDetail.h"
#import "NumberFormat.h"

@implementation MenuCouponDetail

-(void)awakeFromNib{
    
    [super awakeFromNib];
    [self setFrame:CGRectMake(0,0,DEVICE_WIDTH,DEVICE_HEIGHT)];
    [self.pTitle setTextColor:[PCColor color_424242]];
    [self.line setBackgroundColor:[PCColor color_A6A6A6]];
    
    [PCColor set_inner_shadow:self.pImage];
    
    self.pImage.layer.masksToBounds = YES;
    
    [self.pImage.layer setCornerRadius:6];
    [self.shareBtn.layer setCornerRadius:6];
    [self.viewLogo.layer setCornerRadius:2];
    [self.ReadMoreBtn.layer setCornerRadius:2];
    
    [BaseViewController setLineColor:self.lineHeader bottom:self.lineBottom];
    
    [self.line setBackgroundColor:[PCColor color_A6A6A6]];
}

-(void)setPromotionDetail:(NSString *)couponID title:(NSString *)title detail:(NSString *)detail point:(NSString *)point image:(NSString *)image logo:(NSString *)logo available:(NSString *)available dealOwner:(NSString *)dealOwner canUse:(NSString *)canUse{
    
    // get user Email
    id userEmail, userInfo;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if (userDefaults){
        
        userEmail = (id)[userDefaults objectForKey:@"UserEmail"];
        userInfo = (id)[userDefaults objectForKey:@"UserInfo"];
    }
    
    userToken = [NSString stringWithFormat:@"%@",userInfo[@"token"]];
    
    /*
     Suppose user can use coupon
     userEmail = @"p.pimpapare@gmail.com";
     ///////////
     */
    
    if (userEmail!=nil) { // login with Email = user can use coupon
        
        [self getService:userToken couponID:couponID point:point dealOwner:dealOwner available:available canUse:canUse];
        
    }else if(userEmail==nil){ // login with id = user can't use coupon
        
        [self.pUsedLogo setHidden:YES];
        [self.usedPointBtn setHidden:YES];
        [self.notAvailText setHidden:NO];
        [self.notAvailText setText:@"ล็อคอินด้วยอีเมลเพื่อทำการแลก"];
//        [self.notAvailText setText:@"ใช้ได้เฉพาะหน้าร้าน"];
    }
    
    [self.pTitle setText:[NSString stringWithFormat:@"%@",title]];
    [self.pDetail setText:[NSString stringWithFormat:@"%@",detail]];
    [self.pImage sd_setImageWithURL:[NSURL URLWithString:image]
                   placeholderImage:[UIImage imageNamed:@"picture_1_1"]options:SDWebImageProgressiveDownload];
    
    [self.pLogo sd_setImageWithURL:[NSURL URLWithString:logo]
                  placeholderImage:[UIImage imageNamed:@""]options:SDWebImageProgressiveDownload];
}

-(void)getService:(NSString *)token couponID:(NSString *)couponID point:(NSString *)point dealOwner:(NSString *)dealOwner available:(NSString *)available canUse:(NSString *)canUse{
    
    /*
     Suppose user can use coupon
     dealOwner = @"midas";
     available = @"true";
     canUse = @"true";
     point = @"1";
     ///////////
     */
    
    // Check coupon status

    if ([dealOwner isEqualToString:@"midas"])
    {
        if ([available isEqualToString:@"false"])
        {
            [self errorMessage:@"ผู้ใช้สิทธ์เต็มจำนวน"];
            
        }else{
            
            if ([canUse isEqualToString:@"false"])
            {
                [self errorMessage:@"ไม่สามารถแลกสิทธ์เพิ่มได้อีก"];
                
            }else{
                
                [self.notAvailText setHidden:YES];
                [self.pUsedLogo setHidden:NO];
                [self.usedPointBtn setTitle:[NSString stringWithFormat:@"แลก %@ คะแนน",[NumberFormat format_currency:point]] forState:UIControlStateNormal];
                [self.usedPointBtn setHidden:NO];
            }
        }
        
    }else{ // dealOwner = t1c
        
        [self errorMessage:@"ใช้ได้เฉพาะหน้าร้านเท่านั้น"]; // ถ้า get message จาก service จะได้ว่า "ระบบขัดข้อง"
    }
    
    //    [Service getCouponStatus:token couponID:couponID selfID:menuTableView block:^(id result) {
    //
    //        NSString *available = [NSString stringWithFormat:@"%@",result[@"available"]];
    //        if ([available isEqualToString:@"0"]) {
    //            [self.usedPointBtn setHidden:YES];
    //            [self.notAvailText setText:[NSString stringWithFormat:@"%@",result[@"message"]]];
    //            [self.notAvailText setHidden:NO];
    //        }else{
    //            [self.notAvailText setHidden:YES];
    //            [self.pUsedLogo setHidden:NO];
    //            [self.usedPointBtn setTitle:[NSString stringWithFormat:@"แลก %@ คะแนน",point] forState:UIControlStateNormal];
    //            [self.usedPointBtn setHidden:NO];
    //        }
    //        [self action:@"success"];
    //    }];
}

-(void)errorMessage:(NSString *)errorText{
    
    [self.usedPointBtn setHidden:YES];
    [self.notAvailText setText:[NSString stringWithFormat:@"%@",errorText]];
    [self.notAvailText setHidden:NO];
}

-(void)setPromotionStatus:(NSString *)couponID title:(NSString *)title detail:(NSString *)detail point:(NSString *)point image:(NSString *)image logo:(NSString *)logo errorText:(NSString *)errorText{
    
    [self.usedPointBtn setHidden:YES];
    [self.notAvailText setText:[NSString stringWithFormat:@"%@",errorText]];
    [self.notAvailText setHidden:NO];
    
    [self.pTitle setText:[NSString stringWithFormat:@"%@",title]];
    [self.pDetail setText:[NSString stringWithFormat:@"%@",detail]];
    [self.pImage sd_setImageWithURL:[NSURL URLWithString:image]
                   placeholderImage:[UIImage imageNamed:@"picture_1_1"]options:SDWebImageProgressiveDownload];
    
    [self.pLogo sd_setImageWithURL:[NSURL URLWithString:logo]
                  placeholderImage:[UIImage imageNamed:@""]options:SDWebImageProgressiveDownload];
}


@end
