//
//  CurrentDate_Temperature.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/26/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "CurrentDate_Temperature.h"

@implementation CurrentDate_Temperature

+(NSString *)get_date_time{
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    
    NSLocale *locale = [[NSLocale alloc]
                        initWithLocaleIdentifier:@"th-TH"];
    [df setLocale:locale];
    
    NSString *todayText = [df stringFromDate:[NSDate date]];
    
    return todayText;
}

+(NSString *)get_day_name{ // get day name ex. Monday
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEEE"];
    
    // ex. วันจันทร์
    NSLocale *locale = [[NSLocale alloc]
                        initWithLocaleIdentifier:@"th-TH"];
    [dateFormatter setLocale:locale];
    
    [BaseViewController log_comment:@"Get date name " text:[dateFormatter stringFromDate:[NSDate date]]];
    
    return [dateFormatter stringFromDate:[NSDate date]];
}

+(NSString *)get_date_format_dd_mm_yy{ // get date ex. 29/03/59
    
    NSDate *today = [[NSDate alloc] init];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yy"];

    NSLocale *locale = [[NSLocale alloc]
                        initWithLocaleIdentifier:@"th-TH"];
    [dateFormatter setLocale:locale];
    
    NSString *convertedString = [dateFormatter stringFromDate:today];
    
    return convertedString;
}

+(NSString *)get_date_month_name:(NSString *)dateStr{ // get date ex. 29 เม.ย 59
    
    NSArray *splitToArray = [dateStr componentsSeparatedByString:@"-"];
    NSString *year = [splitToArray objectAtIndex:0];
    NSString *month = [splitToArray objectAtIndex:1];
    NSString *day = [splitToArray objectAtIndex:2];
    
    int year_int = [year intValue];
    int yearTH_int = year_int+543;
    
    NSString *yearStr = [NSString stringWithFormat:@"%d",yearTH_int];
    NSString *substringYear = [yearStr substringFromIndex:2]; // ex 59
    
    //    NSString *convertMonth = [NSString stringWithFormat:@"%@",[DateMonth convertMonthToNameTH:month]];
    
    NSMutableString *date = [[NSMutableString alloc]init];
    
    [date appendString:[NSString stringWithFormat:@"%@/",day]];
    //    [date appendString:[NSString stringWithFormat:@"%@",convertMonth]];
    [date appendString:[NSString stringWithFormat:@"%@/",month]];
    [date appendString:[NSString stringWithFormat:@"%@",substringYear]];
    
    return date;
}

+(NSString *)get_date_month:(NSString *)dateStr{ // get date ex. 29/03/59
    
    int date_int = [dateStr intValue];
    
//    if ([dateStr rangeOfString:@"2559"].location != NSNotFound) {
    if (date_int >= 2559) {
        
        NSArray *splitToArray = [dateStr componentsSeparatedByString:@"-"];
        NSString *year = [splitToArray objectAtIndex:0];
        NSString *month = [splitToArray objectAtIndex:1];
        NSString *day = [splitToArray objectAtIndex:2];
        
        NSString *yearStr = [NSString stringWithFormat:@"%@",year];
        NSString *substringYear = [yearStr substringFromIndex:2]; // ex 59
        
        NSMutableString *date = [[NSMutableString alloc]init];
        
        [date appendString:[NSString stringWithFormat:@"%@/",day]];
        [date appendString:[NSString stringWithFormat:@"%@/",month]];
        [date appendString:[NSString stringWithFormat:@"%@",substringYear]];
        
        return date;
        
    }else if ([dateStr rangeOfString:@"0000"].location != NSNotFound){
        
        return @"00/00/00";

    }else{
        
        if((![dateStr isEqualToString:@" "])||([dateStr length]!=0)||(![dateStr isEqualToString:@"(null)"])||([dateStr isEqualToString:@"-"])){
            
            NSArray *splitToArray = [dateStr componentsSeparatedByString:@"-"];
            NSString *year = [splitToArray objectAtIndex:0];
            NSString *month = [splitToArray objectAtIndex:1];
            
            NSString *day;
            
            if ([splitToArray objectAtIndex:0]!=nil) {
                
                day = [splitToArray objectAtIndex:2];
                
            }else{
                
                day = @" ";
            }
            
            int year_int = [year intValue];
            int yearTH_int = year_int+543;
            
            NSString *yearStr = [NSString stringWithFormat:@"%d",yearTH_int];
            NSString *substringYear = [yearStr substringFromIndex:2]; // ex 59
            
            NSMutableString *date = [[NSMutableString alloc]init];
            
            [date appendString:[NSString stringWithFormat:@"%@/",day]];
            [date appendString:[NSString stringWithFormat:@"%@/",month]];
            [date appendString:[NSString stringWithFormat:@"%@",substringYear]];
            
            return date;
        }
        else{
            return 0;
        }
    }
}

+(NSString *)get_current_time{
    
    NSDate *today = [[NSDate alloc] init];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //    [dateFormatter setDateFormat:@"hh:mm a"]; // 12 hr
    [dateFormatter setDateFormat:@"HH:mm"]; // 24 hrs
    
    NSString *timeString = [dateFormatter stringFromDate:today];
    return  timeString;
}

+(NSString *)get_time_am_pm{ // ex. pm am
    
    NSDate *today = [[NSDate alloc] init];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm a"];
    NSString *timeString = [dateFormatter stringFromDate:today];
    
    NSArray *stringArray = [timeString componentsSeparatedByString: @" "];
    NSString *text = [stringArray objectAtIndex:1];
    
    return  text;
}

+(NSString *)get_current_temp:(NSString *)location{
    
    NSString *url = [NSString stringWithFormat:@"http://api.openweathermap.org/data/2.5/weather?q=%@,th&appid=5f637da4579216f4722933271ebea6cf",location];
    
    NSString *urlEscapeStr = [[NSString  stringWithString:url] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]
                                    initWithURL:[NSURL URLWithString:urlEscapeStr]
                                    cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                    timeoutInterval:15];
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:nil
                                                             error:nil];
    
    SBJsonParser* parser = [[SBJsonParser alloc]init];
    NSDictionary *result = [parser objectWithData:responseData];
    
    [BaseViewController log_comment:@"JSON dictionar " text:[NSString stringWithFormat:@"%@",[result description]]];
    
    NSString *temp = [NSString stringWithFormat:@"%@",result[@"main"][@"temp"]]; // ได้ temp หน่วย เคลวิน
    
    NSString * tempText;
    
    if ([temp isEqualToString:@"(null)"]) {
        tempText = @"32";
    }else{
        
        //  แปลง เคลวิน -> ฟาเรนไฮต์ °F = K × 1.8 − 459.69
        double k = [temp doubleValue];
        double f = (k*1.8)-459.69;
        
        // แปลง ฟาเรนไฮต์ -> เซลเซียส °C = 5/9(°F - 32)
        double c = ((f - 32)*5)/9;
        tempText = [NSString stringWithFormat:@"%.f",c]; // @"%.2f" = only 2 decimal places
    }
    
    return tempText;
}

@end
