//
//  MenuShopController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/19/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

#import "MenuShopController.h"

@interface MenuShopController ()<KDCycleBannerViewDataSource, KDCycleBannerViewDelegate>{
    
    BOOL network_connection;
}

@property (weak, nonatomic) IBOutlet KDCycleBannerView *cycleBannerViewTop;
@property (strong, nonatomic) KDCycleBannerView *cycleBannerViewBottom;

@end

@implementation MenuShopController

KDCycleDelegate(@"picture_1_2",3)

-(void)viewWillAppear:(BOOL)animated{
    
    GOOGLE_ANALYTICS(@"MenuShopController");
    
    if (network_connection==NO) {
        
//        [self get_user_info];
        [self getService_fromCategoryDefualts];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addPCLoading:self];
    [self setHiddenPCLoading:NO];
    [self setLoadingGrayColor];
    
    [self setAdvertising];
    [self setPagerAdvertising];
}

-(void)getService_fromCategoryDefualts{
    
    [Service get_category:[self getUserToken] selfID:self block:^(id result) {
        
        NSString *resultText = [NSString stringWithFormat:@"%@",result];
        
        [self removeObjects];
        
        if (![resultText isEqualToString:@"error"]) {
            
            if ([result count] == 0) {
                [self alert_login_again];
            }else{
            for (int i=0; i<[result[@"message"] count];i++) {
                [cID addObject:result[@"message"][i][@"category_id"]];
                [cImage addObject:result[@"message"][i][@"category_img"]];
                [cName addObject:result[@"message"][i][@"category_name"]];
                [colorTextName addObject:result[@"message"][i][@"category_text_color"]];
            }
            network_connection = YES;
            
            [self setHiddenPCLoading:YES];
            [self.collectionView reloadData];
            }
        }
    }];
}

-(void)removeObjects{
    
    cID = [[NSMutableArray alloc]init];
    cName = [[NSMutableArray alloc]init];
    cImage = [[NSMutableArray alloc]init];
    colorTextName = [[NSMutableArray alloc]init];
}

-(void)setAdvertising{
    
    advertising_array = [[NSMutableArray alloc]init];
    
    GET_USER_DEFAULT_INFO(@"AdvertisingInfo2")
    
    for (int i=0;i<[userInfo[@"banner"] count];i++)
    {
        [advertising_array addObject:userInfo[@"banner"][i][@"banner_image"]];
    }
    
    [self class_UI];
}

-(void)class_UI{
    
    [self setBG_Logo];
    
    int height_ads = 0;
    
    if (IS_IPAD==YES) {
        
        if (DEVICE_HEIGHT==IPAD_PRO_HEIGHT)
        {
            height_ads = HEIGHT_ADVERTISING_IPAD_PRO;
        }else
        {
            height_ads = HEIGHT_ADVERTISING_IPAD;
        }
        
    }else{
        
        if(DEVICE_WIDTH==PHONE6_WIDTH)
        {
            height_ads = HEIGHT_ADVERTISING+25;
        }else if (DEVICE_WIDTH>=PHONEPLUS)
        {
            height_ads = HEIGHT_ADVERTISING+42;
        }else
        {
            height_ads = HEIGHT_ADVERTISING;
        }
    }
    
    [heightAdvertising setConstant:height_ads];
    
    TABLEVIEW_COLLECTION_REFRESH(_collectionView);
    [self setCollection];
}

- (void)refresh:(UIRefreshControl *)refreshControl {
    
    [self getService_fromCategoryDefualts];
    [refreshControl endRefreshing];
}

// Set collection view
-(void)setCollection{
    
    [self.collectionView setDataSource:self];
    [self.collectionView setDelegate:self];
    [self.collectionView setBackgroundColor:[UIColor clearColor]];
    [self.collectionView setShowsHorizontalScrollIndicator:NO];
    [self.collectionView setShowsVerticalScrollIndicator:NO];
    
    // For refreshing
    self.collectionView.alwaysBounceVertical = YES;
    
    // Register collectionView
    [self.collectionView registerClass:[PCCollectionCell class] forCellWithReuseIdentifier:@"Cell"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"PCCollectionCell" bundle:nil]  forCellWithReuseIdentifier:@"Cell"];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    //    NSLog(@"%lu",(unsigned long)[cName count]);
    return [cName count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    PCCollectionCell *cell=(PCCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    if(cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"PCCollectionCell" owner:self options:nil][0];
    }
    [cell setIconInCell:[NSString stringWithFormat:@"%@",cImage[indexPath.row]]];
    [cell setNameInCell:[NSString stringWithFormat:@"%@",cName[indexPath.row]]colorText:colorTextName[indexPath.row]];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake((DEVICE_WIDTH/3)-15,(DEVICE_WIDTH/3)-15);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    [self gotoMenuCollection:@"โปรโมชั่น" category_id:cID[indexPath.row]];
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    // TODO: Deselect item
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    return UIEdgeInsetsMake(10,10,10,10);
}

-(void)gotoMenuCollection:(NSString *)headerText category_id:(NSString *)couponID{
    
    MenuCollectionController *menuColleciton = [[NSBundle mainBundle] loadNibNamed:@"MenuCollectionController" owner:self options:nil][0];
    [menuColleciton setHeaderText:[NSString stringWithFormat:@"%@",headerText]categoryID:couponID];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController pushViewController:menuColleciton animated:YES];
    });
}

@end
