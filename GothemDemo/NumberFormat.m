//
//  PhoneFormat.m
//  testGothem
//
//  Created by pimpaporn chaichompoo on 2/17/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "NumberFormat.h"

@implementation NumberFormat

+(NSString *)format_phone:(NSString *)receivePhone{ // receive 0812345678 -> +66 1234 5678
    
    NSString *zero, *firstSection, *second, *third, *firstplit;
    
    NSMutableString *phoneString = [[NSMutableString alloc]init];
    NSArray *convertStr = [receivePhone componentsSeparatedByString:@"-"];
    
    if ([convertStr count]==1) {
        
        zero = [receivePhone substringWithRange:NSMakeRange(0,1)];
        firstSection = [receivePhone substringWithRange:NSMakeRange(1,2)];
        second = [receivePhone substringWithRange:NSMakeRange(3,3)];
        third = [receivePhone substringWithRange:NSMakeRange(6,4)]; // ตัวหลังคือ range
        
        if ([firstSection isEqualToString:@"66"]) {
            
            [phoneString appendString:[NSString stringWithFormat:@"%@",receivePhone]];
            
        }else{
            
            [phoneString appendString:[NSString stringWithFormat:@"+66 %@ ",firstSection]];
            [phoneString appendString:[NSString stringWithFormat:@"%@ ",second]];
            [phoneString appendString:[NSString stringWithFormat:@"%@",third]];
        }
        
    }else if([convertStr count]>1){
        
        firstplit=[convertStr objectAtIndex:0];
        firstSection = [firstplit substringWithRange:NSMakeRange(1,2)];
        //    NSString *first = [firstplit objectAtIndex:2];
        second=[convertStr objectAtIndex:1];
        third=[convertStr objectAtIndex:2];
        
        [phoneString appendString:[NSString stringWithFormat:@"+66 %@ ",firstSection]];
        [phoneString appendString:[NSString stringWithFormat:@"%@ ",second]];
        [phoneString appendString:[NSString stringWithFormat:@"%@",third]];
        
    }else{
        [phoneString appendString:[NSString stringWithFormat:@"%@",receivePhone]];
    }
    
    return phoneString;
}

+(NSString*)format_phone_number_by_lenge:(NSString *)number codeLength:(int) code segmentLength:(int) segment{
    
    int length = (int)[number length];
    
    NSString *result,*firstSegment,*secondSegment,*thirdSegment;
    
    for (int i=0; i<length; i++) {
        
        char c = [number characterAtIndex:i];
        
        if(i<code){
            firstSegment = [firstSegment stringByAppendingFormat:@"%c", c];
        }else if(i<=5){
            secondSegment = [secondSegment stringByAppendingFormat:@"%c", c];
        }else{
            thirdSegment = [thirdSegment stringByAppendingFormat:@"%c", c];
        }
    }
    result = [result stringByAppendingFormat:@"%@-%@-%@", firstSegment,secondSegment,thirdSegment];
    
    return result;
}

+(NSString*)format_number_cashout:(NSString *)number codeLength:(int) code segmentLength:(int) segment{
    
    int length = (int)[number length];
    
    NSString *result,*firstSegment,*secondSegment,*thirdSegment, *fourSegment;
    
    for (int i=0; i<length; i++) {
        
        char c = [number characterAtIndex:i];
        
        if(i<code){
            firstSegment = [firstSegment stringByAppendingFormat:@"%c", c];
        }else if(i<=3){
            secondSegment = [secondSegment stringByAppendingFormat:@"%c", c];
        }else if(i<=8){
            thirdSegment = [thirdSegment stringByAppendingFormat:@"%c", c];
        }else{
            fourSegment = [fourSegment stringByAppendingFormat:@"%c", c];
        }
    }
    result = [result stringByAppendingFormat:@"%@-%@-%@-%@", firstSegment,secondSegment,thirdSegment,fourSegment];
    return result;
}

+(NSString *)format_hour_minite:(NSString *)time{ // receive 12:34:56 -> 12:34
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"HH:mm:ss";
    NSDate *date = [dateFormatter dateFromString:time]; // Convert NSString to NSDate
    
    dateFormatter.dateFormat = @"HH:mm";
    NSString *pmamDateString = [dateFormatter stringFromDate:date];
    return [NSString stringWithFormat:@"%@ น.",pmamDateString];
}

+(NSString *)format_currency:(NSString *)currentcy{
    
    if (([currentcy isEqualToString:@"(null)"])||(currentcy==nil)) {
        return @"-";
    }else{
        
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
        NSString *numberAsString = [numberFormatter stringFromNumber:[NSNumber numberWithDouble:[currentcy doubleValue]]];
        
        return numberAsString;
    }
}

+(NSString *)find_range_of_time:(NSString *)time{ // Finding NSTimeInterval between two time
    
    if (time==nil) {
        return 0;
    }else{
        
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
        [df setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Bangkok"]];
        
        NSLocale *locale = [[NSLocale alloc]
                            initWithLocaleIdentifier:@"th-TH"];
        [df setLocale:locale];
        
        NSString *todayText = [df stringFromDate:[NSDate date]];
        NSDate *today = [df dateFromString:todayText];
        
        NSDate *date2 = [df dateFromString:time];
        NSTimeInterval interval = [date2 timeIntervalSinceDate:today];
        
        int hours = (int)interval / 3600; // integer division to get the hours part
        int minutes = (interval - (hours*3600)) / 60; // interval minus hours part (in seconds) divided by 60 yields minutes
        int seconds = (interval - (hours*3600) - (minutes*60)); // interval minus hours part (in seconds) divided by 60 yields minutes
        
        int day = hours / 24;
        
        if (day >= 3) {
            
            return [NSString stringWithFormat:@"เหลือเวลาอีก %d วัน",day];
        }else
        {
            if (hours<=0) {
                return @"หมดเวลาการใช้งาน";
            }else{
                
                NSString *timeDiff = [NSString stringWithFormat:@"%02d:%02d:%02d", hours, minutes,seconds];
                
                //    NSLog(@"today %@",today);
                //    NSLog(@"time %@",time);
                //    NSLog(@"timeDiff %@",timeDiff);
                
                return timeDiff;
            }
        }
    }
}

+(NSString *)find_range_of_time_service:(NSString *)time{
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    
    NSLocale *locale = [[NSLocale alloc]
                        initWithLocaleIdentifier:@"th-TH"];
    [df setLocale:locale];
    
    NSString *todayText = [df stringFromDate:[NSDate date]];
    
    NSDate *today = [df dateFromString:todayText];
    
    NSDate *date2 = [df dateFromString:time];
    NSTimeInterval interval = [today timeIntervalSinceDate:date2];
    
    int hours = (int)interval / 3600; // integer division to get the hours part
    int minutes = (interval - (hours*3600)) / 60; // interval minus hours part (in seconds) divided by 60 yields minutes
    //    int seconds = (interval - (hours*3600) - (minutes*60)); // interval minus hours part (in seconds) divided by 60 yields minutes
    
    //    NSString *timeDiff = [NSString stringWithFormat:@"%02d:%02d:%02d", hours, minutes,seconds];
    
    if ((minutes>=30)||(hours>0)) {
        
        return @"update";
        
    }else{
        
        return @"not update";
    }
}

+(NSString *)sort_time_dd_mm_yyyy:(NSString *)receive_date{ // ex. -> 2016/03/11
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yy"];
    NSDate *date = [dateFormatter dateFromString:receive_date];
    dateFormatter.dateFormat = @"yy/MM/dd";
    NSString *pmamDateString = [dateFormatter stringFromDate:date];
    
    NSArray *date_array = [pmamDateString componentsSeparatedByString:@"/"];
    NSString *year_str = [NSString stringWithFormat:@"25%@",[date_array objectAtIndex:0]];
    NSString *month_str = [date_array objectAtIndex:1];
    NSString *date_str = [date_array objectAtIndex:2];
    
    int year_int = [year_str intValue]-543;
    
    return [NSString stringWithFormat:@"%d/%@/%@",year_int,month_str,date_str];
}

@end
