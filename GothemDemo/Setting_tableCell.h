
//  Created by pimpaporn chaichompoo on 2/10/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

#import <UIKit/UIKit.h>

@interface Setting_tableCell : UITableViewCell{
    
    CGFloat myLineSpacing;
    BOOL tab;
    
    // setting defaults
    id lockInfo;    
    NSUserDefaults *lockDefaults;
}

@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *detail;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTitle;

/// profile
@property (weak, nonatomic) IBOutlet UILabel *info;
@property (weak, nonatomic) IBOutlet UIImageView *imagee;

@property (weak, nonatomic) NSArray *ActionBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthSelected;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightSelected;

-(void)setFeedUpStyle:(NSString *)title;
-(void)setNotificationStyle:(NSString *)title index:(int)indexMenu;
-(void)setNotificationStyleDetail:(NSString *)title;
-(void)setProfileStyle:(NSString *)title info:(NSString *)info;
-(void)setLogoutCell:(NSString *)title image:(NSString *)image;

-(void)setAboutUsCell:(NSString *)title info:(NSString *)info;
-(void)setQA:(NSString *)title;

- (void)selectedNotiSEttingCell:(int)index;

@end
