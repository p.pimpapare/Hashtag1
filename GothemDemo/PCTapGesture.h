//
//  PCTapGestureRecognizer.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 4/11/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

#import <UIKit/UIKit.h>

@interface PCTapGesture : UITapGestureRecognizer

+(void)set_target_button:(UIButton * _Nonnull)button type:(int)type;

+(void)new_user_tap_on_link:(id _Nonnull)sender;
+(void)existing_user_tap_on_link:(id _Nonnull)sender;
+(void)user_tap_on_link_forgot_password:(id _Nonnull)sender;

+(void)tapDismissKeyBoard:(UIViewController *_Nonnull)classController view:(UIView * _Nonnull)view;
+(void)tapAction:(UIViewController * _Nonnull)classController view:(id _Nonnull)view actionTarget:(SEL _Nonnull)target;

@end
