//
//  PhoneFormat.h
//  testGothem
//
//  Created by pimpaporn chaichompoo on 2/17/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

// This class is used for converting time string from service to v format
#import <Foundation/Foundation.h>

@interface NumberFormat : NSObject

+(NSString *)format_hour_minite:(NSString *)time;
+(NSString *)format_currency:(NSString *)currentcy;
+(NSString *)find_range_of_time:(NSString *)time;
+(NSString *)sort_time_dd_mm_yyyy:(NSString *)date;

+(NSString *)find_range_of_time_service:(NSString *)time;

// Unused
+(NSString *)format_phone:(NSString *)receivePhone;
+(NSString *)format_phone_number_by_lenge:(NSString *)number codeLength:(int) code segmentLength:(int) segment;
+(NSString *)format_number_cashout:(NSString *)number codeLength:(int) code segmentLength:(int) segment;


@end
