
//  Created by pimpaporn chaichompoo on 2/1/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

// This class is used for setting coupon's collection view cell that is used in menu 2.

#import <UIKit/UIKit.h>

@interface PCCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageCell;
@property (weak, nonatomic) IBOutlet UIImageView *iconCell;
@property (weak, nonatomic) IBOutlet UILabel *nameCell;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *icon_height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *icon_width;

@property BOOL tap;

-(void)setImagCell:(NSString *)image;
-(void)setNameInCell:(NSString *)name colorText:(NSString *)color;
-(void)setIconInCell:(NSString *)icon;

@end
