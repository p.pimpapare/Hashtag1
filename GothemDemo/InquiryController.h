//
//  InquiryController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/7/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

#import <UIKit/UIKit.h>

typedef void (^ALAssetsLibraryWriteImageCompletionBlock)(NSURL *assetURL, NSError *error);

@interface InquiryController : BaseViewController{
    
    UIDatePicker *datePicker;
    UIDatePicker *timePicker;
    
    NSArray *titlePro_array;
    NSArray *typeQA_array;
    
    UIPickerView *type_pickker;
    UIPickerView *titleProblem_pickker;
    
    //
    id inquiryData;
    id subtypes;
    int indexSubtype;
    
    NSMutableArray *subtype_name;
    NSMutableArray *subtype_feilds;
    
    NSMutableArray *textfeild_array;
    
    NSMutableArray *field_id;
    NSString *subtype_id;

    BOOL selectedImage;
    //
    NSMutableArray *inquiry_ID;
    NSMutableArray *titleProblem;
    NSMutableArray *subtypeID1;
    NSMutableArray *subtypeID2;
    NSMutableArray *subtypeID3;
    NSMutableArray *subtypeID4;
    
    NSMutableArray *problemName1;
    NSMutableArray *problemName2;
    NSMutableArray *problemName3;
    NSMutableArray *problemName4;
    
    NSMutableArray *sub1Problem2;
    NSMutableArray *sub2Problem2;
    NSMutableArray *sub3Problem2;

    NSMutableArray *sub1Problem3;
    NSMutableArray *sub2Problem3;
    NSMutableArray *sub3Problem3;
    
    int selected;
    int selectedTopic;
    NSString *userNum;
    
    int index;
    int indexSubrow;
    int selectedIndex;

    BOOL select_title;
    
    NSString *problem_id;
    NSString *imageURL;
    UIImage *image;
}

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UILabel *email;
@property (weak, nonatomic) IBOutlet UILabel *userNumber;
@property (weak, nonatomic) IBOutlet UILabel *emailTitle;
@property (weak, nonatomic) IBOutlet UILabel *t1cTitle;

@property (weak, nonatomic) IBOutlet UILabel *emailThe1Card;
@property (weak, nonatomic) IBOutlet UILabel *telThe1Card;

@property (weak, nonatomic) IBOutlet UITextField *dateTxtfield;
@property (weak, nonatomic) IBOutlet UITextField *timeTxtfield;

@property (weak, nonatomic) IBOutlet UITextField *qTypeTxtfield;
@property (weak, nonatomic) IBOutlet UITextField *titleTxtfield;
@property (weak, nonatomic) IBOutlet UITextView *detailTextview;

//@property (weak, nonatomic) IBOutlet UITextField *subTextfeild1;
//@property (weak, nonatomic) IBOutlet UITextField *subTextfeild2;
//@property (weak, nonatomic) IBOutlet UITextField *subtextfeild3;

@property (weak, nonatomic) IBOutlet UIButton *cancleUpload;

@property (weak, nonatomic) IBOutlet UIButton *uploadBtn;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;

@property (weak, nonatomic) IBOutlet UIImageView *dropdownQtype;

@property (weak, nonatomic) IBOutlet UILabel *device;
@property (weak, nonatomic) IBOutlet UILabel *software;
@property (weak, nonatomic) IBOutlet UILabel *appVersion;
@property (weak, nonatomic) IBOutlet UILabel *image_name;
@property (weak, nonatomic) IBOutlet UILabel *topicInfo;

@property (weak, nonatomic) IBOutlet UIView *warningView;
@property (weak, nonatomic) IBOutlet UIImageView *warningIcon;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightDetail;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *hightBaseView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightT1cTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightT1c;

@end
