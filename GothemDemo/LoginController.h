//
//  LoginController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/21/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

// This class is Login class that contains with two buttons, (login with ID and login with email)

#import <UIKit/UIKit.h>

@interface LoginController : BaseViewController

@property (weak, nonatomic) IBOutlet UIView *statusBar;
@property (weak, nonatomic) IBOutlet UIImageView *logo;

@property (weak, nonatomic) IBOutlet UIButton *mobilBtn;
@property (weak, nonatomic) IBOutlet UIView *mobileView;
@property (weak, nonatomic) IBOutlet UILabel *mobileLabel;

@property (weak, nonatomic) IBOutlet UIButton *emailBtn;
@property (weak, nonatomic) IBOutlet UIView *emailView;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;

@property (weak, nonatomic) IBOutlet UILabel *registerText;
@property (weak, nonatomic) IBOutlet UIButton *registerOnWebBtn;
@property (weak, nonatomic) IBOutlet UIButton *registerOnWebBtn2;
@property (weak, nonatomic) IBOutlet UIButton *registerOnwebBtn3;

@property (weak, nonatomic) IBOutlet UILabel *telLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailTxtLabel;

// NSLayoutConstraint
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightLogo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthLogo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightEmail;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightMobile;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topLogo;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomregister;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomregister2;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomregister3;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomregister4;

@end
