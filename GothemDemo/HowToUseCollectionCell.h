//
//  HowToUseCollectionCell.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/4/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HowToUseCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageCell;
@property int menuNum;

@property NSString *tagMenu;
-(NSString *)action:(NSString *) selecttag;

-(void)setHowtoImage:(NSString *)imageC index:(int)indexImage;

@end
