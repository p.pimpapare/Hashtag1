//
//  ReadMoreUpdateController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/4/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "ReadMoreUpdateController.h"

@interface ReadMoreUpdateController ()
@end

@implementation ReadMoreUpdateController

-(void)viewWillAppear:(BOOL)animated{
    
    GOOGLE_ANALYTICS(@"ReadMoreUpdateController");
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self.loadingView setHidden:NO];
    [self.loadingView startAnimating];
    
    [self class_UI];
}

-(void)setDetail_ReadMore:(NSString *)version detail:(NSString *)detail{
    
    [self.loadingView setHidden:YES];
    [self.loadingView stopAnimating];
    
    [self.versionText setText:[NSString stringWithFormat:@"เวอร์ชั่น %@",version]];
    [self.detailText setText:[NSString stringWithFormat:@"%@",detail]];
}

-(void)class_UI{
    
    //set status, header
    [self.statusBar setBackgroundColor:[PCColor color_424242]];
    [PCColor set_whiteColor_headerBar:self.viewMenu];
    
    [PCShadow set_shadow:-5.0 shadowRadius:5.0 shadowOpacity:0.3 view:self.viewShadow];
    [self.detailText setFont:[UIFont fontWithName:@"RSU" size:16]];
}

- (IBAction)backBtn:(id)sender {
    GOTO_PREVIOUS
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)notification_alert:(NSNotification *)noti{
    GOTO_PREVIOUS
}

-(void)goToPrevious{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
