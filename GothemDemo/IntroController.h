//
//  introController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 4/4/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntroController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>{
    
    UICollectionView *_collectionView;
    NSArray *image_array;
    BOOL firstTimes;
}

@property (weak, nonatomic) IBOutlet UIView *viewIntro;

-(void)fromFirstTimesApplicaitonLauch:(BOOL)firstimes;

@end
