//
//  Service.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/25/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "Service.h"

@implementation Service

+(void)header:(DLDownload *)download { // Set service header that using for request service.
    
    GET_USER_DEFAULT_INFO(@"UserInfo")
    
    NSString *token = [NSString stringWithFormat:@"%@",[self check_null_value:userInfo[@"token"]]];

    // device_version, device_mac, device_osversion
    NSArray *device_info_array = @[[NSString stringWithFormat:@"%@",[self check_null_value:[BaseViewController getDeviceInfo][@"version"]]],[NSString stringWithFormat:@"%@",[self check_null_value:[BaseViewController getDeviceInfo][@"mac"]]],[NSString stringWithFormat:@"%@",[self check_null_value:[BaseViewController getDeviceInfo][@"os-version"]]]];

    [download setHTTPHeaderFields:@{@"version":[device_info_array objectAtIndex:0],@"mac":[device_info_array objectAtIndex:1],@"os-type":@"ios",@"os-version":[device_info_array objectAtIndex:2],@"Authorization":token}];
    download.timeout = 100;
}

+(NSString *)check_null_value:(id)text
{
    if (text == nil) {
        return @" ";
    }else{
        return text;
    }
}

+(void)post_user_login_with_ID:(NSString *)userId deviceToken:(NSString *)deviceToken ref_code:(NSString *)ref_code selfID:(UIViewController*)selfID block:(BlockUserInfo)result{
    
    userId = [NSString stringWithFormat:@"%@",[self check_null_value:userId]];
    
    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@user/login/id",BASEURL]];
    download.timeout = 100;
    
    [download setMethod:DLDownloadMethodPOST];
    
    // device_version, device_mac, device_osversion
    NSArray *device_info_array = @[[NSString stringWithFormat:@"%@",[self check_null_value:[BaseViewController getDeviceInfo][@"version"]]],[NSString stringWithFormat:@"%@",[self check_null_value:[BaseViewController getDeviceInfo][@"mac"]]],[NSString stringWithFormat:@"%@",[self check_null_value:[BaseViewController getDeviceInfo][@"os-version"]]]];
    
    [download setHTTPHeaderFields:@{@"version":[device_info_array objectAtIndex:0],@"mac":[device_info_array objectAtIndex:1],@"os-type":@"ios",@"os-version":[device_info_array objectAtIndex:2]}];
    [download setParameters:@{@"id":userId,@"device_token":deviceToken,@"refcode":ref_code}];
    
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            
            NSLog(@"%@",error.localizedDescription);
            result(@"error");
            
        } else {
            
            NSMutableDictionary *dicData = [NSMutableDictionary dictionaryWithDictionary:(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
            
            if (dicData==nil) {
            }
            result(dicData);
        }
    };
    [download start];
}

+(void)post_user_login_with_Email:(NSString *)email password:(NSString *)password ref_code:(NSString *)ref_code deviceToken:(NSString *)deviceToken selfID:(UIViewController*)selfID block:(BlockUserInfo)result{
    
    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@user/login/email",BASEURL]];
    download.timeout = 100;
    
    [download setMethod:DLDownloadMethodPOST];
    
    // device_version, device_mac, device_osversion
    NSArray *device_info_array = @[[NSString stringWithFormat:@"%@",[self check_null_value:[BaseViewController getDeviceInfo][@"version"]]],[NSString stringWithFormat:@"%@",[self check_null_value:[BaseViewController getDeviceInfo][@"mac"]]],[NSString stringWithFormat:@"%@",[self check_null_value:[BaseViewController getDeviceInfo][@"os-version"]]]];
    
    [download setHTTPHeaderFields:@{@"version":[device_info_array objectAtIndex:0],@"mac":[device_info_array objectAtIndex:1],@"os-type":@"ios",@"os-version":[device_info_array objectAtIndex:2]}];
    [download setParameters:@{@"email":email,@"password":password,@"device_token":deviceToken,@"refcode":ref_code}];
    
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            result(@"error");
        } else {
            NSMutableDictionary *dicData = [NSMutableDictionary dictionaryWithDictionary:(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
            result(dicData);
        }
    };
    [download start];
}

+(void)post_user_acception:(NSString *)token selfID:(UIViewController*)selfID block:(BlockUserInfo)result{
    
    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@user/acceptagreement",BASEURL]];
    
    [download setMethod:DLDownloadMethodGET];
    [self header:download];
    
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            result(@"error");
        } else {
            NSMutableDictionary *dicData = [NSMutableDictionary dictionaryWithDictionary:(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
            
            result(dicData);
        }
    };
    [download start];
}

+(void)get_app_url_invite:(NSString *)token selfID:(UIViewController*)selfID block:(BlockProduct)result{
    
    [self check_null_value:token];

    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@invite/url",BASEURL]];
    
    [download setMethod:DLDownloadMethodGET];
    [self header:download];
    
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            result(@"error");
        } else {
            NSMutableDictionary *dicData = [NSMutableDictionary dictionaryWithDictionary:(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
            //            NSLog(@"%@",dicData);
            result(dicData);
        }
    };
    [download start];
}

+(void)get_user_info:(NSString *)token selfID:(UIViewController*)selfID block:(BlockUserInfo)result{
    
    [self check_null_value:token];
    
    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@user/info",BASEURL]];
    
    [download setMethod:DLDownloadMethodGET];
    [self header:download];
    
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            result(@"error");
        } else {
            NSMutableDictionary *dicData = [NSMutableDictionary dictionaryWithDictionary:(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
            
            result(dicData);
        }
    };
    [download start];
}

+(void)post_user_logout:(NSString *)token selfID:(UIViewController*)selfID block:(BlockUserInfo)result{
    
    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@user/logout",BASEURL]];
    
    [download setMethod:DLDownloadMethodPOST];
    [self header:download];
    
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            result(@"error");
        } else {
            NSMutableDictionary *dicData = [NSMutableDictionary dictionaryWithDictionary:(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
            result(dicData);
        }
    };
    [download start];
}

+(void)post_package_response:(NSString *)token package:(NSString *)packageID direction:(NSString *)direction before_packageID:(NSString *)beforePackageID viewTime:(NSString *)view_time selfID:(UIViewController*)selfID block:(BlockUserInfo)result{
    
    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@user/package/response?package_id=%@&direction=%@&before_package_id=%@&request_time=%@",BASEURL,packageID,direction,beforePackageID,view_time]];
    
    [download setMethod:DLDownloadMethodPOST];
    [self header:download];
    
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            result(@"error");
        } else {
            NSMutableDictionary *dicData = [NSMutableDictionary dictionaryWithDictionary:(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
            
            result(dicData);
        }
    };
    [download start];
}

+(void)post_unlock:(NSString *)token responseToken:(NSString *)response_token selfID:(UIViewController*)selfID block:(BlockProduct)result{
    
    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@user/unlock?response_token=%@",BASEURL,response_token]];
    
    [download setMethod:DLDownloadMethodPOST];
    [self header:download];
    
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            result(@"error");
        } else {
            NSMutableDictionary *dicData = [NSMutableDictionary dictionaryWithDictionary:(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
            
            result(dicData);
        }
    };
    [download start];
}

+(void)post_collect_point:(NSString *)token responseToken:(NSString *)response_token unlock_type:(NSString *)unlock_type selfID:(UIViewController*)selfID block:(BlockProduct)result{
    
    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@user/collectpoint?response_token=%@&unlock_type=%@",BASEURL,response_token,unlock_type]];
    
    [download setMethod:DLDownloadMethodPOST];
    [self header:download];
    
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            result(@"error");
            NSLog(@"%@",error.localizedDescription);

        } else {
            NSMutableDictionary *dicData = [NSMutableDictionary dictionaryWithDictionary:(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
            result(dicData);
        }
    };
    [download start];
}

+(void)get_coupon_status:(NSString *)token couponID:(NSString *)couponID selfID:(UIViewController*)selfID block:(BlockProduct)result{
    
    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@coupon/status?coupon_id=%@",BASEURL,couponID]];
    
    [download setMethod:DLDownloadMethodGET];
    [self header:download];
    
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            result(@"error");
        } else {
            NSMutableDictionary *dicData = [NSMutableDictionary dictionaryWithDictionary:(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
            
            result(dicData);
        }
    };
    [download start];
}

+(void)get_user_use_point_history:(NSString *)token offset:(int)offset type:(int)type selfID:(UIViewController*)selfID block:(BlockProduct)result{
    
    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@user/history?offset=%d&type=%d",BASEURL,offset,type]];
    
    [download setMethod:DLDownloadMethodGET];
    [self header:download];
    
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            result(@"error");
        } else {
            NSMutableDictionary *dicData = [NSMutableDictionary dictionaryWithDictionary:(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
            
            result(dicData);
        }
    };
    [download start];
}

+(void)get_coupon_from_CID:(NSString *)token cID:(int)c_ID selfID:(UIViewController*)selfID block:(BlockProduct)result{
    
    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@coupon/%d",BASEURL,c_ID]];
    
    [download setMethod:DLDownloadMethodGET];
    [self header:download];
    
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            result(@"error");
        } else {
            NSMutableDictionary *dicData = [NSMutableDictionary dictionaryWithDictionary:(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
            
            result(dicData);
        }
    };
    [download start];
}

+(void)get_category:(NSString *)token selfID:(UIViewController*)selfID block:(BlockProduct)result{
    
    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@category",BASEURL]];
    
    [download setMethod:DLDownloadMethodGET];
    [self header:download];
    
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            
            if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."]) {
                result(@"error");
            }
        } else {
            
            NSMutableDictionary *dicData = [NSMutableDictionary dictionaryWithDictionary:(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
            
            result(dicData);
        }
    };
    [download start];
}

+(void)get_hot_promotion:(NSString *)token selfID:(UIViewController*)selfID block:(BlockProduct)result{
    
    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@hotpromo",BASEURL]];
    
    [download setMethod:DLDownloadMethodGET];
    [self header:download];
    
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            result(@"error");
        } else {
            
            NSMutableDictionary *dicData = [NSMutableDictionary dictionaryWithDictionary:(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
            
            result(dicData);
        }
    };
    [download start];
}

+(void)get_my_coupon:(NSString *)token selfID:(UIViewController*)selfID block:(BlockProduct)result{
    
    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@user/mycoupon",BASEURL]];
    
    [download setMethod:DLDownloadMethodGET];
    [self header:download];
    
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            result(@"error");
        } else {
            NSMutableDictionary *dicData = [NSMutableDictionary dictionaryWithDictionary:(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
            
            result(dicData);
        }
    };
    [download start];
}

+(void)get_news_feed:(NSString *)token typeNews:(NSString *)newFeed selfID:(UIViewController*)selfID block:(BlockProduct)result{
    
    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@feed2?item_type=%@",BASEURL,newFeed]];
    
    [download setMethod:DLDownloadMethodGET];
    [self header:download];
    
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            result(@"error");
        } else {
            NSMutableDictionary *dicData = [NSMutableDictionary dictionaryWithDictionary:(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
            
            result(dicData);
        }
    };
    [download start];
}

+(void)get_category_brand_grid:(NSString *)token cID:(NSString *)category_ID selfID:(UIViewController*)selfID block:(BlockProduct)result{
    
    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@category/getBrandGrid?category_id=%@",BASEURL,category_ID]];
    
    [download setMethod:DLDownloadMethodGET];
    [self header:download];
    
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            result(@"error");
        } else {
            NSMutableDictionary *dicData = [NSMutableDictionary dictionaryWithDictionary:(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
            
            result(dicData);
        }
    };
    [download start];
}

+(void)get_coupon_from_brand_grid:(NSString *)token bID:(NSString *)brand_ID selfID:(UIViewController*)selfID block:(BlockProduct)result{
    
    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@coupon?brand_id=%@",BASEURL,brand_ID]];
    
    [download setMethod:DLDownloadMethodGET];
    [self header:download];
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            result(@"error");
        } else {
            NSMutableDictionary *dicData = [NSMutableDictionary dictionaryWithDictionary:(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
            
            result(dicData);
        }
    };
    [download start];
}

+(void)post_inquiry:(NSString *)token problemDateTime:(NSString *)date_time problemSubtype:(NSString *)subtype problemDetails:(NSString *)details feilds:(NSMutableString *)feilds deviceManufacturer:(NSString *)device_menufaceturer deviceName:(NSString *)device_name imageFile:(NSData *)image_file selfID:(UIViewController*)selfID block:(BlockProduct)result{
    
    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@inquiry",BASEURL]];
    
    [download setMethod:DLDownloadMethodPOST];
    [self header:download];
    
    [download setParameters:@{@"problem_datetime":date_time,@"problem_subtype":subtype,@"problem_detail":details,@"fields":feilds,@"device_manufacturer":device_menufaceturer,@"device_name":device_name}];
    
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            result(@"error");
        } else {
            NSMutableDictionary *dicData = [NSMutableDictionary dictionaryWithDictionary:(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
            
            result(dicData);
        }
    };
    [download start];
}

+(void)get_user_ingury_history:(NSString *)token offset:(int)offset selfID:(UIViewController*)selfID block:(BlockProduct)result{
    
    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@inquiry?offset=%d",BASEURL,offset]];
    
    [download setMethod:DLDownloadMethodGET];
    [self header:download];
    
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            result(@"error");
        } else {
            NSMutableDictionary *dicData = [NSMutableDictionary dictionaryWithDictionary:(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
            
            result(dicData);
        }
    };
    [download start];
}

+(void)get_user_ingury_history_detail:(NSString *)token id:(int)problem_id selfID:(UIViewController*)selfID block:(BlockProduct)result{
    
    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@inquiry/detail/%d",BASEURL,problem_id]];
    
    [download setMethod:DLDownloadMethodGET];
    [self header:download];
    
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            result(@"error");
        } else {
            NSMutableDictionary *dicData = [NSMutableDictionary dictionaryWithDictionary:(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
            
            result(dicData);
        }
    };
    [download start];
}

+(void)post_user_use_my_coupon:(NSString *)token myCouponID:(NSString *)couponID selfID:(UIViewController*)selfID block:(BlockProduct)result{
    
    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@user/usemycoupon?mycoupon_id=%@",BASEURL,couponID]];
    
    [download setMethod:DLDownloadMethodPOST];
    [self header:download];
    
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            result(@"error");
        } else {
            NSMutableDictionary *dicData = [NSMutableDictionary dictionaryWithDictionary:(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
            
            result(dicData);
        }
    };
    [download start];
}

+(void)get_coupon_detail:(NSString *)token couponID:(NSString *)couponID selfID:(UIViewController*)selfID block:(BlockProduct)result{
    
    DLDownload *download = [[DLDownload alloc] init];
    
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@coupon/detail/%@",BASEURL,couponID]];
    
    [download setMethod:DLDownloadMethodGET];
    [self header:download];
    
    download.callback =  ^(NSData *data, NSError *error) {
    
        if(error) {
            result(@"error");
            NSLog(@"%@",error.localizedDescription);
            
        } else {
            
            // get HTML
            NSString *stringHtml =[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            result(stringHtml);
        }
    };
    [download start];
}

+(void)get_usage_agreement:(UIViewController*)selfID block:(BlockProduct)result{
    
    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@document/usage_agreement",BASEURL]];
    
    [download setMethod:DLDownloadMethodGET];
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            result(@"error");
        } else {
            NSString *stringHtml =[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            result(stringHtml);
        }
    };
    [download start];
}

+(void)get_usage_privacy_policy:(UIViewController*)selfID block:(BlockProduct)result{
    
    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@document/privacy_policy",BASEURL]];
    
    [download setMethod:DLDownloadMethodGET];
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            result(@"error");
        } else {
            NSString *stringHtml =[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            result(stringHtml);
        }
    };
    [download start];
    
}

+(void)get_ingury_type:(NSString *)token selfID:(UIViewController*)selfID block:(BlockProduct)result{
    
    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@inquiry/type",BASEURL]];
    
    [download setMethod:DLDownloadMethodGET];
    [self header:download];
    
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            result(@"error");
        } else {
            NSMutableDictionary *dicData = [NSMutableDictionary dictionaryWithDictionary:(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
            result(dicData);
        }
    };
    [download start];
}

+(void)get_lucky_point_history:(UIViewController *)selfID block:(BlockProduct)result{
    
    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@user/historyluckydraw",BASEURL]];
    
    [download setMethod:DLDownloadMethodGET];
    [self header:download];
    
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            result(@"error");
        } else {
            NSMutableDictionary *dicData = [NSMutableDictionary dictionaryWithDictionary:(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
            result(dicData);
        }
    };
    [download start];
}

+(void)get_lucky_point_html:(UIViewController *)selfID block:(BlockProduct)result{
    
    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@document/luckydraw",BASEURL]];
    
    [download setMethod:DLDownloadMethodGET];
    [self header:download];
    
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            result(@"error");
        } else {
            NSString *stringHtml =[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            result(stringHtml);
        }
    };
    [download start];
}


+(void)getShareUrl:(NSString *)token couponID:(NSString *)couponID selfID:(UIViewController*)selfID block:(BlockProduct)result{
    
    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@coupon/shareurl?coupon_id=%@",BASEURL,couponID]];
    
    [download setMethod:DLDownloadMethodGET];
    [self header:download];
    
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            result(@"error");
            NSLog(@"%@",error.localizedDescription);

        } else {
            NSMutableDictionary *dicData = [NSMutableDictionary dictionaryWithDictionary:(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
            
            result(dicData);
        }
    };
    [download start];
}

// get user history get point
+(void)get_user_get_point_history:(NSString *)token selfID:(UIViewController*)selfID block:(BlockProduct)result{
    
    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@user/historygetpoint",BASEURL]];
    
    [download setMethod:DLDownloadMethodGET];
    [self header:download];
    
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            result(@"error");
        } else {
            NSMutableDictionary *dicData = [NSMutableDictionary dictionaryWithDictionary:(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
            
            result(dicData);
        }
    };
    [download start];
}

// get advertising
+(void)get_banner:(NSString *)token brannerId:(int)branID selfID:(UIViewController*)selfID block:(BlockProduct)result{
    
    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@banner/%d",BASEURL,branID]];
    
    [download setMethod:DLDownloadMethodGET];
    [self header:download];
    
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            result(@"error");
        } else {
            NSMutableDictionary *dicData = [NSMutableDictionary dictionaryWithDictionary:(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
            result(dicData);
        }
    };
    [download start];
}

// get user use coupon
+(void)get_user_use_coupon:(NSString *)token email:(NSString *)email password:(NSString *)password couponID:(NSString *)couponID use:(int)use selfID:(UIViewController*)selfID block:(BlockProduct)result{
    
    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@user/usecoupon?coupon_id=%@&email=%@&password=%@&use=%d",BASEURL,couponID,email,password,use]];
    
    [download setMethod:DLDownloadMethodPOST];
    [self header:download];
    
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            result(@"error");
        } else {
            NSMutableDictionary *dicData = [NSMutableDictionary dictionaryWithDictionary:(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
            result(dicData);
        }
    };
    [download start];
}

// get force update
+(void)get_update_version:(NSString *)token selfID:(UIViewController*)selfID block:(BlockProduct)result{
    
    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@forceupdate",BASEURL]];
    
    [download setMethod:DLDownloadMethodGET];
    [download setParameters:@{@"version_code":version_forceupdate}];
    
    [self header:download];
    
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            result(@"error");
        } else {
            NSMutableDictionary *dicData = [NSMutableDictionary dictionaryWithDictionary:(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
            
            result(dicData);
        }
    };
    [download start];
}

// get advertising
+(void)get_ads_info:(NSString *)token block:(BlockProduct)result{
    
    [self check_null_value:token];
    
    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@update",BASEURL]];
    
    [download setMethod:DLDownloadMethodGET];
    
    // device_version, device_mac, device_osversion
    NSArray *device_info_array = @[[NSString stringWithFormat:@"%@",[self check_null_value:[BaseViewController getDeviceInfo][@"version"]]],[NSString stringWithFormat:@"%@",[self check_null_value:[BaseViewController getDeviceInfo][@"mac"]]],[NSString stringWithFormat:@"%@",[self check_null_value:[BaseViewController getDeviceInfo][@"os-version"]]]];
    
    [download setHTTPHeaderFields:@{@"version":[device_info_array objectAtIndex:0],@"mac":[device_info_array objectAtIndex:1],@"os-type":@"ios",@"os-version":[device_info_array objectAtIndex:2],@"Authorization":token,@"version_code":version_forceupdate}];
    
    [self header:download];
    
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            result(@"error");
        } else {
            NSMutableDictionary *dicData = [NSMutableDictionary dictionaryWithDictionary:(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
            
            result(dicData);
        }
    };
    [download start];
}

+(void)get_ads_update:(NSString *)token date_time:(NSString *)date_time block:(BlockProduct)result{
    
    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@user/probe?lastupdate",BASEURL]];
    
    [download setParameters:@{@"lastupdate":date_time}];
    
    [download setMethod:DLDownloadMethodGET];
    [self header:download];
    
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            result(@"error");
        } else {
            NSMutableDictionary *dicData = [NSMutableDictionary dictionaryWithDictionary:(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
            
            result(dicData);
        }
    };
    [download start];
}

+(void)getIntroductionInfo:(NSString *)token block:(BlockProduct)result{
    
    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@introduction",BASEURL]];
    
    [download setMethod:DLDownloadMethodGET];
    [self header:download];
    
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            result(@"error");
        } else {
            NSMutableDictionary *dicData = [NSMutableDictionary dictionaryWithDictionary:(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
            
            result(dicData);
        }
    };
    [download start];
}

+(void)get_QA:(NSString *)token selfID:(UIViewController*)selfID block:(BlockProduct)result{
    
    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@faq",BASEURL]];
    
    [download setMethod:DLDownloadMethodGET];
    [self header:download];
    
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            result(@"error");
        } else {
            NSMutableDictionary *dicData = [NSMutableDictionary dictionaryWithDictionary:(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
            
            result(dicData);
        }
    };
    [download start];
}

@end
