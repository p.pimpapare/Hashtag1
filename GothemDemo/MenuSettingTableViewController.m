//
//  MenuSettingTableViewController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/20/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "MenuSettingTableViewController.h"

@interface MenuSettingTableViewController (){
    SubMenuSettingController *subMenuSetting;
}
@end

@implementation MenuSettingTableViewController

-(void)viewWillAppear:(BOOL)animated{
    
    GOOGLE_ANALYTICS(@"MenuSettingTableViewController");
    
    GET_USER_DEFAULT_INFO(@"UserEmail")
    
    if (userInfo == nil) {
        user_email = nil;
    }else{
        user_email = [NSString stringWithFormat:@"%@",userInfo];
    }
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self.loadingView startAnimating];
    [self.loadingView setHidden:NO];
    
    [self get_version_update];
    [self setUserProfile];
}

-(void)get_version_update{
    
    GET_USER_DEFAULT_INFO(@"UpdateVersionInfo")
    
    NSString *current_version = [NSString stringWithFormat:@"%@",userInfo[@"current"]];
    
    if([current_version length]!=0){
        
        updateVersion = [NSString stringWithFormat:@"%@",userInfo[@"current"]];
        updateDetail = [NSString stringWithFormat:@"%@",userInfo[@"detail"]];
        
    }else{
        
        updateVersion = [NSString stringWithFormat:@"%@",[BaseViewController getCurrentVersion]];
        updateDetail = @" ";
    }
}

-(void)get_question_answer{
    
    // get question & answer array
    questiontxt = [[NSMutableArray alloc]init];
    answertxt = [[NSMutableArray alloc]init];
    
    [Service get_QA:[self getUserToken] selfID:self block:^(id result) {
        
        NSString *resultText = [NSString stringWithFormat:@"%@",result];
        
        if (![resultText isEqualToString:@"error"]) {
            
            if ([result[@"faqs"] count]!=0) {
                
                for (int i=0;i<[result[@"faqs"] count];i++) {
                    [questiontxt addObject:result[@"faqs"][i][@"question"]];
                    [answertxt addObject:result[@"faqs"][i][@"answer"]];
                }
            }
        }
        [self.loadingView setHidden:YES];
        [self.loadingView stopAnimating];
        
        [self.tableView reloadData];
    }];
}

-(void)setUserProfile{
    
    userToken = [NSString stringWithFormat:@"%@",[self getUserToken]];
    [self set_initail_objects];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    int selectedRow = (int)indexPath.row;
    
    Setting_tableCell *cell = (Setting_tableCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    switch (menuNum) {
            
        case 2:
            
            if (!user_email) {
                
                if (selectedRow==2) {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:URL_CHANGEPASSWORD]];
                }
                if (selectedRow==3) {
                    [self displayAlertLogout];
                }
                
            }else{
                
                if (selectedRow==3) {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:URL_CHANGEPASSWORD]];
                }else if(selectedRow==4){
                    [self displayAlertLogout];
                }
            }
            break;
            
        case 3:
            
            [cell selectedNotiSEttingCell:(int)indexPath.row];
            break;
            
        case 4:
            
            if (selectedRow==2) {
                [self goToSubSettingController:aboutUsDetail[indexPath.row]detail:@" " QAType:YES index:@"3"];
            }else if (selectedRow==3){
                [self goToSubSettingController:aboutUsDetail[indexPath.row]detail:@" " QAType:YES index:@"2"];
            }
            break;
            
        case 8:
            
            [self gotoQA:questiontxt[indexPath.row] anwser:answertxt[indexPath.row]];
            
        default:
            break;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    switch (menuNum) {
            
        case 2:
            if (!user_email) {
                return [profile_array_t1c count];
            }else{
                return [profile_array_email count];
            }break;
        case 3:
            return [noti_array count];
            break;
        case 4:
            return [aboutUsDetail count];
            break;
        case 8:
            return [questiontxt count];
            break;
        default:
            [self.viewBlankInfo setHidden:NO];
            return 0;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Setting_tableCell *cell2 = (Setting_tableCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if(cell2==nil){
        cell2 = [[NSBundle mainBundle] loadNibNamed:@"Setting_tableCell" owner:self options:nil][0];
    }
    
    switch (menuNum) {
            
        case 2:
            
            if (!user_email) {
                
                switch (indexPath.row) {
                    case 0:
                        [cell2 setProfileStyle:profile_array_t1c[indexPath.row] info:[self getUserName]];
                        break;
                    case 1:
                        [cell2 setProfileStyle:profile_array_t1c[indexPath.row] info:[self getUsert1C]];
                        break;
                    case 2:
                        [cell2 setProfileStyle:profile_array_t1c[indexPath.row] info:@" "];
                        break;
                    case 3:
                        [cell2 setLogoutCell:profile_array_t1c[indexPath.row] image:@"ic_logout"];
                        break;
                    default:
                        break;
                }
            }else{
                
                switch (indexPath.row) {
                    case 0:
                        [cell2 setProfileStyle:profile_array_email[indexPath.row] info:[self getUserName]];
                        break;
                    case 1:
                        [cell2 setProfileStyle:profile_array_email[indexPath.row] info:user_email];
                        break;
                    case 2:
                        [cell2 setProfileStyle:profile_array_email[indexPath.row] info:[self getUsert1C]];
                        break;
                    case 3:
                        [cell2 setProfileStyle:profile_array_email[indexPath.row] info:@" "];
                        break;
                    case 4:
                        [cell2 setLogoutCell:profile_array_email[indexPath.row] image:@"ic_logout"];
                        break;
                    default:
                        break;
                }
            }
            
            [self.loadingView setHidden:YES];
            [self.loadingView stopAnimating];
            
            [self setImageBackground:1];
            
            break;
        case 3:
            
            [cell2 setNotificationStyle:noti_array[indexPath.row]index:(int)indexPath.row];
            
            [self.loadingView setHidden:YES];
            [self.loadingView stopAnimating];
            
            [self setImageBackground:2];
            
            break;
            
        case 4:
            switch (indexPath.row) {
                    
                case 0:
                    
                    [cell2 setAboutUsCell:aboutUsDetail[indexPath.row] info:[BaseViewController getCurrentVersion]];
                    
                    break;
                    
                case 1:
                    
                    if (updateVersion==nil) {
                        [cell2 setAboutUsCell:aboutUsDetail[indexPath.row]info:@" "];
                    }else{
                        [cell2 setAboutUsCell:aboutUsDetail[indexPath.row]info:updateVersion];
                    }
                    break;
                    
                default:
                    
                    [cell2 setAboutUsCell:aboutUsDetail[indexPath.row]info:@""];
                    
                    
                    break;
            }
            [self.loadingView setHidden:YES];
            [self.loadingView stopAnimating];
            
            [self setImageBackground:3];
            break;
        case 8:
            [cell2 setQA:questiontxt[indexPath.row]];
            [self setImageBackground:6];
        default:
            break;
    }
    return cell2;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return [[[NSBundle mainBundle] loadNibNamed:@"Setting_tableCell" owner:self options:nil][0] frame].size.height;
}

-(void)setProfile{
    [self.userNumber setText:[NSString stringWithFormat:@"%@",[self getUsert1C]]];
}

-(void)set_initail_objects{
    
    profile_array_t1c = @[@"ชื่อสมาชิก",@"หมายเลข เดอะวันคาร์ด",@"เปลี่ยนรหัสผ่าน",@"ออกจากระบบ"];
    profile_array_email = @[@"ชื่อสมาชิก",@"อีเมล",@"หมายเลข เดอะวันคาร์ด",@"เปลี่ยนรหัสผ่าน",@"ออกจากระบบ"];
    facebookInfo_array = @[@"16/10/2558 13.33"];
    
    noti_array = @[@"การแจ้งเตือน",@"ใช้งานมีเดียสกรีน (Media Screen)"];
    
    aboutUsDetail = @[@"เวอร์ชั่นปัจจุบัน: ",@"เวอร์ชั่นล่าสุด: ",@"ข้อตกลงและเงื่อนไขการใช้งาน",@"นโยบายคุ้มครองข้อมูลส่วนตัว"];
    
    [self class_UI];
}

-(void)class_UI{
    
    SWIPETLIFE_TOGOTOPREVIOUS;
    
    [self.bgImage setFrame:CGRectMake(DEVICE_WIDTH/2-153,DEVICE_HEIGHT,306,306)];
    LINE_WITH_STATUSBAR_HEADER_NORMAL;
    
    [self.tableView setSeparatorColor:[UIColor clearColor]];
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    
    deviceDefaults = [NSUserDefaults standardUserDefaults];
    [self.viewUserInfo setHidden:YES];
}

- (void)swipeRight:(id)sender {
    GOTO_PREVIOUS;
}

- (IBAction)backBtn:(id)sender {
    GOTO_PREVIOUS;
}

-(void)setHeaderText:(NSString *)header{
    
    [self.titleHeader setText:[NSString stringWithFormat:@"%@",header]];
    
    if ([self.titleHeader.text isEqualToString:@"คำถามที่พบบ่อย"]) {
        [self get_question_answer];
    }
}

-(void)receiveMenu:(int)menu{
    menuNum = menu;
}

-(void)setImageBackground:(int)menu{
    
    // background image on menu setting
    switch (menu) {
        case 3:
            [self.topImageBg setConstant:22];
            [self.bottomImageBg setConstant:20];
            [self.rightImageBg setConstant:20];
            [self.leftImageBg setConstant:20];
            [self.bgImage setImage:[UIImage imageNamed:@"icon_profile_bg"]];
            break;
        case 4:
            [self.topImageBg setConstant:20];
            [self.bottomImageBg setConstant:20];
            [self.rightImageBg setConstant:20];
            [self.leftImageBg setConstant:20];
            [self.bgImage setImage:[UIImage imageNamed:@"icon_alert_bg"]];
            break;
        case 5:
            [self.rightImageBg setConstant:20];
            [self.leftImageBg setConstant:20];
            [self.bgImage setImage:[UIImage imageNamed:@"icon_bg_about"]];
            break;
        case 9:
            [self.rightImageBg setConstant:20];
            [self.leftImageBg setConstant:20];
            [self.bgImage setImage:[UIImage imageNamed:@"icon_faq"]];
            break;
        default:
            break;
    }
}

//-(void)gotoUserImage{
//
//    UserImageController *userImage = [[NSBundle mainBundle] loadNibNamed:@"UserImageController" owner:self options:nil][0];
//    [self.navigationController pushViewController:userImage animated:YES];
//}

-(void)goToSubSettingController:(NSString *)title detail:(NSString *)detail QAType:(BOOL)qa index:(NSString *)index{
    
    subMenuSetting = [[NSBundle mainBundle] loadNibNamed:@"SubMenuSettingController" owner:self options:nil][0];
    [subMenuSetting setHeaderText:[NSString stringWithFormat:@"%@",title]detail:detail type:qa];
    [subMenuSetting indexAboutUsDetail:index];
    [self.navigationController pushViewController:subMenuSetting animated:YES];
}

-(void)gotoQA:(NSString *)question anwser:(NSString *)answer{
    
    QuestionAnswerController *QASetting = [[NSBundle mainBundle] loadNibNamed:@"QuestionAnswerController" owner:self options:nil][0];
    [QASetting setQuestionAnswer:question answer:answer];
    [self.navigationController pushViewController:QASetting animated:YES];
}

-(NSString *)action:(NSString *) selecttag{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"selected_newfeed" object:[NSString stringWithFormat:@"%@",selecttag]];
    [self setTagMenu:selecttag];
    return selecttag;
}

-(void)displayAlertLogout{
    
    UIAlertController * alert =   [UIAlertController
                                   alertControllerWithTitle:@"ออกจากระบบ"
                                   message:@"ท่านต้องการออกจากระบบหรือไม่"
                                   preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"ไม่"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action){
                                   // ทำไรถ้ากด OK
                               }];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"ใช่"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action){
                                    
                                    [self.view setUserInteractionEnabled:NO];
                                    [BaseViewController setUserLogoutStatus:YES];
                                    
                                    [Service post_user_logout:userToken selfID:self block:^(id result) {
                                    
                                        LoginController *view_class = [self loadNibWithName:@"LoginController"];
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            [self.navigationController pushViewController:view_class animated:YES];
                                        });

/*                                        UIStoryboard *storybord = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                        UIViewController * loginView = [storybord   instantiateViewControllerWithIdentifier:@"LoginController"] ;
                                        [self.navigationController pushViewController:loginView animated:YES];
 */
                                    }];
                                }];
    
    [alert addAction:noButton];
    [alert addAction:yesButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

@end
