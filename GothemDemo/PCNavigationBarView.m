//
//  PCNavigationBarView.m
//  PlayCash
//
//  Created by Akarapas Wongkaew on 2/5/2559 BE.
//  Copyright © 2559 Akarapas Wongkaew. All rights reserved.
//

#import "PCNavigationBarView.h"

@implementation PCNavigationBarView

-(void)awakeFromNib
{
    [super awakeFromNib];

    [self setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    [self.btnLeft setBackgroundColor:[UIColor clearColor]];
    [self.btnRight setBackgroundColor:[UIColor clearColor]];
}

-(void)setBtnLeftStyle:(PCNavigationBarBtnStyle)style
{
    if (style==PCNavigationBarBtnStyleBack) {
        [self.btnLeft setImage:[UIImage imageNamed:@"right_filled-100"] forState:UIControlStateNormal];
    }
}

-(void)setBtnRightStyle:(PCNavigationBarBtnStyle)style
{
    
}

-(void)settextTitleWithText:(NSString *)text
{
    [self.text_title setText:text];
}

@end
