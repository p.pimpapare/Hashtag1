//
//  UsedCuponSuccess.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/2/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

//  This clas is pop up view for displaying user store coupon success.

#import <UIKit/UIKit.h>

@interface UsedCuponSuccess : UIView

@property (weak, nonatomic) IBOutlet UILabel *textDetails;
@property (weak, nonatomic) IBOutlet UIImageView *logo;

@property (weak, nonatomic) IBOutlet UIButton *confrimBtn;
@property (weak, nonatomic) IBOutlet UIButton *black_btn;

-(void)setlogoCoupon:(NSString *)logoImage;
-(void)setTextAlertView:(NSString *)text;

@end
