
//  Created by pimpaporn chaichompoo on 2/10/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "Setting_tableCell.h"

@implementation Setting_tableCell

- (void)awakeFromNib {
    
    tab = NO;
    
    [super awakeFromNib];
    [self setBackgroundColor:[UIColor clearColor]];
    [self setFrame:CGRectMake(0,0,DEVICE_WIDTH,53)];
    
    [self.detail setText:@"การพยายามปิดแอปพลิเคชั่น\nจะทำให้การแจ้งเตือนช้าหรืออาจไม่ได้รับข้อความ"];
    [self.detail setFont:[UIFont fontWithName:@"RSU" size:11]];
    [self.detail setNumberOfLines:2];
    
    lockDefaults = [NSUserDefaults standardUserDefaults];
}

-(void)setFeedUpStyle:(NSString *)title{
    
    [self.title setText:[NSString stringWithFormat:@"%@",title]];
    [self.imagee setImage:[UIImage imageNamed:@"circle_gray"]];
}

-(void)setNotificationStyle:(NSString *)title index:(int)indexMenu{
    
    if (lockDefaults){
        lockInfo = (id)[lockDefaults objectForKey:@"lockscreen_setting"];
    }
    
    if (indexMenu==0) {
        
        [self.imagee setHidden:YES];
        
    }else{
        
        NSString *image_name;
        
        if([lockInfo isEqualToString:@"selectedBtn"]||(lockInfo==nil)){
            
            image_name = @"checkMark";
            
        }else if([lockInfo isEqualToString:@"unselectedBtn"]){
            
            image_name = @"circle_gray";
            
        }else{
            
            image_name = @"checkMark";
        }
        
        [self.imagee setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",image_name]]];
    }
    
    [self.title setText:[NSString stringWithFormat:@"%@",title]];
}

-(void)setNotificationStyleDetail:(NSString *)title{
    
    self.detail.hidden = NO;
    self.heightTitle.constant = 0;
    [self.title setText:[NSString stringWithFormat:@"%@",title]];
    [self.detail setFrame:CGRectMake(22, 29, 234, 26)];
}

-(void)setProfileStyle:(NSString *)title info:(NSString *)info{
    
    self.info.hidden = NO;
    self.imagee.hidden = YES;
    [self.title setText:[NSString stringWithFormat:@"%@",title]];
    [self.info setText:[NSString stringWithFormat:@"%@",info]];
    [self.info setTextColor:[PCColor color_A8101D]];
}

-(void)setLogoutCell:(NSString *)title image:(NSString *)image{
    
    [self.imagee setHidden:NO];
    [self.title setText:[NSString stringWithFormat:@"%@",title]];
    [self.imagee setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",image]]];
}

-(void)setAboutUsCell:(NSString *)title info:(NSString *)info{
    
    if([info isEqualToString:@"(null)"]){
        
        [self.info setText:@" "];
        
    }else{
        
        [self.info setText:[NSString stringWithFormat:@"%@",info]];
    }
    
    [self.info setHidden:NO];
    [self.title setText:[NSString stringWithFormat:@"%@",title]];
    [self.imagee setHidden:YES];
}

-(void)setQA:(NSString *)title{
    [self.title setText:[NSString stringWithFormat:@"%@",title]];
    [self.imagee setHidden:YES];
}

- (void)selectedNotiSEttingCell:(int)index{
    
    //    NSLog(@"%d",index);
    
    if (lockDefaults){
        
        if(tab==NO){
            
            [self.imagee setImage:[UIImage imageNamed:@"checkMark"]];
            
            tab=YES;
            
            if (index==1)
            {
                [lockDefaults setObject:@"selectedBtn" forKey:@"lockscreen_setting"];
            }else
            {
                [self openSetting];
            }
        }else if(tab==YES){
            
            [self.imagee setImage:[UIImage imageNamed:@"circle_gray"]];
            
            tab=NO;
            
            if (index==1)
            {
                [lockDefaults setObject:@"unselectedBtn" forKey:@"lockscreen_setting"];
            }else
            {
                [self openSetting];
            }
        }
    }
}

-(void)openSetting
{
    NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
    [[UIApplication sharedApplication] openURL:url];
}

@end
