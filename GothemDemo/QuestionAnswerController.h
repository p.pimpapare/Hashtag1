//
//  QAController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/15/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionAnswerController : BaseViewController

@property (weak, nonatomic) IBOutlet UIView *statusBar;
@property (weak, nonatomic) IBOutlet UIView *menuHeader;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;

@property (weak, nonatomic) IBOutlet UILabel *questionTxt;
//@property (weak, nonatomic) IBOutlet UITextView *answerTxt;

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIView *questionView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingView;

-(void)setQuestionAnswer:(NSString *)question answer:(NSString *)answer;

@end
