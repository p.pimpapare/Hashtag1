//
//  HowToUseController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/4/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "HowToUseController.h"

@interface HowToUseController ()

@end

@implementation HowToUseController

-(void)viewWillAppear:(BOOL)animated{
    
    GOOGLE_ANALYTICS(@"HowToUseController");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self set_initial_objects];
}

-(void)setCollection{
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    
    layout.minimumLineSpacing=0;
    layout.minimumInteritemSpacing=0;
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    [_collectionView setShowsHorizontalScrollIndicator:NO];
    
    [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [_collectionView setBackgroundColor:[UIColor clearColor]];
    
    [_collectionView setPagingEnabled:YES];
    
    [_collectionView registerClass:[HowToUseCollectionCell class] forCellWithReuseIdentifier:@"Cell"];
    [_collectionView registerNib:[UINib nibWithNibName:@"HowToUseCollectionCell" bundle:nil]  forCellWithReuseIdentifier:@"Cell"];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [image_array count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    HowToUseCollectionCell *cell=(HowToUseCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    if(cell == nil) {
        cell = [self loadNibWithName:@"HowToUseCollectionCell"];
    }
    [cell setHowtoImage:image_array[indexPath.row]index:(int)indexPath.row];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(DEVICE_WIDTH,DEVICE_HEIGHT-64);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    // set menu color when user tab on that menu
    selectedTabMenu = (int)indexPath.row;
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    // TODO: Deselect item
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0,0,0,0);
}

-(void)set_initial_objects{
    
    image_array = @[@"tutorial1",@"tutorial2",@"tutorial3",@"tutorial4"];
    [self class_UI];
}

-(void)class_UI{
    
    LINE_WITH_STATUSBAR_HEADER_NORMAL;    
    [self setCollection];
}

-(void)goToLogin{
    
    LoginController *view_class = [self loadNibWithName:@"LoginController"];
    dispatch_async(dispatch_get_main_queue(), ^{
    [self.navigationController pushViewController:view_class animated:YES];
    });

/*    UIStoryboard *storybord = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController * loginView = [storybord   instantiateViewControllerWithIdentifier:@"LoginController"] ;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController pushViewController:loginView animated:YES];
    });
 */
}

- (IBAction)back:(id)sender {
    GOTO_PREVIOUS;
}

@end
