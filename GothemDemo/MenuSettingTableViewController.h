//
//  MenuSettingTableViewController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/20/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

//  This class is used for display UITableview inside menu4. (about us,notification setting, profile, etc)

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface MenuSettingTableViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate,NSFetchedResultsControllerDelegate>{
    
    NSString *updateVersion;
    NSString *updateDetail;
    
    int menuNum;
    
    NSString *user_email;
    
    /// set profile
    NSArray *profile_array_t1c; // for t1c users
    NSArray *profile_array_email; // for email users
    NSArray *facebookInfo_array;
    NSString *currentName;
    
    /// set noti
    NSArray *noti_array;
    NSArray *check;
    
    /// set Aboutus
    NSArray *aboutUsDetail;
    
    /// QA
    NSArray *qa_details;
        
    NSMutableArray *questiontxt;
    NSMutableArray *answertxt;
    NSString *userToken;
    
    NSUserDefaults *deviceDefaults;
}

@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UILabel *titleHeader;

@property (weak, nonatomic) IBOutlet UIView *statusBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *menuHeader;

@property (weak, nonatomic) IBOutlet UIView *viewUserInfo;

// ui for profile
@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet UILabel *userNumber;

@property (weak, nonatomic) IBOutlet UIImageView *bgImage;
@property (weak, nonatomic) IBOutlet UIView *viewBlankInfo;


// NSLayoutConstraint
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightHeaderBar;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomImageBg;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topImageBg;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightImageBg;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftImageBg;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingView;

//-(void)setProfile:(NSString *)image userNumber:(NSString *)usernum;
-(void)setHeaderText:(NSString *)header;
-(void)receiveMenu:(int )menu;

@end
