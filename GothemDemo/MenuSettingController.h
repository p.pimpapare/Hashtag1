//
//  MenuSettingController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/19/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

// This class is a base class of menu 4

#import <UIKit/UIKit.h>
#import "GradientView.h"

@interface MenuSettingController : BaseViewController<UITableViewDataSource,UITableViewDelegate>{
    
    NSMutableArray *arrayNameSetting;
    NSMutableArray *arrayImageSetting;
    
    NSString *widgetSetting;
}
// พี่ตู๋
//+(instancetype)mainMenu;

@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet GradientView *vwGradient;

// user profile
@property (weak, nonatomic) IBOutlet UILabel *noTopic;
@property (weak, nonatomic) IBOutlet UILabel *balanceTopic;
@property (weak, nonatomic) IBOutlet UILabel *earnTopic;
@property (weak, nonatomic) IBOutlet UILabel *point1;
@property (weak, nonatomic) IBOutlet UILabel *point2;
@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *userNum;
@property (weak, nonatomic) IBOutlet UILabel *userBalancePoint;
@property (weak, nonatomic) IBOutlet UILabel *userEarnedPoint;
@property (weak, nonatomic) IBOutlet UIView *profileView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightAdvertising;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *profile_view_width;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *profile_view_center_h;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *profile_view_center_v;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *profile_view_left;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *profile_view_right;


@property NSString *tagMenu;
-(NSString *)action:(NSString *) selecttag;
-(void)setUserProfile;

@end
