//
//  SequeInquiryController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/30/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XLBarPagerTabStripViewController.h>
#import <XLPagerTabStripViewController.h>

@interface SequeInquiryController : XLBarPagerTabStripViewController<XLPagerTabStripChildItem,XLPagerTabStripViewControllerDataSource,XLPagerTabStripViewControllerDelegate,UICollectionViewDataSource,UICollectionViewDelegate>{
    
    int selectedTab;
    BOOL _isReload;
    UICollectionView *_collectionView;
    
    NSArray *titleMenu;
    int selectedTabMenu;
    NSMutableArray *swiptTab;
}

@property (weak, nonatomic) IBOutlet UIView *menuBar;

@end
