//
//  MenuCuponDetailCode.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/21/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

// This class is called when the user used coupon success

#import <UIKit/UIKit.h>
#import "DACircularProgressView.h"
#import "DALabeledCircularProgressView.h"

@interface MenuCuponDetailCode : UIView{
    NSString *codeText;
    
    // countdown coupon time
    
    NSTimer *timer;
    
    int minutes,seconds,secondsText,countTime;
    
    NSTimer *timeDelay;
    
    BOOL closeAnimation;
}

@property (weak, nonatomic) IBOutlet UILabel *titleHeader;
@property (weak, nonatomic) IBOutlet UIImageView *codeImage;
@property (weak, nonatomic) IBOutlet UIButton *barCodeBtn;
@property (weak, nonatomic) IBOutlet UIButton *qrCodeBtn;
@property (weak, nonatomic) IBOutlet UILabel *detailTextView;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *timeImage;
@property (weak, nonatomic) IBOutlet UIButton *usedCodeBtn;
@property (weak, nonatomic) IBOutlet UILabel *userId;

@property (weak, nonatomic) IBOutlet UIImageView *lineHeader;
@property (weak, nonatomic) IBOutlet UIImageView *lineBottom;

@property (weak, nonatomic) IBOutlet UIButton *shareBtn;
@property (weak, nonatomic) IBOutlet UIButton *readmoreBtn;

@property (weak, nonatomic) IBOutlet DACircularProgressView *clockProgressView;
@property (strong, nonatomic) NSTimer *clockTimer;

-(void)setMenuCoupon:(NSString *)titleHeader detail:(NSString *)detail time:(NSString *)time code:(NSString *)code;
@end
