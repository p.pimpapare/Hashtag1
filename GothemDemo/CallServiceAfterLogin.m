//
//  CallServiceAfterLogin.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 9/13/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "CallServiceAfterLogin.h"

@interface CallServiceAfterLogin ()

@end

@implementation CallServiceAfterLogin

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)call_service_from:(NSString *)token type:(int)type{
    
    [self setAdvertising1:token];
}

-(void)setAdvertising1:(NSString *)token{
    
    [Service get_banner:token brannerId:1 selfID:self block:^(id result) {
        
        CHECK_NETWORK_CONNECTION(
                                 SET_USER_DEFAULT_INFO(result,@"AdvertisingInfo1")
                                 [self setAdvertising2:token];)
    }];
}

-(void)setAdvertising2:(NSString *)token{
    
    [Service get_banner:token brannerId:2 selfID:self block:^(id result) {
        
        CHECK_NETWORK_CONNECTION(
                                 SET_USER_DEFAULT_INFO(result,@"AdvertisingInfo2")
                                 [self setAdvertising3:token];)
    }];
}

-(void)setAdvertising3:(NSString *)token{
    
    [Service get_banner:token brannerId:3 selfID:self block:^(id result) {
        
        CHECK_NETWORK_CONNECTION(
                                 SET_USER_DEFAULT_INFO(result,@"AdvertisingInfo3")
                                 [self setUpdate:token];)
    }];
}

-(void)setUpdate:(NSString *)token{
    
    [Service get_ads_info:token block:^(id result) {
        
        CHECK_NETWORK_CONNECTION(
                                 SET_USER_DEFAULT_INFO(result,@"LockScreenInfo")
                                 
                                 [self.view setUserInteractionEnabled:YES];
                                 [self setHiddenPCLoading:YES];
                                 [self goto_home];
                                 )
    }];
}

-(void)goto_home{
    
    SET_USER_DEFAULT_INFO(@"NO",@"logoutStatus")    
    [self action:@"gotoHome"];
}

-(NSString *)action:(NSString *) selecttag{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Service_status" object:[NSString stringWithFormat:@"%@",selecttag]];
    [self setTagMenu:selecttag];
    return selecttag;
}

-(void)network_error{
    
    [self action:@"network_error"];
}

-(void)service_error{
    
    [self action:@"service_error"];
}

@end
