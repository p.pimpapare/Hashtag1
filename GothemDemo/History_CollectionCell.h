//
//  History_CollectionCell.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/8/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface History_CollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleHeader;

-(void)setHistoryCell:(NSString *)title;
-(void)setselectedColor:(int)selectedMenu;
@end
