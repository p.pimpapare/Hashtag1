
//
//  LuckyPointController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/31/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "LuckyPointController.h"

@interface LuckyPointController ()

@end

@implementation LuckyPointController

-(void)viewWillAppear:(BOOL)animated{
    
    GOOGLE_ANALYTICS(@"LuckyPointController");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.loadingView startAnimating];
    
    [self class_UI];
    [self getService];
}

-(void)getService{
    
    [Service get_lucky_point_html:self block:^(id result) {
        
       CHECK_NETWORK_CONNECTION([self createWebViewWithHTML:result];)
            
    }];
}

-(void)network_error{
//    ALERT_MESSAGE_YES_NETWORK(GOTO_PREVIOUS)
}

- (void) createWebViewWithHTML:(NSString *)text{
    
    [self.webView setBackgroundColor:[UIColor clearColor]];
    [self.webView loadHTMLString:[text description] baseURL:nil];
}

- (IBAction)backBtn:(id)sender {
    GOTO_PREVIOUS;
}

-(void)class_UI{
    
    LINE_WITH_STATUSBAR_HEADER_NORMAL;
    [self.webView setScalesPageToFit:YES];
}

///// WEB_VIEW

-(void)didReceiveMemoryWarning{
    
    [super didReceiveMemoryWarning];
}

- (void)setView:(UIView *)view{
    
    if (view == nil){
        [self releaseSubviews];
    }
    
    [super setView:view];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    if (!webView.isLoading) {
        
        [self.loadingView stopAnimating];
        [self.loadingView setHidden:YES];
    }
}

-(void)openUrl:(NSString *)urlText{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",urlText]];
    NSURLRequest *req = [NSURLRequest requestWithURL:url];
    
    [self.webView loadRequest:req];
}

-(void)releaseSubviews{
    
    [self.webView setDelegate:nil];
    [self.webView stopLoading];
}

- (void)dealloc{
    
    [self releaseSubviews];
}

- (void)viewWillDisappear:(BOOL)animated{
    
    [self releaseSubviews];
}
///// END OF WEB_VIEW

@end
