//
//  LockScreenController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/9/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "LockScreenController.h"
#import "YLShareAnimationHelper.h"
#import "ASStandardLibrary.h"

@interface LockScreenController (){
    
    UnlockWebViewController *unScene;
    DLDownload *download;
}

@property (nonatomic, strong) AFDropdownNotification *notification;

@end

@implementation LockScreenController

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    GOOGLE_ANALYTICS(@"LockScreenController");
    SET_USER_DEFAULT_INFO(@"YES",@"InLockscreen")
    
    [self set_initial_view];
    
    timeCircle = [NSTimer scheduledTimerWithTimeInterval:0.8 target:self selector:@selector(updateCircleView) userInfo:nil repeats: YES];
    
    /*
     Check times to access for getting different advertising columns
     */
    
    NSString *times = [NSString stringWithFormat:@"%@",(id)[[NSUserDefaults standardUserDefaults] objectForKey:@"TimesAccess"]];
    
    if(times==nil){ // First times
        
        [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"TimesAccess"];
        [self set_service:0 index:0];
    }
    
    else{ // Check colum and index if the user comes from UnlockWebView scene
        
        
        GET_USER_DEFAULT_INFO(@"Selected_Back")
        
        if (userInfo!=nil) { // User come from UnlockController
            
            [self.adImage set_index_image_next];
            
            indexArtworkData = [self.adImage getIndex:nil];
            
            [self icon_ads:indexArtworkData];
            
            [self get_service_point:indexArtworkData direction:@"u"];
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Selected_Back"];
            
        }else{ // Check package loop and index count
            
            GET_USER_DEFAULT_INFO(@"LockScreenInfo")
            
            NSString *package_total = [NSString stringWithFormat:@"%@",userInfo[@"loop_items"][0][@"package_total"]];
            
            if ((userInfo==nil)||([userInfo[@"loop_items"]count]==0)||([package_total isEqualToString:@"0"])) {
                
                [self go_to_home_scene];
                
            }else{
                
                // Fixed Maximun loop package is 4
                int column_num = 0,service_value_num = 0;
                
                if ([userInfo[@"loop_items"] count]<3) {
                    
                    if ([userInfo[@"loop_items"] count]==1) { // loop package 1
                        
                        column_num = 0;
                        service_value_num = 0;
                        
                    }else{ // package loop 2
                        
                        if (([times isEqualToString:@"0"])||([times isEqualToString:@"(null)"])) {
                            
                            column_num = 0;
                            service_value_num = 1;
                            
                        }else{
                            
                            column_num = 1;
                            service_value_num = 0;
                        }
                    }
                    
                }else if([userInfo[@"loop_items"] count]==3){ // loop package 3
                    
                    if (([times isEqualToString:@"0"])||([times isEqualToString:@"(null)"])) {
                        
                        column_num = 0;
                        service_value_num = 1;
                        
                    }else if([times isEqualToString:@"1"]){
                        
                        column_num = 1;
                        service_value_num = 2;
                        
                    }else{
                        
                        column_num = 2;
                        service_value_num = 0;
                    }
                    
                }else{ // loop package 4
                    
                    if (([times isEqualToString:@"0"])||([times isEqualToString:@"(null)"])) {
                        
                        column_num = 0;
                        service_value_num = 1;
                    }
                    else if([times isEqualToString:@"1"]){
                        
                        column_num = 1;
                        service_value_num = 2;
                        
                    }else if([times isEqualToString:@"2"]){
                        
                        column_num = 2;
                        service_value_num = 3;
                    }else{
                        
                        column_num = 3;
                        service_value_num = 0;
                    }
                }
                
                [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%d",service_value_num] forKey:@"TimesAccess"];
                [self set_service:column_num index:0];
            }
        }
    }
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self set_initial_objects];
}

-(void)set_service:(int)column index:(int)index{
    
    /*
     * This () is called for seperating and setting loop package coloum.
     *
     */
    
    column_ads = column;
    
    imageAd = [[NSMutableArray alloc]init];
    artwork_type = [[NSMutableArray alloc]init];
    package_id = [[NSMutableArray alloc]init];
    package_name = [[NSMutableArray alloc]init];
    artwork_data = [[NSMutableArray alloc]init];
    
    GET_USER_DEFAULT_INFO(@"LockScreenInfo")
    
    fix = [NSString stringWithFormat:@"%@",userInfo[@"fix"]];
    fix_int = [fix intValue];
    
    urlBase = [NSString stringWithFormat:@"%@",userInfo[@"image_path"]];
    request_time = [NSString stringWithFormat:@"%@",userInfo[@"request_time"]];
    
    for (int i=0;i<[userInfo[@"loop_items"][column][@"package_items"] count];i++) {
        
        /*// ปิด เพื่อ type download
         NSString *check_artworktype = [NSString stringWithFormat:@"%@",userInfo[@"loop_items"][column][@"package_items"][i][@"artwork_type"]];
         
         if (![check_artworktype isEqualToString:@"2"]) {
         */
        [package_id addObject:userInfo[@"loop_items"][column][@"package_items"][i][@"package_id"]];
        [imageAd addObject:userInfo[@"loop_items"][column][@"package_items"][i][@"artwork_image"]];
        [artwork_type addObject:userInfo[@"loop_items"][column][@"package_items"][i][@"artwork_type"]];
        [artwork_data addObject:userInfo[@"loop_items"][column][@"package_items"][i][@"artwork_data"]];
        
    }
    //    }
    
    totlePackage = [NSString stringWithFormat:@"%lu",(unsigned long)[package_id count]];
    
    /*//  ปิดไว้ในกรณีที่เราต้องการตัด artwork type 2 ไป
     totlePackage = [NSString stringWithFormat:@"%@",userInfo[@"loop_items"][column][@"package_total"]];
     */
    
    [self icon_ads:index];
    
    indexArtworkData = index;
    [self get_service_point:index direction:@"c"];
    
    /// #### StackImageView #### ///
    
    // Sending imageAds array to StackImageView class for setting loop of image
    [self.adImage setBaseUrl:urlBase];
    [self.adImage setDataImageWithArray:imageAd];
}

-(void)panGestureSlideBtn_action:(UIPanGestureRecognizer *)recognizer{
    
    /*
     * This () is used for setting UIPanGestureRecognizer animation
     *
     */
    
    CGPoint translation = [recognizer translationInView:recognizer.view];
    recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x,
                                         recognizer.view.center.y);
    [recognizer setTranslation:CGPointMake(0, 0) inView:recognizer.view];
    
    if(recognizer.state == UIGestureRecognizerStateChanged){
        
        if (self.longTapBtn.frameLeft<=self.view_bg_long_btn.frameLeft){
            
            self.longTapBtn.frameLeft = self.view_bg_long_btn.frameLeft;
            
        }else if(self.longTapBtn.center.x<self.view_bg_long_btn.center.x){
            
            [self.icon_left_width setConstant:18];
            [self.icon_left_height setConstant:19];
            [self.view setNeedsUpdateConstraints];
            
            [UIView animateWithDuration:1 delay:0 usingSpringWithDamping:0.5 initialSpringVelocity:0.5 options:UIViewAnimationOptionCurveEaseIn animations:^{
                
                [self.view layoutIfNeeded];
                
            }completion:^(BOOL finished) {
                
                [self.icon_right_width setConstant:32];
                [self.icon_right_height setConstant:28];
                
                [self.view setNeedsUpdateConstraints];
                
                [UIView animateWithDuration:1 delay:0 usingSpringWithDamping:0.5 initialSpringVelocity:0.5 options:UIViewAnimationOptionCurveEaseIn animations:^{
                    
                    [self.view layoutIfNeeded];
                    
                }completion:nil];
                
            }];
        }
        
        if (self.longTapBtn.frameRight>=self.view_bg_long_btn.frameRight) {
            
            self.longTapBtn.frameRight = self.view_bg_long_btn.frameRight;
            
        }else if(self.longTapBtn.center.x>self.view_bg_long_btn.center.x){
            
            [self.icon_right_width setConstant:26];
            [self.icon_right_height setConstant:22];
            [self.view setNeedsUpdateConstraints];
            
            [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:0.5 initialSpringVelocity:0.5 options:UIViewAnimationOptionCurveEaseIn animations:^{
                
                [self.view layoutIfNeeded];
                
            }completion:^(BOOL finished) {
                
                [self.icon_left_width setConstant:24];
                [self.icon_left_height setConstant:25];
                
                [self.view setNeedsUpdateConstraints];
                
                [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:0.5 initialSpringVelocity:0.5 options:UIViewAnimationOptionCurveEaseIn animations:^{
                    
                    [self.view layoutIfNeeded];
                    
                }completion:nil];
                
            }];
        }
        
    }else if (recognizer.state == UIGestureRecognizerStateEnded) {
        
        [self.icon_left_width setConstant:24];
        [self.icon_left_height setConstant:25];
        [self.icon_right_width setConstant:32];
        [self.icon_right_height setConstant:28];
        
        [self.view setNeedsUpdateConstraints];
        
        [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:0.5 initialSpringVelocity:1 options:UIViewAnimationOptionCurveEaseIn animations:^{
            
            [self.view layoutIfNeeded];
            
        }completion:nil];
        
        [self addPCLoading:self];
        [self setHiddenPCLoading:NO];
        [self setLoadingLockScreen];
        
        if (self.longTapBtn.frameLeft==self.view_bg_long_btn.frameLeft){ // User slide lockscreen's button to the left
            
            [self.centerLongTapBtn setConstant:-120];
            
            if (networkError==YES) { // User can't go to unlock scene, when network connection status = error
                
                [self setHiddenPCLoading:YES];
                [self.centerLongTapBtn setConstant:0];
                
            }else{
                
                [BaseViewController log_comment:@"indexArtworkData" text:[NSString stringWithFormat:@"%@",artwork_data[indexArtworkData]]];
                
                if (adsDownload == YES) { // open app store
                    
                    NSString *artwork_str = [NSString stringWithFormat:@"%@",artwork_data[indexArtworkData]];
                    NSArray *splite_url_appid = [artwork_str componentsSeparatedByString:@"id"];
                    
                    NSString *str_appid = [splite_url_appid objectAtIndex:1];
                    NSString *appid = [str_appid substringWithRange:NSMakeRange(0,10)];
                    
                    SET_USER_DEFAULT_INFO(@"YES",@"GOTO_APPSTORE")
                    
                    //                    // ลอง log ดู artwork_data[indexArtworkData] ว่าให้มาเป็นอะไร
                    //                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"itms-apps://**ax.itunes**.apple.com/app/%@",artwork_data[indexArtworkData]]]];
                    // app/ตามด้วยลิ้งแอพ
                    
                    SKStoreProductViewController *storeProductViewController = [[SKStoreProductViewController alloc] init];
                    
                    // Configure View Controller
                    [storeProductViewController setDelegate:(id)self];
                    [storeProductViewController loadProductWithParameters:@{SKStoreProductParameterITunesItemIdentifier :[NSString stringWithFormat:@"%@",appid]} completionBlock:^(BOOL result, NSError *error) {
                        if (error) {
                            NSLog(@"Error %@ with User Info %@.", error, [error userInfo]);
                            
                        } else {
                            // Present Store Product View Controller
                            [self presentViewController:storeProductViewController animated:YES completion:nil];
                        }
                    }];
                    
                    /*
                     SKStoreProductViewController has special behavior during development. When you release the app, it will work fine for purchasing items.
                     */
                    
                }else{
                    [self go_to_unlock:indexArtworkData];
                }
            }
            
        }else if (self.longTapBtn.frameRight==self.view_bg_long_btn.frameRight) { // User slide lockscreen's button to the right
            
            [self.centerLongTapBtn setConstant:120];
            
            if(right_point==YES){
                //                [self alert_noti:package_collectpoint]; // ยังไม่มี
            }else{
                [self go_to_home_scene];
            }
            
        }else{
            
            [self setHiddenPCLoading:YES];
            
            [UIView animateWithDuration:0.4 animations:^{
                
                [self.longTapBtn setCenter:CGPointMake(DEVICE_W/2, self.longTapBtn.center.y)];
                
            } completion:^(BOOL finished) {}];
        }
    }
}

- (void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController {
    
    [self.centerLongTapBtn setConstant:0];
    [self setHiddenPCLoading:YES];
    
    //// check app installed ?
    NSString *scheme = @"fb://profile/429759140452016"; // test
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",scheme]]]) {
        
        //[[UIApplication sharedApplication]openURL:[NSURL URLWithString:scheme]];
        [self set_service_get_point:@"c"]; // type collect point คือไร
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)alert_noti:(NSString *)alert_title sub_title:(NSString *)sub_title {
    
    /*
     * This () is called when any Ads has point, wheter it be #1 point or Luckydraw point
     *
     */
    
    _notification = [[AFDropdownNotification alloc] init];
    _notification.notificationDelegate = (id)self;
    
    _notification.titleText = [NSString stringWithFormat:@"%@",alert_title];
    _notification.subtitleText = [NSString stringWithFormat:@"%@",sub_title];
    _notification.image = [UIImage imageNamed:@"aaaa"];
    
    [_notification presentInView:self.view withGravityAnimation:YES];
    
    timeAlert = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(stop_notification) userInfo:nil repeats:NO];
}

-(void)stop_notification{
    
    /*
     * This () is called when timerAlert is 3.0 sec
     *
     */
    
    [_notification dismissWithGravityAnimation:YES];
}

-(void)get_service_point:(int)lastIndex direction:(NSString *)directionn{
    
    /*
     * This () is called from set_service() and notification_alert(), for getting point number of each ads
     * in service user/package/response
     */
    
    slide_ads = NO;
    isDisappear = NO;
    
    if (download && isScrolling == NO) {
        
        [download cancel]; // cancel loading DLDownload when user scroll
        
        isScrolling = YES;
    }
    
    directionScroll= directionn;
    [self.circleLeft setHidden:YES];
    [self.circleRight setHidden:YES];
    
    int previousIndex;
    int totlepackage_int = [totlePackage intValue];
    
    if (lastIndex==fix_int-1) { // -1 because lastIndex start at 0, but fix_int start at 1
        
        [self.adImage setCanScrollDown:YES];
        [arrowTopBtn setHidden:NO];
    }
    
    if ((lastIndex==0)||(lastIndex==totlepackage_int)){
        
        previousIndex = lastIndex;
        
    }else{
        
        if ([directionScroll isEqualToString:@"u"]) {
            previousIndex = lastIndex - 1;
        }else if ([directionScroll isEqualToString:@"d"]){
            previousIndex = lastIndex - 1;
        }else{
            previousIndex = lastIndex;
        }
    }
    
    //    NSLog(@"artwork %@",artwork_data[lastIndex]);
    //    NSLog(@"package id %@",package_id[lastIndex]);
    
    if (!download) {
        download = [[DLDownload alloc] init];
    }
    
    download.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@user/package/response?package_id=%@&direction=%@&before_package_id=%@&request_time=%@",BASEURL,package_id[lastIndex],directionScroll,package_id[previousIndex],request_time]];
    
    download.timeout = 100;
    [download setMethod:DLDownloadMethodPOST];
    
    [download setHTTPHeaderFields:@{@"version":[BaseViewController getDeviceInfo][@"version"],@"mac":[BaseViewController getDeviceInfo][@"mac"],@"os-type":@"ios",@"os-version":[BaseViewController getDeviceInfo][@"os-version"],@"Authorization":[self getUserToken]}];
    
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(error) {
            
            [BaseViewController log_comment:@"Error status" text:[NSString stringWithFormat:@"%@",error.localizedDescription]];
            
            if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."]) {
                networkError = YES;
            }
            
        } else {
            
            NSMutableDictionary *result = [NSMutableDictionary dictionaryWithDictionary:(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
            
            [BaseViewController log_comment:@"Result count " text:[NSString stringWithFormat:@"%lu",(unsigned long)[result count]]];

            if (isDisappear==NO&&slide_ads==NO) {
                
                //                NSLog(@"result %@",result);
                
                if ([result count]==0){ // login ซ้ำ
                    
                    [self alert_login_again];
                    
                }else{
                    
                    networkError = NO;
                    
                    response_token = [NSString stringWithFormat:@"%@",result[@"ad"][@"response_token"]];
                    package_unpointpertime = [NSString stringWithFormat:@"%@",result[@"ad"][@"package_unpointpertime"]];
                    package_collectpoint = [NSString stringWithFormat:@"%@",result[@"ad"][@"package_collectpoint"]];
                    NSString *luckydraw = [NSString stringWithFormat:@"%@",result[@"ad"][@"luckydraw"]];
                    NSString *collection_type = [NSString stringWithFormat:@"%@",result[@"ad"][@"collect_type"]];
                    
                    //NSLog(@"collection_type %@",collection_type);
                    //NSLog(@"luckydraw %@",luckydraw);
                    
                    if ([collection_type isEqualToString:@"u"]) { // point on the right handside
                        
                        point_right = YES;
                        
                        [self.circleLeft setHidden:YES];
                        [self.circleRight setHidden:NO];
                        [self.textRight setText:[NSString stringWithFormat:@"+%@",package_collectpoint]];
                        [self.gifbox_icon setHidden:YES];
                        //[self.gifbox_icon_right setHidden:YES];
                        
                    }else if([collection_type isEqualToString:@"c"]){ // point on the left handside
                        
                        [self.circleLeft setHidden:NO];
                        [self.circleRight setHidden:YES];
                        [self.gifbox_icon setHidden:YES];
                        
                        [self.textLeft setText:[NSString stringWithFormat:@"+%@",package_collectpoint]];
                        
                    }else if ([collection_type isEqualToString:@"n"]){
                        
                        [self.circleLeft setHidden:YES];
                        [self.circleRight setHidden:YES];
                        [self.textLeft setText:@" "];
                        [self.textRight setText:@" "];
                        [self.gifbox_icon setHidden:YES];
                        //[self.gifbox_icon_right setHidden:NO];
                    }
                    
                    if ([luckydraw isEqualToString:@"1"]) { // true
                        
                        if ([collection_type isEqualToString:@"u"]) { // luckydraw on the right handside
                            
                            // ยังไม่มี luckydraw ขวา
                            [self.circleLeft setHidden:YES];
                            //[self.circleRight setHidden:NO];
                            [self.textLeft setText:@"+1 "];
                            [self.gifbox_icon setHidden:NO];
                            //[self.gifbox_icon_right setHidden:YES];
                            
                        }else if([collection_type isEqualToString:@"c"]){
                            
                            [self.circleLeft setHidden:NO];
                            [self.circleRight setHidden:YES];
                            
                            [self.gifbox_icon setHidden:YES];
                            
                            [self.circleLeft setHidden:NO];
                            [self.textLeft setText:[NSString stringWithFormat:@"+%@",package_collectpoint]];
                            
                        }else if ([collection_type isEqualToString:@"n"]){ // luckydraw on the left handside
                            
                            [self.circleRight setHidden:YES];
                            [self.circleLeft setHidden:NO];
                            [self.textLeft setText:@"+1 "];
                            //[self.textRight setText:@"+1 "];
                            [self.gifbox_icon setHidden:NO];
                            //[self.gifbox_icon_right setHidden:NO];
                        }
                    }
                }
                //[self.view setUserInteractionEnabled:YES];
                
                [self.adImage setUserInteractionEnabled:YES];
            }
        }
    };
    [download start];
    
}

-(void)notification_alert:(NSNotification *)noti{
    
    /*
     * This () is called from StackImageView class for getting ads index and setting it into get_service_point()
     *
     */
    
    slide_ads = YES;
    isScrolling = NO;
    
    [self set_initial_parameter];
    [self.adImage setUserInteractionEnabled:NO];
    
    NSString *direction_text = [noti object];
    
    if ([direction_text isEqualToString:@"1"]) {
        
        direction = @"u";
        indexArtworkData = [self.adImage getIndex:nil]+1; // น้อยกว่าไป 1 เพราะ ตอน 0 มันยังไม่เข้า function นี้
        
    }else{
        
        direction = @"d";
        indexArtworkData = [self.adImage getIndex:nil]-1;
    }
    
    //    NSLog(@"package id %@",package_id[indexArtworkData]);
    
    if (indexArtworkData<0) {
        
        indexArtworkData = [totlePackage intValue]-1;
        
    }else if(indexArtworkData>=[totlePackage intValue]){
        
        indexArtworkData = 0;
    }
    
    [self icon_ads:indexArtworkData];
    [self get_service_point:indexArtworkData direction:direction];
}

-(void)icon_ads:(int)intdex{
    
    adsDownload = NO;
    
    /*
     * This () is used to set icon of artwork data
     *
     */
    
    //    NSLog(@"artwork_type_text %@ ",artwork_type[intdex]);
    
    NSString *artwork_type_text = [NSString stringWithFormat:@"%@",artwork_type[intdex]];
    
    if ([artwork_type_text isEqualToString:@"1"]) {
        [self.imageLeft setImage:[UIImage imageNamed:@"ic_website"]];
    }else if ([artwork_type_text isEqualToString:@"3"]) {
        [self.imageLeft setImage:[UIImage imageNamed:@"ic_read"]];
    }else if ([artwork_type_text isEqualToString:@"4"]) {
        [self.imageLeft setImage:[UIImage imageNamed:@"ic_video"]];
    }else{ // 2 or 5
        adsDownload = YES;
        [self.imageLeft setImage:[UIImage imageNamed:@"ic_dowload"]];
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [timer invalidate];
    [timeAlert invalidate];
    [timeCircle invalidate];
    
    isDisappear = YES;
    
    SET_USER_DEFAULT_INFO(@"NO",@"InLockscreen")
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"lock_screen" object:nil]; // Solution when NSNoti accessing in twice times
    
    if (slide_left==NO) {
        
        [Service post_collect_point:[self getUserToken] responseToken:response_token unlock_type:@"u" selfID:self block:^(id result){
            
        }];
        
    }else{
        
        [Service post_unlock:[self getUserToken] responseToken:response_token selfID:self block:^(id result) {
            
        }];
    }
}

-(void)setup_timer_set_text:(id)sender{
    
    /*
     * This () is called for update UILabel *timeTxt in every 1 min
     *
     */
    
    if (!timer) {
        
        timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateTextContentTopTime:) userInfo:nil repeats:YES];
        [timer fire]; // เปิด
        //// [timer invalidate]; ปิด timer
    }
}

-(void)updateTextContentTopTime:(id)sender{
    
    /*
     * This () is called for update UILabel *timeTxt in every 1 min
     *
     */
    
    [self.timeTxt setText:[CurrentDate_Temperature get_current_time]];
}

-(void)set_initial_objects{
    
    selectedAd = 0;
    networkError = NO;
    [arrowTopBtn setHidden:YES];
    [self.adImage setCanScrollDown:NO];
    
    [self.timeTxt setText:[NSString stringWithFormat:@"%@",[CurrentDate_Temperature get_current_time]]];
    [self.day setText:[NSString stringWithFormat:@"%@",[CurrentDate_Temperature get_date_format_dd_mm_yy]]];
    [self.date setText:[NSString stringWithFormat:@"%@",[CurrentDate_Temperature get_day_name]]];
    
    [self.day setFont:[PCFont fontRSU:18.0]];
    [self.date setFont:[PCFont fontRSU:16.0]];
    [self.timeTxt setFont:[PCFont fontRSU:25.0]];
    
    [PCLabel set_shadow_label:self.day];
    [PCLabel set_shadow_label:self.date];
    [PCLabel set_shadow_label:self.timeTxt];
    
    self.gifbox_icon.layer.masksToBounds = NO;
    self.gifbox_icon.layer.shadowOffset = CGSizeMake(0,1);
    self.gifbox_icon.layer.shadowRadius = 2;
    
    [self class_UI];
}

-(void)set_initial_parameter{
    
    [self.textLeft setText:@" "];
    [self.gifbox_icon setHidden:YES];
    [self.gifbox_icon_right setHidden:YES];
}

-(void)set_initial_view{
    
    [self set_initial_parameter];
    [self.centerLongTapBtn setConstant:0];
    [self.adImage setIsLoadFromImageName:NO];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
}


-(void)class_UI{
    
    SET_USER_DEFAULT_INFO(@"NO",@"logoutStatus")
    
    [BaseViewController setLineColor:self.lineHeader bottom:self.lineBottom];
    
    [self.lineHeader.layer setZPosition:10];
    
    [self setup_timer_set_text:nil];
    
    panGestureSlideBtn = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureSlideBtn_action:)];
    
    [self.longTapBtn addGestureRecognizer:panGestureSlideBtn];
    
    // Set Shimming Slide text
    FBShimmeringView *shimmeringViewLeft = [[FBShimmeringView alloc] initWithFrame:CGRectMake(-3,17,65,21)];
    [self.viewL addSubview:shimmeringViewLeft];
    
    UILabel *arrowLeft = [[UILabel alloc] initWithFrame:shimmeringViewLeft.bounds];
    arrowLeft.textAlignment = NSTextAlignmentCenter;
    arrowLeft.text = NSLocalizedString(@"< < < < <", nil);
    [arrowLeft setTextColor:[UIColor whiteColor]];
    [arrowLeft setFont:[UIFont fontWithName:@"Avenger" size:13.0]];
    
    shimmeringViewLeft.contentView = arrowLeft;
    shimmeringViewLeft.shimmeringDirection = FBShimmerDirectionLeft;
    shimmeringViewLeft.shimmering = YES;
    
    UILabel *arrowRight = [[UILabel alloc] initWithFrame:CGRectMake(4,17,65,21)];
    [arrowRight setTextColor:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.8]];
    [arrowRight setText:@"> > > > >"];
    [arrowRight setFont:[UIFont fontWithName:@"Avenger" size:13.0]];
    [self.viewR addSubview:arrowRight];
    
    // Set Circle UI
    [self set_circle_ui:30 sub_views:self.circleLeft view:self.viewLeft insert_view:self.imageLeft];
    [self set_circle_ui:30 sub_views:self.circleRight view:self.viewRight insert_view:self.iconRight];
    
    // Set Shadow of UIView
    [PCLabel set_shadow_label:self.textLeft];
    [PCLabel set_shadow_label:self.textRight];
    
    UIColor *colorOne = [UIColor colorWithRed:(0/255.0)  green:(0/255.0)  blue:(0/255.0)  alpha:0.7];
    UIColor *colorTwo = [UIColor clearColor];
    
    UIColor *colorOne_top = [UIColor colorWithRed:(0/255.0)  green:(0/255.0)  blue:(0/255.0)  alpha:0.6];
    UIColor *colorTwo_top = [UIColor clearColor];
    
    gradient = [CAGradientLayer layer];
    [gradient setFrame:CGRectMake(0,0,DEVICE_WIDTH,self.shadow.frame.size.height)];
    gradient.colors = [NSArray arrayWithObjects:(id)[colorTwo CGColor],(id)[colorOne CGColor],nil];
    
    [self.shadow.layer insertSublayer:gradient atIndex:0];
    
    gradientTop = [CAGradientLayer layer];
    [gradientTop setFrame:CGRectMake(0,0,DEVICE_WIDTH,self.shadow.frame.size.height)];
    gradientTop.colors = [NSArray arrayWithObjects:(id)[colorOne_top CGColor],(id)[colorTwo_top CGColor],nil];
    
    [self.shadowTop.layer insertSublayer:gradientTop atIndex:0];
    
    [self set_nsnotification];
}

-(void)set_circle_ui:(int)radius sub_views:(UIView *)subview view:(UIView *)view insert_view:(UIView *)insert_view{
    
    subview.alpha = 0;
    subview.layer.cornerRadius = radius;
    subview.backgroundColor = [UIColor whiteColor];
    [view addSubview:subview];
    [view insertSubview:insert_view aboveSubview:subview];
}

-(void)viewDidLayoutSubviews{
    
}

-(void)set_nsnotification{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notification_alert:) name:@"StackImageView" object:nil];
}

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)updateCircleView {
    
    /*
     * This () is called when Ads has point (#1 point or luckydraw point) and it is used to update circle's view in every 0.8 sec
     *
     */
    
    if(hide==YES){
        
        [self.circleLeft setAlpha:0.0];
        [self.circleRight setAlpha:0.0];
        
        hide = NO;
        
    }else{
        
        [self.circleLeft setAlpha:0.1];
        [self.circleRight setAlpha:0.1];
        
        hide = YES;
    }
}

-(void)go_to_unlock:(int)index{
    
    SET_USER_DEFAULT_INFO(response_token,@"Response_token_key")
    
    slide_left = YES;
    
//    [self get_user_info];
    
    unScene = [self loadNibWithName:@"UnlockWebViewController"];
    [unScene setWebScene:@"Hashtag 1" url:artwork_data[index]];
    [unScene set_loop_index_ads:column_ads index:index];
    
    [self setHiddenPCLoading:YES];
    [self.longTapBtn setUserInteractionEnabled:YES];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController pushViewController:unScene animated:YES];
    });
}

-(void)goto_login_scene{
    
    [BaseViewController setUserLogoutStatus:YES];
    
    LoginController *view_class = [self loadNibWithName:@"LoginController"];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController pushViewController:view_class animated:YES];
    });

/*    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController * loginView = [storyboard   instantiateViewControllerWithIdentifier:@"LoginController"] ;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController pushViewController:loginView animated:YES];
    });
 */
}

-(void)go_to_home_scene{
    
    [self addPCLoading:self];
    [self setHiddenPCLoading:NO];
    [self setLoadingLockScreen];
    
    [self.view setUserInteractionEnabled:NO];
    
    if (right_point==YES)
    {
        [self set_service_get_point:@"c"];
        
    }else{
        [self gotoHome];
    }
}

-(void)set_service_get_point:(NSString *)unlock_type
{
    [Service post_collect_point:[self getUserToken] responseToken:response_token unlock_type:unlock_type selfID:self block:^(id result) {
        
        [BaseViewController log_comment:@"Result colelect point" text:result];
        
        NSString *result_text = [NSString stringWithFormat:@"%@",result];
        
        if (![result_text isEqualToString:@"error"]) {
            
            if([result count]==0){
                
                [self alert_login_again];
                
            }else{
                
                NSString *success_text = [NSString stringWithFormat:@"%@",result[@"success"]];
                
                if ([success_text isEqualToString:@"1"]) {
                    
                    id userInfo;
                    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                    userInfo = (id)[userDefaults objectForKey:@"UserInfo"];
                    
                    NSMutableDictionary *dic_copy = [userInfo mutableCopy];
                    
                    NSString *luckydraw_status = [NSString stringWithFormat:@"%@",result[@"luckydraw"]];
                    
                    if ([luckydraw_status isEqualToString:@"1"]) { // luckydraw
                        
                        [dic_copy removeObjectForKey:@"luckydraw"];
                        [dic_copy setObject:result[@"luckydraw_count"] forKey:@"luckydraw"];
                        
                        [self alert_noti:@"คุณได้รับสิทธิ์คะแนนสะสม 1 สิทธิ์" sub_title:[NSString stringWithFormat:@"คุณมีสิทธิ์สะสมทั้งหมด %@ สิทธิ์",result[@"luckydraw_count"]]];
                        
                    }else if ([luckydraw_status isEqualToString:@"0"]){ // point
                        
                        [dic_copy removeObjectForKey:@"earn"];
                        [dic_copy setObject:result[@"user_point"] forKey:@"earn"];
                        
                        [self alert_noti:@"Hashtag 1" sub_title:[NSString stringWithFormat:@"คุณได้รับคะแนน %@ คะแนน",result[@"point"]]];
                    }
                    
                    [userDefaults setObject:dic_copy forKey:@"UserInfo"];
                    [BaseViewController log_comment:@"Update user info %@" text:[NSString stringWithFormat:@"%@",dic_copy]];
                }
            }
        }
        
        if (adsDownload == NO) {
            [self action_whenLoadingFinish];
        }
    }];
}

-(void)action_whenLoadingFinish{
    
    [self.view setUserInteractionEnabled:YES];
    [self gotoHome];
}

-(void)gotoHome{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main"
                                                         bundle:nil];
    XLPageStripMenuBarController *homeScene =
    (XLPageStripMenuBarController *)
    [storyboard instantiateViewControllerWithIdentifier:@"XLPageStripMenuBarController"];
    [timer invalidate];
    [timeCircle invalidate];
    
    SET_USER_DEFAULT_INFO(@"NO",@"Should_Update_USERINFO")
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController pushViewController:homeScene animated:YES];
    });
    
    [self setHiddenPCLoading:YES];
}

@end
