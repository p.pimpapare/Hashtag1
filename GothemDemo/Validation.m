//
//  Validation.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/7/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "Validation.h"

@implementation Validation

+(BOOL)validateEmail:(NSString *)emailStr{
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailStr];
}

+(BOOL)validatePassword:(NSString *)passwordStr{
    
    if((passwordStr.length<1)||(passwordStr.length>30)){
        return false;
    }else{
        return true;
    }
}

@end
