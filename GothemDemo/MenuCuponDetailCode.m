//
//  MenuCuponDetailCode.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/21/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "MenuCuponDetailCode.h"

@implementation MenuCuponDetailCode

-(void)awakeFromNib{
    
    [super awakeFromNib];
    timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateCountdown) userInfo:nil repeats: YES];
    
    [self.layer setCornerRadius:2];
    [self setFrame:CGRectMake(0,0,DEVICE_WIDTH,DEVICE_HEIGHT)];
    
    [BaseViewController setLineColor:self.lineHeader bottom:self.lineBottom];
    
    UIImage *code39Image = [Code39 code39ImageFromString:[NSString stringWithFormat:@"%@",codeText] Width:231 Height:83];
    self.codeImage.image = code39Image;
    
    // create circle progress view
    self.clockProgressView.trackTintColor = [UIColor whiteColor];
    self.clockProgressView.progressTintColor = [PCColor color_A8101D];
    self.clockProgressView.thicknessRatio = 1.0f;
    self.clockProgressView.clockwiseProgress = NO;
    
    timeDelay = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(addAnimationClock) userInfo:nil repeats: YES];
}

-(void)addAnimationClock{
    
    if(closeAnimation==YES){
        
        [self startAnimation:0];
        
    }else{
        
        [self startAnimation:0.03];
    }
}

// create QR code
- (CIImage *)createQRForString:(NSString *)qrString{
    
    //    NSLog(@"%@",qrString);
    // Need to convert the string to a UTF-8 encoded NSData object
    NSData *stringData = [qrString dataUsingEncoding: NSISOLatin1StringEncoding];
    
    // Create the filter
    CIFilter *qrFilter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    // Set the message content and error-correction level
    [qrFilter setValue:stringData forKey:@"inputMessage"];
    [qrFilter setValue:@"M" forKey:@"inputCorrectionLevel"];
    
    // Send the image back
    return qrFilter.outputImage;
}

//uiqrcodegenerator
- (UIImage *)createNonInterpolatedUIImageFromCIImage:(CIImage *)image withScale:(CGFloat)scale
{
    // Render the CIImage into a CGImage
    CGImageRef cgImage = [[CIContext contextWithOptions:nil] createCGImage:image fromRect:image.extent];
    
    // Now we'll rescale using CoreGraphics
    UIGraphicsBeginImageContext(CGSizeMake(image.extent.size.width * scale, image.extent.size.width * scale));
    CGContextRef context = UIGraphicsGetCurrentContext();
    // We don't want to interpolate (since we've got a pixel-correct image)
    CGContextSetInterpolationQuality(context, kCGInterpolationNone);
    CGContextDrawImage(context, CGContextGetClipBoundingBox(context), cgImage);
    // Get the image out
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    // Tidy up
    UIGraphicsEndImageContext();
    CGImageRelease(cgImage);
    return scaledImage;
}

- (IBAction)barCodeBtn:(id)sender {
    
    UIImage *code39Image = [Code39 code39ImageFromString:[NSString stringWithFormat:@"%@",codeText] Width:231 Height:83];
    self.codeImage.image = code39Image;
    
    [self.barCodeBtn setImage:[UIImage imageNamed:@"btn_barcode_1"] forState:UIControlStateNormal];
    [self.qrCodeBtn setImage:[UIImage imageNamed:@"btn_qrcode_0"] forState:UIControlStateNormal];
}

- (IBAction)qrCodeBtn:(id)sender {
    
    CIImage *qrCode = [self createQRForString:[NSString stringWithFormat:@"%@",codeText]];
    UIImage *qrCodeImg = [self createNonInterpolatedUIImageFromCIImage:qrCode withScale:3*[[UIScreen mainScreen] scale]];
    self.codeImage.image = qrCodeImg;
    
    [self.qrCodeBtn setImage:[UIImage imageNamed:@"btn_qrcode_1"] forState:UIControlStateNormal];
    [self.barCodeBtn setImage:[UIImage imageNamed:@"btn_barcode_0"] forState:UIControlStateNormal];
}
- (IBAction)usedCodeBtn:(id)sender {
    
    timer = nil;
    timeDelay = nil;
    self.clockTimer = nil;
    
    closeAnimation = YES;
    
    [timer invalidate];
    [timeDelay invalidate];
    [self.clockTimer invalidate];
}

-(void)setMenuCoupon:(NSString *)titleHeader detail:(NSString *)detail time:(NSString *)time code:(NSString *)code{
    
    codeText = code;
    countTime = (int)[time integerValue];
    
    [self.titleHeader setText:[NSString stringWithFormat:@"%@",titleHeader]];
    [self.detailTextView setText:[NSString stringWithFormat:@"%@",detail]];
    [self.userId setText:[NSString stringWithFormat:@"%@",code]];
    
    // encode defualts barcode at first times
    UIImage *code39Image = [Code39 code39ImageFromString:[NSString stringWithFormat:@"%@",code] Width:231 Height:83];
    self.codeImage.image = code39Image;
}

- (void)startAnimation:(float)times{
    
    self.clockTimer = [NSTimer scheduledTimerWithTimeInterval:times
                                                       target:self
                                                     selector:@selector(progressChange)
                                                     userInfo:nil
                                                      repeats:YES];
}

- (void)progressChange{
    
    if (closeAnimation==NO) {
        CGFloat progress = ![self.clockTimer isValid] ? : self.clockProgressView.progress + 0.01f;
        [self.clockProgressView setProgress:progress animated:YES];
        
        if (self.clockProgressView.progress >= 1.0f && [self.clockTimer isValid]) {
            [self.clockProgressView setProgress:0.f animated:YES];
        }
    }
}

// countdown time's label
-(void) updateCountdown {
    
    --countTime;
    self.timeLabel.text = [self calculateTimeStringWithTimeInteval:countTime];
    
    if ([self.timeLabel.text isEqualToString:@"00:00"]) {
        [self action:@"finishedUsedCoupon"];
    }
}

-(NSString *)calculateTimeStringWithTimeInteval:(int)secTotal{
    
    //    int day = secTotal / (60*60*24);
    //    secTotal %= (60*60*24);
    //
    //    int hour = secTotal / (60*60);
    //    secTotal %= (60*60);
    
    int minute = secTotal / 60;
    secTotal %= (60);
    
    int second = secTotal;
    
    //    return [NSString stringWithFormat:@"Day:%d Hour:%d Minute:%d Second:%d",day,hour,minute,second];
    return [NSString stringWithFormat:@"%.2d:%.2d",minute,second];
}

-(NSString *)action:(NSString *) selecttag{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"menuUserCupon" object:[NSString stringWithFormat:@"%@",selecttag]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"menuUserCupon_onMyCoupon" object:[NSString stringWithFormat:@"%@",selecttag]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"menuUserCupon_onHotPromo" object:[NSString stringWithFormat:@"%@",selecttag]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"menuUserCupon_onMyCoupon3" object:[NSString stringWithFormat:@"%@",selecttag]];
    
    return selecttag;
}

@end
