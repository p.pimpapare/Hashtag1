//
//  ContainerViewController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/22/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

// This class is a root class (Container's class) that it is used for setting newsfeed scene

#import <UIKit/UIKit.h>

@interface ContainerViewController : BaseViewController

-(void)checkUser;
-(void)addNewfeed;
-(void)hideNewfeed;
-(void)showNewfeed;

@property NSString *selectedBtn;
-(NSString *)action:(NSString *) selecttag;

@end
