//
//  PCFont.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 4/1/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

// This class is used for setting font RSU that is used in mutiply scene.

#import <UIKit/UIKit.h>

@interface PCFont : UIFont

+(UIFont *)fontRSU:(float)sizeText;

@end
