//
//  SequeSettingQAController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/7/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "SequeSettingQAController.h"

@interface SequeSettingQAController ()

@end

@implementation SequeSettingQAController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self class_UI];
}

- (void)class_UI {
    LINE_WITH_STATUSBAR_HEADER_NORMAL;
}

- (IBAction)backBtn:(id)sender {
    GOTO_PREVIOUS;
}

-(void)setTiltleHeader:(NSString *)title{
    // set ไม่ได้จริง
    [self.titleText setText:[NSString stringWithFormat:@"%@",title]];
}

@end
