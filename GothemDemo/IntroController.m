//
//  introController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 4/4/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "IntroController.h"

@interface IntroController ()
@end

@implementation IntroController

-(void)viewWillAppear:(BOOL)animated{
    
    GOOGLE_ANALYTICS(@"IntroController");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self set_initail_objects];
}

-(void)set_initail_objects{
    
    image_array = @[@"tutorial1",@"tutorial2",@"tutorial3",@"tutorial4"];
    [self set_collection];
}

-(void)fromFirstTimesApplicaitonLauch:(BOOL)firstimes{
    
    firstTimes = firstimes;
}

-(void)set_collection{
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    
    layout.minimumLineSpacing=0;
    layout.minimumInteritemSpacing=0;
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    
    _collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0,0,DEVICE_WIDTH,DEVICE_HEIGHT) collectionViewLayout:layout];
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    [_collectionView setShowsHorizontalScrollIndicator:NO];
    
    [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [_collectionView setBackgroundColor:[UIColor clearColor]];
    
    [self.viewIntro addSubview:_collectionView];
    
    [_collectionView setPagingEnabled:YES];
    
    [_collectionView registerClass:[PCCollectionCell class] forCellWithReuseIdentifier:@"Cell"];
    [_collectionView registerNib:[UINib nibWithNibName:@"HowToUseCollectionCell" bundle:nil]  forCellWithReuseIdentifier:@"Cell"];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [image_array count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    HowToUseCollectionCell *cell=(HowToUseCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    if(cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"HowToUseCollectionCell" owner:self options:nil][0];
    }
    
    [cell setHowtoImage:image_array[indexPath.row]index:(int)indexPath.row];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(DEVICE_WIDTH,DEVICE_HEIGHT-64);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    // check cell by tab
        if (indexPath.row==0) {
     [_collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:1 inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
     }else if (indexPath.row==1) {
     [_collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:2 inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
     }else if (indexPath.row==2) {
     [_collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:3 inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
     }else if (indexPath.row==3){
         [self goToLogin];
     }
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    // TODO: Deselect item
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0,0,0,0);
}

-(void)goToLogin{
    
    LoginController *view_class = [[NSBundle mainBundle] loadNibNamed:@"LoginController" owner:self options:nil][0];
    [self.navigationController pushViewController:view_class animated:YES];

/*    UIStoryboard *storybord = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController * loginView = [storybord   instantiateViewControllerWithIdentifier:@"LoginController"] ;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController pushViewController:loginView animated:YES];
    });
 */
}

@end
