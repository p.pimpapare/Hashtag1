
//  Created by pimpaporn chaichompoo on 2/11/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface NewFeedController : BaseViewController<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate>{
    
    __weak IBOutlet UIScrollView *scrollView;
    UICollectionView *_collectionView;
    int selectedTabMenu;
    
    // type 1
    NSMutableArray *itemGuid;
    NSMutableArray *title;
    NSMutableArray *thumbnail;
    BOOL finisedLoadType1;
    
    // type 2
    NSMutableArray *item_id2;
    NSMutableArray *itemGuid2;
    NSMutableArray *title2;
    NSMutableArray *thumbnail2;
    BOOL finisedLoadType2;
    NSString *imageUrl2;
    
    // type 3
    NSMutableArray *item_id3;
    NSMutableArray *itemGuid3;
    NSMutableArray *title3;
    NSMutableArray *thumbnail3;
    BOOL finisedLoadType3;
    NSString *imageUrl3;
    
    NSArray *newfeedMenu;
    
    //loction
    CLLocationManager *locationManager;
    NSString *oldLocation;
}

@property (weak, nonatomic) IBOutlet UILabel *temperature;
@property (weak, nonatomic) IBOutlet UILabel *day;
@property (weak, nonatomic) IBOutlet UILabel *currentPlace;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *temp_c;

@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIView *statusBar;
@property (weak, nonatomic) IBOutlet UIView *viewError;

@property (weak, nonatomic) IBOutlet UIView *viewHeader;

@property (weak, nonatomic) IBOutlet UIView *lineHeader;
@property (weak, nonatomic) IBOutlet UIImageView *lineBottom;

@property (weak, nonatomic) PCNavigationBarView *navBar;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btn_newsfeed_center;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingView;

@property (weak, nonatomic) IBOutlet UIImageView *btn_back;

// Temperature
@property double celsius;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top_line_header;

//-(void)openUrl:(NSString *)url;

@end
