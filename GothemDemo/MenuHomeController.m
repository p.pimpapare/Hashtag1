//
//  MenuHomeController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/19/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "MenuHomeController.h"

@interface MenuHomeController ()<KDCycleBannerViewDataSource, KDCycleBannerViewDelegate>{
    
    NotReadyAlert *notReadyAlert;
    IBOutlet UIImageView *gearimage;
}

@property (nonatomic,retain) MenuHomeController *menuHome;
@property (weak, nonatomic) IBOutlet KDCycleBannerView *cycleBannerViewTop;
@property (strong, nonatomic) KDCycleBannerView *cycleBannerViewBottom;

@end

@implementation MenuHomeController

KDCycleDelegate(@"picture_1_2",3)

-(void)viewWillAppear:(BOOL)animated{
    
    NSMutableArray *view_menu = [[NSMutableArray alloc]initWithObjects:self.menu1,self.menu2,self.menu3,self.menu4,nil];
    
    for (UIView *view in view_menu) {
        [view setUserInteractionEnabled:YES];
    }
    
    [self.view insertSubview:self.cycleBannerViewTop belowSubview:pcblack];
    
    GOOGLE_ANALYTICS(@"MenuHomeController");
    
    [self set_user_profile_info];
    
    [self check_alert_update];
    [self check_version_update];
}

-(void)check_version_update{
    
    NSString *checkTime = [NSString stringWithFormat:@"%@",(id)[[NSUserDefaults standardUserDefaults] objectForKey:@"TimesAccess_ForUpdate"]];
    NSString *currentTime = [NSString stringWithFormat:@"%@", [CurrentDate_Temperature get_date_format_dd_mm_yy]];
    
    if (![checkTime isEqualToString:currentTime]) {
        sameTime = NO;
    }else{
        sameTime = YES;
    }
    
    GET_USER_DEFAULT_INFO(@"READMORE_UPDATE")
    
    NSString *result_text = [NSString stringWithFormat:@"%@",userInfo];
    
    if ([result_text isEqualToString:@"YES"]) {
        readmore = YES;
    }else{
        readmore = NO;
    }
    
    // forceupdate
    [Service get_update_version:[self getUserToken] selfID:self block:^(id result) {
        
        NSString *result_text = [NSString stringWithFormat:@"%@",result];
        
        if (![result_text isEqualToString:@"error"]) {
            
            SET_USER_DEFAULT_INFO(result,@"UpdateVersionInfo")
            
            currentVersion_Str = [NSString stringWithFormat:@"%@",result[@"current"]];
            update_type = [NSString stringWithFormat:@"%@",result[@"update_type"]];
            detailVersion_Str = [NSString stringWithFormat:@"%@",result[@"detail"]];
            
            /*
             200 up to date
             300 new feature
             400 bugfix
             
             400 บังคับให้ดาวน์โหลด
             */
            
            if (![update_type isEqualToString:@"200"])
            {
                if ([update_type isEqualToString:@"400"])
                {
                    [self setForceUpdateVersion:currentVersion_Str detail:detailVersion_Str];
                }
                else
                {
                    [userDefaults setObject:@"YES" forKey:@"Should_Update"];
                    
                    if ((sameTime==NO)||(readmore==YES))
                    {
                        [self setUpdateVersion:currentVersion_Str detail:detailVersion_Str];
                    }
                }
            }else
            {
                [userDefaults setObject:@"NO" forKey:@"Should_Update"];
            }
            
            [[NSUserDefaults standardUserDefaults] setValue:[CurrentDate_Temperature get_date_format_dd_mm_yy] forKey:@"TimesAccess_ForUpdate"];
        }
    }];
}

-(void)check_alert_update{
    
    [self check_version_update];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addPCBlack:self];
    [self setHiddenPCBlack:YES];
    
    [self class_UI];
    [self getService_Advertising];
}

//-(void)viewDidLayoutSubviews{  // ถ้าviewของ viewcontroller มีการเปลี่ยนมันจะเข้า methodนี้
//    [super viewDidLayoutSubviews];
//    gearimage.superview.clipsToBounds = YES;
//}

-(void)getService_Advertising{
    
    advertising_array = [[NSMutableArray alloc]init];
    
    GET_USER_DEFAULT_INFO(@"AdvertisingInfo1")
    
    for (int i=0;i<[userInfo[@"banner"] count];i++) {

        [advertising_array addObject:userInfo[@"banner"][i][@"banner_image"]];
    }
}

-(void)set_user_profile_info{
    
    [self setUserProfile:self.nameLabel numberT1C:self.noLabel balance:self.balanceLabel earn:self.earnLabel luckydraw:self.luckyDraw_point];
}

-(void)class_UI{
    
    [self.viewMenu setUserInteractionEnabled:YES];
    
    [self.subText1 setText:@"เพิ่มความคุ้มค่า ด้วยโปรโมชั่น\nพิเศษสำหรับคุณ"];
    [self.subText5 setText:@"เพื่อลุ้นรับของรางวัล\nพิเศษมากมาย"];
    
    if (IS_IPAD==YES) {

        int heightHotPromo_num = 0, heightMenu5_num = 0, heightScrollView_num = 0, heightAdvertising_num = 0;
        
        if (DEVICE_HEIGHT==IPAD_PRO_HEIGHT) {
            
            heightHotPromo_num = 480;
            heightMenu5_num = 480;
            heightScrollView_num = 730;
            heightAdvertising_num = HEIGHT_ADVERTISING_IPAD_PRO;

        }else{
            
            heightHotPromo_num = 280;
            heightMenu5_num = 280;
            heightScrollView_num = 480;
            heightAdvertising_num = HEIGHT_ADVERTISING_IPAD;
        }
        
        [self.heightHotPromo setConstant:heightHotPromo_num];
        [self.heightMenu5 setConstant:heightMenu5_num];
        [self.heightScrollView setConstant:heightScrollView_num];
        [self.heightAdvertising setConstant:heightAdvertising_num];
        
        [self.headerText1 setFont:[PCFont fontRSU:42.0]];
        [self.headerText2 setFont:[PCFont fontRSU:25.0]];
        [self.headerText3 setFont:[PCFont fontRSU:25.0]];
        [self.headerText4 setFont:[PCFont fontRSU:25.0]];
        [self.headerText5 setFont:[PCFont fontRSU:32.0]];
        
        [self.subText1 setFont:[PCFont fontRSU:30.0]];
        [self.subText2 setFont:[PCFont fontRSU:18.0]];
        [self.subText3 setFont:[PCFont fontRSU:18.0]];
        [self.subText4 setFont:[PCFont fontRSU:18.0]];
        [self.subText5 setFont:[PCFont fontRSU:22.0]];
        
        [self.subText2 setText:@"สิทธิพิเศษพร้อมใช้"];
        [self.subText3 setText:@"พบข่าวสารและโปรโมชั่นของ\nเดอะวันคาร์ด"];
        
    }else{
        
        if (DEVICE_WIDTH==PHONE45_WIDTH) {
            
            [self.cycleBannerViewBottom setFrame:CGRectMake(0,0,DEVICE_WIDTH,HEIGHT_ADVERTISING)];
            [self.heightHotPromo setConstant:130];
            [self.heightMenu5 setConstant:130];
            [self.heightScrollView setConstant:235];
            
        }else if (DEVICE_WIDTH>=PHONEPLUS){
            
            [self.heightAdvertising setConstant:HEIGHT_ADVERTISING+42];
            [self.heightHotPromo setConstant:190];
            [self.heightMenu5 setConstant:190];
            [self.heightScrollView setConstant:350];
            
        }else{
            
            [self.heightAdvertising setConstant:HEIGHT_ADVERTISING+25];
            [self.scrollView setScrollEnabled:NO];
            [self.heightHotPromo setConstant:175];
            [self.heightMenu5 setConstant:175];
            [self.heightScrollView setConstant:310];
        }
        
        [self.subText2 setText:@"สิทธิพิเศษ\nพร้อมใช้"];
        [self.subText3 setText:@"พบข่าวสารและ\nโปรโมชั่นของ\nเดอะวันคาร์ด"];
        
        [self.headerText1 setFont:[PCFont fontRSU:26.0]];
        [self.headerText2 setFont:[PCFont fontRSU:16.0]];
        [self.headerText3 setFont:[PCFont fontRSU:16.0]];
        [self.headerText4 setFont:[PCFont fontRSU:16.0]];
        [self.headerText5 setFont:[PCFont fontRSU:16.0]];
        [self.subText1 setFont:[PCFont fontRSU:17.0]];
        [self.subText2 setFont:[PCFont fontRSU:12.0]];
        [self.subText3 setFont:[PCFont fontRSU:12.0]];
        [self.subText4 setFont:[PCFont fontRSU:12.0]];
        [self.subText5 setFont:[PCFont fontRSU:12.0]];
    }
    
    [self setBG_Logo];
    [self setBg];
    
    [self.scrollView addSubview:self.viewMenu];
    [self.scrollView setContentSize:self.viewMenu.frame.size];
    
    NSMutableArray *label_array = [[NSMutableArray alloc]initWithObjects:self.balanceLabel,self.earnLabel,self.nameLabel,self.noLabel,self.noTopic,self.earnTopic,self.balanceTopic,self.point1,self.point2,nil];
    
    for (UILabel *label in label_array) {
        [label setFont:[PCFont fontRSU:16.0]];
    }

    NSMutableArray *view_array = [[NSMutableArray alloc]initWithObjects:self.viewMenu1,self.viewMenu2,self.viewMenu3,self.viewMenu4,self.viewMenu5,nil];
    
    for (UIView *view in view_array) {
        [view.layer setCornerRadius:2];
        [PCShadow set_shadow:0.2 shadowRadius:1 shadowOpacity:0.3 view:view];
    }
    
    [BaseViewController setLineColor:self.line1 bottom:self.line2];
    [BaseViewController setLineColor:self.line3 bottom:self.line4];
    
    [self.line5 setBackgroundColor:[PCColor color_A8101D]];
    
    [self setPagerAdvertising];
    [self check_invite];
}

-(void)check_invite{
    
    [self.subText4 setText:@"ชวนเพื่อนใช้แอปฯ"];
}

-(void)setForceUpdateVersion:(NSString *)version detail:(NSString *)details{
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"อัพเดทเวอร์ชั่นใหม่" message:[NSString stringWithFormat:@"เวอร์ชั่น %@\n%@",version,details] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* btn_update_now = [UIAlertAction
                                         actionWithTitle:@"อัพเดท"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                                             
                                             [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://**ax.itunes**.apple.com/app/hashtag-1/id1109947887?ls=1&mt=8"]];
                                             // app/ตามด้วยลิ้งแอพเรา
                                             
                                         }];
        
        [alert addAction:btn_update_now];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }];
}

-(void)setUpdateVersion:(NSString *)version detail:(NSString *)details{
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"อัพเดทเวอร์ชั่นใหม่" message:[NSString stringWithFormat:@"เวอร์ชั่น %@\n%@",version,details] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* btn_update_now = [UIAlertAction
                                         actionWithTitle:@"อัพเดทตอนนี้"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                                             
                                             SET_USER_DEFAULT_INFO(@"NO",@"READMORE_UPDATE")
                                             
                                             [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://**ax.itunes**.apple.com/app/hashtag-1/id1109947887?ls=1&mt=8"]];
                                             // app/ตามด้วยลิ้งแอพเรา
                                             
                                         }];
        
        UIAlertAction* btn_update_later = [UIAlertAction
                                           actionWithTitle:@"อัพเดทภายหลัง"
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action){
                                               
                                               SET_USER_DEFAULT_INFO(@"NO",@"READMORE_UPDATE")
                                           }];
        
        UIAlertAction* btn_readmore = [UIAlertAction
                                       actionWithTitle:@"อ่านรายละเอียด"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action){
                                           
                                           SET_USER_DEFAULT_INFO(@"YES",@"READMORE_UPDATE")
                                           
                                           [self goToReadMoreUpdate:currentVersion_Str detailV:detailVersion_Str];
                                       }];
        
        [alert addAction:btn_update_now];
        [alert addAction:btn_update_later];
        [alert addAction:btn_readmore];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

- (IBAction)menu1:(id)sender {
    
    [self.menu1 setUserInteractionEnabled:NO];
    [self goToMenuCouponListController:@"โปรโมชั่น" brand_id:nil];
}

- (IBAction)menu2:(id)sender {
    
    [self.menu2 setUserInteractionEnabled:NO];
    [self goToMenuCoupon:@"รายการคูปอง" type:@"1"];
}

- (IBAction)menu3:(id)sender {
    
    [self.menu3 setUserInteractionEnabled:NO];
    NSURL *facebookURL = [NSURL URLWithString:@"fb://profile/1563548707272160"];
    
    if (![[UIApplication sharedApplication] canOpenURL:facebookURL]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://facebook.com/Hashtag1app/"]];
    }
    [[UIApplication sharedApplication] openURL:facebookURL];
    
}

- (IBAction)menu4:(id)sender {
    
    [self.menu4 setUserInteractionEnabled:NO];
    
    [Service get_app_url_invite:[self getUserToken] selfID:self block:^(id result) {
        
        NSString *reult_text = [NSString stringWithFormat:@"%@",result];
        
        if (![reult_text isEqualToString:@"error"]){
            
            shareUrl = [NSString stringWithFormat:@"%@",result[@"url"]];
            [BaseViewController log_comment:@"Share URL" text:shareUrl];
            
            NSArray *itemsToShare = @[shareUrl];
            
            UIActivityViewController* activityController = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
            
            [activityController setExcludedActivityTypes:
             @[UIActivityTypeAssignToContact,
               UIActivityTypeCopyToPasteboard,
               UIActivityTypePrint,
               UIActivityTypeSaveToCameraRoll,
               UIActivityTypeAddToReadingList,
               UIActivityTypeAirDrop,
               @"com.apple.reminders.RemindersEditorExtension",
               @"com.apple.mobilenotes.SharingExtension"
               ]];
            
            
            if ( [activityController respondsToSelector:@selector(popoverPresentationController)] ) {
                // iOS8
                activityController.popoverPresentationController.sourceView = self.viewMenu4; // เฉพาะ ipad เวลา share
            }
            
            [self presentViewController:activityController animated:YES completion:nil];
        }
    }];
}

- (IBAction)menu5:(id)sender {
    [self goToLuckyPoint];
}

-(void)shareInfo:(NSString *)shareURL{
    [self share_activity:[NSString stringWithFormat:@"%@",shareURL]];
}

- (IBAction)goToMenu3:(id)sender {
    [self goToReceivePoint];
}

-(void)display_notAvaliable:(NSString *)text{
    
    notReadyAlert = [[NSBundle mainBundle] loadNibNamed:@"NotReadyAlert" owner:self options:nil][0];
    [notReadyAlert setTextWarning:[NSString stringWithFormat:@"%@",text]];
    [self.view addSubview:notReadyAlert];
    [self.view setUserInteractionEnabled:NO];
    
    [NSTimer scheduledTimerWithTimeInterval:2.0
                                     target:self
                                   selector:@selector(closeAlert)
                                   userInfo:nil
                                    repeats:NO];
}

-(void)closeAlert{
    [notReadyAlert removeFromSuperview];
    [self.view setUserInteractionEnabled:YES];
}

-(NSString *)action:(NSString *) selecttag{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"select_menu_bar" object:[NSString stringWithFormat:@"%@",selecttag]];
    [self setTagMenu:selecttag];
    return selecttag;
}

-(void)goToMenuCouponListController:(NSString *)headerText brand_id:(NSString *)b_ID{
    
    MenuCouponListController *view_class = [self loadNibWithName:@"MenuCouponListController"];
    [view_class setHeaderText:[NSString stringWithFormat:@"%@",headerText]brand_id:b_ID];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController pushViewController:view_class animated:YES];
    });
}

-(void)goToReadMoreUpdate:(NSString *)version detailV:(NSString *)detailV{
    
    ReadMoreUpdateController *view_class = [self loadNibWithName:@"ReadMoreUpdateController"];
    [view_class setDetail_ReadMore:version detail:detailV];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController pushViewController:view_class animated:YES];
    });
}

-(void)goToMenu3TableView:(NSString *)title menu:(int)menuNum{
    
    UserHistoryTableView *view_class = [self loadNibWithName:@"UserHistoryTableView"];
    [view_class setHeaderText:[NSString stringWithFormat:@"%@",title]menuNum:menuNum];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController pushViewController:view_class animated:YES];
    });
}

-(void)goToMenuCoupon:(NSString *)title type:(NSString *)typeNum{
    
    MenuUserCuponController *view_class = [self loadNibWithName:@"MenuUserCuponController"];
    [view_class setTiltleHeader:[NSString stringWithFormat:@"%@",title]type:[NSString stringWithFormat:@"%@",typeNum]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController pushViewController:view_class animated:YES];
    });
}

-(void)goToLuckyPoint{
    [self pushViewCtrlWithViewCtrlName:@"LuckyPointController"];
}

-(void)goToReceivePoint{
    
    UIStoryboard *storybord = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController * view_class = [storybord   instantiateViewControllerWithIdentifier:@"ContainerRecievePointController"] ;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:view_class animated:YES completion:nil];
    });
}

@end
