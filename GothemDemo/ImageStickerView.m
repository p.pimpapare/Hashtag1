//
//  ImageStickerView.m
//  testCode
//
//  Created by Akarapas Wongkaew on 5/10/2559 BE.
//  Copyright © 2559 Akarapas Wongkaew. All rights reserved.
//

#import "ImageStickerView.h"

@interface ImageStickerView()<UIScrollViewDelegate>
{
    UIScrollView *scrollViewImageUser;
}
@end

@implementation ImageStickerView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib{
    [super awakeFromNib];
    [self setup_object];
 
}

- (id)initWithFrame:(CGRect)theFrame {
    self = [super initWithFrame:theFrame];
    if (self) {
        [self setup_object];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (self) {
        [self setup_object];
    }
    return self;
}
// ########################

-(void)setup_object
{
    if (!scrollViewImageUser) {
        scrollViewImageUser = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        [scrollViewImageUser setAutoresizingMask:-1];
        [scrollViewImageUser setMaximumZoomScale:5.0];
        [scrollViewImageUser setMinimumZoomScale:1.0];
        [scrollViewImageUser setDelegate:self];
        [self addSubview:scrollViewImageUser];
    }
    if (!_imageUser) {
        _imageUser = [[UIImageView alloc] initWithFrame:CGRectMake(0,20, self.frame.size.width, self.frame.size.height)];
        [_imageUser setAutoresizingMask:-1];
        [_imageUser setUserInteractionEnabled:NO];
        [_imageUser setContentMode:UIViewContentModeScaleAspectFit];
        [scrollViewImageUser addSubview:_imageUser];
    }
    
    if (!_imageSticker) {
        
        /*
         if (DEVICE_WIDTH==PHONE4) {
         _imageSticker = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width/2-120,self.frame.size.height/2-165,240,330)];
         }else{
         _imageSticker = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width/2-120,self.frame.size.height/2-165,240,330)];
         }
         */
        
        _imageSticker = [[UIImageView alloc] initWithFrame:CGRectMake(0,20, self.frame.size.width, self.frame.size.height)];
        [_imageSticker setAutoresizingMask:-1];
        [_imageSticker setUserInteractionEnabled:NO];

        [_imageSticker setAlpha:0.5];
//        [self addSubview:_imageSticker];
    }
}

-(void)setImageStickerWithImage:(UIImage *)image
{
    if (_imageSticker) {
        [_imageSticker setImage:image];
    }
}

-(void)setImageUserWithImage:(UIImage *)image
{
    if (_imageUser) {
        [_imageUser setImage:image];
    }
}

-(UIImage *)getImageSticker
{
    return [self imageWithView:self];
}

-(UIImage *)imageWithView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

// ########################

// ######################## UIScrollViewDelegate

- (nullable UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return _imageUser;
}

// ########################

@end
