//
//  ContainerRecievePointController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/31/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "ContainerRecievePointController.h"

@interface ContainerRecievePointController ()

@end

@implementation ContainerRecievePointController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self class_UI];
}

-(void)class_UI{
    [self.statusBar setBackgroundColor:[PCColor color_424242]];
    [PCColor set_shadow_headerBar:self.menuBar];
}

- (IBAction)backBtn:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
