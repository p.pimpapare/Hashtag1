//
//  MenuDetailUsedCoupon.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/17/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

// This class is a pop up view that receive user's password and validate before the user can use coupon 

#import <UIKit/UIKit.h>

@interface MenuDetailInputCode : UIView

@property (weak, nonatomic) IBOutlet UIImageView *logo;
@property (weak, nonatomic) IBOutlet UITextField *codeTextfeild;

@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;

@property (weak, nonatomic) IBOutlet UILabel *emailLabel;

@property (weak, nonatomic) IBOutlet UIView *lineHeader;
@property (weak, nonatomic) IBOutlet UIView *lineGray;
@property (weak, nonatomic) IBOutlet UIView *lineBottom;

@property (weak, nonatomic) IBOutlet UIButton *black_btn;

-(void)set_logo_coupon:(NSString *)logoImage;

@end
