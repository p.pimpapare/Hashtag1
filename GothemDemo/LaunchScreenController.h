//
//  LaunchScreenController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/10/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

// This class is used call after LaunchScreen.storyboard for checking user's condition

//  If user already logged in or the system already have the user's token key, it will display lock screen scene but if users dooesn't login yet or the user's token key is longer than expire date, it will display login scene.

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface LaunchScreenController : BaseViewController

@property (weak, nonatomic) IBOutlet UIImageView *logo;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topLogo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightLogo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthLogo;

-(void)checkUserToken;

@end
