//
//  LockScreenController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/10/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

// This class is LockScreen (MedieScreen) 

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "StackImageView.h"
#import <StoreKit/StoreKit.h> // open appstore in app

@interface LockScreenController : BaseViewController <SKStoreProductViewControllerDelegate>{
    
    int fix_int;
    int selectedAd;
    int column_ads;
    
    NSMutableArray *imageAd;
    NSMutableArray *artwork_type;
    NSMutableArray *package_id;
    NSMutableArray *package_name;
    NSMutableArray *artwork_data;

    NSString *totlePackage;
    NSString *response_token;
    NSString *directionScroll;
    NSString *artwork_dataText;
    NSString *urlBase;
    NSString *request_time;
    NSString *fix;
    
    NSString *direction;
    NSString *ios_type;
    NSString *package_collectpoint;
    NSString *package_unpointpertime;

    BOOL networkError;
    
    __weak IBOutlet UIButton *arrowTopBtn;
    
    UIPanGestureRecognizer *panGestureSlideBtn;
    
    NSTimer *timer;
    NSTimer *timeCircle;
    NSTimer *timeAlert;
    
    BOOL hide;
    BOOL error_login;
    BOOL right_point;
    BOOL slide_left;
    BOOL push_from_menu;
    BOOL isDisappear;
    BOOL slide_ads;
    BOOL point_right;
    BOOL isScrolling;
    BOOL adsDownload;
    int indexArtworkData;
    
    CAGradientLayer *gradient, *gradientTop; // Shadow
}

@property (weak, nonatomic) IBOutlet StackImageView *adImage;

@property (weak, nonatomic) IBOutlet UIButton *longTapBtn;
@property (weak, nonatomic) IBOutlet UIView *viewR;
@property (weak, nonatomic) IBOutlet UIView *viewL;
@property (weak, nonatomic) IBOutlet UIView *viewLeft;
@property (weak, nonatomic) IBOutlet UIView *viewRight;

@property (weak, nonatomic) IBOutlet UIImageView *imageLeft;
@property (weak, nonatomic) IBOutlet UILabel *textLeft;
@property (weak, nonatomic) IBOutlet UILabel *textRight;
@property (weak, nonatomic) IBOutlet UILabel *timeTxt;
@property (weak, nonatomic) IBOutlet UIImageView *gifbox_icon;
@property (weak, nonatomic) IBOutlet UIImageView *gifbox_icon_right;

@property (weak, nonatomic) IBOutlet UIImageView *iconRight;

@property (weak, nonatomic) IBOutlet UIView *circleRight;

@property (weak, nonatomic) IBOutlet UILabel *day;
@property (weak, nonatomic) IBOutlet UILabel *date;

@property (weak, nonatomic) IBOutlet UIView *lineHeader;
@property (weak, nonatomic) IBOutlet UIView *lineBottom;
@property (weak, nonatomic) IBOutlet UIView *circleLeft;
@property (weak, nonatomic) IBOutlet UIView *shadow;
@property (weak, nonatomic) IBOutlet UIView *shadowTop;
@property (weak, nonatomic) IBOutlet UIView *view_bg_long_btn;

@property (weak, nonatomic) IBOutlet UIImageView *image_test;


/// UI for IPAD
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *icon_left_width;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *icon_left_height;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *icon_right_width;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *icon_right_height;

@property (nonatomic) CGFloat lastContentOffset;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *centerLongTapBtn;


@end
