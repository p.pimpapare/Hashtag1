
//  Created by pimpaporn chaichompoo on 2/11/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "PCNewFeedBarCollectionCell.h"

@implementation PCNewFeedBarCollectionCell

-(void)setNewFeedBar:(NSString *)title{
    [self.titleMenu setText:[NSString stringWithFormat:@"%@",title]];
}

// set new feed menu bar 
-(void)setTitleColor:(NSString *)color{
    
    if([color isEqualToString:@"white"]){
        [self.titleMenu setTextColor:[UIColor colorWithRed:(241/255.0)  green:(241/255.0)  blue:(241/255.0)  alpha:1.0]];
    }else{
        [self.titleMenu setTextColor:[UIColor colorWithRed:(255/255.0)  green:(255/255.0)  blue:(255/255.0)  alpha:0.5]];
    }
}

-(void)deleteLine{
    [self.line setHidden:YES];
}

@end
