//
//  SubMenuSettingController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/21/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubMenuSettingController : BaseViewController{
    
    NSString *introImage;
    NSString *introBg;
    NSArray *imageArray;
    
    UIImageView *imageData;
    
    NSString *documentInfo;
}

@property (weak, nonatomic) IBOutlet UIView *statusBar;

@property (weak, nonatomic) IBOutlet UIView *menuHeader;
@property (weak, nonatomic) IBOutlet UILabel *titleHeader;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingView;

-(void)setHeaderText:(NSString *)title detail:(NSString *)detail type:(BOOL)qa;
-(void)indexAboutUsDetail:(NSString *)index;
@end
