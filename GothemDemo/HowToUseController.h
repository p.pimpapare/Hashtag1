//
//  HowToUseController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/4/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

#import <UIKit/UIKit.h>

@interface HowToUseController : BaseViewController<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>{
    
    NSArray *image_array;
    int selectedTabMenu;
}

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIView *menuHeader;
@property (weak, nonatomic) IBOutlet UIView *statusBar;

@end
