//
//  ForgotPassController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/21/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

// This class is allow user to login with user's tel or user's the 1 card number
#import <UIKit/UIKit.h>

@interface MobileLoginController : BaseViewController{
    
    NSString *resultErrorMsg;
    BOOL validateInfo;
}

@property (weak, nonatomic) IBOutlet UIView *statusBar;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UIView *viewBg;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *logo;
@property (weak, nonatomic) IBOutlet UITextField *numTextfield;

@property (weak, nonatomic) IBOutlet UIButton *registerOnWebBtn;
@property (weak, nonatomic) IBOutlet UIButton *registerOnWebBtn2;

@property (weak, nonatomic) IBOutlet UILabel *accessText;
@property (weak, nonatomic) IBOutlet UIView *errorView;
@property (weak, nonatomic) IBOutlet UIImageView *errorIcon;

@property (weak, nonatomic) IBOutlet UILabel *regisText;

@property (weak, nonatomic) IBOutlet UILabel *telLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;

// NSLayoutConstraint
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightLogo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthLogo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightScrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTextfield;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topLogo;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomregister;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomregister2;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomregister3;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomregister4;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topErrorView;

// ipad
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *icon_error_right;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtfeild_right;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btn_right;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *view_error_right;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btn_left;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtfeild_left;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topic_left;

-(void)getService:(NSString *)userId;

@end
