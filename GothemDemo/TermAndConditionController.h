//
//  TermConditionController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/19/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

// This class is called when user's agreement return zero from getUserInfo api

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface TermAndConditionController : BaseViewController{
    
    BOOL selected;
}

@property (weak, nonatomic) IBOutlet UILabel *titleHeader;
@property (weak, nonatomic) IBOutlet UIView *viewHeader;

@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UILabel *titleText;
@property (weak, nonatomic) IBOutlet UIButton *selectBtn;

@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;
@property (weak, nonatomic) IBOutlet UIView *line;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingView;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightMenuBar;

-(void)network_error;

@end
