//
//  History_CollectionCell.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/8/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "History_CollectionCell.h"

@implementation History_CollectionCell

-(void)awakeFromNib{
    
    [super awakeFromNib];
    [self setBackgroundColor:[UIColor clearColor]];
}

-(void)setHistoryCell:(NSString *)title{
    [self.titleHeader setText:[NSString stringWithFormat:@"%@",title]];
}

-(void)setselectedColor:(int)selectedMenu{
    
    if (selectedMenu==0)
    {
        [self.titleHeader setAlpha:0.5];
        [self setBackgroundColor:[PCColor color_8E8E8E]];
    }
    else if (selectedMenu==1)
    {
        [self.titleHeader setAlpha:1];
        [self setBackgroundColor:[PCColor color_A32E3D]];
    }
}

@end
