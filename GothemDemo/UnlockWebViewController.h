//
//  UnlockWebViewController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/9/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

// This class is called when user slide to the left side from lock screen for display artwork's media of ads information.

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "YTPlayerView.h"

@interface UnlockWebViewController : BaseViewController<UIWebViewDelegate,YTPlayerViewDelegate>{
    
    int index_ads;
    int column_ads;
    
    BOOL finishUpdate_userInfo;
    BOOL notiAlert;
    BOOL isDisappear;
    BOOL notViewADS;
    BOOL youtubeAds;
    BOOL notiAlreadyAlert;
    
    NSString *noti_Text;
    
    NSTimer *timeAlert;
    NSTimer *timeLoading;
}
@property (weak, nonatomic) IBOutlet YTPlayerView *playerView;

@property (weak, nonatomic) IBOutlet UIView *viewHeader;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UILabel *titleText;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIButton *btn_goto_ads;

@property (weak, nonatomic) IBOutlet UIView *view_youtube;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingView;

-(void)setWebScene:(NSString *)name url:(NSString *)linkUrl;
-(void)set_loop_index_ads:(int)column index:(int)index;

@end
