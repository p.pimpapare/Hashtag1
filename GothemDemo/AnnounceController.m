//
//  AnnounceController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/31/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "AnnounceController.h"

@interface AnnounceController ()

@end

@implementation AnnounceController

-(void)viewWillAppear:(BOOL)animated{
    
    GOOGLE_ANALYTICS(@"AnnounceController");
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self.loadingView startAnimating];
    [self.loadingView setHidden:NO];
    
    [self getService_fromNewFeedDefualts2:[self getUserToken]];
    [self class_UI];
}

-(void)getService_fromNewFeedDefualts2:(NSString *)token{
    
    [Service get_news_feed:token typeNews:@"3" selfID:self block:^(id result) {
        
        if([BaseViewController check_service_status:result] == true)
        {
            item_id2 = [[NSMutableArray alloc]init];
            itemGuid2 = [[NSMutableArray alloc]init];
            title2 = [[NSMutableArray alloc]init];
            thumbnail2 = [[NSMutableArray alloc]init];
            
            imageUrl2 = [NSString stringWithFormat:@"%@",result[@"imageurl"]];
            
            for (int i=0; i<[result[@"feed"] count];i++) {
                [item_id2 addObject:result[@"feed"][i][@"item_id"]];
                [itemGuid2 addObject:result[@"feed"][i][@"link"]];
                [title2 addObject:result[@"feed"][i][@"title"]];
                [thumbnail2 addObject:result[@"feed"][i][@"thumbnail"]];
            }
            
            finisedLoadType2 = YES;
            
            [self.loadingView setHidden:YES];
            [self.loadingView stopAnimating];
            
            if (result!=nil) {
                [self.tableView reloadData];
            }
        }
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    int selectedRow = (int)indexPath.row;
    [self goToNewfeedWeb:[NSString stringWithFormat:@"%@feed2/%@/detail",BASEURL,item_id2[selectedRow]]];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if ([title2 count]<=0) {
        [self.viewError setHidden:NO];
        return 0;
    }else{
        [self.viewError setHidden:YES];
        return [title2 count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NewFeedTableViewCell *cell = (NewFeedTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if(cell == nil) {
        cell = [self loadNibWithName:@"NewFeedTableViewCell"];
    }
    
    [cell setNewFeedTableCell:title2[indexPath.row]];
    [cell.image sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",imageUrl2,thumbnail2[indexPath.row]]]
                  placeholderImage:[UIImage imageNamed:@"picture_1_1"]options:SDWebImageProgressiveDownload];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return [[self loadNibWithName:@"NewFeedTableViewCell"] frame].size.height;
}

-(void)class_UI{
    
    LINE_WITH_STATUSBAR_HEADER_NORMAL;
    TABLEVIEW_COLLECTION_REFRESH(self.tableView);
    [self.tableView setBackgroundColor:[UIColor clearColor]];
}

- (void)refresh:(UIRefreshControl *)refreshControl {
    [self getService_fromNewFeedDefualts2:[self getUserToken]];
    [refreshControl endRefreshing];
}

-(void)goToNewfeedWeb:(NSString *)urlStr{
    
    Announe_WebviewController *view_class = [self loadNibWithName:@"Announe_WebviewController"];
    [view_class openUrl:[NSString stringWithFormat:@"%@",urlStr]];
    [view_class.view setFrame:CGRectMake(0,0,DEVICE_WIDTH,DEVICE_HEIGHT)];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController pushViewController:view_class animated:YES];
    });
}

- (IBAction)backBtn:(id)sender {
    GOTO_PREVIOUS;
}

@end
