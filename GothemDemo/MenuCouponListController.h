//
//  MenuTableViewListController.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/20/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

#import <UIKit/UIKit.h>

@interface MenuCouponListController : BaseViewController<UITableViewDelegate,UITableViewDataSource>{
    
    NSMutableArray *hotpromoID;
    NSMutableArray *hotpromoTitle;
    NSMutableArray *hotpromoSubTitle;
    NSMutableArray *hotpromoImage;
    NSMutableArray *hotpromoDetails;
    NSMutableArray *hotpromoCounterTime;
    NSMutableArray *hotpromoUsePoint;
    NSMutableArray *hotpromoLogo;
    NSMutableArray *hotpromoBrandName;
    NSMutableArray *hotpromoAvailable;
    NSMutableArray *hotpromoDealOwner;
    NSMutableArray *hotpromoCanUse;

    // menu2
    NSMutableArray *couponID;
    NSMutableArray *couponTitle;
    NSMutableArray *couponSubTitle;
    NSMutableArray *couponImage;
    NSMutableArray *couponDetails;
    NSMutableArray *couponCounterTime;
    NSMutableArray *couponUsePoint;
    NSMutableArray *couponAvailable;
    NSMutableArray *couponBrandName;
    NSMutableArray *couponBrandLogo;
    NSMutableArray *couponDealOwner;
    NSMutableArray *couponCanUse;

    int useStatus;
    int selectedRow;
    
    BOOL menu2;
    BOOL readyReload;

    BOOL finisedLoadingHotPromo;
    BOOL finisedLoadingCoupon;

    NSString *brandID_str;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIView *menuHeader;
@property (weak, nonatomic) IBOutlet UILabel *titleHeader;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIView *statusBar;
@property (weak, nonatomic) IBOutlet UIView *warningBlank;

// error view
@property (weak, nonatomic) IBOutlet UIView *viewPasswordFalse;
@property (weak, nonatomic) IBOutlet UIImageView *imageLogo;
@property (weak, nonatomic) IBOutlet UILabel *errorMsg;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightHeader;

@property (weak, nonatomic) IBOutlet UILabel *warning_text;
@property (weak, nonatomic) IBOutlet UIView *view_share;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingView;


-(void)setHeaderText:(NSString *)title brand_id:(NSString *)brandID;

@end
