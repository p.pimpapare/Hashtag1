//
//  Menu3TableViewCell.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 3/1/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "Menu3TableViewCell.h"

@implementation Menu3TableViewCell

-(void)awakeFromNib{
    
    [super awakeFromNib];
    
    [self setBackgroundColor:[UIColor clearColor]];
}

-(void)setCellInfo:(NSString *)title detail:(NSString *)detail{
    
    [self.titleText setText:[NSString stringWithFormat:@"%@",title]];
    [self.detailText setText:[NSString stringWithFormat:@"%@",detail]];
}

@end
