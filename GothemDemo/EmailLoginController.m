//
//  EmailLoginController.m
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/21/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import "EmailLoginController.h"
#import "ContainerViewController.h"

@interface EmailLoginController ()

@end

@implementation EmailLoginController

-(void)viewWillAppear:(BOOL)animated{
    
    GOOGLE_ANALYTICS(@"EmailLoginController");
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self set_initail_objects];
}

-(void)getService:(NSString *)email password:(NSString *)password{
    
    [self setHiddenPCLoading:NO];
    
    GET_USER_DEFAULT_INFO(@"refcode")
    NSString *refcode = [NSString stringWithFormat:@"%@",userInfo];
    
    [Service post_user_login_with_Email:email password:password ref_code:refcode deviceToken:[BaseViewController getDeviceToken] selfID:self block:^(id result) {
        
        [self.view setUserInteractionEnabled:YES];
        
        NSString *resultText = [NSString stringWithFormat:@"%@",result];
        
        if ([resultText isEqualToString:@"error"]) {
            
            [self setHiddenPCLoading:YES];
            [self network_error];
            
        }else if ([result count]==0) {
            
            [self service_error];
            
        }else{
            
            NSString *successText = [NSString stringWithFormat:@"%@",(NSString *)result[@"success"]];
            
            if ([successText isEqualToString:@"0"]) {
                
                resultErrorMsg = [NSString stringWithFormat:@"%@",result[@"message"]];
                [self login_error];
                
            }else{
                
                [self removeNSUserdefualts];
                
                REMOVE_USER_DEFAULT_INFO(@"UserInfo")
                
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                
                if (userDefaults){
                    
                    [userDefaults setObject:result forKey:@"UserInfo"];
                    
                    [userDefaults setObject:self.emailTextfield.text forKey:@"UserEmail"];
                    [userDefaults setObject:self.passwordTextfield.text forKey:@"PasswordEmail"];
                }
                
                NSString *userAgreed = [NSString stringWithFormat:@"%@",result[@"agreed"]];
                ([userAgreed isEqualToString:@"1"])?[self callService:result[@"token"]]:[self goToTerm];
            }
        }
    }];
}

-(void)removeNSUserdefualts{
    
    NSArray *string_array = @[@"UserInfo",@"newsfeed_setting",@"noti_setting",@"voice_setting",@"alarm_setting",@"led_setting",@"lockscreen_setting"];
    
    for (NSString *string in string_array) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:string];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)set_initail_objects{
    
    validateEmail = NO;
    validatePassword = NO;
    
    [PCTapGesture tapAction:self view:self.telLabel actionTarget:@selector(userTappedOnTel:)];
    [PCTapGesture tapAction:self view:self.self.emailLabel actionTarget:@selector(userTappedOnEmail:)];
    
    [self.telLabel setUserInteractionEnabled:YES];
    [self.emailLabel setUserInteractionEnabled:YES];
    
    [self.regisText setText:CALL_CENTER_TEXT];
    [self.regisText setFont:[PCFont fontRSU:16.0]];
    [self.emailTextfield setFont:[PCFont fontRSU:16.0]];
    [self.passwordTextfield setFont:[PCFont fontRSU:16.0]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notification_alert:) name:@"Service_status" object:nil];
    
    [self class_UI];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)notification_alert:(NSNotification *)noti{
    
    [self.view setUserInteractionEnabled:YES];
    
    NSString *strNoti = [noti object];
    
    if ([strNoti isEqualToString:@"gotoHome"]) {
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController * mainView = [storyboard   instantiateViewControllerWithIdentifier:@"ContainerViewController"] ;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.navigationController pushViewController:mainView animated:YES];
        });
        
    }else if([strNoti isEqualToString:@"network_error"]){
        [self network_error];
    }else{
        [self service_error];
    }
}

-(void)class_UI{
    
    [self setBg];
    
    SWIPETLIFE_TOGOTOPREVIOUS;
    LINE_WITH_STATUSBAR_LOGIN;
    
    [self addPCLoading:self];
    
    [PCTapGesture set_target_button:self.registerOnWebBtn type:1];
    [PCTapGesture set_target_button:self.registeronWeb2 type:2];
    [PCTapGesture set_target_button:self.forgetPassBtn type:3];
    
    [PCTextfield setpadding_textfield:self.emailTextfield framLeft:45 framRight:0];
    [PCTextfield setpadding_textfield:self.passwordTextfield framLeft:45 framRight:0];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"gotoHome" object:nil]; // Solution when NSNoti accessing in twice times

    [self.heightLogo setConstant:HEIGHT_LOGO];
    [self.widthLogo setConstant:WIDTH_LOGO];
    
    if (IS_IPAD==YES) {
        
        [self.topLogo setConstant:150];
        
        int range_ipad = DEVICE_WIDTH/3;
        
        [self.email_txt_left setConstant:range_ipad];
        [self.email_txt_right setConstant:range_ipad];
        
        [self.pass_txt_left setConstant:range_ipad];
        [self.pass_txt_right setConstant:range_ipad];
        
        [self.btn_right setConstant:range_ipad];
        [self.btn_left setConstant:range_ipad];
        
        [self.btn_left setConstant:range_ipad];
        [self.btn_right setConstant:range_ipad];
        
        [self.icon_pass_left setConstant:range_ipad+7];
        [self.icon_email_left setConstant:range_ipad+7];
        
        [self.icon_email_error_right setConstant:range_ipad+7];
        [self.icon_pass_error_right setConstant:range_ipad+7];
        
        [self.view_pass_error_right setConstant:range_ipad+5];
        [self.view_email_error_right setConstant:range_ipad+5];
        
        [self.topForgotPass setConstant:80];
        
        if (DEVICE_HEIGHT==IPAD_PRO_HEIGHT) {
            
            [self set_button_ui:510 constant_two:555 constant_three:554 constant_four:32];
            
        }else{
            
            [self set_button_ui:310 constant_two:355 constant_three:354 constant_four:32];
        }
        
    }else{
        
        if(DEVICE_HEIGHT==PHONE4){
            
            [self.emailTop_constant setConstant:25];
            [self.top_error_view setConstant:4];
            [self.heightLogo setConstant:HEIGHT_LOGO45];
            [self.widthLogo setConstant:WIDTH_LOGO45];
            [self.topLogo setConstant:55];
            
        }else{
            
            [self.emailTop_constant setConstant:60];
            [self.top_error_view setConstant:39];
            
            [self set_button_ui:10 constant_two:53 constant_three:54 constant_four:30];
        }
    }
    [PCTapGesture tapDismissKeyBoard:self view:self.viewBg];
}

-(void)set_button_ui:(int)btn_register_one constant_two:(int)btn_register_two constant_three:(int)btn_register_three constant_four:(int)btn_register_four{
    
    [self.bottomregister setConstant:btn_register_one];
    [self.bottomregister2 setConstant:btn_register_two];
    [self.bottomregister3 setConstant:btn_register_three];
    [self.bottomregister4 setConstant:btn_register_four];
}

-(void)gotoTermCon:(id)sender{
    
    [self pushViewCtrlWithViewCtrlName:@"TermAndConditionBeforeLogin"];
}

-(void)swipeRight:(UISwipeGestureRecognizer*)gestureRecognizer{
    GOTO_PREVIOUS;
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

- (IBAction)loginBottom:(id)sender {
    
    [self.view endEditing:YES];
    [self.view setUserInteractionEnabled:NO];
    
    if(([self.emailTextfield.text length]==0)&&([self.passwordTextfield.text length]==0)){
        
        [self.errorIcon2 setHidden:NO];
        [self.errorView1 setHidden:NO];
        [self.errorIcon1 setHidden:NO];
        [self.errorView2 setHidden:NO];
        [self.view setUserInteractionEnabled:YES];
        
    }else if([self.emailTextfield.text length]==0){
        
        [self.errorIcon2 setHidden:NO];
        [self.errorView1 setHidden:NO];
        
        [self.view setUserInteractionEnabled:YES];
        
    }else if([self.passwordTextfield.text length]==0){
        
        [self.errorIcon1 setHidden:NO];
        [self.errorView2 setHidden:NO];
        
        [self.view setUserInteractionEnabled:YES];
        
    }else{  // login pass
        
        [self getService:self.emailTextfield.text password:self.passwordTextfield.text];
    }
}

// set key board action
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.emailTextfield resignFirstResponder];
    [self.passwordTextfield resignFirstResponder];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    [self.errorIcon1 setHidden:YES];
    [self.errorIcon2 setHidden:YES];
    [self.errorView1 setHidden:YES];
    [self.errorView2 setHidden:YES];
    
    if (DEVICE_WIDTH==PHONE45_WIDTH) {
        
        [PCTextfield animateTextField:self.viewBg up:YES];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (textField == self.emailTextfield){
        
        if(![Validation validateEmail:[self.emailTextfield text]]){
            
            validateEmail = NO;
            [self.errorIcon2 setHidden:NO];
            [self.errorView1 setHidden:NO];
            
        }else {
            
            validateEmail = YES;
            [self.errorIcon2 setHidden:YES];
            [self.errorView1 setHidden:YES];
        }
        
    }else if (textField == self.passwordTextfield){
        
        if (![Validation validatePassword:[self.passwordTextfield text]]) {
            
            validatePassword = NO;
            [self.errorIcon1 setHidden:NO];
            [self.errorView2 setHidden:NO];
            
        }else {
            
            validatePassword = YES;
            [self.errorIcon1 setHidden:YES];
            [self.errorView2 setHidden:YES];
        }
    }
    
    if (DEVICE_WIDTH==PHONE45_WIDTH) {
        [PCTextfield animateTextField:self.viewBg up:NO];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

-(void)goToTerm{
    
    [self.view setUserInteractionEnabled:YES];
    [self pushViewCtrlWithViewCtrlName:@"TermAndConditionController"];
}

-(void)callService:(NSString *)token{
    
    [self.view setUserInteractionEnabled:NO];
    
    CallServiceAfterLogin *viewclass = [[CallServiceAfterLogin alloc]init];
    [viewclass call_service_from:token type:1];
}

-(void)network_error{
    
    [self.view setUserInteractionEnabled:YES];
    
    ALERT_MESSAGE_NETWORK([self.view setUserInteractionEnabled:NO]; [self getService:self.emailTextfield.text password:self.passwordTextfield.text];,[self setHiddenPCLoading:YES];)
}

-(void)service_error{
    
    [self.view setUserInteractionEnabled:YES];
    
    ALERT_MESSAGE(@"ระบบขัดข้อง",@"โปรดลองใหม่อีกครั้ง",[self getService:self.emailTextfield.text password:self.passwordTextfield.text];,[self setHiddenPCLoading:YES];)
}

-(void)login_error{
    
    [self.view setUserInteractionEnabled:YES];
    
    ALERT_MESSAGE(@"ข้อผิดพลาด",resultErrorMsg, [self getService:self.emailTextfield.text password:self.passwordTextfield.text];,[self setHiddenPCLoading:YES];)
}

- (IBAction)backBtn:(id)sender {
    GOTO_PREVIOUS
}

-(void)userTappedOnTel:(id)sender{
    
    NSString *phoneNumber = [@"tel://" stringByAppendingString:CALL_CENTER_TEL];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}

-(void)userTappedOnEmail:(id)sender{
    
    //put email info here:
    NSString *toEmail = CALL_CENTER_EMAIL;
    NSString *subject = @" ";
    NSString *body = @" ";
    
    //opens mail app with new email started
    NSString *email = [NSString stringWithFormat:@"mailto:%@?subject=%@&body=%@", toEmail,subject,body];
    email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
}

@end
