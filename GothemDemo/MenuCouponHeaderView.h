
//  Created by pimpaporn chaichompoo on 2/2/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.

#import <UIKit/UIKit.h>

@interface MenuCouponHeaderView : UIView
@property (weak, nonatomic) IBOutlet UIView *bgView;

@property NSString *tagStatus;

-(NSString *)action:(NSString *) selecttag;
-(void)setMenuCouponHeaderView:(NSString *)titleText;

@end
