//
//  AppDelegate.h
//  GothemDemo
//
//  Created by pimpaporn chaichompoo on 2/19/16.
//  Copyright © 2016 pimpaporn chaichompoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    
    int tag; // if 1 is inquiry's push noti
            //  2 is update_package's push noti
            //  3 is logout's push noti
            //  4 is message 
}

@property (strong, nonatomic) UIWindow *window;
@property NSMutableDictionary *deviceInfo;

@end

